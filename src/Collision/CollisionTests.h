#pragma once
#include "Vector/Vector.h"
#include "Object/Object.h"

//bool SATCollisionTest(Object& Obj1, Object& Obj2);
Collision SATCollisionTest(Object& Obj1, Object& Obj2);

bool RaySurfaceCollisionTest(Surface Surface, Object &Obj, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen);

bool SphereSurfaceCollisionTest(Surface Surface, Object &Obj, Vec3f C, float R, Vec3f &CollisionPoint, float &Depth);
