#include <assert.h>
#include <cmath>
#include <numeric>
#include "CollisionTestsBasic.h"
#include "GlobalDefines.h"

bool
AABBCollisionTest(Vec3f AABB1[2], Vec3f AABB2[2])
{
	if(AABB1[1].x > AABB2[0].x && AABB1[0].x < AABB2[1].x)
		if(AABB1[1].y > AABB2[0].y && AABB1[0].y < AABB2[1].y)
			if(AABB1[1].z > AABB2[0].z && AABB1[0].z < AABB2[1].z)
				return true;
	return false;
}

bool
CompareNormals(const Vec3f& V1, const Vec3f& V2)		//check if 2 normals are the same
{
	if(abs(V1.x - V2.x) < EPSILON)						//same
		if(abs(V1.y - V2.y) < EPSILON)
			if(abs(V1.z - V2.z) < EPSILON)
				return true;
	return false;
}

bool
RayPlaneCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen)
{
	float d = -DOT(PlaneN, PlaneP);
	float t  = (-d -DOT(PlaneN, RayOrigin))/(DOT(PlaneN, RayDir));
	if(t >= 0 && t < MaxLen)
	{
		MaxLen = t;
		return true;
	}
	return false;
}

bool
RayAABBCollisionTest(Vec3f AABB[2], Vec3f RayOrigin, Vec3f RayDir, float& MinLen, float& MaxLen)
{
	// RO.x + t*RD.x == Min.x -> collision with zy-plane
	// t = Min.x - RO.x / RD.x

	Vec3f InvDir;					//precompute inverse of direction			
	InvDir.x = 1.0f/RayDir.x;		//floating point division by zero defined: +Inf
	InvDir.y = 1.0f/RayDir.y;
	InvDir.z = 1.0f/RayDir.z;

	Vec3i Swap;						//precalculate if the bounds have to be swapped
	Swap.x = (RayDir.x < 0);
	Swap.y = (RayDir.y < 0);
	Swap.z = (RayDir.z < 0);


	float T1X = (AABB[Swap.x].x - RayOrigin.x)*InvDir.x;		//intersection x	//Ro + Rd*t ,t[0,Tmax]
	float T2X = (AABB[1-Swap.x].x - RayOrigin.x)*InvDir.x;

	float T1Y = (AABB[Swap.y].y - RayOrigin.y)*InvDir.y;		//intersection y
	float T2Y = (AABB[1-Swap.y].y - RayOrigin.y)*InvDir.y;

	if(T1X < T1Y)				//max of mins
		T1X = T1Y;
	if(T2X > T2Y)				//min of maxs
		T2X = T2Y;

	float T1Z = (AABB[Swap.z].z - RayOrigin.z)*InvDir.z;		//intersection z
	float T2Z = (AABB[1-Swap.z].z - RayOrigin.z)*InvDir.z;

	if(T1X < T1Z)				//max of mins
		T1X = T1Z;
	if(T2X > T2Z)				//min of maxs
		T2X = T2Z;

	if(T1X > MaxLen)				//check that it isn't too far
		return false;
	if(T1X > T2X)				//miss
	{
		return false;
	}
	else if(T2X < 0)			//behind, miss
	{
		return false;
	}
	else if(T1X > 0)			//hit
	{
		MinLen = T1X;
		MaxLen = T2X;
		return true;
	}
	else
	{							//origin inside box
		MinLen = 0;
		MaxLen = T2X;
		return true;
	}
}

bool SolveQuadratic(const float &a, const float &b, const float &c, float &x0, float &x1) 
{ 
	float Root = b*b - 4*a*c; 
	if (Root < 0)
		return false; 
	else if (Root == 0)
		x0 = x1 = - 0.5f*b/a; 
	else 
	{ 
		float sq = sqrt(Root);
		//x0 = (-b + sq)/(2.0f*a);
		//x1 = (-b - sq)/(2.0f*a);
		float q;					//avoiding catastopic cancellation
		if(b > 0)
			q = -0.5f *(b + sq);
		else
			q = -0.5f*(b - sq);
		x0 = q/a; 
		x1 = c/q; 
	} 
	if (x0 > x1)
		std::swap(x0, x1); 

	return true; 
} 

bool RaySphereCollisionTest(Vec3f SC, float Radius, Vec3f RayOrigin, Vec3f RayDir, float& MinLen, float& MaxLen)
{
	// (P - C)^2 - R^2 = 0			sphere equation
	// P = RO + t*RD				ray equation
	// (RO + t*RD - C)^2 - R^2 = 0	combined
	// a = 1, b = 2*RD*(RO-C), c = (RO-C)^2 - R^2
	
	Vec3f ROrigToSphereC = RayOrigin - SC;

	float a = 1;
	float b = 2 * DOT(RayDir, ROrigToSphereC);
	float c = DOT(ROrigToSphereC, ROrigToSphereC) - Radius*Radius;
	float x0, x1;
	if(SolveQuadratic(a, b, c, x0, x1))
	{
		if(x1 < 0)
			return false;		//sphere behind ray origin
		if(x0 < 0)
			MinLen = 0;			//inside sphere
		else
			MinLen = x0;
		MaxLen = x1;
		return true;
	}
	return false;
}

bool PlaneSphereCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f C, float R, float &Depth)
{	
	float PP = DOT(PlaneP, PlaneN);		//project both to normal of plane
	float SP = DOT(C, PlaneN);
	bool OutSide = SP >= PP;			//check on which side of the plane the sphere is on, assume outside
	float Distance;
	if(OutSide)
		Distance = PP -(SP-R);
	else
		Distance = PP -(SP+R);

	if(Distance > 0)
	{
		Depth = Distance;
		return true;
	}
	return false;

}

bool RayTriangleIntersection(Vec3f P1, Vec3f P2, Vec3f P3, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen)
{
	Vec3f AB = P2 - P1; 
	Vec3f BC = P3 - P2; 

	Vec3f Normal = NORMALIZE(CROSS(AB, BC));
	float Len = MaxLen;
	if(RayPlaneCollisionTest(Normal, P1, RayOrigin, RayDir, Len))
	{
		Vec3f P = RayOrigin + Len*RayDir;

		Vec3f CA = P1 - P3;

		Vec3f AP = P - P1; 
		Vec3f BP = P - P2; 
		Vec3f CP = P - P3; 

		Vec3f ABP = CROSS(AB, AP);
		Vec3f BCP = CROSS(BC, BP);
		Vec3f CAP = CROSS(CA, CP);

		if (DOT(Normal, ABP) > 0 && 
			DOT(Normal, BCP) > 0 && 
			DOT(Normal, CAP) > 0)
		{
			//float Area = LEN(CROSS(AB, BC));
			//u = LEN(CAP) / Area;
			//v = LEN(ABP) / Area;
			MaxLen = Len;
			return true;
		}
	}
	return false;
}
