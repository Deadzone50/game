#pragma once
#include "Vector/Vector.h"

bool AABBCollisionTest(Vec3f AABB1[2], Vec3f AABB2[2]);

bool RayPlaneCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen);
bool RaySphereCollisionTest(Vec3f C, float R, Vec3f RayOrigin, Vec3f RayDir, float& MinLen, float &MaxLen);
bool RayAABBCollisionTest(Vec3f AABB[2], Vec3f RayOrigin, Vec3f RayDir, float& MinLen, float &MaxLen);
bool RayTriangleIntersection(Vec3f P1, Vec3f P2, Vec3f P3, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen);

bool CompareNormals(const Vec3f& V1, const Vec3f& V2);		//check if 2 normals are the same

bool PlaneSphereCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f C, float R, float &Depth);
