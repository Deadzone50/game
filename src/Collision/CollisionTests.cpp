#include <assert.h>
#include <cmath>
#include <numeric>
#include "CollisionTestsBasic.h"
#include "CollisionTests.h"
#include "GlobalDefines.h"


Collision
SATCollisionTest(Object& Obj1, Object& Obj2)
{
	Collision Result;
	Result.Hit = false;
	std::vector<Surface> Surfaces1 = *Obj1.GetCollisionSurfaces();

	size_t Size1 = Surfaces1.size();
	std::vector<Vec3f> Norms1;
	Norms1.reserve(Size1);
	Norms1.push_back(Surfaces1[0].N);
	for(auto S = Surfaces1.begin()+1; S != Surfaces1.end();)		//remove duplicate normals and inverse normals
	{
		bool con = false;
		auto NEND1 = Norms1.end();
		for(auto N = Norms1.begin(); N != NEND1; ++N)
		{
			if(CompareNormals(S->N, *N)||CompareNormals(S->N, -(*N)))
			{
				S = Surfaces1.erase(S);								//invalidates surfaces1.end()
				con = true;
				break;
			}
		}
		if(!con)
		{
			Norms1.push_back(S->N);
			++S;
		}
	}
	std::vector<Surface> Surfaces2 = *Obj2.GetCollisionSurfaces();
	size_t Size2 = Surfaces2.size();
	std::vector<Vec3f> Norms2;
	Norms2.reserve(Size2);
	Norms2.push_back(Surfaces2[0].N);
	//auto SEND2 = Surfaces2.end();			erase invalidates
	for(auto S = Surfaces2.begin()+1; S != Surfaces2.end();)		//remove duplicate normals and inverse normals
	{
		bool con = false;
		for(auto N = Norms2.begin(); N != Norms2.end(); ++N)
		{
			if(CompareNormals(S->N, *N)||CompareNormals(S->N, -(*N)))
			{
				S = Surfaces2.erase(S);
				con = true;
				break;
			}
		}
		if(!con)
		{
			Norms2.push_back(S->N);
			++S;
		}
	}

	size_t S1Size = Surfaces1.size();
	size_t S2Size = Surfaces2.size();
	std::vector<Vec3f> NormalsRaw(S1Size + S2Size + S1Size*S2Size);
	int NR = 0;
	for(int S1 = 0; S1 < S1Size;++S1)		//create "cross surfaces"
	{
		NormalsRaw[S1] = Surfaces1[S1].N;
		for(int S2 = 0; S2 < S2Size;++S2)
		{
			NormalsRaw[S1Size + S2] = Surfaces2[S2].N;
			Vec3f Cross = CROSS(Surfaces1[S1].N, Surfaces2[S2].N);
			if(Cross.x + Cross.y + Cross.z)
			{
				NormalsRaw[S1Size+S2Size + S1*S2Size+S2 -NR] = NORMALIZE(Cross);
			}
			else
			{
				++NR;
				NormalsRaw.resize(S1Size + S2Size + S1Size*S2Size - NR);
			}

		}
	}

	std::vector<Vec3f> Normals;
	Normals.reserve(NormalsRaw.size());
#if 1
	auto REND = NormalsRaw.end();

	for(auto N1 = NormalsRaw.begin()+1; N1 != REND; ++N1)		//remove duplicate normals and inverse normals
	{
		bool Found = false;
		for(auto N2 = Normals.begin(); N2 != Normals.end(); ++N2)
		{
			if(CompareNormals(*N1, *N2) || CompareNormals(*N1, -(*N2)))
			{
				Found = true;
				break;
			}
		}
		if(!Found)
		{
			Normals.push_back(*N1);
		}
	}
#else
	Normals = NormalsRaw;
#endif

	float Dist = FLT_MAX;
	Vec3f Normal = {0,0,0};
	auto NEND = Normals.end();
	for(auto N = Normals.begin(); N != NEND;++N)
	{
		float Min1, Min2, Max1, Max2;
		Obj1.ProjectToAxis(Min1, Max1, *N);
		Obj2.ProjectToAxis(Min2, Max2, *N);

		if(Min1 <= Max2 && Min2 <= Max1)							//objects overlap
		{
			float d = 0;
			Vec3f TempN = {0,0,0};
			if(Min1 <= Min2 && Max1 <= Max2)						//Obj1 entering Obj2 in direction of N
			{
				d = Max1-Min2;
				TempN = -(*N);
			}
			else if(Min1 >= Min2 && Max1 >= Max2)					//Obj1 entering Obj2 in direction of -N
			{
				d = Max2-Min1;
				TempN = *N;
			}
			else													//one Obj inside the other
			{
				float d1 = Max1-Min2;								//move Obj1 in direction of -N
				float d2 = Max2-Min1;								//move Obj1 in direction of N
				if(d1 < d2)
				{
					d = d1;
					TempN = -(*N);
				}
				else
				{
					d = d2;
					TempN = *N;
				}
			}
			
			if(d < Dist)
			{
				Dist = d;
				Normal = TempN;
			}
		}
		else
		{
			return Result;
		}
	}
#if 0																//all points equal force for both objects
	int Size1C = Obj1.GetCollisionPoints()->size();
	int Size2C = Obj2.GetCollisionPoints()->size();
	Result.Points1.resize(Size1C);
	Result.Points2.resize(Size2C);
	std::iota(Result.Points1.begin(), Result.Points1.end(), 0);
	std::iota(Result.Points2.begin(), Result.Points2.end(), 0);

	Result.Force1.resize(Size1C);
	Result.Force2.resize(Size2C);
	Vec3f Force = 0.5f*Dist*Normal;
	std::fill(Result.Force1.begin(), Result.Force1.end(), -Force);
	std::fill(Result.Force2.begin(), Result.Force2.end(), Force);

	//Vec3f Force = 0.5f*Dist*Normal;
	//Result.Points1.push_back(Obj1I);
	//Result.Points2.push_back(Obj2I);
	//Result.Force1.push_back(Force);
	//Result.Force2.push_back(-Force);
#endif
	Result.Hit = true;
	Result.Normal = Normal;
	Result.Distance = Dist;
	return Result;
}

bool RaySurfaceCollisionTest(Surface Surface, Object &Obj, Vec3f RayOrigin, Vec3f RayDir, float& MaxLen)
{
	std::vector<int> SurfacePoints = Surface.Points;
	Vec3f SN = Surface.N;
	Vec3f SP = Surface.P;
	float Len = MaxLen;

	bool PlaneHit = RayPlaneCollisionTest(SN, SP, RayOrigin, RayDir, Len);
	int HitCount = 0;
	if(PlaneHit)
	{
		Vec3f P = RayOrigin + Len*RayDir;
		//DrawDebugPoint(P, {1,0,0});

		int Start = *(SurfacePoints.end()-1);
		int End = *SurfacePoints.begin();
		Vec3f PNorm = NORMALIZE(CROSS(Obj.GetPointPos(End) - Obj.GetPointPos(Start), SN));
		Vec3f PP = Obj.GetPointPos(Start);
		Vec3f Dir = NORMALIZE((Obj.GetPointPos(SurfacePoints[1]) - Obj.GetPointPos(SurfacePoints[0])));
		//Vec3f Dir = NORMALIZE({1,1,1});
		float L = FLT_MAX;
		bool Hit = RayPlaneCollisionTest(PNorm, PP, P, Dir, L);
		if(Hit)
		{
			Vec3f ColP = P+L*Dir;
			Vec3f V1 = Obj.GetPointPos(Start) - ColP;
			Vec3f V2 = Obj.GetPointPos(End) - ColP;

			if(DOT(V1,V2) < 0)
			{
				//DrawDebugPoint(ColP, {0,1,0});
				//DrawDebugLine(ColP, ColP+V1,{0,0.7f,0});
				//DrawDebugLine(ColP, ColP+V2,{0,0.7f,0});
				++HitCount;
			}
		}
		auto SEND = SurfacePoints.end()-1;
		for(auto Pi = SurfacePoints.begin(); Pi!= SEND;++Pi)
		{
			Start = *Pi;
			End = *(Pi+1);
			PNorm =  NORMALIZE(CROSS(Obj.GetPointPos(End) - Obj.GetPointPos(Start), SN));
			PP = Obj.GetPointPos(Start);
			L = FLT_MAX;
			Hit = RayPlaneCollisionTest(PNorm, PP, P, Dir, L);
			if(Hit)
			{
				Vec3f ColP = P+L*Dir;
				Vec3f V1 = Obj.GetPointPos(Start) - ColP;
				Vec3f V2 = Obj.GetPointPos(End) - ColP;

				if(DOT(V1,V2) < 0)
				{
					//DrawDebugPoint(ColP, {0,1,0});
					//DrawDebugLine(ColP, ColP+V1,{0,0.7f,0});
					//DrawDebugLine(ColP, ColP+V2,{0,0.7f,0});
					++HitCount;
				}
			}
		}
	}
	else
	{
		return false;
	}
	if(HitCount%2)
	{
		MaxLen = Len;			//update if it hit the surface
		return true;
	}
	return false;
}

bool SphereSurfaceCollisionTest(Surface Surface, Object &Obj, Vec3f Center, float R, Vec3f &CollisionPoint, float &Depth)
{
	Vec3f PlaneN = Surface.N;
	Vec3f PlaneP = Surface.P;
	Vec3f CPoint = {0,0,0};
	float MaxDepth;
	bool PlaneHit = PlaneSphereCollisionTest(PlaneN, PlaneP, Center, R, MaxDepth);
	if(PlaneHit)
	{
		float MaxLen = FLT_MAX;
		if(MaxDepth<0)
			PlaneN = -PlaneN;			//approaching from behind

		bool CenterRayHit = RaySurfaceCollisionTest(Surface, Obj, Center, -PlaneN, MaxLen);
		if(CenterRayHit)
		{
			Depth = MaxDepth;
			CollisionPoint = Center+ MaxLen*(-PlaneN);
			return true;
		}
		else
		{
			float MinDist = FLT_MAX;

			int Start = Surface.Points.back();
			for(size_t i = 0; i < Surface.Points.size(); ++i)
			{
				int End = Surface.Points[i];
				Vec3f S = Obj.GetCollisionPointPos(Start);
				Vec3f E = Obj.GetCollisionPointPos(End);
				Vec3f A = Center - S;
				Vec3f C = Center - E;
				Vec3f B = E - S;
				float CosAngle = DOT(NORMALIZE(A),NORMALIZE(B));
				if(CosAngle >= 0 && DOT(C, -B) >= 0)		//if the center is between the endpoints of the line
				{
					float Angle = acos(CosAngle);
					float Dist = LEN(A)*sin(Angle);		//straight distance from center to line
					if(MinDist > Dist)
					{
						MinDist = Dist;
						CPoint =  S + (DOT(A,B)/LEN(B))* NORMALIZE(B);
					}
				}
				else if(LEN(A) > LEN(C))				//closer to E point
				{
					float Dist = LEN(C);				//straight distance from center to endpoint
					if(MinDist > Dist)
					{
						MinDist = Dist;
						CPoint = E;
					}
				}
				else									//closer to S point
				{	
					float Dist = LEN(A);				//straight distance from center to endpoint
					if(MinDist > Dist)
					{
						MinDist = Dist;
						CPoint = S;
					}
				}

				Start = End;
			}
			
			
			if(MinDist < R)				//collision
			{
				CollisionPoint = CPoint;
				Depth = R-MinDist;
				return true;
			}
		}
	}
	return false;
}
