#include <time.h>
#include <iostream>
#include <fstream>
#include <algorithm>

#include "Collision/CollisionTests.h"
#include "Game.h"
#include "FileIO.h"
#include "GlobalDefines.h"
#include "GLHelperFunctions.h"

extern TextureManager TexManager;
bool
Game::Init()
{
	Paused_ = false;
	Reset_ = false;
	MouseInit_ = true;
	SkipFactor_ = 0;
	MoveSpeed_ = 60;
	CurrentSurface_ = 0;
	DisplayedScreen_ = CameraScreen;

	//StaticViewer_.SetDirection({0,0,-1});
	//StaticViewer_.SetPosition({0,0,1});
	//LoadScene("Scene2.txt", World_, Objects_, PhysObjects_, NPCs_, ID_);
	//Pause();
	Player_ = Player(Vec3f{0,0,4}, Vec3f{0,0,-1}, 60.0f, 1.0f, 0.1f, 1000.0f);
	
	GenerateWorld(0, 40, 100, false);
	//GenerateWorld(0, 300, 200, false);
	//GenerateWorld(0, 10, 500, false);
	//GenerateWorld(0, 10, 2000, false);
#if 0
	ParseObjFile("Assets/models/", "man.obj", Meshes_, Materials_);
	Skeleton Skel = CreateSkeleton(Meshes_.back());
	Meshes_.back().TextureId_ = TexManager.GenerateColorTexture({0.3f,0.3f,0.3f,1});
	for(int i = 0; i < 100; ++i)
	{
		NPCs_.emplace_back(Vec3f{0,0,0}, Vec3f{0,0,0}, Meshes_.back(), Skel);
		NPCs_.back().SetWorld(&World_);
	}
#endif
	
	//ParseObjFile("Assets/models/Office/", "chair.obj", Meshes_, Materials_);
	//Meshes_.back().TextureId_ = TexManager.GenerateColorTexture({0.4f,0.2f,0.2f,1});
	//MeshObjects_.reserve(8192);
	//for(int i1 = 0; i1 < 128; ++i1)
	//{
	//	for(int i2 = 0; i2 < 64; ++i2)
	//	{
	//		MeshObjects_.emplace_back(ID_++, Vec3f{(float)i1,0,(float)i2},Vec3f{0,(float)(i1+i2),0},Meshes_.back());
	//	}
	//}
	//Output_.Objects.reserve(8192);

	Output_.Player = &Player_;
	Output_.WarpMouse = true;
	SkipFactor_ = 1;
	return true;
}
void
Game::GenerateWorld(int Seed, int MaxBuildingHeight, int CitySize, bool Z)
{
	World_.Clear();
	World_ = World(Seed, MaxBuildingHeight, CitySize, Z);
	Output_.Meshes = World_.GetMeshes(Player_);
	//std::mt19937 RandomGenerator(2);
	//Vec3fV Corners = { Vec3f{0, 0, 0}, Vec3f{50, 0, 0}, Vec3f{50, 0, -50}, Vec3f{0, 0, -50} };
	//static Building B(100, Corners, RandomGenerator);
	//Output_.Meshes = B.GetMeshes();
}

void
Game::Exit()
{
	World_.Clear();
	exit(EXIT_SUCCESS);
}

void
Game::ClearScene()
{
	Objects_.clear();
	PhysObjects_.clear();
	World_.Clear();
}
void
Game::ChangeScene(std::string SceneName)
{
	CurrentScene_ = SceneName;
	ClearScene();
	LoadScene(SceneName, World_, Objects_, PhysObjects_);
	Reset_ = true;
}
void
Game::Reset()
{
	for(auto it = PhysObjects_.begin(); it!=PhysObjects_.end();++it)
	{
		it->Reset();
	}
	Reset_ = true;
}
void
Game::Pause()
{
	Paused_ = true;
	//TextLine T;
	//T.Pos = Vec3f{0,1.0f,0};
	//T.C = Vec3f{1,1,1};
	//T.Text = "PAUSED";
	//SSDebugTextLines_[1] = T;
	for(auto it = PhysObjects_.begin(); it!=PhysObjects_.end(); ++it)
	{
		it->Stop();
	}
}
void
Game::UnPause()
{
	Paused_ = false;
	//SSDebugTextLines_[1] = TextLine();
	for(auto it = PhysObjects_.begin(); it!=PhysObjects_.end(); ++it)
	{
		it->Start();
	}
}
void
Game::UpdateObjectPositions()
{
	for(auto it = PhysObjects_.begin(); it!=PhysObjects_.end();++it)
	{
		it->UpdatePosition();				//updates surfaces and edges
	}
}
void
Game::Step(float dt)
{
	if(Paused_)
		return;
	//size_t PEND = PhysObjects_.size();
	//for(size_t it1 = 0; it1 < PEND; ++it1)
	//{
	//	for(size_t it2 = it1+1; it2 < PEND; ++it2)
	//	{
	//		PhysObject *Other = &PhysObjects_[it2]; 
	//		PhysObjects_[it1].TestCollision(Other);						//moves points out of objects
	//	}
	//	PhysObjects_[it1].Step(dt);										//solve all -constrains, and applies acceleration to points
	//}
	//for(auto it = PhysObjects_.begin(); it!=PhysObjects_.end(); ++it)
	//{
		//it->UpdateCPos();
		//it->Step(dt);
	//}

	auto NEND = NPCs_.end();
	for(auto it = NPCs_.begin(); it != NEND; ++it)
	{
		it->Step(dt);
	}
}	

void
Game::DisplayCamera()
{
	DisplayedScreen_ = CameraScreen;
	MouseInit_ = true;

}
void
Game::DisplayMap()
{
	DisplayedScreen_ = MapScreen;
	MouseInit_ = true;
}
