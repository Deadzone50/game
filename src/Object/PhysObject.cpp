#include <vector>
#include <iostream>
#include <fstream>
#include <float.h>
#include <assert.h>

#include "Vector/Vector.h"
#include "Vector/Matrix.h"
#include "Collision/CollisionTestsBasic.h"
#include "PhysObject.h"
#include "GlobalDefines.h"


PhysObject::PhysObject(std::string Name, Vec3f Position, Vec3f Rotation, Vec3f Speed, float Scale, Vec3f Color, float Mass)
						: StaticObject(Name, Position, Rotation, Scale, Color)
{
	Mass_ = Mass;
	
	CreateLinks();

	SetSpeed(Speed);

	SetGravity(Gravity);
	//DrawDebugPoint({0,0,0},{1,1,1});
}
void
PhysObject::Reset()
{
	Stopped_ = false;
	Freeze_ = false;
	Drawn = false;

	//Points_.clear();
	Surfaces_.clear();
	Links_.clear();
	SurfaceEdges_.clear();

	DivideIndex_.clear();

	CollisionShell_ = false;
	ShellEnd_ = 0;

	CollisionPoints_.clear();
	CollisionSurfaces_.clear();
	CollisionLinks_.clear();
	CollisionSurfaceEdges_.clear();
	
	DebugPoints_.clear();
	DebugLines_.clear();
	DebugTriangles_.clear();

	Vec3f Rot = Rotation_;
	Vec3f Pos = Position_;
	Rotation_ = {0,0,0};
	Position_ = {0,0,0};

	LoadObjectMesh();
	
	SetScale(Scale_);

	SetRotation(Rot);

	SetPosition(Pos);

	UpdateSurfaceNormalsAndPos();
	UpdateSurfaceEdges();
	if(!MakeConvex())			//if it changed
	{
		UpdateCollisionSurfaceNormalsAndPos();
		UpdateCollisionSurfaceEdges();
	}
	CreateLinks();

	SetSpeed(Speed_);

	SetGravity(Gravity);

	//DrawDebugPoint({0,0,0},{1,1,1});
}
void
PhysObject::SetSpeed(Vec3f Speed)
{
	Speed_ = Speed;
	if(CollisionShell_)
	{
		for(auto it = CollisionPoints_.begin();it!=CollisionPoints_.begin();++it)
		{
			it->LPos -= Speed_;
		}
	}
	else
	{
		for(auto it = PPoints_.begin(); it != PPoints_.end(); ++it)
		{
			it->LPos -= Speed_;
		}
	}
}
void
PhysObject::LinkConstraints()		//moves endpoints to correct position
{
	for(auto it = Links_.begin(); it!=Links_.end();++it)
	{
		Vec3f Pos1 = GetPointPos(it->I1);
		Vec3f Pos2 = GetPointPos(it->I2);

		Vec3f Diff = Pos1 - Pos2;			//calculate distance and direction between end-points
		float Dist = LEN(Diff);
		Vec3f f;
		if(Dist)							//avoid divide by zero
			f = it->K*(it->RLen - Dist)*(Diff/Dist);
		else
			f = {0,0,0};

		PPoints_[it->I1].CPos += 0.5f*f;
		PPoints_[it->I2].CPos -= 0.5f*f;
	}
	if(CollisionShell_) 
	{
		for(auto it = CollisionLinks_.begin(); it != CollisionLinks_.begin()+ShellEnd_; ++it)	//links keeping shell together
		{
			Vec3f Pos1 = GetCollisionPointPos(it->I1);
			Vec3f Pos2 = GetCollisionPointPos(it->I2);

			Vec3f Diff = Pos1 - Pos2;			//calculate distance and direction between end-points
			float Dist = LEN(Diff);
			Vec3f f;
			if(Dist)							//avoid divide by zero
				f = it->K*(it->RLen - Dist)*(Diff/Dist);
			else
				f = {0,0,0};

			CollisionPoints_[it->I1].CPos += 0.5f*f;
			CollisionPoints_[it->I2].CPos -= 0.5f*f;
		}
		for(auto it = CollisionLinks_.begin()+ShellEnd_; it!=CollisionLinks_.end();++it)			//links from shell to object
		{
			Vec3f Pos1 = GetCollisionPointPos(it->I1);
			Vec3f Pos2 = GetPointPos(it->I2);

			Vec3f Diff = Pos1 - Pos2;			//calculate distance and direction between end-points
			float Dist = LEN(Diff);
			Vec3f f;
			if(Dist)							//avoid divide by zero
				//f = it->K*(it->RLen - Dist)*(Diff/Dist);
				f = (it->RLen - Dist)*(Diff/Dist);
			else
				f = {0,0,0};

			//CollisionPoints_[it->I1].CPos += 0.5f*f;
			PPoints_[it->I2].CPos -= f;
		}
	}
}
float
PhysObject::WallConstraints(unsigned i)	//returns friciton
{
	float size = 10.0f;					//size of room
	bool Collision = false;

	std::vector<PhysPoint> *CP = GetCollisionPoints();

	//float elasticity = 0.5f;
	if(GetCollisionPointPos(i).x < -size)		//wall constraints
	{
		Collision = true;
		(*CP)[i].CPos.x = -size;
	}
	if(GetCollisionPointPos(i).x > size)
	{
		Collision = true;
		(*CP)[i].CPos.x = size;
	}
	if(GetCollisionPointPos(i).y < -size)
	{
		Collision = true;
		(*CP)[i].CPos.y = -size;
	}
	if(GetCollisionPointPos(i).y > size)
	{
		Collision = true;
		(*CP)[i].CPos.y = size;
	}
	if(GetCollisionPointPos(i).z < -size)
	{
		Collision = true;
		(*CP)[i].CPos.z = -size;
	}
	if(GetCollisionPointPos(i).z > size)
	{
		Collision = true;
		(*CP)[i].CPos.z = size;
	}
	if(Collision)
		Friction_ -= 0.02f;
	return 1;
}
void
PhysObject::Step(float dt)
{
	if(Freeze_ || Stopped_)
		return;
	
	for(int a = 0; a<3;++a)
		LinkConstraints();								//check link constraints
	int Size;
	float dt2 = dt*dt;									//time-step squared
	Vec3f TotalVelocity = {0,0,0};
	if(CollisionShell_)
	{
		Size = (int)CollisionPoints_.size();
		for(int i = 0; i < Size; ++i)
		{
			{
				WallConstraints(i);					//check wall constraints
				Vec3f vel = Friction_*(CollisionPoints_[i].CPos - CollisionPoints_[i].LPos);			// TODO: friction is different for different points -> fix
				TotalVelocity += ABS(vel);
				CollisionPoints_[i].LPos = CollisionPoints_[i].CPos;
				CollisionPoints_[i].CPos += vel + dt2*CollisionPoints_[i].Acc;
			}
		}
	}
	else
	{
		Size = (int)PPoints_.size();
		for(int i = 0; i < Size; ++i)
		{
			{
				WallConstraints(i);					//changes friction_
				Vec3f vel = Friction_*(PPoints_[i].CPos - PPoints_[i].LPos);
				TotalVelocity += ABS(vel);
				PPoints_[i].LPos = PPoints_[i].CPos;
				PPoints_[i].CPos += vel + dt2*PPoints_[i].Acc;
			}
		}
	}
	if((Friction_<1)&&(LEN(TotalVelocity) < EPSILON))	//if it has touched a wall and is no longer moving, TODO: should it be divided by the number of points?
		Stopped_ = true;								//stop updating it unless it collides with something
	Friction_ = 1;
}
void
PhysObject::GenerateCollisionManifold(Object &Obj2, Collision &Col)			//obj1 should move in direction of normal
{
	//std::vector<PhysPoint> *Obj1Points = GetCollisionPoints();
	//std::vector<PhysPoint> *Obj2Points = Obj2.GetCollisionPoints();

	std::vector<Surface> *Obj1Surfaces = GetCollisionSurfaces();
	std::vector<Surface> *Obj2Surfaces = Obj2.GetCollisionSurfaces();

	bool SSurface1 = false;
	bool SEdge1 = false;
	bool SSurface2 = false;
	bool SEdge2 = false;

	Surface SupportSurface1, SupportSurface2;
	int SupportPoint1, SupportPoint2;
																	
	auto O1SEND = Obj1Surfaces->end();							//find support surfaces
	for(auto it = Obj1Surfaces->begin(); it != O1SEND; ++it)
	{
		if(CompareNormals(it->N, -Col.Normal))
		{
			SupportSurface1 = *it;
			SSurface1 = true;
			break;
		}
	}
	if(!SSurface1)												//find support edges
	{

	}
	if(!SSurface1 || !SEdge1)
	{
		float PosProjected;										//find support points
		float Min = FLT_MAX;
		std::vector<PhysPoint> *CP = GetCollisionPoints();
		int SIZE = (int)CP->size();
		for(int i = 0; i < SIZE; ++i)
		{
			PhysPoint P = (*CP)[i];
			PosProjected = DOT(P.CPos, Col.Normal);
			if(PosProjected < Min)
			{
				Min = PosProjected;
				SupportPoint1 = i;
			}
		}
	}


	auto O2SEND = Obj2Surfaces->end();
	for(auto it = Obj2Surfaces->begin(); it != O2SEND; ++it)
	{
		if(CompareNormals(it->N, Col.Normal))
		{
			SupportSurface2 = *it;
			SSurface2 = true;
			break;
		}
	}
	if(!SSurface2)
	{

	}
	if(!SSurface2 || !SEdge2)
	{
		float PosProjected;
		float Max = -FLT_MAX;
		std::vector<PhysPoint> *CP = Obj2.GetCollisionPoints();
		int SIZE = (int)CP->size();
		for(int i = 0; i < SIZE; ++i)
		{
			PhysPoint P = (*CP)[i];
			PosProjected = DOT(P.CPos, Col.Normal);
			if(PosProjected > Max)
			{
				Max = PosProjected;
				SupportPoint2 = i;
			}
		}
	}

	Vec3f Force = 0.5f*Col.Distance*Col.Normal;							//create temporary points and calculate forces
	if(SSurface1)
	{
		Col.Points1 = SupportSurface1.Points;
		Col.Force1.resize(Col.Points1.size());
		std::fill(Col.Force1.begin(), Col.Force1.end(), Force);
	}
	//else if(SEdge1)
	//{

	//}
	else
	{
		Col.Points1.push_back(SupportPoint1);
		Col.Force1.push_back(Force);
	}


	if(SSurface2)
	{
		Col.Points2 = SupportSurface2.Points;
		Col.Force2.resize(Col.Points2.size());
		std::fill(Col.Force2.begin(), Col.Force2.end(), -Force);
	}
	//else if(SEdge2)
	//{

	//}
	else
	{
		Col.Points2.push_back(SupportPoint2);
		Col.Force2.push_back(-Force);
	}
	
}
Collision
PhysObject::EdgeSurfaceCollisionTest(Object& Obj)
{
	Collision Result;

	std::vector<Surface> *SurfacePlanes = GetCollisionSurfaces();
	std::vector<CollisionLine> OtherSurfaceEdges = *Obj.GetCollisionSurfaceEdges();

	auto SEEND = OtherSurfaceEdges.end();
	for(auto SE = OtherSurfaceEdges.begin(); SE!= SEEND;++SE)			//get start and endpoints world position
	{
		SE->S = Obj.GetCollisionPointPos(SE->Si);
		SE->E = Obj.GetCollisionPointPos(SE->Ei);
	}

	int SPEND = (int)SurfacePlanes->size();
	for(int i = 0; i < SPEND; ++i)										//for every (collision)surface check if the surface edges collides with them
	{
		Vec3f SPoint = (*SurfacePlanes)[i].P;
		Vec3f SNormal = (*SurfacePlanes)[i].N;
		for(auto SE = OtherSurfaceEdges.begin(); SE!= OtherSurfaceEdges.end();)
		{
			Vec3f RayOrigin = SE->S;
			Vec3f RayDir = SE->E - SE->S;
			float t = LEN(RayDir);
			RayDir = RayDir/t;			//normalize

			bool RayHit = RayPlaneCollisionTest(SNormal, SPoint, RayOrigin, RayDir, t);

			Vec3f RayOriginToSurface = SPoint - RayOrigin;

			if(DOT(RayOriginToSurface, SNormal) < 0)		//ray starts outside
			{
				if(!RayHit)
				{
					SE = OtherSurfaceEdges.erase(SE);		//remove lines completely outside surface
					continue;
				}
				else
				{
					SE->S = RayOrigin + t*RayDir;			//update new startpoint
					SE->CutSurS = i;
				}
			}
			else
			{												//ray starts inside
				if(RayHit)
				{
					SE->E = RayOrigin + t*RayDir;			//update new endpoint
					SE->CutSurE = i;
				}
			}
			++SE;
		}
	}					//OtherSurfaceEdges contains each linesegment that is colliding with this object

	SEEND = OtherSurfaceEdges.end();
	for(auto SE = OtherSurfaceEdges.begin(); SE!= SEEND;++SE)			//draw collision edges
	{
		DrawDebugLine(SE->S, SE->E, 0.7f*Obj.Color_);
		if(SE->CutSurS != -1 && SE->CutSurE != -1)						//if the line is cut in both ends
		{
			SE->Middle = true;											// TODO: place new point in middle
		}
	}
	std::vector<Vec3f> ValueVectorPoints;
	std::vector<Vec3f> ValueVectorSurface;
	std::vector<int> CollisionPoints;
	std::vector<int> SurfaceCollisionPoints;
	if(OtherSurfaceEdges.size() > 0)
	{
		Result.Hit = true;

		int CollisionSurfaceIndex = -1;
		auto OSEEND = OtherSurfaceEdges.end();
		for(auto SE = OtherSurfaceEdges.begin(); SE != OSEEND; ++SE)
		{
			float DistanceS = FLT_MAX;
			float DistanceE = FLT_MAX;
			int CSS = -1;
			int CSE = -1;

			int SIZE = (int)SurfacePlanes->size();
			for(int i = 0; i < SIZE; ++i)										//find closest surface except the ones the points are on
			{
				Surface *S = &(*SurfacePlanes)[i];
				Vec3f SurfaceNormal = S->N;
				float LinePointProjected, PlanePointProjected, Distance;

				PlanePointProjected = DOT(S->P, SurfaceNormal);								//from origin

				if(SE->CutSurS == -1 || (SE->Middle && (SE->CutSurS != i)))
				{
					LinePointProjected = DOT(SE->S, SurfaceNormal);
					Distance = PlanePointProjected - LinePointProjected;
					if(Distance < 0)
					{
						Distance = 0;
						abort();
					}
					if(Distance < DistanceS)
					{
						DistanceS = Distance;
						CSS = i;
					}
				}
				if(SE->CutSurE == -1 || (SE->Middle && (SE->CutSurE != i)))
				{
					LinePointProjected = DOT(SE->E, SurfaceNormal);
					Distance = PlanePointProjected - LinePointProjected;
					if(Distance < 0)
					{
						Distance = 0;
						abort();
					}
					if(Distance < DistanceE)
					{
						DistanceE = Distance;
						CSE = i;
					}
				}
			}									//have the shortest distance to move both ends of every line, aswell as the surfaces they "collide" with
												//float Distance;
												//if(DistanceS < DistanceE)			//pick shortest distance and move both ends that much, the larger will be
												//{									//on the collision sufrace
												//	Distance = DistanceS;
												//	SE->CSurface = CSS;
												//	SE->DistanceToMoveE = DistanceS;
												//	SE->DistanceToMoveS = DistanceS;
												//	CollisionPoints.push_back(SE->Si);		//only pick one point
												//	ValueVector1.push_back(Distance);
												//}
												//else
												//{
												//	Distance = DistanceE;
												//	SE->CSurface = CSE;
												//	SE->DistanceToMoveE = DistanceE;
												//	SE->DistanceToMoveS = DistanceE;
												//	CollisionPoints.push_back(SE->Ei);
												//	ValueVector1.push_back(Distance);
												//}
												//int CollisionSurfaceIndex = SE->CSurface;
												//for(size_t i = 0; i < (*SurfacePlanes)[CollisionSurfaceIndex].Points.size(); ++i)
												//{
												//	SurfaceCollisionPoints.push_back((*SurfacePlanes)[CollisionSurfaceIndex].Points[i]);
												//	ValueVector2.push_back(-SE->DistanceToMoveE);
												//}
			if(CSS != -1)
			{
				CollisionSurfaceIndex = CSS;
				Vec3f CollisionDir = (*SurfacePlanes)[CollisionSurfaceIndex].N;
				CollisionPoints.push_back(SE->Si);
				ValueVectorPoints.push_back(DistanceS*CollisionDir);
			}
			if(CSE != -1)
			{
				CollisionSurfaceIndex = CSE;
				Vec3f CollisionDir = (*SurfacePlanes)[CollisionSurfaceIndex].N;
				CollisionPoints.push_back(SE->Ei);
				ValueVectorPoints.push_back(DistanceE*CollisionDir);
			}
			if((CSS != -1) && (CSE != -1) && CSS != CSE)
			{
				int iasd = 0;
			}
		}																//gone through all surface edges, have all the points colliding and how far to move them
		float Distance = -FLT_MAX;

		size_t SIZE1 = ValueVectorPoints.size();
		Vec3f Value;
		int CP;
		//std::vector<int> CollisionPointsPruned;
		//std::vector<Vec3f> ValueVectorPointsPruned;
		//ValueVectorPointsPruned.reserve(SIZE1);
		//CollisionPointsPruned.reserve(SIZE1);
		for(size_t i = 0; i < SIZE1; ++i)								//pick the largest, and remove doubles
		{
			int P = CollisionPoints[i]; 
			//if(std::find(CollisionPointsPruned.begin(), CollisionPointsPruned.end(), P) == CollisionPointsPruned.end())
			//{
			Vec3f V = ValueVectorPoints[i];
			//CollisionPointsPruned.push_back(P);
			//ValueVectorPointsPruned.push_back(V);
			float d = LEN(V);
			if(d > Distance)
			{
				Distance = d;
				CP = P;
				Value = V;
			}
			//}
		}

		//size_t SIZE = (*SurfacePlanes)[CollisionSurfaceIndex].Points.size();
		//for(size_t i = 0; i < SIZE; ++i)
		//{
		//	SurfaceCollisionPoints.push_back((*SurfacePlanes)[CollisionSurfaceIndex].Points[i]);
		//	ValueVectorSurface.push_back(-Distance*(*SurfacePlanes)[CollisionSurfaceIndex].N);
		//}

		SIZE1 = (*SurfacePlanes)[CollisionSurfaceIndex].Points.size();												// TODO: place new point in middle
		for(size_t i = 0; i < SIZE1; ++i)								//pick the largest, and remove doubles
		{
			int P = CollisionPoints[i]; 
			Vec3f V = ValueVectorPoints[i];
			float d = LEN(V);
			if(d > Distance)
			{
				Distance = d;
				CP = P;
				Value = V;
			}
		}



		//Result.Points2 = CollisionPointsPruned;
		//Result.Force2 = ValueVectorPointsPruned;
		Result.Points2.push_back(CP);
		Result.Force2.push_back(Value);

		Result.Points1 = SurfaceCollisionPoints;
		Result.Force1 = ValueVectorSurface;
	}
	return Result;
}
void
PhysObject::TestCollision(PhysObject *Object)
{
	if(Freeze_)
	{
		return;
	}

	Vec3f OtherAABB[2];
	Object->GetAABB(OtherAABB[0], OtherAABB[1]);
	if(!AABBCollisionTest(AABB_, OtherAABB))				//test AABB
		return;
	Collision Col = SATCollisionTest(*this, *Object);		//test SAT
	if(!Col.Hit)
		return;
	//Col = EdgeSurfaceCollisionTest(*Object);		//find collision edges
	GenerateCollisionManifold(*Object, Col);
	ClearDebugDraws();

	Stopped_ = false;
	Object->Stopped_ = false;
	Friction_ -= 0.02f;
	Object->Friction_ -= 0.02f;

	//for(auto CL = Col.CollisionLines.begin(); CL!= Col.CollisionLines.end(); ++CL)
	int Size1 = (int)Col.Points1.size();
	int Size2 = (int)Col.Points2.size();
	std::vector<PhysPoint> *CP1 = GetCollisionPoints();
	std::vector<PhysPoint> *CP2 = Object->GetCollisionPoints();
	for(int i = 0; i < Size1; ++i)							//My points
	{
		DrawDebugPoint((*CP1)[Col.Points1[i]].CPos, {1,1,0});
		DrawDebugLine((*CP1)[Col.Points1[i]].CPos,(*CP1)[Col.Points1[i]].CPos + Col.Force1[i],{1,1,1});

		(*CP1)[Col.Points1[i]].CPos += Col.Force1[i];
	}
	for(int i = 0; i < Size2; ++i)							//Other Points
	{
		DrawDebugPoint((*CP2)[Col.Points2[i]].CPos, {1,0,0});
		DrawDebugLine((*CP2)[Col.Points2[i]].CPos,(*CP2)[Col.Points2[i]].CPos + Col.Force2[i],{1,1,1});

		(*CP2)[Col.Points2[i]].CPos += Col.Force2[i];			//move my surface away from collision-point
	}
}
void
PhysObject::Stop()
{
	Freeze_ = true;
}
void
PhysObject::Start()
{
	Freeze_ = false;
}

void
PhysObject::SetGravity(bool b)
{
	Vec3f G = {0,0,0};
	if(b)
		G = Vec3f{0,-9.81f,0};

	if(CollisionShell_)
	{
		size_t Size = CollisionPoints_.size();
		for(size_t i = 0; i < Size; ++i)
		{
			CollisionPoints_[i].Acc = G;
		}
	}
	else
	{
		size_t Size = PPoints_.size();
		for(size_t i = 0; i < Size; ++i)
		{
			PPoints_[i].Acc = G;
		}
	}
}