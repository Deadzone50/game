#pragma once
#include "Object.h"


class StaticObject : public Object
{
public:
	StaticObject(std::string Name, Vec3f Position, Vec3f Rotation, float Scale, Vec3f Color);
	bool					CheckIfConvex(int SurfaceIndex, int &PBehind, int &PInfront);
	std::vector<int>		CreateNewSurface(std::vector<int> Pi, Vec3f N);
	inline bool				CheckIfPointsAreBehindPlane(Vec3f N, Vec3f P, std::vector<int> Pi);
	void					StepQuickHull();
	void					QuickHull();
	//bool					StepGiftWrap();
	void					GiftWrap();
	void					CreateBox();
	bool					MakeConvex();
	void					CreateLinks();

	void					CalculateNormalAndPos(Surface &S);

	std::vector<PhysPoint>	PointsLeft_;
	std::vector<int>		PointsLeftI_;
	std::vector<int>		CollisionPointsI_;

};