#pragma once
#include <vector>
#include "Vector/Vector.h"
#include "StaticObject.h"
#include "Collision/CollisionTests.h"
#include "Containers.h"

class PhysObject : public StaticObject
{
public:
	PhysObject(std::string Name, Vec3f Position, Vec3f Rotation, Vec3f Speed, float Scale, Vec3f Color, float Mass);
	void						Reset();
	void						Step(float dt);
	void						LinkConstraints();
	float						WallConstraints(unsigned i);
	void						SetSpeed(Vec3f Speed);
	void						Stop();
	void						Start();
	void						SetGravity(bool b);

	void						TestCollision(PhysObject *Object);
	Collision					EdgeSurfaceCollisionTest(Object& Obj);
	void						GenerateCollisionManifold(Object &Obj, Collision &Col);
	
	Vec3f					Speed_;
	float 					Mass_;
	float					Friction_ = 1;
	bool					Freeze_ = false;
	bool					Stopped_ = false;
};
