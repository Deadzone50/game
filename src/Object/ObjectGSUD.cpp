#include "Object.h"
#include "Vector/Matrix.h"
#include "Character/Character.h"

int
Object::GetID()
{
	return ID_;
}
void
Object::GetAABB(Vec3f& Min, Vec3f& Max)
{
	Min = AABB_[0];
	Max = AABB_[1];
}
//inline Vec3f
//Object::GetPointPos(int i)
//{
//	return PPoints_[i].CPos;
//}
//inline Vec3f
//Object::GetCollisionPointPos(int i)
//{
//	if(CollisionShell_)
//		return CollisionPoints_[i].CPos;
//	else
//		return GetPointPos(i);
//}
std::vector<PhysPoint>*
Object::GetPoints()
{
	return &PPoints_;
}
std::vector<CollisionLine>*
Object::GetSurfaceEdges()
{
	return &SurfaceEdges_;
}
std::vector<Surface>*
Object::GetSurfaces()
{
	return &Surfaces_;
}

std::vector<PhysPoint>*
Object::GetCollisionPoints()
{
	if(CollisionShell_)
		return &CollisionPoints_;
	else
		return &PPoints_;
}
std::vector<CollisionLine>*
Object::GetCollisionSurfaceEdges()
{
	if(CollisionShell_)
		return &CollisionSurfaceEdges_;
	else
		return &SurfaceEdges_;
}
std::vector<Surface>*
Object::GetCollisionSurfaces()
{
	if(CollisionShell_)
		return &CollisionSurfaces_;
	else
		return &Surfaces_;
}
Vec3f
Object::GetPosition()
{
	return Position_;
}
							//********************* SET ***********************
void
Object::SetPosition(Vec3f Pos)
{
	for(auto it = PPoints_.begin(); it!=PPoints_.end(); ++it)
	{
		it->CPos += Pos-Position_;
		it->LPos += Pos-Position_;
	}
	for(auto it = CollisionPoints_.begin(); it!=CollisionPoints_.end(); ++it)
	{
		it->CPos += Pos-Position_;
		it->LPos += Pos-Position_;
	}
	Position_ = Pos;
	UpdateAABB();
}
void
Object::SetRotation(Vec3f Rot)
{
	if(!SUM(Rot))				//skips everything if no rotation
	{
		Rotation_ = {0,0,0};
		return;
	}
	UpdateAABB();
	//Vec3f Center{0,0,0};								
	//Center.x = (AABB_[0].x+AABB_[1].x)/2;			//calculate object centerpoint to rotate around
	//Center.y = (AABB_[0].y+AABB_[1].y)/2;
	//Center.z = (AABB_[0].z+AABB_[1].z)/2;

	Mat3f NewR;
	NewR.Rotate(Rot);

	//Mat3f OldR;
	//OldR.Rotate(-Rotation_);

	for(auto it = PPoints_.begin(); it != PPoints_.end(); ++it)
	{
		//Vec3f P = it->CPos - Center;	//vector from center to the point		
		//P = OldR*P + NewR*P + Center;
		//it->CPos = P;
		//it->LPos = P;
		Vec3f P = NewR*it->CPos;
		it->CPos = P;
		it->LPos = P;
	}
	for(auto it = CollisionPoints_.begin(); it!=CollisionPoints_.end(); ++it)
	{
		//Vec3f P = it->CPos - Center;	//vector from center to the point		
		//P = OldR*P + NewR*P + Center;
		Vec3f P = NewR*it->CPos;
		it->CPos = P;
		it->LPos = P;
	}

	Rotation_ = Rot;
	UpdateAABB();
}
void
Object::SetScale(float Scale)
{
	UpdateAABB();
	Scale_ = Scale;
	if(Scale == 1)				//skips rest if no change
	{
		return;
	}
	Vec3f Center{0,0,0};								//use centerpoint
	/*Center.x = (AABB_[0].x+AABB_[1].x)/2;
	Center.y = (AABB_[0].y+AABB_[1].y)/2;
	Center.z = (AABB_[0].z+AABB_[1].z)/2;*/

	Mat3f R;
	R = R*Scale_;

	for(auto it = PPoints_.begin(); it != PPoints_.end(); ++it)
	{
		Vec3f P = it->CPos - Center;			//vector from center to the point

		P = R*P + Center;
		it->CPos = P;
		it->LPos = P;
	}
	//for(auto it = Links_.begin(); it != Links_.end(); ++it)
	//{
	//	it->RLen = LEN(GetPointPos(it->I1)-GetPointPos(it->I2));
	//}
	UpdateAABB();
}
							//********************* UPDATE ***********************
void
Object::UpdatePosition()
{
	UpdateAABB();
	//if(CollisionShell_)
	{
		UpdateCollisionSurfaceNormalsAndPos();
		UpdateCollisionSurfaceEdges();
	}
	//else
	{
		UpdateSurfaceNormalsAndPos();
		UpdateSurfaceEdges();
	}
}

void
Object::UpdateAABB()
{
	AABB_[0].x = FLT_MAX;
	AABB_[0].y = FLT_MAX;
	AABB_[0].z = FLT_MAX;
	AABB_[1].x = -FLT_MAX;
	AABB_[1].y = -FLT_MAX;
	AABB_[1].z = -FLT_MAX;
	for(auto it = GetCollisionPoints()->begin(); it!=GetCollisionPoints()->end(); ++it)
	{
		Vec3f P = it->CPos;
		if(P.x < AABB_[0].x)
			AABB_[0].x = P.x;
		if(P.x > AABB_[1].x)
			AABB_[1].x = P.x;

		if(P.y < AABB_[0].y)
			AABB_[0].y = P.y;
		if(P.y > AABB_[1].y)
			AABB_[1].y = P.y;

		if(P.z < AABB_[0].z)
			AABB_[0].z = P.z;
		if(P.z > AABB_[1].z)
			AABB_[1].z = P.z;
	}
}
void
Object::UpdateSurfaceNormalsAndPos()						//turns vertex-indices into surfaces
{
	//int Size = (int)Surfaces_.size();
	//for(int i = 0; i < Size; ++i)
	//{
	//	int NumPoints = (int)Surfaces_[i].Points.size();
	//	Vec3f C = {0};
	//	for(int i2 = 0; i2 < NumPoints; ++i2)		//calculate center, helps if the surface is small and far from world origin
	//	{
	//		C += GetPointPos(Surfaces_[i].Points[i2]);
	//	}
	//	C = C/(float)NumPoints;
	//	Vec3f A = GetPointPos(Surfaces_[i].Points[NumPoints-1]);
	//	Vec3f B = GetPointPos(Surfaces_[i].Points[0]);
	//	Vec3f CrossSum = CROSS(A-C, B-C);
	//	for(int i2 = 0; i2 < NumPoints-1; ++i2)		//take cross of all pairs of vectors, that way the majority should be in the right orientation
	//	{
	//		A = GetPointPos(Surfaces_[i].Points[i2]);
	//		B = GetPointPos(Surfaces_[i].Points[i2+1]);
	//		CrossSum += CROSS(A-C, B-C);
	//	}
	//	Surfaces_[i].N = NORMALIZE(CrossSum);
	//	Surfaces_[i].P = A;
	//}
}
void
Object::UpdateCollisionSurfaceNormalsAndPos()						//turns vertex-indices into surfaces
{
	//std::vector<Surface> *Surfaces = GetCollisionSurfaces();

	//int Size = (int)Surfaces->size();
	//for(int i = 0; i < Size; ++i)
	//{
	//	int NumPoints = (int)(*Surfaces)[i].Points.size();
	//	Vec3f C = {0};
	//	for(int i2 = 0; i2 < NumPoints; ++i2)		//calculate center, helps if the surface is small and far from world origin
	//	{
	//		C += GetCollisionPointPos((*Surfaces)[i].Points[i2]);
	//	}
	//	C = C/(float)NumPoints;
	//	Vec3f A = GetCollisionPointPos((*Surfaces)[i].Points[NumPoints-1]);
	//	Vec3f B = GetCollisionPointPos((*Surfaces)[i].Points[0]);
	//	Vec3f CrossSum = CROSS(A-C, B-C);
	//	for(int i2 = 0; i2 < NumPoints-1; ++i2)		//take cross of all pairs of vectors, that way the majority should be in the right orientation
	//	{
	//		A = GetCollisionPointPos((*Surfaces)[i].Points[i2]);
	//		B = GetCollisionPointPos((*Surfaces)[i].Points[i2+1]);
	//		CrossSum += CROSS(A-C, B-C);
	//	}
	//	(*Surfaces)[i].N = NORMALIZE(CrossSum);
	//	(*Surfaces)[i].P = A;
	//}
}
void
Object::UpdateSurfaceEdges()				//TODO: only dynamic objects need to do this
{
	SurfaceEdges_.clear();
	int Firsti, Si, Ei;
	int Size = (int)Surfaces_.size();
	for(int i = 0; i < Size; ++i)						//create lines going around the edges of the surfaces
	{
		Vec3f Color = Surfaces_[i].C;
		auto Pi = Surfaces_[i].Points.begin();

		Firsti = *Pi;
		Si = Firsti;
		++Pi;
		for(; Pi!= Surfaces_[i].Points.end(); ++Pi)
		{
			Ei = *Pi;

			CollisionLine L;
			//L.S = GetPointPos(Si);
			//L.E = GetPointPos(Ei);
			L.Si = Si;
			L.Ei = Ei;
			L.MySurface1 = i;
			L.C = Color;
			SurfaceEdges_.push_back(L);

			Si = Ei;
		}
		CollisionLine L;
		//L.S = GetPointPos(Si);
		//L.E = GetPointPos(Firsti);
		L.Si = Si;
		L.Ei = Firsti;
		L.MySurface1 = i;
		L.C = Color;
		SurfaceEdges_.push_back(L);
	}
	int OSESize = (int)SurfaceEdges_.size();
	for(int i1 = 0; i1 < OSESize; ++i1)				//prune doubles
	{
		for(int i2 = i1+1; i2 < OSESize; ++i2)
		{
			if((SurfaceEdges_[i1].Si == SurfaceEdges_[i2].Si) || (SurfaceEdges_[i1].Si == SurfaceEdges_[i2].Ei))
				if((SurfaceEdges_[i1].Ei == SurfaceEdges_[i2].Si) || (SurfaceEdges_[i1].Ei == SurfaceEdges_[i2].Ei))
				{
					SurfaceEdges_[i1].MySurface2 = SurfaceEdges_[i2].MySurface1;
					SurfaceEdges_.erase(SurfaceEdges_.begin()+i2);
					--OSESize;
					--i2;
				}
		}
	}
	//for(int i1 = 0; i1 < OSESize; ++i1)
	//{
	//	assert(SurfaceEdges_[i1].MySurface1);
	//	assert(SurfaceEdges_[i1].MySurface2);
	//}
}
void
Object::UpdateCollisionSurfaceEdges()				//TODO: only dynamic objects need to do this
{
	//std::vector<std::vector<int>> *CS = GetCollisionSurfacePoints();
	std::vector<CollisionLine> *CSE = GetCollisionSurfaceEdges();
	std::vector<Surface> *Surfaces = GetCollisionSurfaces();

	CSE->clear();
	int Firsti, Si, Ei;
	int Size = (int)Surfaces->size();
	for(int i = 0; i < Size; ++i)						//create lines going around the edges of the surfaces
	{
		Vec3f Color = (*Surfaces)[i].C;
		auto Pi = (*Surfaces)[i].Points.begin();

		Firsti = *Pi;
		Si = Firsti;
		++Pi;
		for(; Pi!= (*Surfaces)[i].Points.end(); ++Pi)
		{
			Ei = *Pi;

			CollisionLine L;
			//L.S = GetCollisionPointPos(Si);
			//L.E = GetCollisionPointPos(Ei);
			L.Si = Si;
			L.Ei = Ei;
			L.MySurface1 = i;
			L.C = Color;
			CSE->push_back(L);

			Si = Ei;
		}
		CollisionLine L;
		//L.S = GetCollisionPointPos(Si);
		//L.E = GetCollisionPointPos(Firsti);
		L.Si = Si;
		L.Ei = Firsti;
		L.MySurface1 = i;
		L.C = Color;
		CSE->push_back(L);
	}
	int OSESize = (int)CSE->size();
	for(int i1 = 0; i1 < OSESize; ++i1)				//prune doubles
	{
		for(int i2 = i1+1; i2 < OSESize; ++i2)
		{
			if(((*CSE)[i1].Si == (*CSE)[i2].Si) || ((*CSE)[i1].Si == (*CSE)[i2].Ei))
				if(((*CSE)[i1].Ei == (*CSE)[i2].Si) || ((*CSE)[i1].Ei == (*CSE)[i2].Ei))
				{
					(*CSE)[i1].MySurface2 = (*CSE)[i2].MySurface1;
					CSE->erase(CSE->begin()+i2);
					--OSESize;
					--i2;
				}
		}
	}
//for(int i1 = 0; i1 < OSESize; ++i1)
//{
//	assert(SurfaceEdges_[i1].MySurface1);
//	assert(SurfaceEdges_[i1].MySurface2);
//}
}
							//********************* DRAW ***********************
void
Object::SurfaceToDebugTriangles(Surface Surface, Vec3f Color, bool Collision)
{
	//size_t NumPoints = Surface.Points.size();
	//if(NumPoints == 4)
	//{
	//	Triangle T1, T2;
	//	T1.C = Color;
	//	T2.C = 0.5f*Color;
	//	if(Collision)
	//	{
	//		T1.P1 = GetCollisionPointPos(Surface.Points[0]);
	//		T1.P2 = GetCollisionPointPos(Surface.Points[1]);
	//		T1.P3 = GetCollisionPointPos(Surface.Points[2]);

	//		T2.P1 = GetCollisionPointPos(Surface.Points[0]);
	//		T2.P2 = GetCollisionPointPos(Surface.Points[2]);
	//		T2.P3 = GetCollisionPointPos(Surface.Points[3]);
	//	}
	//	else
	//	{
	//		T1.P1 = GetPointPos(Surface.Points[0]);
	//		T1.P2 = GetPointPos(Surface.Points[1]);
	//		T1.P3 = GetPointPos(Surface.Points[2]);

	//		T2.P1 = GetPointPos(Surface.Points[0]);
	//		T2.P2 = GetPointPos(Surface.Points[2]);
	//		T2.P3 = GetPointPos(Surface.Points[3]);
	//	}
	//	DrawDebugText(T1.P1+0.1f*Surface.N, {1,1,1}, (float)Surface.Points[0], false);
	//	DrawDebugText(T1.P2+0.1f*Surface.N, {1,1,1}, (float)Surface.Points[1], false);
	//	DrawDebugText(T1.P3+0.1f*Surface.N, {1,1,1}, (float)Surface.Points[2], false);
	//	DrawDebugText(T2.P3+0.1f*Surface.N, {1,1,1}, (float)Surface.Points[3], false);

	//	DebugTriangles_.push_back(T1);
	//	DebugTriangles_.push_back(T2);
	//}
	//else if(NumPoints == 3)
	//{
	//	Triangle T1;
	//	T1.C = Color;
	//	if(Collision)
	//	{
	//		T1.P1 = GetCollisionPointPos(Surface.Points[0]);
	//		T1.P2 = GetCollisionPointPos(Surface.Points[1]);
	//		T1.P3 = GetCollisionPointPos(Surface.Points[2]);
	//	}
	//	else
	//	{
	//		T1.P1 = GetPointPos(Surface.Points[0]);
	//		T1.P2 = GetPointPos(Surface.Points[1]);
	//		T1.P3 = GetPointPos(Surface.Points[2]);
	//	}
	//	DrawDebugText(T1.P1+0.1f*Surface.N, {1,1,1}, (float)Surface.Points[0], false);
	//	DrawDebugText(T1.P2+0.1f*Surface.N, {1,1,1}, (float)Surface.Points[1], false);
	//	DrawDebugText(T1.P3+0.1f*Surface.N, {1,1,1}, (float)Surface.Points[2], false);

	//	DebugTriangles_.push_back(T1);
	//}
	//else
	//{
	//	abort();			//other than 3 or 4 points not supported
	//}
}


void
Object::DrawDebugSurface(size_t Si, bool Collision, Vec3f Col)
{
	Surface S;
	std::vector<Surface> *Sur;
	if(Collision)
	{
		S = (*GetCollisionSurfaces())[Si];
		Sur = GetCollisionSurfaces();
	}
	else
	{
		S = (*GetSurfaces())[Si];
		Sur = GetSurfaces();
	}
	if(SUM(Col))
		S.C = Col;
	Line L;
	L.S = S.P;
	L.E = S.P + S.N;
	L.C = 20*S.C;
	DebugLines_.push_back(L);

	DrawDebugText(S.P + 0.5f*S.N+Vec3f{0,0.2f,0}, {1,0,1}, (float)Si, false);		//surface number
	SurfaceToDebugTriangles(S, S.C, Collision);

	//Poly pol;
	//pol.C = S.C;
	//if(Collision)
	//{
	//	for(auto Pit = (*Sur)[Si].Points.begin(); Pit != (*Sur)[Si].Points.end(); ++Pit)
	//	{
	//		pol.P.push_back(GetCollisionPointPos(*Pit));
	//		DrawDebugText(GetCollisionPointPos(*Pit)+0.1f*S.N, {1,1,1}, (float)*Pit, false);
	//	}
	//}
	//else
	//{
	//	for(auto Pit = (*Sur)[Si].Points.begin(); Pit != (*Sur)[Si].Points.end(); ++Pit)
	//	{
	//		pol.P.push_back(GetPointPos(*Pit));
	//		DrawDebugText(GetPointPos(*Pit)+0.1f*S.N, {1,1,1}, (float)*Pit, false);
	//	}
	//}
	//DebugPolygons_.push_back(pol);
}
void
Object::DrawAABB()
{
	if(DebugLines_.size() > MaxDebugLines)
		DebugLines_.clear();
	Line L;
	L.C = Vec3f{1,1,1};

	L.S = AABB_[0];
	L.E = AABB_[1];
	DebugLines_.push_back(L);
}