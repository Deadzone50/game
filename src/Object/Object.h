#pragma once
#include <vector>
#include "Vector/Vector.h"
#include "Debug/Debug.h"
#include "Containers.h"
#include "GlobalDefines.h"
#include "Mesh.h"

class Object : public DebugDraws
{
public:
	Object() {};
	Object(std::string Name, Vec3f Position, Vec3f Rotation, float Scale, Vec3f Color);

	int						GetID();
	void					GetAABB(Vec3f& Min, Vec3f& Max);

	void					SurfaceToDebugTriangles(Surface Surface, Vec3f Color, bool Collision);

	Vec3f							GetPointPos(int i) {return {0,0,0};}
	std::vector<PhysPoint>			*GetPoints();
	std::vector<CollisionLine>		*GetSurfaceEdges();
	std::vector<Surface>			*GetSurfaces();

	Vec3f							GetCollisionPointPos(int i) {return {0,0,0};}
	std::vector<PhysPoint>			*GetCollisionPoints();
	std::vector<CollisionLine>		*GetCollisionSurfaceEdges();
	std::vector<Surface>			*GetCollisionSurfaces();

	void					LoadObjectMesh();

	void					SetScale(float Scale);
	void					SetRotation(Vec3f Rot);
	void					SetPosition(Vec3f Pos);

	Vec3f					GetPosition();


	void					UpdatePosition();
	void					UpdateSurfaceNormalsAndPos();
	void					UpdateCollisionSurfaceNormalsAndPos();
	void					UpdateSurfaceEdges();
	void					UpdateCollisionSurfaceEdges();
	
	void					UpdateAABB();


	void					ProjectToAxis(float& Min, float& Max, Vec3f Axis);
	int						RayCollisionTest(Vec3f RayOrigin, Vec3f RayDir, float& MaxLen, bool NoAABB = false);
	
	bool					DrawOutline;
	bool					DrawLinks;
	bool					DrawCollisionLinks;

	void					DrawAABB();
	void					DrawDebugSurface(size_t Si, bool Collision, Vec3f C = {0,0,0});

	std::vector<PhysPoint>			PPoints_;		//mesh
	std::vector<Surface>			Surfaces_;
	std::vector<Link>				Links_;
	std::vector<CollisionLine>		SurfaceEdges_;
	std::vector<int>				DivideIndex_;

	bool							CollisionShell_;
	int								ShellEnd_;
	std::vector<PhysPoint>			CollisionPoints_;	//collision mesh
	std::vector<Surface>			CollisionSurfaces_;
	std::vector<Link>				CollisionLinks_;
	std::vector<CollisionLine>		CollisionSurfaceEdges_;

	Vec3f							AABB_[2];

	int								ID_;
	std::string						Name_;
	Vec3f							Position_;
	Vec3f							Rotation_;
	float							Scale_;
	Vec3f							Color_;

	bool							Drawn;
	unsigned int					MaxDebugPoints;
	unsigned int					MaxDebugLines;
	unsigned int					MaxDebugPolygons;	
};

class MeshObject
{
public:
	MeshObject() {}
	MeshObject(Vec3f Pos, Vec3f Rot, Mesh &M)
	{
		ID_ = GlobalId++;
		Mesh_ = &M;
		Model_.Rotate(Rot);
		Model_.Translate(Pos);
	}
	size_t					GetNumberOfPoints()						{return Mesh_->Vertices_.size();}
	Vec3f					GetMeshPointPosition(size_t PointIndex)	{return Mesh_->Vertices_[PointIndex].Position;}

	size_t					Draw();
protected:
	int						ID_;
	Mesh					*Mesh_;
	Mat4f					Model_;
};