#include <cassert>
#include <algorithm>
#include <functional>
#include "StaticObject.h"
#include "GlobalDefines.h"

StaticObject::StaticObject(std::string Name, Vec3f Position, Vec3f Rotation, float Scale, Vec3f Color)
							: Object(Name, Position, Rotation, Scale, Color)
{
	if(!MakeConvex())			//if it changed
	{
		UpdateCollisionSurfaceNormalsAndPos();
		UpdateCollisionSurfaceEdges();
	}
	//CreateLinks();
}

bool
StaticObject::CheckIfConvex(int SurfaceIndex, int &PBehind, int &PInfront)
{
	bool Result = true;
	Surface S = Surfaces_[SurfaceIndex];
	std::vector<int> CPoints;
	int SurIndexStart = 0;
	int SurIndexEnd = (int)Surfaces_.size();
	if(!DivideIndex_.empty())
	{
		for(auto D = DivideIndex_.begin(); D != DivideIndex_.end(); ++D)	//which part of the object to check
		{
			if(SurfaceIndex >= *D)
			{
				SurIndexStart = *D;
			}
			else
			{
				SurIndexEnd = *D;
				break;
			}
		}
	}
	for(int i = SurIndexStart; i < SurIndexEnd; ++i)					//add all the points from the surfaces
	{
		for(auto it = Surfaces_[i].Points.begin(); it != Surfaces_[i].Points.end(); ++it)
		{
			if(find(CPoints.begin(), CPoints.end(),*it) == CPoints.end())
				CPoints.push_back(*it);
			//CPoints.insert(CPoints.end(),Surfaces_[i].begin(), Surfaces_[i].end());
		}
	}
	int Size = (int)Surfaces_[SurfaceIndex].Points.size();
	//int Size = CPoints.size();
	for(int i = 0; i < Size; ++i)										//remove the points that are part of the surface
	{
		auto it = find(CPoints.begin(),CPoints.end(), Surfaces_[SurfaceIndex].Points[i]);
		if(it != CPoints.end())
		{
			CPoints.erase(it);
		}
	}

	for(auto p = CPoints.begin(); p != CPoints.end(); ++p)
	{
		if(DOT(S.P - GetPointPos(*p),S.N)+EPSILON >= 0)					//check if point is behind or on of surface
		{
			++PBehind;
		}
		else
		{
			++PInfront;
			Result = false;
		}
	}
	return Result;
}

inline bool
StaticObject::CheckIfPointsAreBehindPlane(Vec3f N, Vec3f P, std::vector<int> Pi)
{
	int Size = (int)Pi.size();
	for(int i = 0; i < Size; ++i)
	{
		if(Pi[i] == -1)
			continue;
		else if(DOT(N, P - GetPointPos(Pi[i])) < -EPSILON)		//check if behind plane between the test points
		{
			return false;
		}
	}
	return true;
}
std::vector<int>
StaticObject::CreateNewSurface(std::vector<int> Pi, Vec3f N)		//always makes a convex surface, even if it wasn't one before
{
	int Size = (int)Pi.size();
	//if(Size < 3)
	//	return std::vector<int>();
	std::vector<int> S(Size);
	std::vector<int> LeftOver = Pi;
	Vec3f DirOnPlane = GetPointPos(Pi[1]) - GetPointPos(Pi[0]);
	int Start = 0;						//pick the point furthest in a direction on the plane, guaranteed to be part of convex hull
	for(int i = 0;i < Size; ++i)
	{
		if(CheckIfPointsAreBehindPlane(DirOnPlane, GetPointPos(Pi[i]), Pi))
		{
			Start = i;
			break;
		}
	}
	LeftOver.erase(LeftOver.begin()+Start);
	S[0] = Pi[Start];
	int Si = 0;
	for(int i = 0; i < Size; ++i)							//does never check the first point again->cant create cycles
	{
		if(S[Si] == Pi[i] || i == Start)
			continue;
		Vec3f N1 = CROSS(GetPointPos(Pi[i]) - GetPointPos(S[Si]), N);
		bool Ok = CheckIfPointsAreBehindPlane(N1, GetPointPos(S[Si]), Pi);

		if(Ok)
		{
			++Si;
			S[Si] = Pi[i];
			auto it = find(LeftOver.begin(), LeftOver.end(), Pi[i]);
			LeftOver.erase(it);
			i = -1;
			if(Si == Size-1)	//done, all points ordered
				break;
		}
	}
	if(LeftOver.size())
	{
		int i = 0;
		++Si;
		for(;Si < Size;++Si)
		{
			S[Si] = LeftOver[i];
			++i;
		}
	}
	return S;
}
void
StaticObject::StepQuickHull()
{
	ClearDebugDraws();

	Surface S1, S2, S3;
	S1.C = {1,1,1};
	S2.C = {1,1,1};
	S3.C = {1,1,1};
	std::vector<std::vector<int>> PointsInfront;
	std::vector<int> NewPoint;
	for(auto &S : CollisionSurfaces_)
	{
		std::vector<int> Infront;
		float MaxDist = 0;
		int NewP;
		for(int i =  0; i < (int)PointsLeft_.size(); ++i)
		{
			float Dist = -DOT(S.P - PointsLeft_[i].CPos,S.N);
			if(Dist > 0)					//find points infront of surface
			{
				Infront.push_back(i);
				if(Dist > MaxDist)
				{
					NewP = i;
					MaxDist = Dist;
				}
			}
		}
		if(MaxDist)
			NewPoint.push_back(NewP);
		else
			NewPoint.push_back(-1);
		PointsInfront.push_back(Infront);
	}
	Vec3f PA, PB, PC;
	int CSSize = (int)CollisionSurfaces_.size();
	for(int i = 0; i < CSSize; ++i)
	{
		if(NewPoint[i] > -1)		//if it found one
		{
			Surface S = CollisionSurfaces_[i];
			DrawDebugSurface(i, true, {1,1,1});
			PhysPoint NewPhys = PointsLeft_[NewPoint[i]];
			PointsLeft_.erase(PointsLeft_.begin()+NewPoint[i]);

			std::vector<int> Pi;
			
			for(size_t i2 = 0; i2 < CSSize;++i2)
			{
				if(NewPoint[i2] > -1)
				{
					if(i2 == i)
						continue;
					auto it = std::find(PointsInfront[i2].begin(), PointsInfront[i2].end(), NewPoint[i]);
					if(it != PointsInfront[i2].end())
					{
						Pi.insert(Pi.end(), CollisionSurfaces_[i2].Points.begin(), CollisionSurfaces_[i2].Points.end());
						CollisionSurfaces_.erase(CollisionSurfaces_.begin() + i2);
						--CSSize;
						--i2;
					}
				}
			}
			
			if(Pi.size())
			{
				Pi.insert(Pi.end(), S.Points.begin(), S.Points.end());
				CollisionSurfaces_.erase(CollisionSurfaces_.begin() + i);
				int Size = (int)Pi.size();
				for(int i1 = 0; i1 < Size; ++i1)
				{
					for(int i2 = i1+1; i2 < Size; ++i2)
					{
						if(Pi[i1] == Pi[i2])
						{
							Pi.erase(Pi.begin()+i2);
							--Size;
							--i2;
						}
					}
				}
				std::sort(Pi.begin(), Pi.end(), std::greater<int>());
				for(auto &P : Pi)
				{
					CollisionPoints_.erase(CollisionPoints_.begin()+P);
				}
				S.Points = CreateNewSurface(Pi, S.N);
			}


			int NP = (int)CollisionPoints_.size();
			CollisionPoints_.push_back(NewPhys);

			DrawDebugPoint(NewPhys.CPos, {1,0,1});

			PA = CollisionPoints_[S.Points[0]].CPos;
			PB = CollisionPoints_[S.Points[1]].CPos;
			PC = CollisionPoints_[S.Points[2]].CPos;

			S1.Points.clear();
			S2.Points.clear();
			S3.Points.clear();
			S1.P = PA;
			S2.P = PB;
			S3.P = PC;

			S1.Points.push_back(S.Points[0]);	S2.Points.push_back(S.Points[1]);	S3.Points.push_back(S.Points[2]);
			S1.Points.push_back(S.Points[1]);	S2.Points.push_back(S.Points[2]);	S3.Points.push_back(S.Points[0]);
			S1.Points.push_back(NP);			S2.Points.push_back(NP);			S3.Points.push_back(NP);

			S1.N = CROSS(PB-PA, NewPhys.CPos-PB);
			S2.N = CROSS(PC-PB, NewPhys.CPos-PC);
			S3.N = CROSS(PA-PC, NewPhys.CPos-PA);

			CollisionSurfaces_.erase(CollisionSurfaces_.begin());	//remove the old surface

			int SI = (int)CollisionSurfaces_.size();

			CollisionSurfaces_.push_back(S1);
			CollisionSurfaces_.push_back(S2);
			CollisionSurfaces_.push_back(S3);
			DrawDebugSurface(SI, true, {1,0,1});
			DrawDebugSurface(SI+1, true, {1,0,1});
			DrawDebugSurface(SI+2, true, {1,0,1});
			break;
		}
	}
	

}
void
StaticObject::QuickHull()
{
	PointsLeft_ = PPoints_;
	Surface S;
	PhysPoint A, B, C;

	A = PointsLeft_.back();
	PointsLeft_.pop_back();
	B = PointsLeft_.back();
	PointsLeft_.pop_back();
	C = PointsLeft_.back();
	PointsLeft_.pop_back();
	CollisionPoints_.push_back(A);
	CollisionPoints_.push_back(B);
	CollisionPoints_.push_back(C);

	S.C = {1,1,1};

	S.P = A.CPos;
	S.N = CROSS(B.CPos-A.CPos, C.CPos-B.CPos);
	S.Points.push_back(0);
	S.Points.push_back(1);
	S.Points.push_back(2);
	CollisionSurfaces_.push_back(S);

	S.N = CROSS(B.CPos-C.CPos, A.CPos-B.CPos);
	S.Points.clear();
	S.Points.push_back(2);
	S.Points.push_back(1);
	S.Points.push_back(0);
	CollisionSurfaces_.push_back(S);
}
void
StaticObject::CalculateNormalAndPos(Surface &S)
{
	int NumPoints = (int)S.Points.size();
	Vec3f C = {0};
	for(int i2 = 0; i2 < NumPoints; ++i2)		//calculate center, helps if the surface is small and far from world origin
	{
		//C += GetCollisionPointPos(S.Points[i2]);
		C += GetPointPos(S.Points[i2]);
	}
	C = C/(float)NumPoints;
	//Vec3f A = GetCollisionPointPos(S.Points[NumPoints-1]);
	//Vec3f B = GetCollisionPointPos(S.Points[0]);
	Vec3f A = GetPointPos(S.Points[NumPoints-1]);
	Vec3f B = GetPointPos(S.Points[0]);
	Vec3f CrossSum = CROSS(A-C, B-C);
	for(int i2 = 0; i2 < NumPoints-1; ++i2)		//take cross of all pairs of vectors, that way the majority should be in the right orientation
	{
		//A = GetCollisionPointPos(S.Points[i2]);
		//B = GetCollisionPointPos(S.Points[i2+1]);
		A = GetPointPos(S.Points[i2]);
		B = GetPointPos(S.Points[i2+1]);
		CrossSum += CROSS(A-C, B-C);
	}
	S.N = NORMALIZE(CrossSum);
	S.P = A;
}
void
StaticObject::GiftWrap()
{
	std::vector<std::pair<int, int>> Edges;			//unused triangle edges


	std::vector<int> AllPointsI;
	PointsLeftI_.resize(PPoints_.size());
	AllPointsI.resize(PPoints_.size());
	for(size_t i = 0; i < PPoints_.size(); ++i)
	{
		PointsLeftI_[i] = (int)i;
		AllPointsI[i] = (int)i;
	}
	float MinX = FLT_MAX;						//find leftmost point of object
	int PiX = -1;
	for(size_t i = 0; i < PPoints_.size(); ++i)
	{
		if(PPoints_[i].CPos.x < MinX)
		{
			MinX = PPoints_[i].CPos.x;
			PiX = (int)i;
		}
	}
	CollisionPointsI_.push_back(PiX);
	auto it = find(PointsLeftI_.begin() , PointsLeftI_.end(), PiX);
	PointsLeftI_.erase(it);
	
	Vec3f P1ToNewP;
	Vec3f N = {-1,0,0};
	PhysPoint P1 = PPoints_[CollisionPointsI_.back()];
	//DrawDebugLine(P1.CPos, P1.CPos+N, {1,0,0});
	float MinCos = FLT_MAX;
	int ChosenIndex = 0;
	for(size_t i = 0; i < PointsLeftI_.size(); ++i)	//find smallest angle
	{
		PhysPoint NewP = PPoints_[PointsLeftI_[i]];
		P1ToNewP = NORMALIZE(NewP.CPos - P1.CPos);
		float Cos = -DOT(N, P1ToNewP);
		if(Cos < MinCos)
		{
			MinCos = Cos;
			ChosenIndex = (int)i;
		}
	}
	CollisionPointsI_.push_back(PointsLeftI_[ChosenIndex]);
	PointsLeftI_.erase(PointsLeftI_.begin()+ChosenIndex);

	std::vector<int> Pi(3, -1);
	Pi[0] = CollisionPointsI_[0];
	Pi[1] = CollisionPointsI_[1];
	Surface NewSurface, NewSurfaceInv;
	NewSurface.C = {1,1,1};
	NewSurfaceInv.C = {1,1,1};
	NewSurfaceInv.Points.resize(3);
	int ChosenPoint;
	for(size_t i = 0; i < PointsLeftI_.size(); ++i)
	{
		Pi[2] = PointsLeftI_[i];
		NewSurface.Points = CreateNewSurface(Pi, N);
		CalculateNormalAndPos(NewSurface);
		
		ChosenPoint = PointsLeftI_[i];
		PointsLeftI_[i] = -1;			//don't count temporarily
		bool Valid = CheckIfPointsAreBehindPlane(NewSurface.N, NewSurface.P, PointsLeftI_);
		if(Valid)
		{
			PointsLeftI_.erase(PointsLeftI_.begin() + i);
			CollisionSurfaces_.push_back(NewSurface);
			break;
		}
		else
			PointsLeftI_[i] = ChosenPoint;
	}
	Edges.push_back(std::pair<int,int>(NewSurface.Points[0],NewSurface.Points[1]));
	Edges.push_back(std::pair<int,int>(NewSurface.Points[1],NewSurface.Points[2]));
	Edges.push_back(std::pair<int,int>(NewSurface.Points[2],NewSurface.Points[0]));

	while(!Edges.empty())
	{
		std::pair<int,int> CEdge = Edges.back();
		Edges.pop_back();

		//std::vector<int> Pi(3, -1);
		Pi[0] = std::get<1>(CEdge);
		Pi[1] = std::get<0>(CEdge);
		//N = std::get<2>(CEdge);
		//Surface NewSurface;
		//NewSurface.C = {1,1,1};
		//int ChosenPoint;
		bool Found = false;
		for(size_t i = 0; i < PointsLeftI_.size(); ++i)
		{
			Pi[2] = PointsLeftI_[i];
			//N = CROSS(GetPointPos(Pi[1])-GetPointPos(Pi[0]), GetPointPos(Pi[2])-GetPointPos(Pi[1]));

			NewSurface.Points = Pi;
			CalculateNormalAndPos(NewSurface);

			ChosenPoint = PointsLeftI_[i];
			PointsLeftI_[i] = -1;			//don't count temporarily
			bool Valid = CheckIfPointsAreBehindPlane(NewSurface.N, NewSurface.P, AllPointsI);
			if(Valid)
			{
				PointsLeftI_.erase(PointsLeftI_.begin() + i);
				CollisionSurfaces_.push_back(NewSurface);

				Edges.push_back(std::pair<int,int>(Pi[1],Pi[2]));
				Edges.push_back(std::pair<int,int>(Pi[2],Pi[0]));
				Found = true;
				break;
			}
			else
				PointsLeftI_[i] = ChosenPoint;
		}
		if(!Found)
		for(size_t i = 0; i < Edges.size(); ++i)
		{
			int EdgeP0 = std::get<0>(Edges[i]);
			int EdgeP1 = std::get<1>(Edges[i]);

			if(EdgeP0 == Pi[0] || EdgeP0 == Pi[1])
			{
				Pi[2] = EdgeP1;
			}
			else if(EdgeP1 == Pi[0] || EdgeP1 == Pi[1])
			{
				Pi[2] = EdgeP0;
			}
			else
			{
				continue;			//not a shared edge
			}
			//bool Pi0Shared = (EdgeP0 == Pi[0])||(EdgeP1 == Pi[0]);
			bool Pi1Shared = (EdgeP0 == Pi[1])||(EdgeP1 == Pi[1]);

			NewSurface.Points = Pi;
			CalculateNormalAndPos(NewSurface);
			bool Valid = CheckIfPointsAreBehindPlane(NewSurface.N, NewSurface.P, AllPointsI);
			if(Valid)
			{
				Edges.erase(Edges.begin() + i);
				CollisionSurfaces_.push_back(NewSurface);

				if(Edges.size() == 1 && PointsLeftI_.empty())
				{
					Found = true;
					break;
				}

				if(Pi1Shared)
					Edges.push_back(std::pair<int,int>(Pi[2],Pi[0]));
				else
					Edges.push_back(std::pair<int,int>(Pi[1],Pi[2]));
				Found = true;
				break;
			}
		}
		if(!Found && !Edges.empty())
		{
			Edges.insert(Edges.begin(), CEdge);
		}

	}

	std::vector<int> ProcessedPoints;
	for(size_t i = 0; i < CollisionSurfaces_.size(); ++i)
	{
		if(CollisionSurfaces_[i].Points[0] == CollisionSurfaces_[i].Points[1] || CollisionSurfaces_[i].Points[0] == CollisionSurfaces_[i].Points[2] || CollisionSurfaces_[i].Points[1] == CollisionSurfaces_[i].Points[2])
		{
			CollisionSurfaces_.erase(CollisionSurfaces_.begin() + i);
			continue;
		}
		for(int I = 0; I < 3; ++I)
		{
			int PointI = CollisionSurfaces_[i].Points[I];
			size_t FindI = 0;
			for(; FindI < ProcessedPoints.size(); ++FindI)
			{
				if(ProcessedPoints[FindI] == PointI)
					break;
			}
			if(FindI == ProcessedPoints.size())
			{
				ProcessedPoints.push_back(PointI);
				CollisionPoints_.push_back(PPoints_[PointI]);
			}
			CollisionSurfaces_[i].Points[I] = (int)FindI;
		}
	}


	//Vec3f P1ToP2 = NORMALIZE(P2.CPos - P1.CPos);
	//Vec3f Cross = CROSS(P1ToP2, N);
	//N = CROSS(Cross, P1ToP2);
	//P1 = CollisionPoints_.back();
	//PhysPoint P0 = *(CollisionPoints_.end() - 2);
	//Vec3f P1ToP0 = P0.CPos - P1.CPos;
	////DrawDebugLine(P1.CPos, P1.CPos+N, {0,1,0});
	////DrawDebugLine(P1.CPos, P1.CPos+P1ToP0, {1,0,1});
	//MinCos = FLT_MAX;
	//for(auto &NewP : PointsLeft_)					//find smallest angle
	//{
	//	P1ToNewP = NewP.CPos - P1.CPos;
	//	float L2 = LEN(P1ToP0);
	//	float dot = DOT(P1ToNewP, P1ToP0)/L2;
	//	Vec3f PTmp = P1.CPos + dot*NORMALIZE(P1ToP0);
	//	Vec3f PTmpToNewP = NORMALIZE(NewP.CPos - PTmp);
	//	float Cos = -DOT(N, PTmpToNewP);
	//	if(Cos < MinCos)
	//	{
	//		//DrawDebugPoint(PTmp, {1,0,1});
	//		//DrawDebugLine(PTmp, NewP.CPos, {1,0,1});
	//		//DrawDebugLine(PTmp, PTmp+N, {0,1,0});
	//		MinCos = Cos;
	//		P2 = NewP ;
	//	}
	//}
	//CollisionPoints_.push_back(P2);
	//it = find(PointsLeft_.begin() , PointsLeft_.end(), P2);
	//PointsLeft_.erase(it);
	//DrawDebugPoint(P2.CPos, {0,0,1});

	//std::vector<int> Pi;
	//Pi.push_back(0);			//not sure if correct
	//Pi.push_back(1);
	//Pi.push_back(2);
	//Surface NewSurface;
	//NewSurface.C = {1,1,1};
	//NewSurface.Points = CreateNewSurface(Pi, N);
	//CalculateNormalAndPos(NewSurface);
	//CollisionSurfaces_.push_back(NewSurface);
}

void
StaticObject::CreateBox()
{
	CollisionPoints_.push_back(PhysPoint({AABB_[0].x, AABB_[0].y, AABB_[0].z}));	//corners of bounding box
	CollisionPoints_.push_back(PhysPoint({AABB_[0].x, AABB_[0].y, AABB_[1].z}));
	CollisionPoints_.push_back(PhysPoint({AABB_[0].x, AABB_[1].y, AABB_[1].z}));
	CollisionPoints_.push_back(PhysPoint({AABB_[0].x, AABB_[1].y, AABB_[0].z}));
	CollisionPoints_.push_back(PhysPoint({AABB_[1].x, AABB_[0].y, AABB_[0].z}));
	CollisionPoints_.push_back(PhysPoint({AABB_[1].x, AABB_[0].y, AABB_[1].z}));
	CollisionPoints_.push_back(PhysPoint({AABB_[1].x, AABB_[1].y, AABB_[1].z}));
	CollisionPoints_.push_back(PhysPoint({AABB_[1].x, AABB_[1].y, AABB_[0].z}));

	CollisionSurfaces_ = std::vector<Surface>(6);														//create the collision surfaces
	CollisionSurfaces_[0].Points.push_back(0);CollisionSurfaces_[0].Points.push_back(1);CollisionSurfaces_[0].Points.push_back(2);CollisionSurfaces_[0].Points.push_back(3); CollisionSurfaces_[0].C = Color_;
	CollisionSurfaces_[1].Points.push_back(4);CollisionSurfaces_[1].Points.push_back(7);CollisionSurfaces_[1].Points.push_back(6);CollisionSurfaces_[1].Points.push_back(5); CollisionSurfaces_[1].C = Color_;
	CollisionSurfaces_[2].Points.push_back(1);CollisionSurfaces_[2].Points.push_back(5);CollisionSurfaces_[2].Points.push_back(6);CollisionSurfaces_[2].Points.push_back(2); CollisionSurfaces_[2].C = Color_;
	CollisionSurfaces_[3].Points.push_back(0);CollisionSurfaces_[3].Points.push_back(3);CollisionSurfaces_[3].Points.push_back(7);CollisionSurfaces_[3].Points.push_back(4); CollisionSurfaces_[3].C = Color_;
	CollisionSurfaces_[4].Points.push_back(2);CollisionSurfaces_[4].Points.push_back(6);CollisionSurfaces_[4].Points.push_back(7);CollisionSurfaces_[4].Points.push_back(3); CollisionSurfaces_[4].C = Color_;
	CollisionSurfaces_[5].Points.push_back(0);CollisionSurfaces_[5].Points.push_back(4);CollisionSurfaces_[5].Points.push_back(5);CollisionSurfaces_[5].Points.push_back(1); CollisionSurfaces_[5].C = Color_;
}


bool
StaticObject::MakeConvex()			//returns true if it already was convex
{
	bool Convex = true;
	for(unsigned int Si = 0; Si < Surfaces_.size(); ++Si)		//check if convex
	{
		int PBehind = 0;
		int PInfront = 0;
		if(!CheckIfConvex(Si, PBehind, PInfront))
		{
			Convex = false;
			break;
		}
	}
	if(!Convex)
	{
		//AABB_[0].x = FLT_MAX;
		//AABB_[0].y = FLT_MAX;
		//AABB_[0].z = FLT_MAX;
		//AABB_[1].x = -FLT_MAX;
		//AABB_[1].y = -FLT_MAX;
		//AABB_[1].z = -FLT_MAX;
		//size_t MM[6];				//contact points, points of the object furthest in any direction
		//for(size_t i = 0; i < Points_.size(); ++i)
		//{
		//	Vec3f P = Points_[i].CPos;
		//	if(P.x < AABB_[0].x)
		//	{
		//		AABB_[0].x = P.x;
		//		MM[0] = i;
		//	}
		//	if(P.x > AABB_[1].x)
		//	{
		//		AABB_[1].x = P.x;
		//		MM[1] = i;
		//	}
		//	if(P.y < AABB_[0].y)
		//	{
		//		AABB_[0].y = P.y;
		//		MM[2] = i;
		//	}
		//	if(P.y > AABB_[1].y)
		//	{
		//		AABB_[1].y = P.y;
		//		MM[3] = i;
		//	}
		//	if(P.z < AABB_[0].z)
		//	{
		//		AABB_[0].z = P.z;
		//		MM[4] = i;
		//	}
		//	if(P.z > AABB_[1].z)
		//	{
		//		AABB_[1].z = P.z;
		//		MM[5] = i;
		//	}
		//}
		//if(LEN(AABB_[0]-AABB_[1]) < 10)
		if(true)
		{
			CollisionShell_ = true;					//use a different shape for collision testing
			GiftWrap();
		}
		else
		{
			//SubdivideIntoConvex();
		}
	}
	return Convex;
}
void
StaticObject::CreateLinks()
{
	float K = 0.9f;
	Link L;
	L.K = K;
	L.TFact = 2;
	if(CollisionShell_)
	{
		for(int i1 = 0; i1 < (int)CollisionPoints_.size(); ++i1)			//link every collision point with each other
		{
			for(int i2 = i1+1; i2 < (int)CollisionPoints_.size(); ++i2)
			{
				L.I1 = i1;
				L.I2 = i2;
				L.RLen = LEN(GetCollisionPointPos(i1) - GetCollisionPointPos(i2));
				CollisionLinks_.push_back(L);
			}
		}
		ShellEnd_ = (int)CollisionLinks_.size();
		for(size_t i1 = 0; i1 < CollisionPoints_.size(); ++i1)		//link every collision point with all points of the object
		{
			for(size_t i2 = 0; i2 < PPoints_.size(); ++i2)
			{
				L.I1 = (unsigned)i1;
				L.I2 = (unsigned)i2;
				L.RLen = LEN(GetCollisionPointPos((int)i1) - GetPointPos((int)i2));
				CollisionLinks_.push_back(L);
			}
		}
	}
	else
	{
		for(size_t i = 0; i < Surfaces_.size(); ++i)					//create links connecting all points within surfaces
		{
			for(size_t i1 = 0; i1 < Surfaces_[i].Points.size(); ++i1)
			{
				for(size_t i2 = i1+1; i2 < Surfaces_[i].Points.size(); ++i2)
				{
					L.I1 = Surfaces_[i].Points[i1];
					L.I2 = Surfaces_[i].Points[i2];
					L.RLen = LEN(GetPointPos(L.I1) - GetPointPos(L.I2));
					Links_.push_back(L);
				}
			}
		}
		for(size_t i1 = 0; i1 < PPoints_.size(); i1+=3)			//create links connecting some points
		{
			for(size_t i2 = i1+1; i2 < PPoints_.size(); i2+=5)
			{
				L.I1 = (unsigned)i1;
				L.I2 = (unsigned)i2;
				L.RLen = LEN(GetPointPos(L.I1) - GetPointPos(L.I2));
				Links_.push_back(L);
			}
		}
		//for(size_t i1 = 0; i1 < Points_.size(); ++i1)				//create links connecting all points
		//{
		//	for(size_t i2 = i1+1; i2 < Points_.size(); ++i2)
		//	{
		//		L.I1 = i1;
		//		L.I2 = i2;
		//		L.RLen = LEN(GetPointPos(L.I1) - GetPointPos(L.I2));
		//		Links_.push_back(L);
		//	}
		//}

		int LSize = (int)Links_.size();
		for(int i1 = 0; i1 < LSize; ++i1)				//prune doubles
		{
			for(int i2 = i1+1; i2 < LSize; ++i2)
			{
				if((Links_[i1].I1 == Links_[i2].I1) || (Links_[i1].I1 == Links_[i2].I2))
					if((Links_[i1].I2 == Links_[i2].I1) || (Links_[i1].I2 == Links_[i2].I1))
					{
						Links_.erase(Links_.begin()+i2);
						--LSize;
						--i2;
					}
			}
		}
	}
}
