
#include <vector>
#include <float.h>
#include <assert.h>
#include <algorithm>

#include "Vector/Vector.h"
#include "Object.h"
#include "GlobalDefines.h"
#include "FileIO.h"
#include "Collision/CollisionTestsBasic.h"

Object::Object(std::string Name, Vec3f Position, Vec3f Rotation, float Scale, Vec3f Color)
{
	ID_ = GlobalId++;
	Name_ = Name;
	Position_ = {0,0,0};
	Color_ = Color;
	CollisionShell_ = false;
	ShellEnd_ = 0;
	DrawOutline = true;
	DrawLinks = false;
	DrawCollisionLinks = false;
	Drawn = false;
	MaxDebugPoints = 200;
	MaxDebugLines = 200;
	MaxDebugPolygons = 200;
	
	LoadObjectMesh();

	SetScale(Scale);			//updates AABB

	SetRotation(Rotation);

	SetPosition(Position);

	UpdateSurfaceNormalsAndPos();
	UpdateSurfaceEdges();
	UpdateCollisionSurfaceNormalsAndPos();
	UpdateCollisionSurfaceEdges();
}
void
Object::LoadObjectMesh()
{
	LoadMesh(Name_, Color_, PPoints_, Surfaces_);
}

void
Object::ProjectToAxis(float& Min, float& Max, Vec3f Axis)
{
	float PosProjected;
	Min = FLT_MAX;
	Max = -FLT_MAX;
	//int2 i2 = {0};
	std::vector<PhysPoint> *CP = GetCollisionPoints();
	int SIZE = (int)CP->size();

	for(int i = 0; i < SIZE; ++i)
	{
		PhysPoint P = (*CP)[i];
		PosProjected = DOT(P.CPos, Axis);
		if(PosProjected < Min)
		{
			Min = PosProjected;
			//i2.i[0] = i;
		}
		if(PosProjected > Max)
		{
			Max = PosProjected;
			//i2.i[1] = i;
		}
	}
	//return i2;
}

int
Object::RayCollisionTest(Vec3f RayOrigin, Vec3f RayDir, float& MaxLen, bool NoAABB)		//works for concave surfaces also
{
	int Result = -1;
	float MinLen = 0;
	float OrigLen = MaxLen;
	if(NoAABB || RayAABBCollisionTest(AABB_, RayOrigin, RayDir, MinLen, MaxLen))
	{
		for(int i = 0; i < (int)Surfaces_.size(); ++i)
		{
			bool Hit = RaySurfaceCollisionTest(Surfaces_[i], *this, RayOrigin, RayDir, MaxLen);
			if(Hit)			//updates if closer hit was found
			{
				Result = i;			//hit
			}
		}
	}
	if(Result == -1)
		MaxLen=OrigLen;
	//else
	//{
	//	ClearDebugDraws();
	//	DrawDebugSurface(Result, false, 0.2f*Surfaces_[Result].C);
	//}
	return Result;
}