#include <GL/freeglut.h>
#include <iostream>
#include "Tools/Rendering.h"

void RenderString(Vec3f pos, const char *string, Vec3f Color)
{  
	glColor3f(Color.x, Color.y, Color.z); 
	glRasterPos3f(pos.x, pos.y, pos.z);
	glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_10, (const unsigned char*)string);
}

void RenderBlock(Building &Bu, Block B, bool flat)
{
	size_t NumCorners = B.CornersI.size();
	for(size_t i = 0; i < NumCorners-1; ++i)
	{
		if(flat)
		{
			Vec3f p0 = Bu.GetPoint(B.CornersI[i]);
			Vec3f p1 = Bu.GetPoint(B.CornersI[i+1]);
			glBegin(GL_LINES);
			glVertex3f(p0.x, 0, p0.z);
			glVertex3f(p1.x, 0, p1.z);
			glEnd();
		}
		else
		{
			Vec3f p0 = Bu.GetPoint(B.CornersI[i]);
			Vec3f p1 = Bu.GetPoint(B.CornersI[i+1]);
			Vec3f p2 = Bu.GetPoint(B.CornersI[i+1]) + Vec3f{0, B.CornerHeight[i+1], 0};
			Vec3f p3 = Bu.GetPoint(B.CornersI[i]) + Vec3f{0, B.CornerHeight[i], 0};
			glBegin(GL_LINE_LOOP);
			glVertex3f(p0.x, p0.y, p0.z);
			glVertex3f(p1.x, p1.y, p1.z);
			glVertex3f(p2.x, p2.y, p2.z);
			glVertex3f(p3.x, p3.y, p3.z);
			glEnd();
		}		
	}
}

void RenderObject(BuildingObject BO, bool flat)
{
	if(flat)
	{
		Vec3f p0 = {BO.Position.x-BO.Size.x/2, 0, BO.Position.z-BO.Size.z/2};
		Vec3f p1 = {BO.Position.x+BO.Size.x/2, 0, BO.Position.z-BO.Size.z/2};
		Vec3f p2 = {BO.Position.x+BO.Size.x/2, 0, BO.Position.z+BO.Size.z/2};
		Vec3f p3 = {BO.Position.x-BO.Size.x/2, 0, BO.Position.z+BO.Size.z/2};
		glBegin(GL_LINE_LOOP);
		glVertex3f(p0.x, p0.y, p0.z);
		glVertex3f(p1.x, p1.y, p1.z);
		glVertex3f(p2.x, p2.y, p2.z);
		glVertex3f(p3.x, p3.y, p3.z);
		glEnd();
		RenderString(p0, BO.Type.c_str());
	}
	else
	{
		Vec3f p0 = {BO.Position.x-BO.Size.x/2, BO.Position.y, BO.Position.z-BO.Size.z/2};
		Vec3f p1 = {BO.Position.x+BO.Size.x/2, BO.Position.y, BO.Position.z-BO.Size.z/2};
		Vec3f p2 = {BO.Position.x+BO.Size.x/2, BO.Position.y, BO.Position.z+BO.Size.z/2};
		Vec3f p3 = {BO.Position.x-BO.Size.x/2, BO.Position.y, BO.Position.z+BO.Size.z/2};
		glBegin(GL_LINE_LOOP);
		glVertex3f(p0.x, p0.y, p0.z);
		glVertex3f(p1.x, p1.y, p1.z);
		glVertex3f(p2.x, p2.y, p2.z);
		glVertex3f(p3.x, p3.y, p3.z);
		glEnd();
		glBegin(GL_LINE_LOOP);
		glVertex3f(p0.x, p0.y+BO.Size.y, p0.z);
		glVertex3f(p1.x, p1.y+BO.Size.y, p1.z);
		glVertex3f(p2.x, p2.y+BO.Size.y, p2.z);
		glVertex3f(p3.x, p3.y+BO.Size.y, p3.z);
		glEnd();
		glBegin(GL_LINES);
		glVertex3f(p0.x, p0.y,			p0.z);
		glVertex3f(p0.x, p0.y+BO.Size.y,p0.z);
		glVertex3f(p1.x, p1.y,			p1.z);
		glVertex3f(p1.x, p1.y+BO.Size.y,p1.z);
		glVertex3f(p2.x, p2.y,			p2.z);
		glVertex3f(p2.x, p2.y+BO.Size.y,p2.z);
		glVertex3f(p3.x, p3.y,			p3.z);
		glVertex3f(p3.x, p3.y+BO.Size.y,p3.z);
		glEnd();
		RenderString(p0, BO.Type.c_str());
	}
}

void RenderZone(Building &B, BuildingZone &Zone, Vec3f Col)
{
	glColor3f(Col.x, Col.y, Col.z);
	glBegin(GL_POLYGON);
	for(auto i : Zone.CornersI)
	{
		Vec3f p0 = B.GetPoint(i);
		glVertex3f(p0.x, p0.y, p0.z);
	}
	glEnd();
	glColor3f(Col.x*2, Col.y*2, Col.z*2);
	glBegin(GL_LINE_LOOP);
	Vec3f Center = {0};
	for(auto i : Zone.CornersI)
	{
		Vec3f p0 = B.GetPoint(i);
		Center += p0;
		glVertex3f(p0.x, p0.y, p0.z);
	}
	glEnd();
	RenderString(Center/Zone.CornersI.size(), Zone.Type.c_str(), Col*2);
}

void RenderBuilding(Building B)
{
	Vec3f C = {0.5f, 0.5f, 0.5f};
	for(auto Zone : B.LayoutInner_.CorridorZones)
	{
		RenderZone(B, Zone, C);
		glColor3f(C.x*2, C.y*2, C.z*2);
		for(auto Obj : Zone.Objects)
		{
			RenderObject(Obj);
		}
	}
	C = {0.5f, 0, 0.5f};
	for(auto Zone : B.LayoutInner_.StairZones)
	{
		RenderZone(B, Zone, C);
		glColor3f(C.x*2, C.y*2, C.z*2);
		for(auto Obj : Zone.Objects)
		{
			RenderObject(Obj);
		}
	}
	C = {0.5f, 0.5f, 0};
	for(auto Zone : B.LayoutInner_.RoomZones)
	{
		RenderZone(B, Zone, C);
		glColor3f(C.x*2, C.y*2, C.z*2);
		for(auto Obj : Zone.Objects)
		{
			RenderObject(Obj);
		}
	}
	glColor3f(1, 0, 0);
	RenderBlock(B, B.LayoutOuter_.Wall);
	glColor3f(0,0,1);
	for(auto Window : B.LayoutOuter_.Windows)
	{
		RenderBlock(B, Window);
	}
	glColor3f(0,1,0);
	for(auto Door : B.LayoutOuter_.Doors)
	{
		RenderBlock(B, Door);
	}

	//for(auto L : B.DebugLines_)
	//{
	//	glEnd();
	//	glBegin(GL_LINES);
	//	glColor3f(L.C.x, L.C.y, L.C.z);
	//	glVertex3f(L.S.x, L.S.y, L.S.z);
	//	glVertex3f(L.E.x, L.E.y, L.E.z);
	//	glEnd();
	//}
}
