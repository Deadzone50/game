#include <cstdlib>
#include <iostream>

#include <GL/freeglut.h>
#include "World/Building.h"
#include "Tools/Rendering.h"

std::mt19937 RandomGenerator(0);

static std::vector<Building> TestBuildings;

void reshape (int Width, int Height)
{
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height); // Set viewport to the size of the window
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glFrustum(-0.05f, 0.05f, -0.05f, 0.05f, 0.1f, 1000.0f);
	gluPerspective(70, (float)Width/Height, 0.1f, 1000.0f);
	gluLookAt(	78,150,-78,
				78,0,-78,
				0,0,-1);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
	for(auto TB : TestBuildings)
	{
		RenderBuilding(TB);
	}
    glutSwapBuffers();
}

// Keyboard callback fu1nction ( called on keyboard event handling )
void keyboard(unsigned char key, int x, int y)
{
    if (key == 'q' || key == 'Q')
        exit(EXIT_SUCCESS);
}
void skeyboard(int key, int x, int y)
{
	glMatrixMode(GL_MODELVIEW);
	float speed = 0.5f;
	if(glutGetModifiers() & GLUT_ACTIVE_SHIFT)
	{
		speed*=10;
	}
	if(key == GLUT_KEY_LEFT)
	{
		glTranslatef(speed,0,0);
	}
	else if(key == GLUT_KEY_RIGHT)
	{
		glTranslatef(-speed,0,0);
	}
	else if(key == GLUT_KEY_UP)
	{
		glTranslatef(0,0,speed);
	}
	else if(key == GLUT_KEY_DOWN)
	{
		glTranslatef(0,0,-speed);
	}
	else if(key == GLUT_KEY_PAGE_UP)
	{
		glTranslatef(0,speed,0);
	}
	else if(key == GLUT_KEY_PAGE_DOWN)
	{
		glTranslatef(0,-speed,0);
	}
}

// Main execution  function 
int main(int argc, char *argv[])
{
    glutInit(&argc, argv);      // Initialize GLUT
    glutInitDisplayMode (GLUT_DOUBLE); // Set up a basic display (double)buffer 
    //~ glutInitDisplayMode (GLUT_SINGLE); // Set up a basic display (double)buffer 
    glutInitWindowSize (800, 800);
    glutCreateWindow("Building visualizer");  // Create a window
    glutDisplayFunc(display);   // Register display callback
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard); // Register keyboard callback
    glutSpecialFunc(skeyboard); // Register special keyboard callback

	float Z = 0;
	for(int i = 0; i < 5; ++i)	//triangle
	{
		Vec3fV C = {	Vec3f{i*53.0f, 0, Z},				Vec3f{i*53.0f+50.0f, 0, Z},
			Vec3f{i*53.0f+50.0f, 0, Z - 50.0f}};
		TestBuildings.emplace_back(10, C, RandomGenerator);
	}
	Z = -53.0f;
	for(int i = 0; i < 5; ++i)	//square
	{
		Vec3fV C = {	Vec3f{i*53.0f, 0, Z},				Vec3f{i*53.0f+50.0f, 0, Z},
						Vec3f{i*53.0f+50.0f, 0, Z - 50.0f},	Vec3f{i*53.0f, 0, Z - 50.0f}};
		TestBuildings.emplace_back(10, C, RandomGenerator);
	}
	
	Z = -2*53.0f;
	for(int i = 0; i < 5; ++i)	//non-square
	{
		Vec3fV C = {	Vec3f{i*53.0f, 0, Z},				Vec3f{i*53.0f+50.0f, 0, Z},
						Vec3f{i*53.0f+50.0f, 0, Z - 50.0f},	Vec3f{i*53.0f, 0, Z - 75.0f}};
		TestBuildings.emplace_back(10, C, RandomGenerator);
	}

	Z = -3*53.0f-25.0f;
	for(int i = 0; i < 5; ++i)	//octagon
	{
		Vec3fV C = {	Vec3f{i*53.0f+10.0f, 0, Z},				Vec3f{i*53.0f+50.0f-10.0f, 0, Z},
						Vec3f{i*53.0f+50.0f, 0, Z-10.0f},		Vec3f{i*53.0f+50.0f, 0, Z+10.0f-50.0f},
						Vec3f{i*53.0f+50.0f-10.0f, 0, Z-50.0f},	Vec3f{i*53.0f+10.0f, 0, Z-50.0f},
						Vec3f{i*53.0f, 0, Z+10.0f-50.0f},		Vec3f{i*53.0f, 0, Z-10.0f}};
		TestBuildings.emplace_back(10, C, RandomGenerator);
	}

    glutMainLoop();             // Enter main event loop
    return (EXIT_SUCCESS);
}
