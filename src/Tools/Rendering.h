#pragma once

#include "Vector/Vector.h"
#include "World/Building.h"

void RenderString(Vec3f pos, const char *string, Vec3f Color = {1,1,1});

void RenderBlock(Building &Bu, Block B, bool flat = false);

void RenderObject(BuildingObject BO, bool flat = false);

void RenderZone(Building &B, BuildingZone &Zone, Vec3f Col);

void RenderBuilding(Building B);
