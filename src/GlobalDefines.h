#pragma once
#include <random>

#define DRAWDISTANCE 1000.0f
#define EPSILON 0.0001f
#define PI 3.14159265358979323846f

#define MIN(a,b) a < b ? a : b
#define MAX(a,b) a > b ? a : b

extern bool Gravity;
extern std::mt19937 GlobalRand;
extern unsigned int GlobalId;