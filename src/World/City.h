#pragma once

#include <random>
#include "Object/Object.h"
#include "Character/AI.h"
#include "World/Building.h"
#include "Containers.h"

struct Yard
{
	Vec3fV Corners;
	std::vector<int> Roads;
	int Neighbour = -1;
};

struct Road
{
	Vec3f P1, P2;
	std::vector<size_t> P2Connections;
	std::vector<size_t> P1Connections;
};

struct Intersection
{
	Vec3f Position;
	std::vector<size_t> Connections;
};

class City
{
public:
	City(Vec3f Position, float Size, int MaxHeight, int Seed);
	std::vector<Mesh*>		GetMeshes(Player &CameraPos, bool Debug = false);
	Vec3f					GetNextWaypoint(Vec3f CurrentPos, Vec3f Target)		{return NavMesh_.GetNextWaypoint(CurrentPos, Target);}

	std::vector<Road>		Roads_;
	std::vector<Yard>		Yards_;
	std::vector<Building>	Buildings_;
	std::vector<Mesh*>		Meshes_;
	Lines					Lines_;

	Mesh					IntersectionMesh_;
	Mesh					RoadMesh_;
	Mesh					SidewalkMesh_;

	NavMesh					NavMesh_;
};