#include <cassert>
#include <chrono>
#include <iostream>

#include "City.h"
#include "GlobalDefines.h"
#include "FileIO.h"
#include "GLHelperFunctions.h"

extern TextureManager TexManager;

void GetRoadAndSidewalkCornerPositions(Vertex &RoadP0,	Vertex &RoadP1,	 Vertex &RoadP2,  Vertex &RoadP3,
									   Vertex &SideLP0, Vertex &SideLP1, Vertex &SideLP2, Vertex &SideLP3,
									   Vertex &SideRP0, Vertex &SideRP1, Vertex &SideRP2, Vertex &SideRP3,
									   size_t RoadI, std::vector<Road> &Roads, float HalfRoadWidth, float SidewalkWidth)
{
	Road Road1 = Roads[RoadI];
	Vec3f Start = {Road1.P1.x, 0, Road1.P1.z};
	Vec3f End = {Road1.P2.x, 0, Road1.P2.z};
	Vec3f Road1Dir = NORMALIZE(End-Start);
	Vec3f Road1Right = CROSS(Road1Dir, {0,1,0});

	float RoadLen = LEN(End-Start);
	float RODistanceFromEnd = 0;float RIDistanceFromEnd = 0;
	float LODistanceFromEnd = 0;float LIDistanceFromEnd = 0;
	float RODistanceFromStart = 0;float RIDistanceFromStart = 0;
	float LODistanceFromStart = 0;float LIDistanceFromStart = 0;

	float Angle = 0;
	int CEND = (int)Road1.P2Connections.size();
	if(CEND > 2 || !CEND){abort();}				//Not a T intersection
	bool Corner = (CEND == 1);
	bool Straight = false;
	for(int i = 0; i < CEND; ++i)				//check all roads going to/from the road P2
	{
		Road &Road2 = Roads[Road1.P2Connections[i]];
		Vec3f Road2Dir = NORMALIZE(Road2.P2-Road2.P1);
		if(Road1.P2 == Road2.P2)						//invert road2
			Road2Dir = -Road2Dir;
		if(DOT(Road1Right, Road2Dir) > 0.1f)			//right turn
		{
			Angle = PI - acos(DOT(Road1Dir, Road2Dir));
			RODistanceFromEnd = (HalfRoadWidth+SidewalkWidth)/tan(Angle/2);
			RIDistanceFromEnd = RODistanceFromEnd-(SidewalkWidth)/tan(Angle);
		}
		else if(DOT(-Road1Right, Road2Dir) > 0.1f)		//left turn
		{
			Angle = PI - acos(DOT(Road1Dir, Road2Dir));
			LODistanceFromEnd = (HalfRoadWidth+SidewalkWidth)/tan(Angle/2);
			LIDistanceFromEnd = LODistanceFromEnd-(SidewalkWidth)/tan(Angle);
		}
		else
			Straight = true;
	}
	if(Corner)
	{
		if(RODistanceFromEnd)	//right
		{
			RIDistanceFromEnd = RODistanceFromEnd - tan(Angle/2)*SidewalkWidth;
			LIDistanceFromEnd = -RIDistanceFromEnd;
			LODistanceFromEnd = -RODistanceFromEnd;
		}
		if(LODistanceFromEnd)	//left
		{
			LIDistanceFromEnd = LODistanceFromEnd - tan(Angle/2)*SidewalkWidth;
			RIDistanceFromEnd = -LIDistanceFromEnd;
			RODistanceFromEnd = -LODistanceFromEnd;
		}
	}
	else if(Straight)
	{							//T intersection
		if(RODistanceFromEnd)	//right
		{
			RIDistanceFromEnd = RODistanceFromEnd;
			LIDistanceFromEnd = RODistanceFromEnd;
			LODistanceFromEnd = RODistanceFromEnd;
		}
		if(LODistanceFromEnd)	//left
		{
			LIDistanceFromEnd = LODistanceFromEnd;
			RIDistanceFromEnd = LODistanceFromEnd;
			RODistanceFromEnd = LODistanceFromEnd;
		}
	}

	Angle = 0;
	CEND = (int)Road1.P1Connections.size();
	if(CEND > 2 || !CEND){abort();}				//Not a T intersection
	Corner = (CEND == 1);
	Straight = false;
	for(int i = 0; i < CEND; ++i)				//check all roads going to/from the road P1
	{
		Road &Road2 = Roads[Road1.P1Connections[i]];
		Vec3f Road2Dir = NORMALIZE(Road2.P2-Road2.P1);
		if(Road1.P1 == Road2.P2)						//invert road2
			Road2Dir = -Road2Dir;
		if(DOT(Road1Right, Road2Dir) > 0.1f)			//right turn
		{
			Angle = PI - acos(DOT(-Road1Dir, Road2Dir));
			RODistanceFromStart = (HalfRoadWidth+SidewalkWidth)/tan(Angle/2);
			RIDistanceFromStart = RODistanceFromStart-(SidewalkWidth)/tan(Angle);
		}
		else if(DOT(-Road1Right, Road2Dir) > 0.1f)		//left turn
		{
			Angle = PI - acos(DOT(-Road1Dir, Road2Dir));
			LODistanceFromStart = (HalfRoadWidth+SidewalkWidth)/tan(Angle/2);
			LIDistanceFromStart = LODistanceFromStart-(SidewalkWidth)/tan(Angle);
		}
		else
			Straight = true;
	}
	if(Corner)
	{
		if(RODistanceFromStart)	//right
		{
			RIDistanceFromStart = RODistanceFromStart - tan(Angle/2)*SidewalkWidth;
			LIDistanceFromStart = -RIDistanceFromStart;
			LODistanceFromStart = -RODistanceFromStart;
		}
		if(LODistanceFromStart)	//left
		{
			LIDistanceFromStart = LODistanceFromStart - tan(Angle/2)*SidewalkWidth;
			RIDistanceFromStart = -LIDistanceFromStart;
			RODistanceFromStart = -LODistanceFromStart;
		}
	}
	else if(Straight)
	{							//T intersection
		if(RODistanceFromStart)	//right
		{
			RIDistanceFromStart = RODistanceFromStart;
			LIDistanceFromStart = RODistanceFromStart;
			LODistanceFromStart = RODistanceFromStart;
		}									
		if(LODistanceFromStart)	//left		
		{									
			LIDistanceFromStart = LODistanceFromStart;
			RIDistanceFromStart = LODistanceFromStart;
			RODistanceFromStart = LODistanceFromStart;
		}
	}

	RoadP0.Position = Start - Road1Right*HalfRoadWidth + LIDistanceFromStart*Road1Dir;
	RoadP1.Position = Start + Road1Right*HalfRoadWidth + RIDistanceFromStart*Road1Dir;
	RoadP2.Position = End + Road1Right*HalfRoadWidth   - RIDistanceFromEnd*Road1Dir;
	RoadP3.Position = End - Road1Right*HalfRoadWidth   - LIDistanceFromEnd*Road1Dir;

	SideLP0.Position = Start - Road1Right*(HalfRoadWidth + SidewalkWidth) + LODistanceFromStart*Road1Dir;
	SideLP1.Position = Start - Road1Right*(HalfRoadWidth)				  + LIDistanceFromStart*Road1Dir;
	SideLP2.Position = End - Road1Right*(HalfRoadWidth)					  - LIDistanceFromEnd*Road1Dir;
	SideLP3.Position = End - Road1Right*(HalfRoadWidth + SidewalkWidth)	  - LODistanceFromEnd*Road1Dir;

	SideRP0.Position = Start + Road1Right*(HalfRoadWidth)				  + RIDistanceFromStart*Road1Dir;
	SideRP1.Position = Start + Road1Right*(HalfRoadWidth + SidewalkWidth) + RODistanceFromStart*Road1Dir;
	SideRP2.Position = End + Road1Right*(HalfRoadWidth + SidewalkWidth)	  - RODistanceFromEnd*Road1Dir;
	SideRP3.Position = End + Road1Right*(HalfRoadWidth)					  - RIDistanceFromEnd*Road1Dir;

	RoadP3.TexCoord = {0, RoadLen-LIDistanceFromEnd};	RoadP2.TexCoord = {1, RoadLen-RIDistanceFromEnd};
	RoadP0.TexCoord = {0, LIDistanceFromStart};			RoadP1.TexCoord = {1, RIDistanceFromStart};

	SideRP3.TexCoord = {0, RoadLen-RIDistanceFromEnd};	SideRP2.TexCoord = {SidewalkWidth, RoadLen-RODistanceFromEnd};
	SideRP0.TexCoord = {0, RIDistanceFromStart};		SideRP1.TexCoord = {SidewalkWidth, RODistanceFromStart};

	SideLP3.TexCoord = {0, RoadLen-LODistanceFromEnd};	SideLP2.TexCoord = {SidewalkWidth, RoadLen-LIDistanceFromEnd};
	SideLP0.TexCoord = {0, LODistanceFromStart};		SideLP1.TexCoord = {SidewalkWidth, LIDistanceFromStart};
}
Vec3fV
SortPointsCounterClockwise(Vec3fV Points)			//TODO this only needs to reverse order?
{
	Vec3fV Result;
	Vec3fV PointsLeft = Points;
	Result.push_back(PointsLeft[0]);
	PointsLeft.erase(PointsLeft.begin());
	for(auto it = PointsLeft.begin(); it != PointsLeft.end();)
	{
		Vec3f Current = Result.back();
		Vec3f Outward = CROSS((*it - Current), Vec3f{0,1,0});
		bool Valid = true;
		for(auto it2 = Points.begin(); it2 != Points.end(); ++it2)
		{
			if(DOT(*it2 - Current, Outward) > EPSILON)
			{
				Valid = false;
				break;
			}
		}
		if(Valid)
		{
			Result.push_back(*it);
			PointsLeft.erase(it);
			it = PointsLeft.begin();
		}
		else
		{
			++it;
		}
	}
	return(Result);
}
#if 0
if(Neighbouri != -1)
{
	Building N;
	N.Corners_ = Yards[Neighbouri].Corners;
	N.Height_ = (float)(RandomGenerator() % MaxHeight) + 5;
	Result.push_back(N);

	std::vector<int> Roads1 = Yards[Yardi].Roads;
	std::vector<int> Roads2 = Yards[Neighbouri].Roads;
	size_t Road1Size = Roads1.size();
	size_t Road2Size = Roads2.size();
	Vec2f WalkWayP1 = {};
	Vec2f WalkWayP2 = {};
	for(size_t i1 = 0; i1 < Road1Size; ++i1)													//find a shared road
	{
		bool Stop = false;
		for(size_t i2 = 0; i2 < Road2Size; ++i2)
		{
			if(Roads1[i1] == Roads2[i2])
			{
				Stop = true;
				WalkWayP1 = Roads[Roads1[i1]].V1;
				WalkWayP2 = Roads[Roads1[i1]].V2;
				break;
			}
		}
		if(Stop)
			break;
	}
	Building WalkWay;
	WalkWay.Corners_.push_back(WalkWayP1);
	WalkWay.Corners_.push_back(WalkWayP2);
	WalkWay.Height_ = (float)MaxHeight + 500;
	Result.push_back(WalkWay);
}
#endif
float
CalculateArea(Vec2fV V)
{
	float Result = 0;
	size_t END = V.size();
	for(size_t i = 0; i < END; ++i)
	{
		Result += (V[i].x * V[(i + 1) % END].y) - (V[i].y * V[(i + 1) % END].x);
	}
	return(Result / 2);
}
bool
CheckIfParallel(Vec2f RoadDir, Vec2f NewRoadDir)
{
	if(abs(DOT(RoadDir, NewRoadDir)) > 1 - EPSILON)
		if(abs(DOT(RoadDir, NewRoadDir)) < 1 + EPSILON)		//TODO: remove this
			return true;
	return false;
}
bool
CheckIfParallel(Vec3f RoadDir, Vec3f NewRoadDir)
{
	if(abs(DOT(RoadDir, NewRoadDir)) > 1 - EPSILON)
		if(abs(DOT(RoadDir, NewRoadDir)) < 1 + EPSILON)
			return true;
	return false;
}
bool
CheckIfInBounds(Vec2f P, Vec2f P1, Vec2f P2)
{
	float Error = 0.5f;
	if((P.x >= P1.x - Error && P.x <= P2.x + Error) || (P.x >= P2.x - Error && P.x <= P1.x + Error))
		if((P.y >= P1.y - Error && P.y <= P2.y + Error) || (P.y >= P2.y - Error && P.y <= P1.y + Error))
			return true;
	return false;
}
bool
CheckIfInBounds(Vec3f P, Vec3f P1, Vec3f P2)
{
	float Error = 0.5f;
	if((P.x >= P1.x - Error && P.x <= P2.x + Error) || (P.x >= P2.x - Error && P.x <= P1.x + Error))
		if((P.y >= P1.y - Error && P.y <= P2.y + Error) || (P.y >= P2.y - Error && P.y <= P1.y + Error))
			if((P.z >= P1.z - Error && P.z <= P2.z + Error) || (P.z >= P2.z - Error && P.z <= P1.z + Error))
			return true;
	return false;
}
bool inline
CheckIfClose(Vec2f V1, Vec2f V2, float Dist = 1)
{
	if(V1.x < V2.x + Dist && V1.x > V2.x - Dist)
		if(V1.y < V2.y + Dist && V1.y > V2.y - Dist)
			return true;
	return false;
}
Vec3f
NewFindNextCorner(int &Roadi, size_t NumRoads, int MaxCheckedRoad, std::vector<Road> &Roads,
				  bool Right, bool &Invert, float HalfRoadWidth)
{
	Road &Road1 = Roads[Roadi];
	Vec3f Start, End;
	std::vector<size_t> P2Connections;
	if(Invert)
	{
		Start = Road1.P2;
		End = Road1.P1;
		P2Connections = Road1.P1Connections;
	}
	else
	{
		Start = Road1.P1;
		End = Road1.P2;
		P2Connections = Road1.P2Connections;
	}
	Vec3f Road1Dir = NORMALIZE(End - Start);
	Vec3f Road1Right = CROSS(Road1Dir, Vec3f{0,1,0});

	size_t END = P2Connections.size();
	if(!END)			//dead end
	{
		assert(!true);
		Roadi = -1;
		return {0,0};
	}
	float Error = 0.1f;
	for(size_t i = 0; i < END; ++i)							//find left and right turns
	{
		Road Road2 = Roads[P2Connections[i]];
		Vec3f Road2Dir = NORMALIZE(Road2.P2-Road2.P1);
		if(End == Road2.P2)							//other road inverted
		{
			Invert = true;
			Road2Dir = -Road2Dir;
		}
		else
		{
			Invert = false;
		}
		Vec3f TestDir;
		if(Right)
			TestDir = Road1Right;
		else
			TestDir = -Road1Right;

		if(DOT(TestDir, Road2Dir) > Error)				//note only works in T intersections
		{
			Roadi = (int)P2Connections[i];
			if(MaxCheckedRoad < Roadi)
			{
				float Angle = PI - acosf(DOT(Road1Dir, Road2Dir));
				float DistanceFromIntersection = HalfRoadWidth/tanf(Angle/2);

				return(End - DistanceFromIntersection * Road1Dir + HalfRoadWidth * TestDir);
			}
			else
			{
				Roadi = -1;
				return(Vec3f{0,0,0});
			}
		}
	}
	for(size_t i = 0; i < END; ++i)							//if no left/right truns, continue straigth
	{
		Road Road2 = Roads[P2Connections[i]];
		Vec3f Road2Dir = NORMALIZE(Road2.P2-Road2.P1);

		if(End == Road2.P2)							//other road inverted
		{
			Invert = true;
			Road2Dir = -Road2Dir;
		}
		else
		{
			Invert = false;
		}
		if(DOT(Road1Dir, Road2Dir) > (1 - Error))
		{
			Roadi = (int)P2Connections[i];
			if(MaxCheckedRoad < Roadi)
			{
				return NewFindNextCorner(Roadi, NumRoads, MaxCheckedRoad, Roads, Right, Invert, HalfRoadWidth);
			}
			else
			{
				Roadi = -1;
				return(Vec3f{0,0,0});
			}
		}
	}
	Roadi = -1;
	return {0,0,0};
}

City::City(Vec3f Position, float Size, int MaxHeight, int Seed)
{
	Vec3fV EdgePoints(8);											//every corner and midpoints of the sides
	EdgePoints[0] = Position + Vec3f{0,		0, Size};
	EdgePoints[1] = Position + Vec3f{Size,	0, Size};
	EdgePoints[2] = Position + Vec3f{Size,	0, 0};
	EdgePoints[3] = Position + Vec3f{Size,	0, -Size};
	EdgePoints[4] = Position + Vec3f{0,		0, -Size};
	EdgePoints[5] = Position + Vec3f{-Size, 0, -Size};
	EdgePoints[6] = Position + Vec3f{-Size, 0, 0};
	EdgePoints[7] = Position + Vec3f{-Size, 0, Size};

	Roads_.push_back(Road{EdgePoints[1], EdgePoints[3]});		//roads around city
	Roads_.push_back(Road{EdgePoints[3], EdgePoints[5]});
	Roads_.push_back(Road{EdgePoints[5], EdgePoints[7]});
	Roads_.push_back(Road{EdgePoints[7], EdgePoints[1]});

	std::mt19937 RandomGenerator(Seed);
	int PR, CR;

	PR = RandomGenerator() % 4;								//take 2 random sides
	CR = RandomGenerator() % 4;
	while(PR == CR)											//make sure they are different roads
	{
		CR = RandomGenerator() % 4;
	}
	Vec3f PRP1 = Roads_[PR].P1;								//pick 2 points along the roads
	Vec3f PRP2 = Roads_[PR].P2;
	float PRlen = LEN(PRP2 - PRP1);
	float dist = (float)(RandomGenerator() % (int)(PRlen / 2)) + PRlen / 4;
	Vec3f NRP1 = PRP1 + dist * (PRP2 - PRP1) / PRlen;

	Vec3f CRP1 = Roads_[CR].P1;
	Vec3f CRP2 = Roads_[CR].P2;
	float CRlen = LEN(CRP2 - CRP1);
	dist = (float)(RandomGenerator() % (int)(CRlen / 4)) + CRlen / 8;
	Vec3f NRP2 = CRP1 + dist * (CRP2 - CRP1) / CRlen;

	Roads_.erase(Roads_.begin() + PR);						//split old road in two
	Road OR11 = {PRP1, NRP1};
	Road OR21 = {NRP1, PRP2};

	if(CR > PR)
	{
		--CR;
	}
	Road ColRoad1 = Roads_[CR];
	Roads_.erase(Roads_.begin() + CR);						//split old road in two
	Road OR31 = {ColRoad1.P1, NRP2};
	Road OR41 = {NRP2, ColRoad1.P2};
	Roads_.push_back(OR31);
	Roads_.push_back(OR41);
	Roads_.push_back(OR11);
	Roads_.push_back(OR21);
	Roads_.push_back(Road());								//add new road
	Roads_.back().P1 = NRP1;
	Roads_.back().P2 = NRP2;

	std::cout << "Creating roads\n";
	auto start = std::chrono::high_resolution_clock::now();
	const int MaxRoads = (int)Size;								//TODO: what value should be used?
	for(int i = 1; i < MaxRoads; ++i)
	{
		PrintProgessToCout((float)i / MaxRoads);
		size_t NumberOfRoads = Roads_.size();
		int ParentRoad = RandomGenerator() % NumberOfRoads;

		Vec3f ParentRoadP1 = Roads_[ParentRoad].P1;
		Vec3f ParentRoadP2 = Roads_[ParentRoad].P2;
		Vec3f ParentRoadDir = ParentRoadP2 - ParentRoadP1;
		float Len = LEN(ParentRoadDir);
		if(Len < 200)															//skip if chosen road is short
			continue;
		ParentRoadDir = ParentRoadDir / Len;

		float DistanceAlongRoad = (float)((RandomGenerator() % (int)(Len - 150)) + 75);	//not in first or last 75 meters
		//Vec2f NewRoadDir = {ParentRoadDir.y, -ParentRoadDir.x};						//rotated 90 clockwise
		Vec3f NewRoadDir = CROSS(ParentRoadDir, Vec3f{0,1,0});
		if(RandomGenerator() % 2)
			NewRoadDir = -NewRoadDir;
		Vec3f NewRoadP1 = ParentRoadP1 + DistanceAlongRoad * ParentRoadDir;

		if(	(NewRoadP1.x + NewRoadDir.x < Position.x - Size) || (NewRoadP1.x + NewRoadDir.x > Position.x + Size)
		 || (NewRoadP1.y + NewRoadDir.y < Position.y - Size) || (NewRoadP1.y + NewRoadDir.y > Position.y + Size)
		 || (NewRoadP1.z + NewRoadDir.z < Position.z - Size) || (NewRoadP1.z + NewRoadDir.z > Position.z + Size)
		   )
			NewRoadDir = -NewRoadDir;

		float T = FLT_MAX;
		float t0;
		int CollisionRoad = -1;
		for(int i2 = 0; i2 < NumberOfRoads; ++i2)
		{
			if(i2 == ParentRoad)			//skip parent road
				continue;

			Vec3f RoadP1 = Roads_[i2].P1;
			Vec3f RoadP2 = Roads_[i2].P2;
			Vec3f RoadDir = NORMALIZE(RoadP2 - RoadP1);
			if(CheckIfParallel(RoadDir, NewRoadDir))
				continue;

			Vec3f PDiff = NewRoadP1 - RoadP1;
			float Upper = (PDiff.x*RoadDir.z - PDiff.z*RoadDir.x);
			float Lower = (RoadDir.x*NewRoadDir.z - RoadDir.z*NewRoadDir.x);
			t0 = Upper / Lower;


			if(t0 > 0 && t0 < T)
			{
				Vec3f P = NewRoadP1 + t0 * NewRoadDir;
				if(CheckIfInBounds(P, RoadP1, RoadP2))
				{
					T = t0;
					CollisionRoad = i2;
				}
			}
		}
		if(T < 5 || T == FLT_MAX)
			continue;		//if the new road is too short, or no collision road found
		Vec3f NewRoadP2 = NewRoadP1 + T * NewRoadDir;

		Road OR1 = {ParentRoadP1, NewRoadP1};
		Road OR2 = {NewRoadP1, ParentRoadP2};
		Road OR3, OR4;
		if(CollisionRoad != -1)
		{
			Road ColRoad = Roads_[CollisionRoad];
			OR3 = {ColRoad.P1, NewRoadP2};
			OR4 = {NewRoadP2, ColRoad.P2};
			if(LEN(OR3.P2 - OR3.P1) < 5 || LEN(OR4.P2 - OR4.P1) < 5)
			{
				continue;
			}

		}
		Roads_.erase(Roads_.begin() + ParentRoad);			//split old road in two
		Roads_.push_back(OR1);
		Roads_.push_back(OR2);

		if(CollisionRoad != -1)
		{
			if(CollisionRoad > ParentRoad)
			{
				--CollisionRoad;
			}
			Roads_.erase(Roads_.begin() + CollisionRoad);
			Roads_.push_back(OR3);
			Roads_.push_back(OR4);
		}
		Roads_.push_back(Road());
		Roads_.back().P1 = NewRoadP1;
		Roads_.back().P2 = NewRoadP2;
	}
	const size_t NumRoads = Roads_.size();
	const float HalfRoadWidth = 3.5f;								//TODO: fix values
	const float SidewalkWidth = 1.5f;

	RoadMesh_.TextureId_ = TexManager.AddTexture("Assets/Textures/Road1.png");
	SidewalkMesh_.TextureId_ = TexManager.AddTexture("Assets/Textures/Sidewalk.png");

	for(size_t I1 = 0; I1 < NumRoads; ++I1)					//create road/sidewalk mesh
	{
		Road &Road1 = Roads_[I1];
		for(size_t i = I1+1; i < NumRoads; ++i)				//find all roads connected to this road
		{
			Road &Road2 = Roads_[i];
			if(Road1.P2 == Road2.P1)
			{
				Road1.P2Connections.push_back(i);
				Road2.P1Connections.push_back(I1);
			}
			else if(Road1.P2 == Road2.P2)
			{
				Road1.P2Connections.push_back(i);
				Road2.P2Connections.push_back(I1);
			}
			else if(Road1.P1 == Road2.P1)
			{
				Road1.P1Connections.push_back(i);
				Road2.P1Connections.push_back(I1);
			}
			else if(Road1.P1 == Road2.P2)
			{
				Road1.P1Connections.push_back(i);
				Road2.P2Connections.push_back(I1);
			}
		}

		Vertex RoadP0, RoadP1, RoadP2, RoadP3;
		Vertex SideLP0, SideLP1, SideLP2, SideLP3;
		Vertex SideRP0, SideRP1, SideRP2, SideRP3;

		GetRoadAndSidewalkCornerPositions(RoadP0, RoadP1, RoadP2, RoadP3,				//	p3-p2
										  SideLP0, SideLP1, SideLP2, SideLP3,			//	| /|
										  SideRP0, SideRP1, SideRP2, SideRP3,			//	|/ |
										  I1, Roads_, HalfRoadWidth, SidewalkWidth);	//	p0-p1

		RoadMesh_.AddQuad(RoadP0, RoadP1, RoadP2, RoadP3);
		SidewalkMesh_.AddQuad(SideLP0, SideLP1, SideLP2, SideLP3);
		SidewalkMesh_.AddQuad(SideRP0, SideRP1, SideRP2, SideRP3);

		NavMesh_.AddQuad(SideLP0.Position, SideRP1.Position, SideRP2.Position, SideLP3.Position);
	}
	Meshes_.push_back(&SidewalkMesh_);
	Meshes_.push_back(&RoadMesh_);
	Meshes_.push_back(&IntersectionMesh_);

	std::vector<Intersection> Intersections;
	for(size_t i = 0; i < NumRoads; ++i)					//create intersections
	{
		const Road &Road1 = Roads_[i];
		if(Road1.P1Connections.size() > 1)
		{
			std::vector<size_t> Tmp = Road1.P1Connections;
			Tmp.push_back(i);
			std::sort(Tmp.begin(), Tmp.end());		//sort so it can be compared
			bool Found = false;
			for(auto it = Intersections.begin(); it != Intersections.end(); ++it)	//check if it is allready in the list
			{
				if(it->Connections == Tmp)
				{
					Found = true;
					break;
				}
			}
			if(!Found)
			{
				Intersections.push_back({Road1.P1, Tmp});
			}
		}
		if(Road1.P2Connections.size() > 1)
		{
			std::vector<size_t> Tmp = Road1.P2Connections;
			Tmp.push_back(i);
			std::sort(Tmp.begin(), Tmp.end());
			bool Found = false;
			for(auto it = Intersections.begin(); it != Intersections.end(); ++it)	//check if it is allready in the list
			{
				if(it->Connections == Tmp)
				{
					Found = true;
					break;
				}
			}
			if(!Found)
			{
				Intersections.push_back({Road1.P2, Tmp});
			}
		}
	}

	IntersectionMesh_.TextureId_ = TexManager.AddTexture("Assets/Textures/Road1Cross.png");
	for(auto it = Intersections.begin(); it != Intersections.end(); ++it)
	{
		Vec3f IntersectionPos = it->Position;
		assert(it->Connections.size() == 3);
		Road IntersectionRoads[3];
		Vec3f RoadDirs[3];
		for(int i = 0; i < 3; ++i)
		{
			IntersectionRoads[i] = Roads_[it->Connections[i]];
			if(IntersectionRoads[i].P1 == IntersectionPos)			//all roads go to intersection 
			{
				Vec3f Tmp = IntersectionRoads[i].P1;
				IntersectionRoads[i].P1 = IntersectionRoads[i].P2;
				IntersectionRoads[i].P2 = Tmp;
			}
			RoadDirs[i] = NORMALIZE(IntersectionRoads[i].P2 - IntersectionRoads[i].P1);
		}
		Vertex LB, RB, RT, LT;
		LT.TexCoord = {0,1}; RT.TexCoord = {1,1};
		LB.TexCoord = {0,0}; RB.TexCoord = {1,0};
		Vec3f Dir;
		float Angle1;
		float Angle2;
		float DistanceBefore;
		float DistanceAfter;
		if(DOT(RoadDirs[0], -RoadDirs[1]) > 0.9f)					//check if straight
		{
			Dir = RoadDirs[0];
			Angle1 = acos(DOT(RoadDirs[0], RoadDirs[2]));
			Angle2 = PI - Angle1;

		}
		else if(DOT(RoadDirs[0], -RoadDirs[2]) > 0.9f)				//check if straight
		{
			Dir = RoadDirs[0];
			Angle1 = acos(DOT(RoadDirs[0], RoadDirs[1]));
			Angle2 = PI - Angle1;
		}
		else if(DOT(RoadDirs[1], -RoadDirs[2]) > 0.9f)				//check if straight
		{
			Dir = RoadDirs[1];
			Angle1 = acos(DOT(RoadDirs[1], RoadDirs[0]));
			Angle2 = PI - Angle1;
		}
		else
		{
			abort();			//not T intersection
		}
		DistanceBefore = (HalfRoadWidth+SidewalkWidth)/tan(Angle1/2);
		//DistanceBefore = 1;
		DistanceAfter = (HalfRoadWidth+SidewalkWidth)/tan(Angle2/2);
		//DistanceAfter = 1;
		Vec3f Right = CROSS(Dir, {0,1,0});

		LT.Position =  IntersectionPos + DistanceAfter*Dir  - Right*(HalfRoadWidth+SidewalkWidth);
		RT.Position =  IntersectionPos + DistanceAfter*Dir  + Right*(HalfRoadWidth+SidewalkWidth);
		LB.Position =  IntersectionPos - DistanceBefore*Dir - Right*(HalfRoadWidth+SidewalkWidth);
		RB.Position =  IntersectionPos - DistanceBefore*Dir + Right*(HalfRoadWidth+SidewalkWidth);

		IntersectionMesh_.AddQuad(LB, RB, RT, LT);
	}

	auto end = std::chrono::high_resolution_clock::now();
	std::cout << "Time: " << std::chrono::duration<float>(end - start).count()*1000 << " ms\n";
	int MaxCheckedRoad = -1;
	float TotalHalfWidth = HalfRoadWidth + SidewalkWidth;
	std::cout << "Creating yards\n";
	start = std::chrono::high_resolution_clock::now();
	for(size_t I1 = 0; I1 < NumRoads; ++I1)					//find intersections, TODO: use the intersections for this
	{
		PrintProgessToCout((float)I1 / NumRoads);
		int i1R = (int)I1;
		int i1L = (int)I1;
		bool R = true;
		bool L = true;
		Yard LeftYard;
		Yard RightYard;
		//std::vector<int> RYardRoads;
		//std::vector<int> LYardRoads;

		bool Invert = false;
		Vec3f P0 = NewFindNextCorner(i1R, NumRoads, MaxCheckedRoad, Roads_, true, Invert, TotalHalfWidth);
		if(i1R < 0)
			R = false;
		else
			RightYard.Corners.push_back(P0);
		while(R)
		{
			Vec3f Px = NewFindNextCorner(i1R, NumRoads, MaxCheckedRoad, Roads_, true, Invert, TotalHalfWidth);
			if(Px == P0)
			{
				//RightYard.Roads = RYardRoads;
				break;
			}
			if(i1R < 0)
				R = false;
			else
				RightYard.Corners.push_back(Px);
		}

		Invert = false;
		P0 = NewFindNextCorner(i1L, NumRoads, MaxCheckedRoad, Roads_, false, Invert, TotalHalfWidth);
		if(i1L < 0)
			L = false;
		else
			LeftYard.Corners.push_back(P0);
		while(L)
		{
			Vec3f Px = NewFindNextCorner(i1L, NumRoads, MaxCheckedRoad, Roads_, false, Invert, TotalHalfWidth);
			if(Px == P0)
			{
				//LeftYard.Roads = LYardRoads;
				break;
			}
			if(i1L < 0)
				L = false;
			else
				LeftYard.Corners.push_back(Px);
		}
		if(R)
		{
			RightYard.Corners = SortPointsCounterClockwise(RightYard.Corners);
			if(L)
				RightYard.Neighbour = (int)Yards_.size() + 1;
			Yards_.push_back(RightYard);
		}
		if(L)
		{
			LeftYard.Corners = SortPointsCounterClockwise(LeftYard.Corners);
			if(R)
				LeftYard.Neighbour = (int)Yards_.size() - 1;
			Yards_.push_back(LeftYard);
		}
		++MaxCheckedRoad;
	}
	end = std::chrono::high_resolution_clock::now();
	std::cout << "Time: " << std::chrono::duration<float>(end - start).count()*1000 << " ms\n";

	int NumYards = (int)Yards_.size();
	std::cout << "Creating buildings\n";
	start = std::chrono::high_resolution_clock::now();
	Buildings_.reserve(NumYards);
	for(int i = 0; i < NumYards; ++i)
	{
		PrintProgessToCout((float)i / NumYards);
		Buildings_.emplace_back(MaxHeight, Yards_[i].Corners, RandomGenerator);
	}
	auto END = Buildings_.end();
	for(auto it = Buildings_.begin(); it != END; ++it)
	{
		std::vector<Mesh*> M1 = it->GetMeshes();
		Meshes_.insert(Meshes_.end(), M1.begin(), M1.end());
	}

	end = std::chrono::high_resolution_clock::now();
	std::cout << "Time: " << std::chrono::duration<float>(end - start).count()*1000 << " ms\n";
	std::cout << "All done\n";
}

std::vector<Mesh*>
City::GetMeshes(Player &CameraPos, bool Debug)
{
	//std::vector<Mesh*> M;
	//auto END = Buildings_.end();
	//for(auto it = Buildings_.begin(); it != END; ++it)
	//{
	//	std::vector<Mesh*> M1 = it->GetMeshes();
	//	M.insert(M.end(), M1.begin(), M1.end());
	//}
	//return M;
	(void)Debug;
	(void)CameraPos;
	return Meshes_;
}