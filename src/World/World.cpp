#include <random>			//rand
#include <map>
#include <cmath>
#include <cstring>
#include <algorithm>
#include <iostream>

#include "GlobalDefines.h"
#include "World.h"
#include "FileIO.h"

float CombinedNoise[ZONESIZE][ZONESIZE];
float Noise1[ZONESIZE][ZONESIZE];
float Noise2[ZONESIZE][ZONESIZE];
float Noise3[ZONESIZE][ZONESIZE];
float Noise4[ZONESIZE][ZONESIZE];

float BilinearInterpolation(float x, float y)
{
	int x1 = (int)x;
	int x2 = x1 + 1;
	int y1 = (int)y;
	int y2 = y1 + 1;
	float XRatio = x - x1;
	float YRatio = y - y1;

	float ValueX1 = (1-YRatio)*Noise1[y1][x1] + (YRatio)*Noise1[y2][x1];
	float ValueX2 = (1-YRatio)*Noise1[y1][x2] + (YRatio)*Noise1[y2][x2];
	float Value = (1-XRatio)*ValueX1 + (XRatio)*ValueX2;

	return Value;
}

void Generate2DNoise(int Size, int Octaves, int Ratio, int Seed)
{			
	std::mt19937 RandomGenerator(Seed);		//always same
	memset(CombinedNoise, 0, sizeof CombinedNoise);
	int Divider = 0;
	for(int i = 0; i < Octaves; ++i)
	{
		Divider += (int)pow(2,i);
	}

	for(int y = 0; y < Size; ++y)
	{
		for(int x = 0; x < Size; ++x)
		{
			Noise1[y][x] = ((float)(RandomGenerator())/(float)RandomGenerator.max());
			//Noise1[y][x] = 0.1f;
			CombinedNoise[y][x] += Noise1[y][x]/Divider;
		}
	}
	for(int i = 1; i < Octaves; ++i)
	{
		for(int y = 0; y < Size; ++y)
		{
			for(int x = 0; x < Size; ++x)
			{
				int Div = (int)pow(Ratio,i)*2;
				int Multiplier = (int)pow(Ratio,i);
				CombinedNoise[y][x] += BilinearInterpolation((float)x/Div, (float)y/Div)*Multiplier/Divider;
			}
		}
	}

}

inline static void
MakeTriangleFans(int x, int y, std::vector<Vec3i> &Triangles_)
{
	if(((x%2) && (y%2)))
	{
		if((x == ZONESIZE-1))												//create tirangle fans
		{
			if(y != ZONESIZE-1)
			{
				int M = (y)*ZONESIZE + (x);
				int A = (y-1)*ZONESIZE + (x);
				int B = (y-1)*ZONESIZE + (x-1);
				int C = (y)*ZONESIZE + (x-1);
				int D = (y+1)*ZONESIZE + (x-1);
				int E = (y+1)*ZONESIZE + (x);

				Triangles_.push_back(Vec3i{M, A, B});
				Triangles_.push_back(Vec3i{M, B, C});
				Triangles_.push_back(Vec3i{M, C, D});
				Triangles_.push_back(Vec3i{M, D, E});
			}
			else
			{
				int M = y*ZONESIZE + (x);
				int A = (y-1)*ZONESIZE + (x);
				int B = (y-1)*ZONESIZE + (x-1);
				int C = (y)*ZONESIZE + (x-1);

				Triangles_.push_back(Vec3i{M, A, B});
				Triangles_.push_back(Vec3i{M, B, C});
			}
		}
		else if(y == ZONESIZE-1)
		{
			int M = (y)*ZONESIZE + (x);
			int A = (y)*ZONESIZE + (x+1);
			int B = (y-1)*ZONESIZE + (x+1);
			int C = (y-1)*ZONESIZE + (x);
			int D = (y-1)*ZONESIZE + (x-1);
			int E = (y)*ZONESIZE + (x-1);

			Triangles_.push_back(Vec3i{M, A, B});
			Triangles_.push_back(Vec3i{M, B, C});
			Triangles_.push_back(Vec3i{M, C, D});
			Triangles_.push_back(Vec3i{M, D, E});
		}
		else
		{
			int M = (y)*ZONESIZE + (x);
			int A = (y-1)*ZONESIZE + (x-1);
			int B = (y)*ZONESIZE + (x-1);
			int C = (y+1)*ZONESIZE + (x-1);
			int D = (y+1)*ZONESIZE + (x);
			int E = (y+1)*ZONESIZE + (x+1);
			int F = (y)*ZONESIZE + (x+1);
			int G = (y-1)*ZONESIZE + (x+1);
			int H = (y-1)*ZONESIZE + (x);

			Triangles_.push_back(Vec3i{M, A, B});
			Triangles_.push_back(Vec3i{M, B, C});
			Triangles_.push_back(Vec3i{M, C, D});
			Triangles_.push_back(Vec3i{M, D, E});
			Triangles_.push_back(Vec3i{M, E, F});
			Triangles_.push_back(Vec3i{M, F, G});
			Triangles_.push_back(Vec3i{M, G, H});
			Triangles_.push_back(Vec3i{M, H, A});
		}
	}
}

//Zone::Zone(int Seed)
//{
//	Generate2DNoise(ZONESIZE, 4, 3, Seed);
//	DebugPoints_.resize(ZONESIZE*ZONESIZE);
//	for(int y = 0; y < ZONESIZE; ++y)
//	{
//		for(int x = 0; x < ZONESIZE; ++x)
//		{
//			DebugPoints_[y*ZONESIZE + x] = Point{{(float)x, 16*CombinedNoise[y][x],(float)y}, {0,1,0}};
//		}
//	}
//
//	for(int y = 0; y < ZONESIZE; ++y)
//	{
//		for(int x = 0; x < ZONESIZE; ++x)
//		{
//			//MakeTriangleFans(x, y, Points_, Triangles_);
//			MakeTriangleFans(x, y, Triangles2_);
//		}
//	}
//	ReduceGeometryFans();
//	Lines_.reserve(Triangles2_.size()*3);								//TODO: reimplement
//	Triangles_.reserve(Triangles2_.size());
//	Vec3f Col = {0,0.5f,0};
//	for(auto it = Triangles2_.begin(); it != Triangles2_.end(); ++it)
//	{
//		Vec3f P1 = DebugPoints_[it->x].P;
//		Vec3f P2 = DebugPoints_[it->y].P;
//		Vec3f P3 = DebugPoints_[it->z].P;
//
//		Lines_.push_back({P1, P2, {1,1,1}});
//		Lines_.push_back({P2, P3, {1,1,1}});
//		Lines_.push_back({P3, P1, {1,1,1}});
//		Triangles_.push_back({P1,P2,P3,Col});
//
//		Vec3f A = P2 - P1;
//		Vec3f B = P3 - P2;
//		Vec3f N = CROSS(A, B);
//		DebugLines_.push_back({P1, P1+N, {1,1,1}});
//	}
//}
static inline Vec3i
MergeTriangleFan(Vec3i T1, Vec3i T2, Points &Points_)
{
	Vec3i T;
	T.P1 = T1.P1;
	T.P2 = T1.P2;
	T.P3 = T2.P3;

	Points_[T2.P2].P.y = (Points_[T1.P2].P.y + Points_[T2.P3].P.y)/2;

	return T;
}
static inline Vec3i
MergeTriangleFanBig(Vec3i T1, Vec3i T2, Points &Points_)
{
	Vec3i T;
	T.P1 = T1.P2;
	T.P2 = T1.P3;
	T.P3 = T2.P3;

	Points_[T1.P1].P.y = (Points_[T1.P2].P.y + Points_[T2.P3].P.y)/2;

	return T;
}
static bool
PlanarTest(Vec3i t1, Vec3i t2, float Deg, Points &Points_)
{
	Triangle T1 = {Points_[t1.P1].P, Points_[t1.P2].P, Points_[t1.P3].P};
	Triangle T2 = {Points_[t2.P1].P, Points_[t2.P2].P, Points_[t2.P3].P};

	float TestAngleRad = Deg *(3.14f/180);		//degrees to radians
	Vec3f N1 = CROSS(T1.P2 - T1.P1, T1.P3 - T1.P2);
	Vec3f N2 = CROSS(T2.P2 - T2.P1, T2.P3 - T2.P2);
	float Cos = DOT(N1, N2)/(LEN(N1)*LEN(N2));
	float Angle = acosf(Cos);
	if(Angle < TestAngleRad)
		return true;
	else
		return false;
}
static std::vector<int>
Decimate8(int i, float Deg, std::vector<Vec3i> &Triangles2_, Points &Points_)
{
	int T0 = i;
	int T1 = i + 1;
	int T2 = i + 2;
	int T3 = i + 3;
	int T4 = i + 4;
	int T5 = i + 5;
	int T6 = i + 6;
	int T7 = i + 7;
	bool Q1 = false;
	bool Q2 = false;
	bool Q3 = false;
	bool Q4 = false;

	std::vector<int> Result;

	if(PlanarTest(Triangles2_[T0], Triangles2_[T1], Deg, Points_))
	{
		Triangles2_[T0] = MergeTriangleFan(Triangles2_[T0], Triangles2_[T1], Points_);
		Q1 = true;
		Result.push_back(T1);
	}
	if(PlanarTest(Triangles2_[T2], Triangles2_[T3], Deg, Points_))
	{
		Triangles2_[T2] = MergeTriangleFan(Triangles2_[T2], Triangles2_[T3], Points_);
		Q2 = true;
		Result.push_back(T3);
	}
	if(PlanarTest(Triangles2_[T4], Triangles2_[T5], Deg, Points_))
	{
		Triangles2_[T4] = MergeTriangleFan(Triangles2_[T4], Triangles2_[T5], Points_);
		Q3 = true;
		Result.push_back(T5);
	}
	if(PlanarTest(Triangles2_[T6], Triangles2_[T7], Deg, Points_))
	{
		Triangles2_[T6] = MergeTriangleFan(Triangles2_[T6], Triangles2_[T7], Points_);
		Q4 = true;
		Result.push_back(T7);
	}

	if(Q1 && Q2)
	{
		if(PlanarTest(Triangles2_[T0], Triangles2_[T2], Deg/2, Points_))
		{
			Triangles2_[T0] = MergeTriangleFanBig(Triangles2_[T0], Triangles2_[T2], Points_);
			Result.push_back(T2);
			Q1 = false;
			Q2 = false;
		}
	}
	if(Q1 && Q4)
	{
		if(PlanarTest(Triangles2_[T0], Triangles2_[T6], Deg/2, Points_))
		{
			Triangles2_[T0] = MergeTriangleFanBig(Triangles2_[T6], Triangles2_[T0], Points_);
			Result.push_back(T6);
			Q1 = false;
			Q4 = false;
		}
	}
	if(Q2 && Q3)
	{
		if(PlanarTest(Triangles2_[T2], Triangles2_[T4], Deg/2, Points_))
		{
			Triangles2_[T2] = MergeTriangleFanBig(Triangles2_[T2], Triangles2_[T4], Points_);
			Result.push_back(T4);
			Q2 = false;
			Q3 = false;
		}
	}
	if(Q3 && Q4)
	{
		if(PlanarTest(Triangles2_[T4], Triangles2_[T6], Deg/2, Points_))
		{
			Triangles2_[T4] = MergeTriangleFanBig(Triangles2_[T4], Triangles2_[T6], Points_);
			Result.push_back(T6);
			Q3 = false;
			Q4 = false;
		}
	}
	std::sort(Result.begin(), Result.end());
	return(Result);
}

//void
//Zone::ReduceGeometryFans()							//TODO: reimplement
//{
	//Triangles_.clear();
	//Lines_.clear();

	//float Deg = 20;

	//std::vector<int> TrianglesToRemove[(ZONESIZE-1)/2][(ZONESIZE-1)/2];
	//int i = 0;
	//for(int y = 0; y < (ZONESIZE-1)/2; ++y)
	//{
	//	for(int x = 0; x < (ZONESIZE-1)/2; ++x)
	//	{
	//		std::vector<int> TTR = Decimate8(i, Deg, Triangles2_, DebugPoints_);
	//		TrianglesToRemove[y][x].insert(TrianglesToRemove[y][x].end(), TTR.begin(), TTR.end());
	//		i += 8;
	//	}
	//}

	////***DELETE OLD TRIANGLES
	//std::cout << "Deleting triangles\n";
	//float ZS2 = ((ZONESIZE-1)*(ZONESIZE-1))/2;
	//int TrigD = 0;
	//for(int y = 0; y < (ZONESIZE-1)/2; ++y)
	//{
	//	for(int x = 0; x < (ZONESIZE-1)/2; ++x)
	//	{
	//		PrintProgessToCout((y*ZONESIZE+x)/ZS2);
	//		for(auto it = TrianglesToRemove[y][x].begin(); it != TrianglesToRemove[y][x].end(); ++it)
	//		{
	//			int T = (*it)-TrigD;
	//			Triangles2_.erase(Triangles2_.begin() + T);
	//			++TrigD;
	//		}
	//	}
	//}
//}
World::World(int Seed, int MaxBuildingHeight, int CitySize, bool Z)
{
	//if(Z)
	//	Zones_.push_back(Zone(Seed));
	//else
		Cities_.emplace_back(Vec3f{0,0,0}, (float)CitySize, MaxBuildingHeight, Seed);
}
void
World::Clear()
{
	Zones_.clear();
	Cities_.clear();
}
std::vector<Mesh*>
World::GetMeshes(Player CameraPos)
{
	std::vector<Mesh*> M;
	for(auto it = Cities_.begin(); it != Cities_.end(); ++it)
	{
		std::vector<Mesh*> M1 = it->GetMeshes(CameraPos);
		M.insert(M.end(), M1.begin(), M1.end());
	}
	return M;
}
Vec3f
World::GetNextWaypoint(Vec3f CurrentPos, Vec3f Target)
{
	return Cities_.back().GetNextWaypoint(CurrentPos, Target);
}