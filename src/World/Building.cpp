#include <iostream>
#include <map>
#include "World/Building.h"
#include "Vector/Vector.h"
#include "Collision/CollisionTestsBasic.h"
#include "Debug/Debug.h"

uint32_t Building::AddPoint(Vec3f V)
{
	uint32_t NumPoints = Points_.size();
	for(uint32_t i = 0; i < NumPoints; ++i)
	{
		if(	(Points_[i].x-EPSILON < V.x && Points_[i].x+EPSILON > V.x) &&
			(Points_[i].y-EPSILON < V.y && Points_[i].y+EPSILON > V.y) &&
			(Points_[i].z-EPSILON < V.z && Points_[i].z+EPSILON > V.z))
		{
			return i;
		}
	}
	Points_.push_back(V);
	return Points_.size()-1;
}

uint32_t GetNextPointIndex(std::vector<uint32_t> &Indices, uint32_t index)
{
	size_t NumIndexes = Indices.size();
	for(size_t i = 0; i < NumIndexes;++i)
	{
		if(Indices[i] == index)
		{
			return Indices[(i+1)%NumIndexes];
		}
	}
	ASSERT(false);
	return 0;
}

uint32_t GetPrevPointIndex(std::vector<uint32_t> &Indices, uint32_t index)
{
	size_t NumIndexes = Indices.size();
	for(size_t i = 0; i < NumIndexes;++i)
	{
		if(Indices[i] == index)
		{
			if(i == 0)
				return Indices.back();
			else
				return Indices[i-1];
		}
	}
	ASSERT(false);
	return 0;
}

bool PointOnLine(Vec3f Point, Vec3f Start, Vec3f End)
{
	//float d = DOT((Point-Start), (End-Start)/(LEN(End-Start)))/(LEN(End-Start);
	float d = DOT((Point-Start), (End-Start))/DOT((End-Start), (End-Start));
	if((d < -EPSILON) || (1.0f+EPSILON < d))
	{
		return false;
	}
	Vec3f P = Start + d*(End-Start);
	return (LEN(Point-P) < EPSILON);
}

std::vector<Vec2i> Building::IntersectWallAndZone(Block &Wall, BuildingZone &Zone)
{
	size_t NumWallC = Wall.CornersI.size();
	size_t NumZoneC = Zone.CornersI.size();
	std::vector<Vec2i> IntersectLines;
	for(uint32_t i1 = 0; i1 < NumWallC; ++i1)
	{
		Vec2i Line;
		bool Found = false;
		for(uint32_t i2 = 0; i2 < NumZoneC; ++i2)
		{//if both are on the same wall
			if(	PointOnLine(GetPoint(Zone.CornersI[i2]), GetPoint(Wall.CornersI[i1]), GetPoint(Wall.CornersI[(i1+1)%NumWallC])) &&
				PointOnLine(GetPoint(Zone.CornersI[(i2+1)%NumZoneC]), GetPoint(Wall.CornersI[i1]), GetPoint(Wall.CornersI[(i1+1)%NumWallC])))
			{
				Line.x = Zone.CornersI[i2];
				Line.y = Zone.CornersI[(i2+1)%NumZoneC];
				Found = true;
			}
		}
		if(Found)
		{
			IntersectLines.push_back(Line);
		}
	}
	return IntersectLines;
}

std::vector<uint32_t> IntersectZoneAndZone(BuildingZone &Zone1, BuildingZone &Zone2)
{
	std::vector<uint32_t> IntersectIndices;
	for(auto i1 : Zone1.CornersI)
	{
		for(auto i2 : Zone2.CornersI)
		{
			if(i1 == i2)
			{
				IntersectIndices.push_back(i1);
			}
		}
	}
	return IntersectIndices;
}

void Building::PlaceOuterWalls(int MaxHeight, Vec3fV Corners, std::mt19937 &RandomGenerator)
{
	Block Wall;
	Wall.Type = wall;
	Wall.Thickness = 0.2f;
	for (uint32_t i = 0; i < Corners.size(); ++i)
	{
		Wall.CornersI.push_back(i);
	}
	Wall.CornersI.push_back(0);
	Points_ = Corners;
	float H = (float)(RandomGenerator() % MaxHeight + 4);
	Wall.CornerHeight.resize(Wall.CornersI.size(), H);
	LayoutOuter_.Wall = Wall;
	LayoutInner_.MainZone.CornersI = Wall.CornersI;
}

void Building::CreateRoom(uint32_t StartI, uint32_t EndI, BuildingZone &Room)
{
	uint32_t NumPoints = LayoutInner_.MainZone.CornersI.size()-1;
	Vec3f StartP = GetPoint(StartI);
	Vec3f EndP = GetPoint(EndI);
	Vec3f RoomDir = CROSS(EndP - StartP, {0,1,0});
	Room.CornersI.push_back(StartI);
	uint32_t NextI = GetNextPointIndex(LayoutInner_.MainZone.CornersI, StartI);
	while(NextI != EndI)
	{
		Vec3f P = GetPoint(NextI);
		if(DOT(P-StartP, RoomDir) > 0)
		{
			Room.CornersI.push_back(NextI);
		}
		NextI = GetNextPointIndex(LayoutInner_.MainZone.CornersI, NextI);
	}
	Room.CornersI.push_back(EndI);
}

void InsertIndex(std::vector<uint32_t> &Indices, uint32_t AfterWhere, uint32_t Index)
{
	Indices.insert(Indices.begin()+AfterWhere+1, Index);
	if(Index == Indices.back())	//if it is the last one
		Indices.front() = Index;
	else if(Index == Indices.front())//if it is the first one
		Indices.back() = Index;
}

void Building::CreateCorridor(uint32_t P0I, uint32_t P1I, uint32_t &P2I, uint32_t &P3I, Vec3f Dir, BuildingZone &Corridor)
{
	uint32_t NumPoints = LayoutInner_.MainZone.CornersI.size()-1;
	Vec3f P0 = GetPoint(P0I);
	Vec3f P1 = GetPoint(P1I);
	float t0 = FLT_MAX;
	float t1 = FLT_MAX;
	uint32_t CollisionI0 = 0;
	uint32_t CollisionI1 = 0;
	for(uint32_t i = 0; i < NumPoints; ++i)
	{
		if(LayoutInner_.MainZone.CornersI[i] == P0I || LayoutInner_.MainZone.CornersI[i] == P1I)
			continue;

		Vec3f WallP[2] = {GetPoint(LayoutInner_.MainZone.CornersI[i]), GetPoint(LayoutInner_.MainZone.CornersI[i+1])};
		Vec3f WallDir = {WallP[1] - WallP[0]};
		Vec3f PlaneP = WallP[0];
		Vec3f PlaneN = NORMALIZE(CROSS(WallDir, {0,1,0}));

		float Current = t0;
		if(RayPlaneCollisionTest(PlaneN, PlaneP, P0, Dir, Current))
		{
			if((Current > 0.1f) && PointOnLine(P0 + Current*Dir, WallP[0], WallP[1]))
			{
				t0 = Current;
				CollisionI0 = i;
			}
		}
		Current = t1;
		if(RayPlaneCollisionTest(PlaneN, PlaneP, P1, Dir, Current))
		{
			if((Current > 0.1f) && PointOnLine(P1 + Current*Dir, WallP[0], WallP[1]))
			{
				t1 = Current;
				CollisionI1 = i;
			}
		}
	}
	ASSERT(t0 < FLT_MAX);
	ASSERT(t1 < FLT_MAX);
	P2I = AddPoint(P1 +t1*Dir);
	if(Points_.size()-1 == P2I)
	{
		InsertIndex(LayoutInner_.MainZone.CornersI, CollisionI1, P2I);
	}
	P3I = AddPoint(P0 +t0*Dir);
	if(Points_.size()-1 == P3I)
	{
		InsertIndex(LayoutInner_.MainZone.CornersI, CollisionI0, P3I);
	}
	Corridor.CornersI.push_back(P0I);
	Corridor.CornersI.push_back(P1I);
	Corridor.CornersI.push_back(P2I);
	Corridor.CornersI.push_back(P3I);
}

void Building::PlaceCorridors(std::mt19937 &RandomGenerator)
{
	uint32_t NumCorners = (uint32_t)LayoutInner_.MainZone.CornersI.size();
	uint32_t WallStartIndex = RandomGenerator() % (NumCorners-1);
	uint32_t Place = RandomGenerator()%2;
	float Width = 3.0f;
	uint32_t WallI[2] = {	LayoutInner_.MainZone.CornersI[WallStartIndex], 
							LayoutInner_.MainZone.CornersI[WallStartIndex+1]};

	Vec3f WallP[2] = {GetPoint(WallI[0]), GetPoint(WallI[1])};
	BuildingZone Corridor;
	Corridor.Type = "Corridor";
	Corridor.ID = ZoneID_++;
	Vec3f WallDir = (WallP[1] - WallP[0])/LEN(WallP[1] - WallP[0]);
	BuildingZone Rooms;
	Rooms.Type = "Rooms";
	Rooms.ID = ZoneID_++;
	Rooms.LeadsTo.push_back(Corridor.ID);
	Corridor.LeadsTo.push_back(Rooms.ID);
	if(Place == 0)	//along
	{
		Vec3f NextWall = {GetPoint(GetNextPointIndex(LayoutInner_.MainZone.CornersI, WallI[1])) - WallP[1]};
		Vec3f P1 = WallP[1] + Width * NextWall/LEN(NextWall);
		//Vec3f WidthVector = Width * CROSS(WallDir, {0, -1, 0});
		//Vec3f P1 = WallP[1] + DOT(NextWall, WidthVector)/LEN(NextWall) * NextWall/LEN(NextWall);	//project at wall

		uint32_t P0I = WallI[1];
		uint32_t P1I = AddPoint(P1);

		InsertIndex(LayoutInner_.MainZone.CornersI, WallStartIndex+1, P1I);

		uint32_t P2I, P3I;
		CreateCorridor(P0I, P1I , P2I, P3I, -WallDir, Corridor);
		CreateRoom(P1I, P2I, Rooms);
	}
	else //middle
	{
		uint32_t P0I = AddPoint((WallP[0]+WallP[1])/2-Width/2*WallDir);
		uint32_t P1I = AddPoint((WallP[0]+WallP[1])/2+Width/2*WallDir);

		InsertIndex(LayoutInner_.MainZone.CornersI, WallStartIndex, P0I);
		InsertIndex(LayoutInner_.MainZone.CornersI, WallStartIndex+1, P1I);

		uint32_t P2I, P3I;
		Vec3f Dir = CROSS(WallDir, {0,-1,0});
		CreateCorridor(P0I, P1I , P2I, P3I, Dir, Corridor);
		CreateRoom(P3I, P0I, Rooms);	//"left" rooms
		LayoutInner_.RoomZones.push_back(Rooms);

		Rooms.CornersI.clear();			//"right" rooms
		Rooms.ID = ZoneID_++;
		Corridor.LeadsTo.push_back(Rooms.ID);
		CreateRoom(P1I, P2I, Rooms);

	}
	LayoutInner_.CorridorZones.push_back(Corridor);
	LayoutInner_.RoomZones.push_back(Rooms);
}

void Building::PlaceOuterDoor(std::mt19937 &RandomGenerator)
{
	std::vector<Vec2i> Possible = IntersectWallAndZone(LayoutOuter_.Wall, LayoutInner_.CorridorZones[0]);
	size_t NumPossibilities = Possible.size();
	for(int i = 0; i < NumPossibilities; ++i)
	{
		DrawDebugLine(GetPoint(Possible[i].x), GetPoint(Possible[i].y), {1,0,1});
	}
	Block Door;
	Door.Type = door;
	Door.Thickness = 0.1f;
	
	int WallIndex = RandomGenerator() % NumPossibilities;
	float t = (float)(RandomGenerator() % 101) / 100.0f;
	float DoorWidth = 1.0f;
	Vec3f WallStart = GetPoint(Possible[WallIndex].x);
	Vec3f WallEnd = GetPoint(Possible[WallIndex].y);
	Vec3f WallVector = WallEnd - WallStart;
	Vec3f DoorStart = WallStart + WallVector*t;
	if(LEN(DoorStart - WallStart) < 1.0f)
	{
		DoorStart = WallStart +1.0f*WallVector/LEN(WallVector);
	}
	Vec3f DoorEnd = DoorStart +DoorWidth*WallVector/LEN(WallVector);
	if(LEN(DoorEnd - WallStart) + 1 > LEN(WallVector))
	{
		DoorStart = WallEnd -(DoorWidth+1.0f)*WallVector/LEN(WallVector);
		DoorEnd = DoorStart +DoorWidth*WallVector/LEN(WallVector);
	}
	Door.CornersI = {AddPoint(DoorStart), AddPoint(DoorEnd)};
	float H = 2.0f;
	Door.CornerHeight.resize(2, H);
	LayoutOuter_.Doors.push_back(Door);
}

void Building::PlaceStairs(std::mt19937 &RandomGenerator)
{
	//end of corridor
	ASSERT((LayoutInner_.CorridorZones[0].CornersI.size() == 4));
	BuildingZone Stairs;
	Stairs.Type = "Stairs";
	Stairs.ID = ZoneID_++;
	uint32_t r = RandomGenerator() % 4;
	uint32_t i0 = LayoutInner_.CorridorZones[0].CornersI[r];
	uint32_t i1 = LayoutInner_.CorridorZones[0].CornersI[(r+1)%4];
	uint32_t i2 = LayoutInner_.CorridorZones[0].CornersI[(r+2)%4];
	uint32_t i3 = LayoutInner_.CorridorZones[0].CornersI[(r+3)%4];

	Vec3f p0 = GetPoint(i0);
	Vec3f p1 = GetPoint(i1);
	Vec3f p2 = GetPoint(i2);
	Vec3f p3 = GetPoint(i3);

	if(LEN(p0 - p1) < LEN(p0 - p3))
	{
		Vec3f CorridorDir = (p2 - p1)/LEN(p2 - p1);
		uint32_t New1 = AddPoint(p1 + 3.0f*CorridorDir);
		uint32_t New2 = AddPoint(p0 + 3.0f*CorridorDir);
		Stairs.CornersI.push_back(i0);
		Stairs.CornersI.push_back(i1);
		Stairs.CornersI.push_back(New1);
		Stairs.CornersI.push_back(New2);
		//p0 += 3.0f*CorridorDir;
		//p1 += 3.0f*CorridorDir;
		LayoutInner_.CorridorZones[0].CornersI[r] = New2;
		LayoutInner_.CorridorZones[0].CornersI[(r+1)%4] = New1;
	}
	else
	{
		Vec3f CorridorDir = (p1 - p0)/LEN(p1 - p0);
		uint32_t New1 = AddPoint(p0 + 3.0f*CorridorDir);
		uint32_t New2 = AddPoint(p3 + 3.0f*CorridorDir);
		Stairs.CornersI.push_back(i3);
		Stairs.CornersI.push_back(i0);
		Stairs.CornersI.push_back(New1);
		Stairs.CornersI.push_back(New2);
		//p0 += 3.0f*CorridorDir;
		//p3 += 3.0f*CorridorDir;
		LayoutInner_.CorridorZones[0].CornersI[r] = New1;
		LayoutInner_.CorridorZones[0].CornersI[(r+3)%4] = New2;
	}
	LayoutInner_.StairZones.push_back(Stairs);
}

float ZoneArea(Vec3fV &Corners)
{
	float Result = 0;
	size_t NumCorners = Corners.size();
	for(int i = 0; i < NumCorners; ++i)
	{
		Result += Corners[i].x*Corners[(i+1)%NumCorners].z - Corners[i].z*Corners[(i+1)%NumCorners].x;
	}
	return fabsf(Result)/2;
}

bool CutLine(Vec3f &Start, Vec3f &End, Vec3f PlanePoint, Vec3f PlaneNormal)	//moves points under plane to plane
{
	float t = LEN(End-Start);
	Vec3f LineDir = (End-Start)/t;
	if(RayPlaneCollisionTest(PlaneNormal, PlanePoint-EPSILON*PlaneNormal, Start+EPSILON*LineDir, LineDir, t))
	{
		if(DOT(LineDir, PlaneNormal) > 0)
		{
			Start = Start+(EPSILON+t)*LineDir;
		}
		else
		{
			End = Start+(EPSILON+t)*LineDir;
		}
		return true;
	}
	return false;
}

void Building::CutBlock(Block &B, Vec3f Point, Vec3f Normal)
{
	size_t NumCorners = B.CornersI.size();
	ASSERT((NumCorners == 5));
	std::map<uint32_t, Vec3f> New;
	for(size_t i = 0; i < NumCorners-1; ++i)
	{
		Vec3f Start = GetPoint(B.CornersI[i]);
		Vec3f End = GetPoint(B.CornersI[(i+1)]);
		if(CutLine(Start, End, Point, Normal))
		{
			New[B.CornersI[i]] = Start;
			New[B.CornersI[i+1]] = End;
		}
	}
	for(size_t i = 0; i < NumCorners; ++i)
	{
		Vec3f &P = GetPoint(B.CornersI[i]);
		P = New[B.CornersI[i]];
	}
}

void Building::CutBuildingZone(BuildingZone &BZ, Vec3f Point, Vec3f Normal)
{
	size_t NumCorners = BZ.CornersI.size();
	ASSERT((NumCorners == 4));
	std::map<uint32_t, Vec3f> New;
	for(size_t i = 0; i < NumCorners; ++i)
	{
		Vec3f Start = GetPoint(BZ.CornersI[i]);
		Vec3f End = GetPoint(BZ.CornersI[(i+1)%NumCorners]);
		if(CutLine(Start, End, Point, Normal))
		{
			New[BZ.CornersI[i]] = Start;
			New[BZ.CornersI[(i+1)%NumCorners]] = End;
		}
	}
	for(size_t i = 0; i < NumCorners; ++i)
	{
		Vec3f &P = GetPoint(BZ.CornersI[i]);
		P = New[BZ.CornersI[i]];
	}
}

void Building::CutBuilding(Vec3f Point, Vec3f Normal)	//cuts everything under plane
{
	//ASSERT((Normal == Vec3f{1,0,0} || Normal == Vec3f{-1,0,0} ||
	//		Normal == Vec3f{0,0,1} || Normal == Vec3f{0,0,-1}));
	//CutBlock(LayoutOuter_.Wall, Point, Normal);
	//for(auto BZ : LayoutInner_.RoomZones)
	//{
	//	CutBuildingZone(BZ, Point, Normal);
	//}
	//for(auto BZ : LayoutInner_.CorridorZones)
	//{
	//	CutBuildingZone(BZ, Point, Normal);
	//}

	for(auto &P : Points_)
	{
		Vec3f PointToPlane = Point-P;
		float t = DOT(PointToPlane, Normal);
		if( t > 0)	//under plane
		{
			P += t*Normal;
		}
	}
}

BuildingZone& Building::GetZone(uint32_t ID)
{
	for(BuildingZone& Z : LayoutInner_.CorridorZones)
	{
		if(Z.ID == ID)
		{
			return Z;
		}
	}
	for(BuildingZone& Z : LayoutInner_.RoomZones)
	{
		if(Z.ID == ID)
		{
			return Z;
		}
	}
	for(BuildingZone& Z : LayoutInner_.StairZones)
	{
		if(Z.ID == ID)
		{
			return Z;
		}
	}
}

Vec3f GetCorridorDir(Vec3fV &Corners)
{
	if(LEN(Corners[0] - Corners[1]) < LEN(Corners[0] - Corners[3]))
	{
		return (Corners[3]-Corners[0])/LEN(Corners[3]-Corners[0]);
	}
	else
	{
		return (Corners[1]-Corners[0])/LEN(Corners[1]-Corners[0]);
	}
}

void Building::PlaceRooms(std::mt19937 &RandomGenerator)
{
	std::vector<BuildingZone> NewRooms;
	for(auto BZ : LayoutInner_.RoomZones)
	{
		ASSERT((BZ.CornersI.size() == 4));
		Vec3fV Corners = {	GetPoint(BZ.CornersI[0]),
						GetPoint(BZ.CornersI[1]),
						GetPoint(BZ.CornersI[2]),
						GetPoint(BZ.CornersI[3])};
		float Area = ZoneArea(Corners);
		uint32_t NumApartments = (uint32_t)Area/50;
		BuildingZone &Corridor = GetZone(BZ.LeadsTo[0]);

		std::vector<uint32_t> IntersectPoints = IntersectZoneAndZone(BZ, Corridor);
		uint32_t LeftTopI;
		Vec3f RightTopP;
		if(GetNextPointIndex(BZ.CornersI, IntersectPoints[0]) == IntersectPoints[1])
		{
			LeftTopI = IntersectPoints[1];
			RightTopP = GetPoint(IntersectPoints[0]);
		}
		else
		{
			LeftTopI = IntersectPoints[0];
			RightTopP = GetPoint(IntersectPoints[1]);
		}
		Vec3f LeftTopP = GetPoint(LeftTopI);
		uint32_t LeftBottomI = GetNextPointIndex(BZ.CornersI, LeftTopI);
		Vec3f LeftBottomP = GetPoint(LeftBottomI);

		Vec3f CorridorDirPerApartment = (RightTopP-LeftTopP)/NumApartments;
		
		if(LEN(CorridorDirPerApartment) < 3)
		{
			NumApartments = (uint32_t)LEN(RightTopP-LeftTopP)/3;
			CorridorDirPerApartment = (RightTopP-LeftTopP)/NumApartments;
		}
		if(LEN(LeftBottomP-LeftTopP) > 50/3)
		{
			CutBuilding(LeftTopP+50/3*(LeftBottomP-LeftTopP)/LEN(LeftBottomP-LeftTopP), (LeftTopP-LeftBottomP)/LEN(LeftBottomP-LeftTopP));
			LeftBottomP = GetPoint(LeftBottomI);
		}

		for(uint32_t i = 0; i < NumApartments; ++i)
		{
			BuildingZone Room;
			Room.Type = "Room";
			Room.ID = ZoneID_++;

			Room.CornersI.push_back(LeftTopI);
			Room.CornersI.push_back(LeftBottomI);
			LeftTopI = AddPoint(LeftTopP+CorridorDirPerApartment*(i+1));
			LeftBottomI = AddPoint(LeftBottomP+CorridorDirPerApartment*(i+1));
			Room.CornersI.push_back(LeftBottomI);
			Room.CornersI.push_back(LeftTopI);
			NewRooms.push_back(Room);
		}
	}
	LayoutInner_.RoomZones = NewRooms;
}

void Building::PlaceWindows(std::mt19937 &RandomGenerator)
{
	Block Window;
	Window.Type = window;
	Window.Thickness = 0.05f;
	float H = 2;
	Window.CornerHeight.resize(2, H);
	std::map<uint32_t, float> WindowWidths;
	for(auto BZ : LayoutInner_.RoomZones)
	{
		if(BZ.Type == "Room")
		{
			std::vector<Vec2i> Possible = IntersectWallAndZone(LayoutOuter_.Wall, BZ);
			for(int i = 0; i < Possible.size(); ++i)
			{
				Vec3f Start = GetPoint(Possible[i].x);
				Vec3f End = GetPoint(Possible[i].y);
				float Len = LEN(End-Start);
				if(Len <= 2)
				{
					continue;	//dont add window
				}
				Vec3f Dir = (End-Start)/Len;
				Start += Dir;	//remove 1m from ends
				End -= Dir;

				float r = 1; //RandomGenerator()%101/100.0f;
				float g = 0; //RandomGenerator()%101/100.0f;
				float b = 1; //RandomGenerator()%101/100.0f;
				DrawDebugLine(Start+Vec3f{0,0.01f,0}, End+Vec3f{0,0.01f,0}, {r,g,b});

				if(WindowWidths.find((uint32_t)round(Len)) == WindowWidths.end())
				{
					WindowWidths[(uint32_t)round(Len)] = ((RandomGenerator()%101)/100.0f)*(Len - 2.1f)+0.1f;
				}
				float W = WindowWidths[(uint32_t)round(Len)];
				Window.CornersI = {AddPoint((Start+End)/2-W/2*Dir),AddPoint((Start+End)/2+W/2*Dir)};
				LayoutOuter_.Windows.push_back(Window);
			}
		}
	}

}

Building::Building(int MaxHeight, Vec3fV& Corners, std::mt19937& RandomGenerator)
{
	LayoutInner_.MainZone.ID = ZoneID_++;
	LayoutInner_.MainZone.Type = "Main";
	PlaceOuterWalls(MaxHeight, Corners, RandomGenerator);	
	PlaceCorridors(RandomGenerator);
	//PlaceRooms(RandomGenerator);
	PlaceStairs(RandomGenerator);
	PlaceOuterDoor(RandomGenerator);
	//PlaceWindows(RandomGenerator);

	GenerateMesh(RandomGenerator);
}
