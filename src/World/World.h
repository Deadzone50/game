#pragma once
#include <vector>
#include "Vector/Vector.h"
#include "Character/Character.h"
#include "Debug/Debug.h"
#include "Containers.h"
#include "World/City.h"

#define ZONESIZE 61

class Zone : public DebugDraws
{
public:
	Zone(int Seed);

	//void			ReduceGeometryGrid();
	void			ReduceGeometryFans();

	//float Terrain_[ZONESIZE][ZONESIZE];
	//std::vector<NPC> NPCs_;
	//std::vector<Item> Items_;

	std::vector<Vec3i>	Triangles2_;
};

class World
{
public:
	World() {}
	World(int Seed, int MaxBuildingHeight, int CitySize, bool Z);
	void				Clear();
	std::vector<Mesh*>	GetMeshes(Player CameraPos);
	Vec3f				GetNextWaypoint(Vec3f CurrentPos, Vec3f Target);

	std::vector<Zone>	Zones_;
	std::vector<City>	Cities_;
};

