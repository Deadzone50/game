#pragma once

#include "Debug/Debug.h"
#include "Object/Object.h"
#include "Containers.h"

#define NUMWALLS 4
#define NUMWINDOWS 3

enum BlockType
{
	wall, window, door
};

struct Block
{
	float					Thickness;
	std::vector<uint32_t>	CornersI;
	std::vector<float>		CornerHeight;
	unsigned int			TextureId;
	BlockType				Type;
};

struct BuildingObject
{
	std::string		Type;
	Vec3f			Size;		//width, height, depth
	Vec3f			Position;	//center of base
	float			Rotation;	//radians
};

struct BuildingZone
{
	uint32_t ID;
	std::string	Type;
	std::vector<uint32_t>		CornersI;
	std::vector<BuildingObject> Objects;
	std::vector<Block>			Walls;
	std::vector<Block>			Doors;
	std::vector<uint32_t>		LeadsTo;
};

struct BuildingLayoutOuter
{
	//BuildingObjects
	Block				Wall;
	std::vector<Block>	Doors;
	std::vector<Block>	Windows;
	//Stairs
};

struct BuildingLayoutInner
{
	BuildingZone				MainZone;
	std::vector<BuildingZone>	CorridorZones;
	std::vector<BuildingZone>	StairZones;
	std::vector<BuildingZone>	RoomZones;
	//Floors
		//Rooms/Zones
			//BuildingObjects
			//Walls
			//Doors
			//Windows
			//Stairs
};

class Building : public DebugDraws
{
public:
	Building(int MaxHeight, Vec3fV& Corners, std::mt19937& RandomGenerator);
	//std::vector<Mesh*>	GetMeshes();
	//void				SetupMesh() {Mesh_.SetupMesh();}

	std::vector<Vec2i> IntersectWallAndZone(Block &Wall, BuildingZone &Zone);
	void CutBuilding(Vec3f Point, Vec3f Normal);
	void CutBuildingZone(BuildingZone &BZ, Vec3f Point, Vec3f Normal);
	void CutBlock(Block &B, Vec3f Point, Vec3f Normal);

	void PlaceOuterWalls(int MaxHeight, Vec3fV Corners, std::mt19937 &RandomGenerator);
	void PlaceCorridors(std::mt19937 &RandomGenerator);
	void PlaceOuterDoor(std::mt19937 &RandomGenerator);
	void PlaceStairs(std::mt19937 &RandomGenerator);
	void PlaceRooms(std::mt19937 &RandomGenerator);
	void PlaceWindows(std::mt19937 &RandomGenerator);

	void CreateCorridor(uint32_t P0I, uint32_t P1I, uint32_t &P2I, uint32_t &P3I, Vec3f Dir, BuildingZone &Corridor);
	void CreateRoom(uint32_t StartI, uint32_t EndI, BuildingZone &Room);

	BuildingZone& GetZone(uint32_t ID);

	///Mesh Functions
	void					GenerateMesh(std::mt19937& RandomGenerator);
	std::vector<Mesh*>		GetMeshes();

//private:
	float					Height_;
	//Vec3fV				Corners_;
	//std::vector<uint32_t>	CornersI_;
	BuildingLayoutOuter		LayoutOuter_;
	BuildingLayoutInner		LayoutInner_;

	Vec3f&					GetPoint(uint32_t i)	{return Points_[i];}
	uint32_t				AddPoint(Vec3f V);
	Vec3fV					Points_;

	uint32_t				ZoneID_ = 0;

	Mesh					WallMesh_, WindowMesh_, FloorMesh_;
	std::vector<Mesh*>		Meshes_;
};
