#include "World/building.h"
#include "GLHelperFunctions.h"

extern TextureManager TexManager;

//~ CreateOuterWall(Vec3f WallStart, Vec3f WallEnd, float Bottom, Vec2f WallSize, Vec2f WindowSize, int NumWindows, int NumFloors, Vec3f Normal, Mesh& WallMesh, Mesh& WindowMesh)
//~ {
	//~ Vec2f WindowDistance;
	//~ Vec3f WallDir = NORMALIZE(WallEnd - WallStart);

	//~ float FloorHeight = WallSize.y / NumFloors;

	//~ WindowDistance.y = FloorHeight - WindowSize.y;
	//~ WindowDistance.x = (WallSize.x - NumWindows * WindowSize.x) / NumWindows;
	//~ WindowMesh.Reserve(NumWindows * NumFloors);

	//~ std::vector<unsigned int> SharedIndices;



	//~ if (NumFloors < NumWindows)
	//~ {
		//~ WallMesh.Reserve(2 * NumFloors + NumWindows * NumFloors + 1);
		//~ {																//Horizontal pieces
			//~ {
				//~ float B = Bottom;
				//~ float T = B + WindowDistance.y / 2;
				//~ LB.Position = { WallStart.x, B, WallStart.z };			//first floor under window
				//~ LT.Position = { WallStart.x, T, WallStart.z };
				//~ RT.Position = { WallEnd.x, T, WallEnd.z };
				//~ RB.Position = { WallEnd.x, B, WallEnd.z };

				//~ LB.TexCoord = { 0,B };
				//~ LT.TexCoord = { 0,T };
				//~ RT.TexCoord = { WallSize.x,T };
				//~ RB.TexCoord = { WallSize.x,B };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);

				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI + 2);

				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 3);
				//~ WallMesh.Indices_.push_back(FirstI);

				//~ SharedIndices.push_back(FirstI);
				//~ SharedIndices.push_back(FirstI + 1);
				//~ SharedIndices.push_back(FirstI + 2);
				//~ SharedIndices.push_back(FirstI + 3);
			//~ }
			//~ for (int i = 1; i < NumFloors; ++i)
			//~ {
				//~ float B = Bottom + WindowDistance.y / 2 + WindowSize.y + (i - 1) * FloorHeight;
				//~ float T = B + WindowDistance.y;
				//~ LB.Position = { WallStart.x, B, WallStart.z };			//between windows
				//~ LT.Position = { WallStart.x, T, WallStart.z };
				//~ RT.Position = { WallEnd.x, T, WallEnd.z };
				//~ RB.Position = { WallEnd.x, B, WallEnd.z };

				//~ LB.TexCoord = { 0,B };
				//~ LT.TexCoord = { 0,T };
				//~ RT.TexCoord = { WallSize.x,T };
				//~ RB.TexCoord = { WallSize.x,B };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);

				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI + 2);

				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 3);
				//~ WallMesh.Indices_.push_back(FirstI);

				//~ SharedIndices.push_back(FirstI);
				//~ SharedIndices.push_back(FirstI + 1);
				//~ SharedIndices.push_back(FirstI + 2);
				//~ SharedIndices.push_back(FirstI + 3);
			//~ }
			//~ {
				//~ float B = Bottom + WindowDistance.y / 2 + WindowSize.y + (NumFloors - 1) * FloorHeight;
				//~ float T = B + WindowDistance.y / 2;
				//~ LB.Position = { WallStart.x, B, WallStart.z };			//last floor over window
				//~ LT.Position = { WallStart.x, T, WallStart.z };
				//~ RT.Position = { WallEnd.x, T, WallEnd.z };
				//~ RB.Position = { WallEnd.x, B, WallEnd.z };

				//~ LB.TexCoord = { 0,B };
				//~ LT.TexCoord = { 0,T };
				//~ RT.TexCoord = { WallSize.x,T };
				//~ RB.TexCoord = { WallSize.x,B };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);

				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI + 2);

				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 3);
				//~ WallMesh.Indices_.push_back(FirstI);

				//~ SharedIndices.push_back(FirstI);
				//~ SharedIndices.push_back(FirstI + 1);
				//~ SharedIndices.push_back(FirstI + 2);
				//~ SharedIndices.push_back(FirstI + 3);
			//~ }
		//~ }
		//~ for (int i1 = 0; i1 < NumFloors; ++i1)								//vertical pieces
		//~ {
			//~ float B = Bottom + FloorHeight * i1 + WindowDistance.y / 2;
			//~ float T = B + WindowSize.y;
			//~ {																//left of first window
				//~ float S = 0;
				//~ Vec3f Start = WallStart;
				//~ float W = WindowDistance.x / 2;
				//~ Vec3f End = Start + W * WallDir;

				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };
				//~ RT.TexCoord = { S + W,T };
				//~ RB.TexCoord = { S + W,B };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ unsigned int Shared1 = SharedIndices[i1 * 4 + 3];
				//~ unsigned int Shared2 = SharedIndices[(i1 + 1) * 4];
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Indices_.push_back(Shared1);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);

				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(Shared2);
				//~ WallMesh.Indices_.push_back(Shared1);

				//~ Start = End;
				//~ W = WindowSize.x;
				//~ End = Start + W * WallDir;
				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };

				//~ LB.TexCoord = { 0,0 };
				//~ RB.TexCoord = { 1,0 };
				//~ RT.TexCoord = { 1,1 };
				//~ LT.TexCoord = { 0,1 };

				//~ FirstI = (unsigned int)WindowMesh.Vertices_.size();
				//~ WindowMesh.Vertices_.push_back(LB);
				//~ WindowMesh.Vertices_.push_back(RB);
				//~ WindowMesh.Vertices_.push_back(RT);
				//~ WindowMesh.Vertices_.push_back(LT);

				//~ WindowMesh.Indices_.push_back(FirstI);
				//~ WindowMesh.Indices_.push_back(FirstI + 1);
				//~ WindowMesh.Indices_.push_back(FirstI + 2);

				//~ WindowMesh.Indices_.push_back(FirstI + 2);
				//~ WindowMesh.Indices_.push_back(FirstI + 3);
				//~ WindowMesh.Indices_.push_back(FirstI);

			//~ }
			//~ for (int i = 1; i < NumWindows; ++i)								//between windows
			//~ {
				//~ float S = WindowDistance.x / 2 + WindowSize.x + (i - 1) * (WindowDistance.x + WindowSize.x);
				//~ Vec3f Start = WallStart + S * WallDir;
				//~ float W = WindowDistance.x;
				//~ Vec3f End = Start + W * WallDir;

				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };

				//~ LB.TexCoord = { S,B };
				//~ LT.TexCoord = { S,T };
				//~ RT.TexCoord = { S + W,T };
				//~ RB.TexCoord = { S + W,B };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 3);
				//~ WallMesh.Indices_.push_back(FirstI);

				//~ Start = End;
				//~ W = WindowSize.x;
				//~ End = Start + W * WallDir;
				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };

				//~ LB.TexCoord = { 0,0 };
				//~ RB.TexCoord = { 1,0 };
				//~ RT.TexCoord = { 1,1 };
				//~ LT.TexCoord = { 0,1 };

				//~ FirstI = (unsigned int)WindowMesh.Vertices_.size();
				//~ WindowMesh.Vertices_.push_back(LB);
				//~ WindowMesh.Vertices_.push_back(RB);
				//~ WindowMesh.Vertices_.push_back(RT);
				//~ WindowMesh.Vertices_.push_back(LT);

				//~ WindowMesh.Indices_.push_back(FirstI);
				//~ WindowMesh.Indices_.push_back(FirstI + 1);
				//~ WindowMesh.Indices_.push_back(FirstI + 2);

				//~ WindowMesh.Indices_.push_back(FirstI + 2);
				//~ WindowMesh.Indices_.push_back(FirstI + 3);
				//~ WindowMesh.Indices_.push_back(FirstI);
			//~ }
			//~ {																//right of last window
				//~ float S = WindowDistance.x / 2 + WindowSize.x + (NumWindows - 1) * (WindowDistance.x + WindowSize.x);
				//~ Vec3f Start = WallStart + S * WallDir;
				//~ float W = WindowDistance.x / 2;
				//~ Vec3f End = Start + W * WallDir;

				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ LB.TexCoord = { S,B };
				//~ LT.TexCoord = { S,T };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ unsigned int Shared1 = SharedIndices[i1 * 4 + 2];
				//~ unsigned int Shared2 = SharedIndices[(i1 + 1) * 4 + 1];
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(LT);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(Shared1);
				//~ WallMesh.Indices_.push_back(Shared2);

				//~ WallMesh.Indices_.push_back(Shared2);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI);
			//~ }
		//~ }
	//~ }
	//~ else																		//more floors than windows
	//~ {																			//Vertical pieces
		//~ WallMesh.Reserve(2 * NumWindows + NumWindows * NumFloors + 1);
		//~ {
			//~ float B = Bottom;
			//~ float T = B + WallSize.y;
			//~ {																	//left of first window
				//~ float S = 0;
				//~ Vec3f Start = WallStart;
				//~ float W = WindowDistance.x / 2;
				//~ Vec3f End = Start + W * WallDir;

				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };
				//~ LB.TexCoord = { S,B };
				//~ LT.TexCoord = { S,T };
				//~ RT.TexCoord = { S + W,T };
				//~ RB.TexCoord = { S + W,B };
				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 3);
				//~ WallMesh.Indices_.push_back(FirstI);

				//~ SharedIndices.push_back(FirstI);
				//~ SharedIndices.push_back(FirstI + 1);
				//~ SharedIndices.push_back(FirstI + 2);
				//~ SharedIndices.push_back(FirstI + 3);
			//~ }
			//~ for (int i = 1; i < NumWindows; ++i)								//between windows
			//~ {
				//~ float S = WindowDistance.x / 2 + WindowSize.x + (i - 1) * (WindowDistance.x + WindowSize.x);
				//~ Vec3f Start = WallStart + S * WallDir;
				//~ float W = WindowDistance.x;
				//~ Vec3f End = Start + W * WallDir;

				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };
				//~ LB.TexCoord = { S,B };
				//~ LT.TexCoord = { S,T };
				//~ RT.TexCoord = { S + W,T };
				//~ RB.TexCoord = { S + W,B };
				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 3);
				//~ WallMesh.Indices_.push_back(FirstI);

				//~ SharedIndices.push_back(FirstI);
				//~ SharedIndices.push_back(FirstI + 1);
				//~ SharedIndices.push_back(FirstI + 2);
				//~ SharedIndices.push_back(FirstI + 3);
			//~ }
			//~ {																//right of last window
				//~ float S = WindowDistance.x / 2 + WindowSize.x + (NumWindows - 1) * (WindowDistance.x + WindowSize.x);
				//~ Vec3f Start = WallStart + S * WallDir;
				//~ float W = WindowDistance.x / 2;
				//~ Vec3f End = Start + W * WallDir;

				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };
				//~ LB.TexCoord = { S,B };
				//~ LT.TexCoord = { S,T };
				//~ RT.TexCoord = { S + W,T };
				//~ RB.TexCoord = { S + W,B };
				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 3);
				//~ WallMesh.Indices_.push_back(FirstI);

				//~ SharedIndices.push_back(FirstI);
				//~ SharedIndices.push_back(FirstI + 1);
				//~ SharedIndices.push_back(FirstI + 2);
				//~ SharedIndices.push_back(FirstI + 3);
			//~ }
		//~ }
		//~ for (int i1 = 0; i1 < NumWindows; ++i1)							//Horizontal pieces
		//~ {
			//~ float S = WindowDistance.x / 2 + i1 * (WindowDistance.x + WindowSize.x);
			//~ Vec3f Start = WallStart + S * WallDir;
			//~ float W = WindowSize.x;
			//~ Vec3f End = Start + W * WallDir;
			//~ {															//first floor under window
				//~ float B = Bottom;
				//~ float T = B + WindowDistance.y / 2;

				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ LT.TexCoord = { S,T };
				//~ RT.TexCoord = { S + W,T };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ unsigned int SharedI1 = SharedIndices[4 * i1 + 1];
				//~ unsigned int SharedI2 = SharedIndices[4 * i1 + 4];
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);
				//~ WallMesh.Indices_.push_back(SharedI1);
				//~ WallMesh.Indices_.push_back(SharedI2);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(SharedI1);

				//~ B = T;
				//~ T = B + WindowSize.y;
				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };

				//~ LB.TexCoord = { 0,0 };
				//~ RB.TexCoord = { 1,0 };
				//~ RT.TexCoord = { 1,1 };
				//~ LT.TexCoord = { 0,1 };

				//~ FirstI = (unsigned int)WindowMesh.Vertices_.size();
				//~ WindowMesh.Vertices_.push_back(LB);
				//~ WindowMesh.Vertices_.push_back(RB);
				//~ WindowMesh.Vertices_.push_back(RT);
				//~ WindowMesh.Vertices_.push_back(LT);

				//~ WindowMesh.Indices_.push_back(FirstI);
				//~ WindowMesh.Indices_.push_back(FirstI + 1);
				//~ WindowMesh.Indices_.push_back(FirstI + 2);

				//~ WindowMesh.Indices_.push_back(FirstI + 2);
				//~ WindowMesh.Indices_.push_back(FirstI + 3);
				//~ WindowMesh.Indices_.push_back(FirstI);
			//~ }
			//~ for (int i = 1; i < NumFloors; ++i)							//between windows
			//~ {
				//~ float B = Bottom + WindowDistance.y / 2 + WindowSize.y + (i - 1) * FloorHeight;
				//~ float T = B + WindowDistance.y;

				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };
				//~ LB.TexCoord = { S,B };
				//~ LT.TexCoord = { S,T };
				//~ RT.TexCoord = { S + W,T };
				//~ RB.TexCoord = { S + W,B };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Vertices_.push_back(RT);
				//~ WallMesh.Vertices_.push_back(LT);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 2);
				//~ WallMesh.Indices_.push_back(FirstI + 3);
				//~ WallMesh.Indices_.push_back(FirstI);

				//~ B = T;
				//~ T = B + WindowSize.y;
				//~ LB.Position = { Start.x, B, Start.z };
				//~ LT.Position = { Start.x, T, Start.z };
				//~ RT.Position = { End.x, T, End.z };
				//~ RB.Position = { End.x, B, End.z };

				//~ LB.TexCoord = { 0,0 };
				//~ RB.TexCoord = { 1,0 };
				//~ RT.TexCoord = { 1,1 };
				//~ LT.TexCoord = { 0,1 };

				//~ FirstI = (unsigned int)WindowMesh.Vertices_.size();
				//~ WindowMesh.Vertices_.push_back(LB);
				//~ WindowMesh.Vertices_.push_back(RB);
				//~ WindowMesh.Vertices_.push_back(RT);
				//~ WindowMesh.Vertices_.push_back(LT);

				//~ WindowMesh.Indices_.push_back(FirstI);
				//~ WindowMesh.Indices_.push_back(FirstI + 1);
				//~ WindowMesh.Indices_.push_back(FirstI + 2);

				//~ WindowMesh.Indices_.push_back(FirstI + 2);
				//~ WindowMesh.Indices_.push_back(FirstI + 3);
				//~ WindowMesh.Indices_.push_back(FirstI);
			//~ }
			//~ {															//top floor over window
				//~ float B = Bottom + WindowDistance.y / 2 + WindowSize.y + (NumFloors - 1) * FloorHeight;
				//~ //float T = B + WindowDistance.y/2;

				//~ LB.Position = { Start.x, B, Start.z };
				//~ RB.Position = { End.x, B, End.z };
				//~ LB.TexCoord = { S,B };
				//~ RB.TexCoord = { S + W,B };

				//~ unsigned int FirstI = (unsigned int)WallMesh.Vertices_.size();
				//~ unsigned int SharedI1 = SharedIndices[4 * i1 + 7];
				//~ unsigned int SharedI2 = SharedIndices[4 * i1 + 2];
				//~ WallMesh.Vertices_.push_back(LB);
				//~ WallMesh.Vertices_.push_back(RB);
				//~ WallMesh.Indices_.push_back(FirstI);
				//~ WallMesh.Indices_.push_back(FirstI + 1);
				//~ WallMesh.Indices_.push_back(SharedI1);
				//~ WallMesh.Indices_.push_back(SharedI1);
				//~ WallMesh.Indices_.push_back(SharedI2);
				//~ WallMesh.Indices_.push_back(FirstI);
			//~ }
		//~ }
	//~ }
//~ }

void Building::GenerateMesh(std::mt19937& RandomGenerator)
{
	uint8_t WallType = RandomGenerator() % NUMWALLS;
	uint8_t WindowType = RandomGenerator() % NUMWINDOWS;
	if (WallType == 0)
	{
		float w = (RandomGenerator() % 101) / 200.0f;
		std::vector<Vec4f> Colors = { Vec4f{w,w,w,1} };
		std::vector<Vec2f> ColorCoords(1);
		WallMesh_.TextureId_ = TexManager.GenerateColorTexture(Colors, ColorCoords);
	}
	else if (WallType == 1)
		WallMesh_.TextureId_ = TexManager.AddTexture("Assets/Textures/WallBrick.png");
	else if (WallType == 2)
		WallMesh_.TextureId_ = TexManager.AddTexture("Assets/Textures/WallConcrete.png");
	else if (WallType == 3)
		WallMesh_.TextureId_ = TexManager.AddTexture("Assets/Textures/WallPlate.png");
	if (WindowType == 0)
	{
		std::vector<Vec4f> Colors = { Vec4f{0,0,1,1} };
		std::vector<Vec2f> ColorCoords(1);
		WindowMesh_.TextureId_ = TexManager.GenerateColorTexture(Colors, ColorCoords);
	}
	else if (WindowType == 1)
		WindowMesh_.TextureId_ = TexManager.AddTexture("Assets/Textures/WindowType1.png");
	else if (WindowType == 2)
		WindowMesh_.TextureId_ = TexManager.AddTexture("Assets/Textures/WindowType2.png");
	float w = (RandomGenerator() % 101) / 200.0f + 0.4f;
	std::vector<Vec4f> Colors = { Vec4f{w,w,w,1} };
	std::vector<Vec2f> ColorCoords(1);
	FloorMesh_.TextureId_ = TexManager.GenerateColorTexture(Colors, ColorCoords);

	size_t NumCorners = LayoutOuter_.Wall.CornersI.size() -1;
	for (size_t wall = 0; wall < NumCorners; ++wall)	//CCW order of corners
	{
		Vec3f WallStart = GetPoint(LayoutOuter_.Wall.CornersI[wall]);
		Vec3f WallEnd = GetPoint(LayoutOuter_.Wall.CornersI[wall+1]);
		Vec3f WallDir = WallEnd - WallStart;
		float WallLenght = LEN(WallDir);
		WallDir = WallDir / WallLenght;
		Vec3f Normal = CROSS(WallDir, { 0,1,0 });

		Vertex LT, LB, RT, RB;
		LB.Normal = Normal;
		LT.Normal = Normal;
		RT.Normal = Normal;
		RB.Normal = Normal;

		float B = 0;
		float T = B + LayoutOuter_.Wall.CornerHeight[0];
		LB.Position = { WallStart.x, B, WallStart.z };
		LT.Position = { WallStart.x, T, WallStart.z };
		RT.Position = { WallEnd.x, T, WallEnd.z };
		RB.Position = { WallEnd.x, B, WallEnd.z };
		LB.TexCoord = { 0, B };
		LT.TexCoord = { 0, T };
		RT.TexCoord = { WallLenght, T };
		RB.TexCoord = { WallLenght, B };
		WallMesh_.AddQuad(LB, RB, RT, LT);
	}
	Meshes_.push_back(&WallMesh_);
	//Meshes_.push_back(&WindowMesh_);
	//Meshes_.push_back(&FloorMesh_);
}

std::vector<Mesh*> Building::GetMeshes()
{
	return Meshes_;
}