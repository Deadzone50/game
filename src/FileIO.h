#pragma once
#include "Containers.h"
#include "Object/PhysObject.h"
#include "Menu.h"
#include "World/World.h"

void PrintProgessToCout(float Progress);

void LoadMesh(std::string Name, Vec3f Color, std::vector<PhysPoint> &Points, std::vector<Surface> &Surfaces);

void LoadScene(std::string SceneName, World	&World, std::vector<Object> &Objects, std::vector<PhysObject> &PhysObjects);

void LoadMenuFile(std::string MenuName, std::vector<MenuItem> &Items);

void ParseObjFile(std::string FullPath, std::string Name, std::vector<Mesh> &Meshes, std::vector<Material> &Materials);