#include "Character/Character.h"
#include "Containers.h"

bool operator==(const PhysPoint &P1, const PhysPoint &P2)
{
	if((P1.CPos == P2.CPos) && (P1.LPos == P2.LPos) && (P1.Acc == P2.Acc))
		return true;
	else
		return false;
}
