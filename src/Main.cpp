﻿#include <GL/glew.h>
#include <GL/glut.h>
#include <chrono>
#include <iostream>
#include <iomanip>		//std::setprecision
#include <random>

#include "Vector/Vector.h"
#include "GLHelperFunctions.h"
#include "GlobalDefines.h"
#include "App.h"
#include "stb_image.h"

//Globals
std::mt19937 GlobalRand(0);
unsigned int GlobalId = 0;

Input In;
App App_;

auto t0h = std::chrono::high_resolution_clock::now();
GLuint TriangleProgram, CharacterProgram;
GLint AttributeVertexPosition, AttributeVertexNormal, AttributeTexCoord, AttributeVertexColor;
GLint UniformVP, UniformModel, UniformTexture;

GLint AttributeSSDWeights, AttributeSSDJointIndex;
GLint UniformSSDT, UniformTest, UniformSSDFlag;

TextureManager TexManager;

void KeyDown(unsigned char Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	In.KeyStates[Key] = true;
	In.Changed = true;
}

void KeyUp(unsigned char Key, int MouseX, int MouseY)
{	
	(void)MouseX;
	(void)MouseY;

	In.KeyStates[Key] = false;
	if('a' <= Key && Key <= 'z')									//97-122
		In.KeyStates[Key-32] = false;
	else if('A' <= Key && Key <= 'Z')								//65-90
		In.KeyStates[Key+32] = false;
}
void KeySpecialDown(int Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	In.KeySpecialStates[Key] = true;
	In.Changed = true;
}
void KeySpecialUp(int Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	In.KeySpecialStates[Key] = false;
}
Player *Cam;
void MouseFunc(int button, int state, int x, int y)
{
	(void)x;
	(void)y;
	In.Changed = true;
	if(button == 3 || button == 4)	//mousewheel
	{
		if(state == GLUT_DOWN)
			In.MouseButton[button] = true;
	}
	else
	{
		if(state == GLUT_DOWN)								//left,right, middle buttons
			In.MouseButton[button] = true;
		else
			In.MouseButton[button] = false;
	}

	//if(AnimatorActive && (state == GLUT_DOWN))
	//{
	//	Mat4f VP = Cam->GetPerspectiveVP();

	//	Mat4f InvVP = INVERT(VP);
	//	float NX = ((float)x/In.ScreenSize.x)*2-1;
	//	float NY = ((float)(In.ScreenSize.y - y)/In.ScreenSize.y)*2-1;

	//	Vec4f NPos1 = Vec4f{NX, NY, -1.0f, 1.0f};
	//	Vec4f NPos2 = Vec4f{NX, NY, +1.0f, 1.0f};

	//	Vec4f OPos1 = InvVP * NPos1;
	//	Vec4f OPos2 = InvVP * NPos2;

	//	In.MousePos3d = OPos1.GetXYZ();
	//	In.MouseDir = NORMALIZE(OPos2.GetXYZ() - OPos1.GetXYZ());
	//}

}

void MousePassiveMove(int x, int y)
{
	In.MousePos.x = x;
	In.MousePos.y = In.ScreenSize.y - y;

	Mat4f VP = Cam->GetPerspectiveVP();

	Mat4f InvVP = INVERTreal(VP);
	float NX = ((float)x/In.ScreenSize.x)*2-1;
	float NY = ((float)(In.ScreenSize.y - y)/In.ScreenSize.y)*2-1;

	Vec4f NPos1 = Vec4f{NX, NY, -1.0f, 1.0f};
	Vec4f NPos2 = Vec4f{NX, NY, +1.0f, 1.0f};

	Vec4f OPos1 = InvVP * NPos1;
	Vec4f OPos2 = InvVP * NPos2;
	float w1 = 1.0f/OPos1.w;
	float w2 = 1.0f/OPos2.w;
	Vec3f OP1 = OPos1.GetXYZ()*w1;
	Vec3f OP2 = OPos2.GetXYZ()*w2;

	In.MousePos3d = OP1;
	In.MouseDir = NORMALIZE(OP2 - OP1);
}

void Reshape(int width, int height)
{
	In.ScreenSize.x = width;
	In.ScreenSize.y = height;
	App_.GetPlayer()->SetScreenSize((float)width, (float)height);
	glViewport(0, 0, width, height);
}

void UseProgram(GLint Shader)					//set the variables to the correct location for the current shader program
{
	glUseProgram(Shader);
	AttributeVertexPosition	= glGetAttribLocation(Shader, "VPos");
	AttributeVertexNormal	= glGetAttribLocation(Shader, "VNormal");
	AttributeTexCoord		= glGetAttribLocation(Shader, "TexCoord");
	AttributeVertexColor	= glGetAttribLocation(Shader, "VColor");

	UniformVP				= glGetUniformLocation(Shader, "VP");
	UniformModel			= glGetUniformLocation(Shader, "Model");
	UniformTexture			= glGetUniformLocation(Shader, "Texture");
}

bool InitResources()
{
	GLint TVertShader, FragShader;
	if((TVertShader = CreateShader("Shaders/triangle.v.glsl", GL_VERTEX_SHADER)) == 0)
		return false;
	if((FragShader = CreateShader("Shaders/triangle.f.glsl", GL_FRAGMENT_SHADER)) == 0)
		return false;
	if((TriangleProgram = CreateProgram(TVertShader, FragShader)) == 0)
		return false;
#if 0
	GLint CVertShader;
	if((CVertShader = CreateShader("Shaders/character.v.glsl", GL_VERTEX_SHADER)) == 0)
		return false;
	if((CharacterProgram = CreateProgram(CVertShader, FragShader)) == 0)
		return false;
	glDeleteShader(CVertShader);
#endif

	glDeleteShader(TVertShader);
	glDeleteShader(FragShader);
	//if(!BindAttribute(	"VPos",				AttributeVertexPosition,	TriangleProgram))	return false;
	//if(!BindAttribute(	"VNormal",			AttributeVertexNormal,		TriangleProgram))	return false;
	//if(!BindAttribute(	"TexCoord",			AttributeTexCoord,			TriangleProgram))	return false;
	//if(!BindAttribute(	"VColor",			AttributeVertexColor,		TriangleProgram))	return false;

	//if(!BindUniform(	"VP",				UniformVP,					TriangleProgram))	return false;
	//if(!BindUniform(	"Model",			UniformModel,				TriangleProgram))	return false;
	//if(!BindUniform(	"Texture",			UniformTexture,				TriangleProgram))	return false;
#if 0
	if(!BindAttribute(	"VPos",				AttributeVertexPosition,	CharacterProgram))	return false;
	if(!BindAttribute(	"VNormal",			AttributeVertexNormal,		CharacterProgram))	return false;
	if(!BindAttribute(	"TexCoord",			AttributeTexCoord,			CharacterProgram))	return false;
	if(!BindAttribute(	"VColor",			AttributeVertexColor,		CharacterProgram))	return false;

	if(!BindUniform(	"VP",				UniformVP,					CharacterProgram))	return false;
	if(!BindUniform(	"Model",			UniformModel,				CharacterProgram))	return false;
	if(!BindUniform(	"Texture",			UniformTexture,				CharacterProgram))	return false;

	if(!BindUniform(	"SSDT",				UniformSSDT,				CharacterProgram))	return false;
	if(!BindAttribute(	"SSDWeight",		AttributeSSDWeights,		CharacterProgram))	return false;
	if(!BindAttribute(	"SSDJointIndex",	AttributeSSDJointIndex,		CharacterProgram))	return false;
#endif
	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	stbi_set_flip_vertically_on_load(true);
	TexManager.LoadFont("Assets/Font9x9.png");

	App_.InitAnimator();
	App_.InitGame();
	App_.InitMenu();
	App_.SetActive(GameActive);
	MeshOutput O = App_.GetMeshOutput();
	Cam = O.Player;

	//UseProgram(TriangleProgram);
	//glUniform1i(UniformTexture, 0);
	//UseProgram(CharacterProgram);
	//glUniform1i(UniformTexture, 0);

	glClearColor(0.6f, 0.8f, 1.0f, 1);
	glPointSize(5);
	return true;
}
void FreeResources()
{

}
void Logic(MeshOutput &O)
{
	if(O.WarpMouse)
	{
		//if(In.MouseDelta.x || In.MouseDelta.y)
		//if(O.MouseDelta.x || O.MouseDelta.y)
		{
			glutWarpPointer(In.ScreenSize.x/2, In.ScreenSize.y/2);
			In.MousePos.x = In.ScreenSize.x/2;
			In.MousePos.y = In.ScreenSize.y/2;
			//glutSetCursor(GLUT_CURSOR_NONE);
		}
	}
	else
	{
		//glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
	}
}
Mesh
RenderTextOnScreen(std::string &String, Vec3f &Color, Vec3f &Position)
{
	Vec3f Pos;											//TODO: different shader so colors can be changed?
	Pos.x = In.ScreenSize.x*(Position.x+1)*0.5f;
	Pos.y = (In.ScreenSize.y-10)*(Position.y+1)*0.5f;
	Pos.z = 0;
	return TexManager.CreateTextMesh(String, Pos);
}

void SetPerspective()
{
	Mat4f VP = Cam->GetPerspectiveVP();
	glUniformMatrix4fv(UniformVP, 1, GL_TRUE, VP.m);
}

void SetOrthographic()
{
	Mat4f VP = Ortho(0, (float)In.ScreenSize.x, 0, (float)In.ScreenSize.y, -1, 1);
	//Mat4f MVP = Ortho(-1, 1, -1, 1, -1, 1);
	Mat4f Model;
	glUniformMatrix4fv(UniformModel, 1, GL_TRUE, Model.m);
	glUniformMatrix4fv(UniformVP, 1, GL_TRUE, VP.m);
}
unsigned char Frame = 200;
std::vector<Mesh> Text;
void Render(MeshOutput &O)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	size_t Trigs = 0;

	UseProgram(CharacterProgram);
	glUniform1i(UniformTexture, 0);				//bind texture unit 0 to UniformTexture
	SetPerspective();
	auto CEND = O.NPCs.end();
	for(auto it = O.NPCs.begin(); it != CEND; ++it)
	{
		Trigs += (*it)->Draw();
	}
	
	UseProgram(TriangleProgram);
	glUniform1i(UniformTexture, 0);				//bind texture unit 0 to UniformTexture
	SetPerspective();
	auto MEND = O.Meshes.end();
	for(auto it = O.Meshes.begin(); it != MEND; ++it)
	{
		Trigs += (*it)->DrawMesh();
	}
	auto OEND = O.Objects.end();
	for(auto it = O.Objects.begin(); it != OEND; ++it)
	{
		Trigs += (*it)->Draw();
	}

	SetOrthographic();
	auto SSMEND = O.SSMeshes.end();
	for(auto it = O.SSMeshes.begin(); it != SSMEND; ++it)
	{
		Trigs += (*it)->DrawMesh();
	}

	++Frame;
	if(Frame > 6)
	{
		TextLine TL;
		std::stringstream ss;
		ss << "Number Of Triangles: " << Trigs;
		TL.Pos = {-1,0.95f,0};
		TL.Text = ss.str();
		O.SSText.push_back(TL);

		Frame = 0;
		Text.clear();
		auto TEND = O.SSText.end();
		for(auto it = O.SSText.begin(); it != TEND; ++it)
		{
			if(it->Text.size())
			{
				Text.push_back(RenderTextOnScreen(it->Text, it->C, it->Pos));
			}
		}
	}

	for(auto it = Text.begin(); it != Text.end(); ++it)
	{
		Trigs += it->DrawMesh();
	}


	glutSwapBuffers();
}

void MainLoop()
{
	MeshOutput O = App_.GetMeshOutput();

	Cam = O.Player;
	Logic(O);
	Render(O);

	auto now = std::chrono::high_resolution_clock::now();
	In.dt = std::chrono::duration<float>(now - t0h).count();
	t0h = std::chrono::high_resolution_clock::now();
	
	App_.HandleInput(In);
}

int PauseExit(int Code)
{
	std::cin.ignore();
	return Code;
}

int main(int argc, char **argv)
{
	Mat4f M1, M2;
	M1.SetRow(0,Vec4f{1,2,3,4});
	M1.SetRow(1,Vec4f{5,6,7,8});
	M1.SetRow(2,Vec4f{9,10,11,12});
	M1.SetRow(3,Vec4f{13,14,15,16});
	M2.SetRow(0,Vec4f{1,2,3,4});
	M2.SetRow(1,Vec4f{5,6,7,8});
	M2.SetRow(2,Vec4f{9,10,11,12});
	M2.SetRow(3,Vec4f{13,14,15,16});
	Mat4f M = M1*M2;

	In.ScreenSize = Vec2i{800,500};

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(In.ScreenSize.x, In.ScreenSize.y);
	glutInitWindowPosition(100,20);
	glutCreateWindow("CyberHunter");

	glutDisplayFunc(MainLoop);
	glutIdleFunc(MainLoop);
	glutReshapeFunc(Reshape);
	
	glutKeyboardFunc(KeyDown);			glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(KeySpecialDown);	glutSpecialUpFunc(KeySpecialUp);
	glutMouseFunc(MouseFunc);
	glutMotionFunc(MousePassiveMove);
	glutPassiveMotionFunc(MousePassiveMove);
	
	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glDepthFunc(GL_LESS);
	
	
	GLenum GlewStatus = glewInit();
	if(GlewStatus != GLEW_OK)
	{
		std::cout << "Error glewInit: " << glewGetErrorString(GlewStatus) << std::endl;
		return PauseExit(EXIT_FAILURE);
	}
	if (!GLEW_VERSION_2_1)
	{
		std::cout << "Error: your graphic card does not support OpenGL 2.1\n";
		return PauseExit(EXIT_FAILURE);
	}

	if(!InitResources())
	{
		std::cout << "InitResources failed\n";
		return PauseExit(EXIT_FAILURE);
	}

	t0h = std::chrono::high_resolution_clock::now();
	glutMainLoop();

	FreeResources();
	return EXIT_SUCCESS;
}
