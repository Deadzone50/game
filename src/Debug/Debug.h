#pragma once
#include "Vector/Vector.h"
#include "Containers.h"

#define ASSERT(test)														\
do {int l = __LINE__;														\
	if(!(test))																\
	{																		\
		std::cerr << "ASSERT faied: " << __FILE__ << ": " <<l <<std::endl;	\
		*(int*)0 = 0;std::cin.ignore();													\
		exit(-1);															\
	}																		\
} while (0)

class DebugDraws
{
public:
	void DrawDebugPoint(Vec3f Pos, Vec3f Color, bool ScreenSpace = false);
	void DrawDebugLine(Vec3f S, Vec3f E, Vec3f Color, bool ScreenSpace = false);
	void DrawDebugText(Vec3f Pos, Vec3f Color, float i, bool ScreenSpace = false);
	void DrawDebugText(Vec3f Pos, Vec3f Color, std::string Text, bool ScreenSpace = false);
	void ClearDebugDraws();

	Points				DebugPoints_;
	Lines				DebugLines_;
	Triangles			DebugTriangles_;
	TextLines			DebugText_;

	Points				SSDebugPoints_;
	Lines				SSDebugLines_;
	Triangles			SSDebugTriangles_;
	TextLines			SSDebugText_;
};
