﻿#include "Debug/Debug.h"

void
DebugDraws::DrawDebugPoint(Vec3f Pos, Vec3f Color, bool ScreenSpace)
{
	Point P;
	P.P = Pos;
	P.C = Color;
	if(ScreenSpace)
	{
		SSDebugPoints_.push_back(P);
	}
	else
	{
		DebugPoints_.push_back(P);
	}
}
void
DebugDraws::DrawDebugLine(Vec3f S, Vec3f E, Vec3f Color, bool ScreenSpace)
{
	Line L;
	L.S = S;
	L.E = E;
	L.C = Color;
	if(ScreenSpace)
	{
		SSDebugLines_.push_back(L);
	}
	else
	{
		DebugLines_.push_back(L);
	}
}
void
DebugDraws::DrawDebugText(Vec3f Pos, Vec3f Color, float i, bool ScreenSpace)
{
	std::stringstream ss;
	ss << i;
	std::string Text = ss.str();
	DrawDebugText(Pos, Color, Text, ScreenSpace);
}
void
DebugDraws::DrawDebugText(Vec3f Pos, Vec3f Color, std::string Text, bool ScreenSpace)
{
	TextLine TL;
	TL.Pos = Pos;
	TL.C = Color;
	TL.Text = Text;
	if(ScreenSpace)
	{
		SSDebugText_.push_back(TL);
	}
	else
	{
		DebugText_.push_back(TL);
	}
}
void
DebugDraws::ClearDebugDraws()
{
	DebugPoints_.clear();
	DebugLines_.clear();
	DebugTriangles_.clear();
	DebugText_.clear();

	SSDebugPoints_.clear();
	SSDebugLines_.clear();
	SSDebugTriangles_.clear();
	SSDebugText_.clear();
}
