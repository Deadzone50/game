#pragma once

#include <vector>
#include "Object/PhysObject.h"
#include "Character/Character.h"
#include "Containers.h"
#include "World/World.h"
#include "Menu.h"

enum Screen {MapScreen, CameraScreen};

class Game
{
public:
	Game() {}
	bool					Init();
	void					Exit();

	void					Reset();
	void					Pause();
	void					UnPause();
	void					ChangeScene(std::string Name);
	void					ClearScene();
	
	void					UpdateObjectPositions();
	void					Step(float dt);
	int						HandleButtons(const Input& In);
	void					HandleMouse(Input &In);

	void					DisplayCamera();
	void					DisplayMap();

	MeshOutput&				GetMeshOutput();

	Player*					GetPlayer()							{return &Player_;}
	void					GenerateWorld(int Seed, int MaxBuildingHeight, int CitySize, bool Z);
	
protected:
	World					World_;
	std::vector<Object>		Objects_;
	std::vector<PhysObject>	PhysObjects_;

	//std::vector<Object*>	AllObjects_;
	Player					Player_;
	Player					StaticViewer_;
	bool					Paused_;
	bool					Reset_;
	bool					MouseInit_;
	//bool					Gravity_ = true;

	Screen					DisplayedScreen_;

	std::string				CurrentScene_;
	int						SkipFactor_;
	float					MoveSpeed_;
	size_t					CurrentSurface_;

	std::vector<MeshObject>		MeshObjects_;
	std::vector<Mesh>			Meshes_;
	std::vector<Material>		Materials_;
	std::vector<NPC>			NPCs_;

	MeshOutput					Output_;

	Points		DebugPoints_;
	Lines		DebugLines_;
	//Triangles	DebugTriangles_;
	//TextLines	SSDebugTextLines_ = TextLines(2);
};
