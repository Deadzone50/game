﻿#include "App.h"

Player*
App::GetPlayer()
{
	if(MenuActive_)
	{
		return Menu_.GetPlayer();
	}
	else
	{
		switch(CurrentActive_)
		{
			case AnimatorActive:
				return Animator_.GetPlayer();
				break;
			case GameActive:
				return Game_.GetPlayer();
				break;
			default:
				return nullptr;
				break;
		}
	}
}

MeshOutput &
App::GetMeshOutput()
{
	MeshOutput *M;
	if(MenuActive_)
	{
		M = &Menu_.GetMeshOutput();
	}
	else
	{
		switch(CurrentActive_)
		{
			case AnimatorActive:
				M = &Animator_.GetMeshOutput();
				break;
			case GameActive:
				M = &Game_.GetMeshOutput();
				break;
			default:
				abort();
				break;
		}
	}
	M->SSText.push_back(Text_[0]);
	return *M;
}

void
App::HandleInput(Input &In)
{
	if(In.Changed && In.KeyStates[27])			//ESC key, toggle menu
	{
		MenuActive_ = !MenuActive_;
	}
	if(MenuActive_)
	{
		Menu_.SetResolution(In.ScreenSize);
		if(In.Changed)						//if a new key has been pressed
		{
			if(In.KeyStates['0']){Menu_.InputNumber("0");}
			if(In.KeyStates['1']){Menu_.InputNumber("1");}
			if(In.KeyStates['2']){Menu_.InputNumber("2");}
			if(In.KeyStates['3']){Menu_.InputNumber("3");}
			if(In.KeyStates['4']){Menu_.InputNumber("4");}
			if(In.KeyStates['5']){Menu_.InputNumber("5");}
			if(In.KeyStates['6']){Menu_.InputNumber("6");}
			if(In.KeyStates['7']){Menu_.InputNumber("7");}
			if(In.KeyStates['8']){Menu_.InputNumber("8");}
			if(In.KeyStates['9']){Menu_.InputNumber("9");}
			if(In.KeyStates[8]){Menu_.InputBackspace();}

			if(In.MouseButton[0])				//left mouse button
			{
				Vec2f ScaledPos;
				ScaledPos.x = (float)In.MousePos.x/(In.ScreenSize.x/2)-1;
				ScaledPos.y = (float)In.MousePos.y/(In.ScreenSize.y/2)-1;
				std::string Button = Menu_.Click(ScaledPos);
				if(!Button.compare("Animation"))
				{
					CurrentActive_ = AnimatorActive;
				}
				else if(!Button.compare("City"))
				{
					CurrentActive_ = GameActive;
				}
				else if(!Button.compare("Generate City"))
				{
					int Seed = Menu_.GetValue("Seed");
					int Size = Menu_.GetValue("Size");
					int MaxHeight = Menu_.GetValue("Max Height");
					if(Size >= 20 && MaxHeight > 0)
					{
						Game_.GenerateWorld(Seed, MaxHeight, Size, false);
					}
				}
				else if(!Button.compare("Quit"))
				{
					exit(EXIT_SUCCESS);
				}
			}
		}
	}
	else if(CurrentActive_ == GameActive)
	{
		Game_.HandleMouse(In);
		Game_.HandleButtons(In);
		Game_.Step(In.dt);
	}
	else if(CurrentActive_ == AnimatorActive)
	{
		Animator_.HandleMouse(In);
		Animator_.HandleButtons(In);
	}
	float fps = (1.0f/In.dt);
	std::stringstream ss;
	ss << In.dt*1000 << "ms, FPS: " <<fps;
	Text_[0].Pos = {-1,1,0};
	Text_[0].C = {1,0,0};
	Text_[0].Text = ss.str();

	In.Changed = false;
}