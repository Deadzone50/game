#pragma once
#include <vector>
#include "float.h"
#include "Vector/Matrix.h"
#include "Vector/Vector.h"
///*****************Graphics containers******************
struct Vertex
{
	Vec3f Position;
	Vec3f Normal;
	Vec2f TexCoord;
};
struct SSDData
{
	int JointIndex;
	float Weight;
};
struct Image
{
	unsigned char *Data;
	int W,H,BPP;
};
struct Material
{

};
struct Line
{
	Vec3f S, E, C;
};
struct Point
{
	Vec3f P, C;
};
struct Triangle
{
	Vec3f P1, P2, P3, C;
};
struct TextLine
{
	Vec3f Pos,C;
	std::string Text;
};
typedef std::vector<Line> Lines;
typedef std::vector<Point> Points;
typedef std::vector<Triangle> Triangles;
typedef std::vector<TextLine> TextLines;

typedef std::vector<Vec3f> Vec3fV;
typedef std::vector<Vec2f> Vec2fV;

class Player;		//forward declaration

struct Input
{
	bool *KeyStates = new bool[256]{false};
	bool *KeySpecialStates = new bool[256]{false};
	bool Changed;
	Vec2i MousePos;
	//Vec2f MouseDelta;
	Vec3f MousePos3d;
	Vec3f MouseDir;
	bool* MouseButton = new bool[5]{false};
	Vec2i ScreenSize;
	float dt;
};
class MeshObject;
class Mesh;
class NPC;
struct MeshOutput
{
	std::vector<Mesh*>			Meshes;
	std::vector<NPC*>			NPCs;
	std::vector<MeshObject*>	Objects;
	std::vector<Mesh*>			SSMeshes;
	TextLines					SSText;			//should be destroyed after every loop
	bool WarpMouse;
	//Vec2f MouseDelta;
	Player *Player;
};
struct PhysPoint
{
	PhysPoint() {}
	PhysPoint(Vec3f P) : CPos(P){LPos = CPos;}

	Vec3f CPos, LPos;
	Vec3f Acc = Vec3f{0,-9.81f,0};
};
bool operator==(const PhysPoint &P1, const PhysPoint &P2);

struct Link
{
	Link() {}
	Link(unsigned I1, unsigned I2, float SpringK, float RestLength, float TearFactor) :
		I1(I1), I2(I2), K(SpringK), RLen(RestLength), TFact(TearFactor){}
	unsigned I1, I2;
	float K, RLen, TFact;
};
struct Surface
{
	Surface() {}
	Surface(Vec3f N, Vec3f P, Vec3f C): N(N), P(P), C(C) {}
	Vec3f N,P,C;
	std::vector<int> Points;
};
struct Collision
{
	bool Hit = false;
	Vec3f Normal;
	float Distance;

	std::vector<Vec3f> Force1; 
	std::vector<Vec3f> Force2; 
	std::vector<int> Points1;
	std::vector<int> Points2;
};
struct CollisionLine
{
	Vec3f S,E;
	int MySurface1 = 0;
	int MySurface2 = 0;
	int Si = -1;
	int Ei = -1;
	Vec3f C;

	//float DistanceFromS = 0;			//distance from endpoints to collisionpoints
	//float DistanceFromE = 0;
	//float DistanceToMoveS;				//how far to move endpoints to fix the collision
	//float DistanceToMoveE;
	//Surface* CutSurS = NULL;	//surfaces that limit the collisionline
	//Surface* CutSurE = NULL;
	int CutSurS = -1;	//surfaces that limit the collisionline
	int CutSurE = -1;
	int CSurfaceS;				//collision surface index
	int CSurfaceE;
	bool Middle = false;
};
