#include "Vector.h"

#include <cmath>
#include <string>
#include <sstream>


Vec3f
Vec4f::GetXYZ()
{
	Vec3f Result;
	Result.x = x;
	Result.y = y;
	Result.z = z;
	return Result;
}

bool
operator==(const Vec4f &V1, const Vec4f &V2)
{
	if(V1.x == V2.x)
		if(V1.y == V2.y)
			if(V1.z == V2.z)
				if(V1.w == V2.w)
					return true;
	return false;
}
Vec4f operator*(const Vec4f &V1, const Vec4f &V2)
{
	Vec4f V;
	V.x = V1.x*V2.x;
	V.y = V1.y*V2.y;
	V.z = V1.z*V2.z;
	V.w = V1.w*V2.w;
	return V;
}
Vec4f operator+(const Vec4f &V1, const Vec4f &V2)
{
	Vec4f V;
	V.x = V1.x+V2.x;
	V.y = V1.y+V2.y;
	V.z = V1.z+V2.z;
	V.w = V1.w+V2.w;
	return V;
}
Vec4f operator-(const Vec4f &V1, const Vec4f &V2)
{
	Vec4f V;
	V.x = V1.x-V2.x;
	V.y = V1.y-V2.y;
	V.z = V1.z-V2.z;
	V.w = V1.w-V2.w;
	return V;
}
void operator*=(Vec4f &V1, const Vec4f &V2)
{
	V1.x *= V2.x;
	V1.y *= V2.y;
	V1.z *= V2.z;
	V1.w *= V2.w;
}
void operator+=(Vec4f &V1, const Vec4f &V2)
{
	V1.x += V2.x;
	V1.y += V2.y;
	V1.z += V2.z;
	V1.w += V2.w;
}
void operator-=(Vec4f &V1, const Vec4f &V2)
{
	V1.x -= V2.x;
	V1.y -= V2.y;
	V1.z -= V2.z;
	V1.w -= V2.w;
}

Vec4f operator*(float f, const Vec4f &V1)
{
	Vec4f V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	V.z=f*V1.z;
	V.w=f*V1.w;
	return V;
}
Vec4f operator*(const Vec4f & V, float f)	{return f*V;}
Vec4f operator*(int i, const Vec4f &V1)
{
	Vec4f V;
	V.x=i*V1.x;
	V.y=i*V1.y;
	V.z=i*V1.z;
	V.w=i*V1.w;
	return V;
}
Vec4f operator*(const Vec4f & V, int i)		{return i*V;}
Vec4f operator/(const Vec4f &V1, float f)
{
	Vec4f V;
	V.x=V1.x/f;
	V.y=V1.y/f;
	V.z=V1.z/f;
	V.w=V1.w/f;
	return V;
}
Vec4f operator-(const Vec4f &V1)
{
	Vec4f V;
	V.x = -V1.x;
	V.y = -V1.y;
	V.z = -V1.z;
	V.w = -V1.w;
	return V;
}

std::ostream& operator<<(std::ostream &os, const Vec4f &V)
{
	os << V.x << " , " << V.y << " , " << V.z << " , " << V.w;
	return os;
}
std::istream& operator>>(std::istream &is, Vec4f &V)
{
	is >> V.x >> V.y >> V.z >> V.w;
	return is;
}