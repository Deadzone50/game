#pragma once
#include <vector>
#include "Vector/Vector.h"

typedef std::vector<Vec3f> State;
typedef std::vector<Vec3f> Points;
typedef std::vector<Vec3f> Lines;

struct Spring
{
	Spring() {}
	Spring(unsigned index1, unsigned index2, float spring_k, float rest_length) :
			I1(index1), I2(index2), K(spring_k), RLen(rest_length) {}
	unsigned I1, I2;
	float K, RLen;
};

class ParticleSystem
{
public:
	virtual					~ParticleSystem() {};
	virtual State			EvalF(const State&) const = 0;
	virtual void			Reset() = 0;
	const State&			GetState() { return state_; }
	void					SetState(State s) { state_ = s; }
	virtual Points			GetPoints() { return Points(); }
	bool					Collisions = true;
protected:
	State					state_;
};

class PendulumSystem : public ParticleSystem
{
public:
							PendulumSystem(unsigned n) : n_(n) { Reset(); }
	State					EvalF(const State&) const override;
	void					Reset() override;
	Points					GetPoints() override;

private:
	unsigned				n_;
	std::vector<Spring>		springs_;
};

//~ class ClothSystem: public ParticleSystem {
//~ public:
							//~ ClothSystem(unsigned x, unsigned y) : x_(x), y_(y) { reset(); }
	//~ State					evalF(const State&) const override;
	//~ void					reset() override;
	//~ Points					getPoints() override;
	//~ Lines					getLines() override;
	//~ FW::Vec2i				getSize() { return FW::Vec2i(x_, y_); }
//~ 
//~ private:
	//~ unsigned				x_, y_;
	//~ std::vector<Spring>		springs_;
//~ };

void eulerStep(ParticleSystem& ps, float Step);
void trapezoidStep(ParticleSystem& ps, float Step);
void midpointStep(ParticleSystem& ps, float Step);
void rk4Step(ParticleSystem& ps, float Step);