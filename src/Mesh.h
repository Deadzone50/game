#pragma once
#include"Containers.h"

enum MeshType
{
	MeshType_Triangle,
	MeshType_Line,
	MeshType_Point
};

class Mesh						//setup for OpenGl
{
public:
	Mesh()						{Changed_ = true; VAO_ = 0; VBO_ = 0; EBO_ = 0; TextureId_ = 0;}
	~Mesh()						{DeleteBuffers();}
	void						DeleteBuffers();

	//Mesh &operator=(const Mesh &) = delete;
	//Mesh(const Mesh&) = delete;
	//Mesh(Mesh &&Other);
	//Mesh &operator=(Mesh &&Other);

	bool						RayCollisionTest(Vec3f RayOrigin, Vec3f RayDir, float &Max);
	void						SetupMesh();

	size_t						DrawMesh();
	void						Reserve(unsigned int Quads);
	void						ShrinkToFit();
	void						Clear()							{Vertices_.clear(); Indices_.clear(); Changed_ = true;}
	void						AddVertex(const Vertex &V)		{Vertices_.push_back(V);}
	void						AddIndex(const unsigned int &I)	{Indices_.push_back(I);}
	unsigned int				GetCurrentIndex()				{return (unsigned int)Vertices_.size();}
	void						AddQuad(const Vertex &LB, const Vertex &RB, const Vertex &RT, const Vertex &LT)
	{
		unsigned int FirstI = GetCurrentIndex();													   //	LT-RT
		AddVertex(LB);	AddVertex(RB);	AddVertex(RT);	AddVertex(LT);								   //	| /|
		Indices_.push_back(FirstI);		Indices_.push_back(FirstI+1);	Indices_.push_back(FirstI+2);  //	|/ |
		Indices_.push_back(FirstI+2);	Indices_.push_back(FirstI+3);	Indices_.push_back(FirstI);	   //	LB-RB
	}

	std::vector<Vertex>			Vertices_;
	std::vector<unsigned int>	Indices_;
	unsigned int				TextureId_;
	unsigned int 				VAO_, VBO_, EBO_;
	bool						Changed_;
	MeshType					Type_ = MeshType_Triangle;
};