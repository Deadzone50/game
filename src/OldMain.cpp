﻿//#include <GL/glew.h>
#include <GL/glut.h>
#include <chrono>
#include <iostream>
#include "Vector/Vector.h"
#include "GlobalDefines.h"
#include "Skeleton.h"
#include "Game.h"
#include "PhysObject.h"

#include "Animator.h"
#include "Character.h"

Input In;

auto t0h = std::chrono::high_resolution_clock::now();

Game Game_= Game();
Animator Animator_;
bool AnimatorActive = false;


void KeyDown(unsigned char Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	In.KeyStates[Key] = true;
	In.Changed = true;
}
void KeyUp(unsigned char Key, int MouseX, int MouseY)
{	
	(void)MouseX;
	(void)MouseY;

	In.KeyStates[Key] = false;
	if(97 <= Key && Key <= 122)								//only affect a-z
	if (!(glutGetModifiers() == GLUT_ACTIVE_SHIFT))			//releases the uppercase key aswell
		In.KeyStates[Key-32] = false;
	if(65 <= Key && Key <= 90)								//only affect A-Z
		if (!(glutGetModifiers() == GLUT_ACTIVE_SHIFT))		//releases the lowercase key aswell
			In.KeyStates[Key+32] = false;
}
void KeySpecialDown(int Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	In.KeySpecialStates[Key] = true;
	In.Changed = true;
	//if (glutGetModifiers() == GLUT_ACTIVE_SHIFT)
	//	In.ShiftKey = true;
	//else
	//	In.ShiftKey = false;
}
void KeySpecialUp(int Key, int MouseX, int MouseY)
{
	(void)MouseX;
	(void)MouseY;
	In.KeySpecialStates[Key] = false;
}


void MouseFunc(int button, int state, int x, int y)
{
	(void)x;
	(void)y;
	In.Changed = true;
	if(button == 3 || button == 4)	//mousewheel
	{
		if(state == GLUT_DOWN)
			In.MouseButton[button] = true;
	}
	else
	{
		if(state == GLUT_DOWN)								//left,right, middle buttons
			In.MouseButton[button] = true;
		else
			In.MouseButton[button] = false;
	}
}

void Set3D()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, GLfloat(In.ScreenSize.x)/GLfloat(In.ScreenSize.y), 0.01f, DRAWDISTANCE);
	//				FOV, Aspect ratio, near-, far-field
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void Set2D()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.03f, 1.03f, -1.03f, 1.03f, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Mesh::SetupMesh() {}

void MousePassiveMove(int x, int y)
{
	In.MousePos.x = x;
	In.MousePos.y = In.ScreenSize.y - y;

	if(AnimatorActive)
	{
		Set3D();
		Output O = Animator_.GetOutput();
		Player *Cam = O.Player_;
		gluLookAt(Cam->GetPosition().x, Cam->GetPosition().y, Cam->GetPosition().z,
				  Cam->GetPosition().x+Cam->GetDirection().x, Cam->GetPosition().y+Cam->GetDirection().y, Cam->GetPosition().z+Cam->GetDirection().z,
				  Cam->GetUp().x, Cam->GetUp().y, Cam->GetUp().z);

		double ModelViewMatrix[16];
		double ProjectionMatrix[16];
		GLint Viewport[4];

		glGetDoublev(GL_MODELVIEW_MATRIX, ModelViewMatrix);
		glGetDoublev(GL_PROJECTION_MATRIX, ProjectionMatrix);
		glGetIntegerv(GL_VIEWPORT, Viewport);

		GLdouble RealY = In.ScreenSize.y - (GLint) y - 1;

		GLdouble ObjX1, ObjY1, ObjZ1;
		GLdouble ObjX2, ObjY2, ObjZ2;
		gluUnProject((GLdouble)x, RealY, 0, ModelViewMatrix, ProjectionMatrix, Viewport, &ObjX1, &ObjY1, &ObjZ1);	//create a ray in world space
		gluUnProject((GLdouble)x, RealY, 1, ModelViewMatrix, ProjectionMatrix, Viewport, &ObjX2, &ObjY2, &ObjZ2);

		In.MousePos3d = {(float)ObjX1, (float)ObjY1, (float)ObjZ1};
		In.MouseDir = NORMALIZE(Vec3f{(float)ObjX2, (float)ObjY2, (float)ObjZ2} - Vec3f{(float)ObjX1, (float)ObjY1, (float)ObjZ1});
	}
}

void Reshape(int width, int height)
{
	//glViewport(0, 0, GLsizei(width), GLsizei(height));
	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//gluPerspective(60, GLfloat(width)/GLfloat(height), 0.01f, 20000.0);
	////				FOV, Aspect ratio, near-, far-field
	//glMatrixMode(GL_MODELVIEW);
	In.ScreenSize.x = width;
	In.ScreenSize.y = height;
	glViewport(0, 0, GLsizei(width), GLsizei(height));
	//Set3D();
}

unsigned int FrameCount = 0;
void
Display()
{
	glClearColor(1, 1, 1, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Set3D();
	
	Output O;
	if(AnimatorActive)
		O = Animator_.GetOutput();
	else
		O = Game_.GetOutput();

	Player *Cam = O.Player_;
	gluLookAt(Cam->GetPosition().x, Cam->GetPosition().y, Cam->GetPosition().z,				// Set the camera
			  Cam->GetPosition().x+Cam->GetDirection().x, Cam->GetPosition().y+Cam->GetDirection().y, Cam->GetPosition().z+Cam->GetDirection().z,
			  Cam->GetUp().x, Cam->GetUp().y, Cam->GetUp().z);
	
	//Points P = O.P;
	//auto Pend = P.end();
	//glBegin(GL_QUADS);
	//for(auto it = P.begin(); it != Pend;++it)
	//
	//for(auto it = P.begin(); it != Pend; ++it)
	//{
	//	glColor3f(it->C.x, it->C.y, it->C.z);
	//	glVertex3f(it->P.x, it->P.y, 0);
	//	glVertex3f(it->P.x+1, it->P.y, 0);
	//	glVertex3f(it->P.x+1, it->P.y+1, 0);
	//	glVertex3f(it->P.x, it->P.y+1, 0);
	//}
	//glEnd();

	if(O.WarpMouse)
	{
		if(In.MouseDelta.x || In.MouseDelta.y)
		{
			glutWarpPointer(In.ScreenSize.x/2, In.ScreenSize.y/2);
			In.MousePos.x = In.ScreenSize.x/2;
			In.MousePos.y = In.ScreenSize.y/2;
			//glutSetCursor(GLUT_CURSOR_NONE);
		}
	}
	else
	{
		glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
	}

	auto Pend = O.P.end();
	glPointSize(5);
	glBegin(GL_POINTS);				//draw points
	for(auto it = O.P.begin(); it != Pend;++it)
	{
		glColor3f(it->C.x, it->C.y, it->C.z);
		glVertex3f(it->P.x,it->P.y,it->P.z);
	}
	glEnd();
	auto Lend = O.L.end();
	glBegin(GL_LINES);				//draw lines
	for(auto it = O.L.begin(); it != Lend;++it)
	{
		glColor3f(0.1f*it->C.x, 0.1f*it->C.y, 0.1f*it->C.z);
		glVertex3f(it->S.x,it->S.y,it->S.z);
		glColor3f(it->C.x, it->C.y, it->C.z);
		glVertex3f(it->E.x, it->E.y, it->E.z);
	}
	glEnd();
	auto Tend = O.T.end();
	glBegin(GL_TRIANGLES);			//draw triangles
	for(auto it = O.T.begin(); it != Tend; ++it)
	{
		glColor3f(it->C.x, it->C.y, it->C.z);
		glVertex3f(it->P1.x, it->P1.y, it->P1.z);
		glVertex3f(it->P2.x, it->P2.y, it->P2.z);
		glVertex3f(it->P3.x, it->P3.y, it->P3.z);
	}
	glEnd();
	auto TLend = O.TL.end();
	for(auto it = O.TL.begin(); it != TLend; ++it)
	{
		glColor3f(it->C.x, it->C.y, it->C.z);
		glRasterPos3f(it->Pos.x, it->Pos.y, it->Pos.z);
		for(size_t i = 0; i < it->Text.size();++i)
		{
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, it->Text[i]);
		}
	}

	Set2D();										//2D Drawing
	Pend = O.SSP.end();
	glPointSize(5);
	glBegin(GL_POINTS);				//draw points
	for(auto it = O.SSP.begin(); it != Pend;++it)
	{
		glColor3f(it->C.x, it->C.y, it->C.z);
		glVertex3f(it->P.x,it->P.y,it->P.z);
	}
	glEnd();
	Lend = O.SSL.end();
	glBegin(GL_LINES);				//draw lines
	for(auto it = O.SSL.begin(); it != Lend;++it)
	{
		glColor3f(0.1f*it->C.x, 0.1f*it->C.y, 0.1f*it->C.z);
		glVertex3f(it->S.x,it->S.y,it->S.z);
		glColor3f(it->C.x, it->C.y, it->C.z);
		glVertex3f(it->E.x, it->E.y, it->E.z);
	}
	glEnd();
	Tend = O.SST.end();
	glBegin(GL_TRIANGLES);			//draw triangles
	for(auto it = O.SST.begin(); it != Tend; ++it)
	{
		glColor3f(it->C.x, it->C.y, it->C.z);
		glVertex3f(it->P1.x, it->P1.y, it->P1.z);
		glVertex3f(it->P2.x, it->P2.y, it->P2.z);
		glVertex3f(it->P3.x, it->P3.y, it->P3.z);
	}
	glEnd();

	TLend = O.SSTL.end();
	for(auto it = O.SSTL.begin(); it != TLend; ++it)		//textlines
	{
		glColor3f(it->C.x, it->C.y, it->C.z);
		glRasterPos3f(it->Pos.x, it->Pos.y, it->Pos.z);
		for(size_t i = 0; i < it->Text.size();++i)
		{
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, it->Text[i]);
		}
	}
	glutSwapBuffers();
	auto now = std::chrono::high_resolution_clock::now();
	In.dt = std::chrono::duration<float>(now - t0h).count();
	t0h = std::chrono::high_resolution_clock::now();
	
	if(AnimatorActive)
		Animator_.HandleInput(In);
	else
		Game_.HandleInput(In);

	++FrameCount;
	if(FrameCount == 50)
	{
		std::cout << "dt: " << In.dt << std::endl;
		FrameCount = 0;
	}
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(800,500);
	glutInitWindowPosition(100,20);
	glutCreateWindow("CyberHunter");

	glutDisplayFunc(Display);
	glutIdleFunc(Display);
	glutReshapeFunc(Reshape);
	
	glutKeyboardFunc(KeyDown);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(KeySpecialDown);
	glutSpecialUpFunc(KeySpecialUp);
	glutMouseFunc(MouseFunc);
	glutMotionFunc(MousePassiveMove);
	glutPassiveMotionFunc(MousePassiveMove);
	
	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LESS);
	
	Game_.Init();

	t0h = std::chrono::high_resolution_clock::now();
	glutMainLoop();
}
