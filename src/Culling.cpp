#include "Culling.h"

inline bool
CheckIfTriangleIsInfrontOfPlane(Triangle T, Vec3f N, Vec3f P)
{
	if(DOT(N, P - T.P1) >= 0)
		return false;
	if(DOT(N, P - T.P2) >= 0)
		return false;
	if(DOT(N, P - T.P3) >= 0)
		return false;

	return true;
}
inline bool
CheckIfTriangleIsBehindPlane(Triangle T, Vec3f N, Vec3f P)
{
	if(DOT(N, P - T.P1) < 0)
		return false;
	if(DOT(N, P - T.P2) < 0)
		return false;
	if(DOT(N, P - T.P3) < 0)
		return false;

	return true;
}
inline bool
CheckPIsInfrontOfPlane(Vec3f TestP, Vec3f N, Vec3f P)
{
	return(DOT(N, P - TestP) < 0);
}

inline bool
CheckIfLineIsInfrontOfPlane(Line L, Vec3f N, Vec3f P)
{
	return(CheckPIsInfrontOfPlane(L.S, N, P) || CheckPIsInfrontOfPlane(L.E, N, P));
}
inline bool
CheckIfLineIsBehindPlane(Line L, Vec3f N, Vec3f P)
{
	return(!CheckPIsInfrontOfPlane(L.S, N, P) || !CheckPIsInfrontOfPlane(L.E, N, P));
}

inline size_t
CullTriangles(Triangles &Trigs, Vec3f N, Vec3f NL, Vec3f NR, Vec3f P1, Vec3f P2)
{
	Triangles Result;
	Result.reserve(Trigs.size());
	auto END = Trigs.end();
	size_t Culled = 0;
	for(auto it = Trigs.begin(); it != END; ++it)
	{
		if(CheckIfTriangleIsBehindPlane(*it, N, P2) && CheckIfTriangleIsInfrontOfPlane(*it, NL, P1) && CheckIfTriangleIsInfrontOfPlane(*it, NR, P1))
			Result.push_back(*it);
		else
			++Culled;
	}
	Trigs = Result;
	return(Culled);
}
inline size_t
CullLines(Lines &L, Vec3f N, Vec3f NL, Vec3f NR, Vec3f P1, Vec3f P2)
{
	Lines Result;
	Result.reserve(L.size());
	size_t Culled = 0;
	auto END = L.end();
	for(auto it = L.begin(); it != END; ++it)
	{
		if(CheckIfLineIsBehindPlane(*it, N, P2) && CheckIfLineIsInfrontOfPlane(*it, NL, P1) && CheckIfLineIsInfrontOfPlane(*it, NR, P1))
			Result.push_back(*it);
		else
			++Culled;
	}
	L = Result;
	return(Culled);
}