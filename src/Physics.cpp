#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>

#include "Vector\Vector.h"
#include "Physics.h"

void eulerStep(ParticleSystem& ps, float Step)
{
	// Euler integrator.
	State X0 = ps.GetState();		//current state
	State F0 = ps.EvalF(X0);		//forces acting upon current state
	int n = (int)X0.size();
	State X1 = State(n);			//new state
	for(int i = 0;i < n;++i)		//calculate new state after one step
	{
		X1[i] = X0[i] + Step*F0[i];
	}
	ps.SetState(X1);				//set the new state as the current state
}

void trapezoidStep(ParticleSystem& ps, float step) {
	//trapezoid integrator.
	State X0 = ps.GetState();
	State F0 = ps.EvalF(X0);
	int n = (int)X0.size();
	State X1 = State(n);
	State X2 = State(n);
	for(int i = 0; i < n; ++i)
	{
		X1[i] = X0[i] + step*F0[i];
	}
	State F1 = ps.EvalF(X1);

	for(int i = 0; i < n; ++i)
	{
		X2[i] = X0[i] + step*0.5f*(F0[i]+F1[i]);
	}
	ps.SetState(X2);
}

void midpointStep(ParticleSystem& ps, float step)
{
	State X0 = ps.GetState();
	State F0 = ps.EvalF(X0);
	int n = (int)X0.size();
	State Xm = State(n), X1 = State(n);
	for (int i = 0; i < n; ++i)
	{
		Xm[i] = X0[i] + (0.5f * step) * F0[i];
	}
	auto Fm = ps.EvalF(Xm);
	for (int i = 0; i < n; ++i)
	{
		X1[i] = X0[i] + step * Fm[i];
	}
	ps.SetState(X1);
}

void rk4Step(ParticleSystem& ps, float step)
{
	//RK4 Runge-Kutta integrator.
	State X1 = ps.GetState();
	int n = (int)X1.size();
	
	State X2 = State(n);	//states
	State X3 = State(n);
	State X4 = State(n);

	State F1 = State(n);	//forces
	State F2 = State(n);
	State F3 = State(n);
	State F4 = State(n);

	State RK4 = State(n);
	
	F1 = ps.EvalF(X1);

	for(int i = 0; i < n; ++i)
	{
		X2[i] = X1[i] + step*0.5f*F1[i];
	}
	F2 = ps.EvalF(X2);

	for(int i = 0; i < n; ++i)
	{
		X3[i] = X1[i] + step*0.5f*F2[i];
	}
	F3 = ps.EvalF(X3);

	for(int i = 0; i < n; ++i)
	{
		X4[i] = X1[i] + step*F3[i];
	}
	F4 = ps.EvalF(X4);

	for(int i = 0; i < n; ++i)
	{
		RK4[i] = X1[i] + (step/6.0f)*(F1[i] + 2.0f*F2[i] + 2.0f*F3[i] + F4[i]);
	}
	ps.SetState(RK4);
}

inline Vec3f fGravity(float mass)
{
	return Vec3f{0, -9.8f * mass, 0};
}

// force acting on particle at pos1 due to spring attached to pos2 at the other end
inline Vec3f fSpring(const Vec3f& pos1, const Vec3f& pos2, float k, float rest_length)
{
	Vec3f tmp;
	Vec3f X = (pos1 - pos2);
	float len = LEN(X);
	tmp = -k*(len - rest_length)*(X/len);

	return tmp;
}

inline Vec3f fDrag(const Vec3f& v, float k)
{
	Vec3f tmp = -k*v;
	return tmp;
}

//~ void SpringSystem::reset()
//~ {
	//~ const auto start_pos = Vec3f(0.1f, -0.5f, 0.0f);
	//~ const auto spring_k = 30.0f;
	//~ state_ = State(4);
	//~ // YOUR CODE HERE (R2)
	//~ // Set the initial state for a particle system with one particle fixed
	//~ // at origin and another particle hanging off the first one with a spring.
	//~ // Place the second particle initially at start_pos.
	//~ state_[0] = 0;				//position	x
	//~ state_[1] = 0;				//velocity	v
	//~ state_[2] = start_pos;
	//~ state_[3] = 0;
	//~ const auto RestLength = start_pos.length();
	//~ spring_ = Spring(0, 1, spring_k, RestLength);
//~ }
//~ 
//~ State SpringSystem::evalF(const State& state) const {
	//~ const auto drag_k = 0.5f;
	//~ const auto mass = 1.0f;
//~ 
	//~ State f(4);
	//~ // YOUR CODE HERE (R2)
	//~ // Return a derivative for the system as if it was in state "state".
	//~ // You can use the fGravity, fDrag and fSpring helper functions for the forces.
	//~ f[0] = 0;					//velocity		v
	//~ f[1] = 0;					//acceleration	F/m
	//~ f[2] = state[3];
	//~ f[3] = fGravity(mass)+fSpring(state[2], state[0], spring_.k, spring_.rlen)+fDrag(state[3], drag_k) + Vec3f(0, 0, Wind);
	//~ return f;
//~ }
//~ 
//~ Points SpringSystem::getPoints() {
	//~ auto p = Points(2);
	//~ p[0] = state_[0]; p[1] = state_[2];
	//~ return p;
//~ }
//~ 
//~ Lines SpringSystem::getLines() {
	//~ auto l = Lines(2);
	//~ l[0] = state_[0]; l[1] = state_[2];
	//~ return l;
//~ }

void PendulumSystem::Reset()
{
	const float spring_k = 1000.0f;
	//~ const Vec3f start_point = Vec3f{0,0,0};
	const Vec3f end_point = Vec3f{0.05f, -1.5f, 0.0f};
	state_ = State(2*n_);
	// Set the initial state for a pendulum system with n_ particles
	// connected with springs into a chain from start_point to end_point.
	Spring S;
	const Vec3f Step = end_point/(float)(n_-1);		//dividing the chain into segments
	S.RLen = LEN(Step);							//same for all of them
	S.K = spring_k;
	for(unsigned int i = 0; i < n_; ++i)
	{
		state_[i*2] = (int)i*Step;		//position	x
		state_[i*2+1] = Vec3f{0,0,0};	//velocity	v
		if(i+1 < n_)
		{
			S.I1 = i;
			S.I2 = i+1;
			springs_.push_back(S);
		}
	}

}


State PendulumSystem::EvalF(const State& state) const
{
	const float drag_k = 0.5f;
	const float mass = 0.5f;
	State f = State(2*n_);
	//Return a derivative of the system state "state".
	for(unsigned int i = 1; i < n_; ++i)
	{
		f[i*2] = state[i*2+1];		//velocity	v
		
		f[i*2+1] = (fGravity(mass)+fSpring(state[i*2], state[i*2-2], springs_.at(i-1).K, springs_.at(i-1).RLen)+fDrag(state[i*2+1], drag_k))/mass;		//acceleration	F/m
		if(i+1 < n_)				//second spring
			f[i*2+1] +=	fSpring(state[i*2], state[i*2+2], springs_.at(i).K, springs_.at(i).RLen)/mass;
	}

	return f;
}

Points PendulumSystem::GetPoints()
{
	Points p = Points(n_);
	for (auto i = 0u; i < n_; ++i)
	{
		p[i] = state_[i*2];
	}
	return p;
}

//~ void ClothSystem::reset() {
	//~ const auto spring_k = 300.0f;
	//~ const auto width = 1.5f, height = 1.5f; // width and height of the whole grid
	//~ state_ = State(2*x_*y_);
	//~ // YOUR CODE HERE (R5)
	//~ // Construct a particle system with a x_ * y_ grid of particles,
	//~ // connected with a variety of springs as described in the handout:
	//~ // structural springs, shear springs and flex springs.
//~ 
	//~ Objects = std::vector<Sphere>(1);		//adding a sphere
	//~ 
	//~ Spring Sx, Sy, Ss, Sfx, Sfy;		//all types of springs
	//~ //auto StepX = Vec3f(LBorder, 0, 0)/(x_-1);
	//~ //auto StepY = Vec3f(LBorder, 0, 0)/(y_-1);
	//~ auto StepX = width/(x_-1);
	//~ auto StepY = height/(y_-1);
	//~ auto Start = Vec3f(-width/2, 0, 0);				//top left corner
//~ 
	//~ Sx.rlen = StepX;								// structural springs
	//~ Sx.k = spring_k;
	//~ Sx.i1 = 0;
	//~ Sx.i2 = 0;
	//~ Sy.rlen = StepY;
	//~ Sy.k = spring_k;
	//~ Sy.i1 = 0;
	//~ Sy.i2 = 0;
//~ 
	//~ Ss.rlen = std::sqrt(StepX*StepX + StepY*StepY);	// shear springs
	//~ Ss.k = spring_k;
	//~ Ss.i1 = 0;
	//~ Ss.i2 = 0;
//~ 
	//~ Sfx.rlen = StepX*2;								// flex springs
	//~ Sfx.k = spring_k;
	//~ Sfy.rlen = StepY*2;
	//~ Sfy.k = spring_k;
	//~ Sfx.i1 = 0;
	//~ Sfx.i2 = 0;
	//~ Sfy.i1 = 0;
	//~ Sfy.i2 = 0;
//~ 
	//~ springs_.push_back(Sx);			//UGLY HACK: adding the values(k and lenghts) of the springs as the first five springs of the vector
	//~ springs_.push_back(Sy);
	//~ springs_.push_back(Ss);
	//~ springs_.push_back(Sfx);
	//~ springs_.push_back(Sfy);
//~ 
	//~ for(int iy = 0; iy < y_; ++iy)
	//~ {
		//~ for(int ix = 0; ix < x_; ++ix)
		//~ {
			//~ state_[iy*x_*2 + ix*2] =Start + Vec3f(StepX * ix, 0, -StepY * iy);		//position
			//~ state_[iy*x_*2 + ix*2 +1] = 0;											//velocity
			//~ 
			//~ if(ix + 1 < x_)							// structural springs
			//~ {
				//~ Sx.i1 = iy*x_ + ix;
				//~ Sx.i2 = iy*x_ + ix +1;
				//~ springs_.push_back(Sx);
			//~ }
			//~ if(iy + 1 < y_)
			//~ {
				//~ Sy.i1 = iy*x_ + ix;
				//~ Sy.i2 = (iy+1)*x_ + ix;
				//~ springs_.push_back(Sy);
			//~ }
			//~ 
			//~ if((ix + 1 < x_)&&(iy + 1 < y_))		// shear springs
			//~ {
				//~ Ss.i1 = iy*x_ + ix;
				//~ Ss.i2 = (iy+1)*x_ + ix +1;
				//~ springs_.push_back(Ss);
			//~ }
			//~ if((ix - 1 >= 0)&&(iy + 1 < y_))
			//~ {
				//~ Ss.i1 = iy*x_ + ix;
				//~ Ss.i2 = (iy+1)*x_ + ix-1;
				//~ springs_.push_back(Ss);
			//~ }
			//~ if(ix + 2 < x_)							// flex springs
			//~ {
				//~ Sfx.i1 = iy*x_ + ix;
				//~ Sfx.i2 = iy*x_ + ix +2;
				//~ springs_.push_back(Sfx);
			//~ }
			//~ if(iy + 2 < y_)
			//~ {
				//~ Sfy.i1 = iy*x_ + ix;
				//~ Sfy.i2 = (iy+2)*x_ + ix;
				//~ springs_.push_back(Sfy);
			//~ }
		//~ }
	//~ }
//~ }
//~ 
//~ State ClothSystem::evalF(const State& state) const {
	//~ const auto drag_k = 0.08f;
	//~ const auto n = x_ * y_;
	//~ static const auto mass = 0.025f;
	//~ auto f = State(2*n);
	//~ // YOUR CODE HERE (R5)
	//~ // This will be much like in R2 and R4.
	//~ float Sk = springs_[0].k; //same for all
	//~ float Sxl = springs_[0].rlen;	//lengths for different types of springs
	//~ float Syl = springs_[1].rlen;
	//~ float Ssl = springs_[2].rlen;
	//~ float Sfxl = springs_[3].rlen;
	//~ float Sfyl = springs_[4].rlen;
//~ 
	//~ for(int iy = 0; iy < y_; ++iy)
	//~ {
		//~ for(int ix = 0; ix < x_; ++ix)
		//~ {
			//~ Vec3f F = 0;
			//~ Vec3f V = state[iy*x_*2 + ix*2 +1];
			//~ 
//~ 
			//~ if(ix+1 < x_)																			//x+
			//~ {
				//~ F += fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix+1)*2], Sk, Sxl);			//struct
				//~ if(iy+1 < y_)
					//~ F += fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + (ix+1)*2], Sk, Ssl);	//shear down
				//~ if(iy-1 >= 0)
					//~ F += fSpring(state[iy*x_*2 + ix*2], state[(iy-1)*x_*2 + (ix+1)*2], Sk, Ssl);	//shear up
				//~ if(ix+2 < x_)
					//~ F += fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix+2)*2], Sk, Sfxl);		//flex
//~ 
			//~ }
			//~ if(ix-1 >= 0)																			//x-
			//~ {
				//~ F += fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix-1)*2], Sk, Sxl);			//struct
				//~ if(iy+1 < y_)
					//~ F += fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + (ix-1)*2], Sk, Ssl);	//shear down
				//~ if(iy-1 >= 0)
					//~ F += fSpring(state[iy*x_*2 + ix*2], state[(iy-1)*x_*2 + (ix-1)*2], Sk, Ssl);	//shear up
				//~ if(ix-2 >= 0)
					//~ F += fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix-2)*2], Sk, Sfxl);		//flex
//~ 
			//~ }
			//~ if(iy+1 < y_)																			//y+
			//~ {
				//~ F += fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + ix*2], Sk, Syl);			//struct
				//~ if(iy+2 < y_)
					//~ F += fSpring(state[iy*x_*2 + ix*2], state[(iy+2)*x_*2 + ix*2], Sk, Sfyl);		//flex
			//~ }
			//~ if(iy-1 >= 0)																			//y-
			//~ {
				//~ F += fSpring(state[iy*x_*2 + ix*2], state[(iy-1)*x_*2 + ix*2], Sk, Syl);			//struct
				//~ if(iy-2 >= 0)
					//~ F += fSpring(state[iy*x_*2 + ix*2], state[(iy-2)*x_*2 + ix*2], Sk, Sfyl);		//flex
			//~ }
			//~ f[iy*x_*2 + ix*2 +1] = (F + fGravity(mass)+fDrag(V, drag_k))/mass + Vec3f(0, 0, Wind);	//force/mass
			//~ 
			//~ if(Collisions)
			//~ for(Sphere Object : Objects)
			//~ {
				//~ Vec3f P1 = state[iy*x_*2 + ix*2];
				//~ Vec3f P2 = Object.Origin;
				//~ Vec3f D = P1-P2;
				//~ if(D.length() < Object.Radius)			//collision check
				//~ {
					//~ //float tmp = std::abs(V.dot(D/D.length()));		//simple collision abs makes it always point outwards from the sphere
					//~ //V += 2.0f*D/D.length()*(tmp);
					//~ V = D/D.length();									//move the cloth outward
					//~ //f[iy*x_*2 + ix*2 +1] += fDrag(V, drag_k)/mass;
				//~ }
			//~ }
//~ 
			//~ 
			//~ f[iy*x_*2 + ix*2] = V;				//velocity
//~ 
			//~ //Vec3f Tmp = 0;																		//tried only calculating spring forces only once per spring but it was slower than the above
			//~ //if(ix+1 < x_)																			//x+
			//~ //{
			//~ //	Tmp = fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix+1)*2], Sk, Sxl)/mass;		//struct
			//~ //	f[iy*x_*2 + ix*2 +1] += Tmp;														//force on this point
			//~ //	f[iy*x_*2 + (ix+1)*2 +1] -= Tmp;													//force on other point
			//~ //	if(iy+1 < y_)
			//~ //	{
			//~ //		Tmp = fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + (ix+1)*2], Sk, Ssl)/mass;	//shear down
			//~ //		f[iy*x_*2 + ix*2 +1] += Tmp;
			//~ //		f[(iy+1)*x_*2 + (ix+1)*2 +1] -= Tmp;
			//~ //	}
			//~ //	if(ix+2 < x_)
			//~ //	{
			//~ //		Tmp = fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix+2)*2], Sk, Sfxl)/mass;	//flex
			//~ //		f[iy*x_*2 + ix*2 +1] += Tmp;
			//~ //		f[iy*x_*2 + (ix+2)*2 +1] -= Tmp;
			//~ //	}
			//~ //	
			//~ //}
			//~ //if(ix-1 >= 0)																			//x-
			//~ //{
			//~ //	if(iy+1 < y_)
			//~ //	{
			//~ //		Tmp = fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + (ix-1)*2], Sk, Ssl)/mass;	//shear down
			//~ //		f[iy*x_*2 + ix*2 +1] += Tmp;
			//~ //		f[(iy+1)*x_*2 + (ix-1)*2 +1] -= Tmp;
			//~ //	}
			//~ //}
			//~ //if(iy+1 < y_)																			//y+
			//~ //{
			//~ //	Tmp = fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + ix*2], Sk, Syl)/mass;			//struct
			//~ //	f[iy*x_*2 + ix*2 +1] += Tmp;
			//~ //	f[(iy+1)*x_*2 + ix*2 +1] -= Tmp;
			//~ //	if(iy+2 < y_)
			//~ //	{
			//~ //		Tmp = fSpring(state[iy*x_*2 + ix*2], state[(iy+2)*x_*2 + ix*2], Sk, Sfyl)/mass;		//flex
			//~ //		f[iy*x_*2 + ix*2 +1] += Tmp;
			//~ //		f[(iy+2)*x_*2 + ix*2 +1] -= Tmp;
			//~ //	}
			//~ //}
			//~ //f[iy*x_*2 + ix*2 +1] += (fGravity(mass)+fDrag(V, drag_k))/mass;					//force/mass
		//~ }
	//~ }
	//~ f[0] = 0;		//corners fixed
	//~ f[0+1] = 0;
//~ 
	//~ f[(x_-1)*2] = 0;
	//~ f[(x_-1)*2 +1] = 0;
	//~ return f;
//~ }
