#pragma once
#include "Containers.h"

struct NavMeshNode
{
	Vec3f						Center;
	std::vector<Vec3f>			Corners;
	std::vector<NavMeshNode*>	Neighbours;
};

class NavMesh
{
public:
	void						AddNavMeshPiece();
	void						AddQuad(Vec3f LB, Vec3f RB, Vec3f RT, Vec3f LT);
	Vec3f						GetNextWaypoint(Vec3f CurrentPos, Vec3f Target);
	NavMeshNode*				GetNavMeshNode(Vec3f CurrentPos);
private:

	std::vector<NavMeshNode>	NavNodes_;
};

enum BehaviourType
{
	BehaviourIdle,
	BehaviourAimlessWander
};
enum AnimationType
{
	AnimationIdle,
	AnimationWalking
};

struct CharacterState
{
	Vec3f			CurrentGoal;
	AnimationType	CurrentAnimation;
};

class World;

class AIClass
{
public:
	AIClass()									{CurrentBehaviour_ = BehaviourAimlessWander; CurrentGoal_ = {0};}
	CharacterState	GetCurrentState(Vec3f CurrentPos);
	void			SetCurrentGoal(Vec3f P)		{CurrentGoal_ = P;}
	void			SetWorld(World *W)			{World_ = W;}
private:
	Vec3f			CurrentGoal_;
	BehaviourType	CurrentBehaviour_;
	World*			World_ = nullptr;
};