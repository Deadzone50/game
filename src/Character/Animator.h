﻿#pragma once
#include "Character.h"
#include "Containers.h"

class Animator
{
public:
	Animator()	{}
	void				Init();
	MeshOutput&			GetMeshOutput();
	void				HandleMouse(Input &In);
	void				HandleButtons(Input &In);
	//void				Update();
	void				Reset();


	void				Step(float dt);

	Player				*GetPlayer() {return &Player_;};

private:
	MeshOutput			Output_;

	Player				Player_;
	NPC					CurrentModel_;

	Mesh				CurrentMesh_;
	Skeleton			CurrentSkeleton_;
	Mesh				LineMesh_;
	Mesh				PointMesh_;
	std::vector<Vec2f>	ColorCoords;


	int					CurrentJointI_ = -1;
	float				MoveSpeed_ = 1;
	int					CurrentSurface_ = 0;
	bool				MouseInit_ = true;
};