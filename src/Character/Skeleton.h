#pragma once
#include <vector>

#include "Containers.h"
#include "Vector/Matrix.h"
#include "Vector/Vector.h"



class Joint
{
public:
	void	Draw(Points &DP, Lines &DL);
	Vec3f	GetWorldPos();
	Vec3f	GetRotation()			{return Rotation_;}
	Vec3f	GetPosition()			{return Position_;}

	void	SetRotation(Vec3f Rot)	{Rotation_ =  Rot;}		//in parents cordinate system
	void	Rotate(Vec3f Rot)		{Rotation_ += Rot;}		//in parents cordinate system
	void	Rotate(Vec3f Axis, float Angle);				//in parents cordinate system
	void	SetPosition(Vec3f Pos)	{Position_ =  Pos;}		//in parents cordinate system
	void	Move(Vec3f Pos)			{Position_ += Pos;}		//in parents cordinate system

	void	SetLenghtFromParent(float Len)		{LenghtFromParent_ = Len;}
	void	SetLenghtFromParent()				{LenghtFromParent_ = LEN(Position_);}
	float	GetLenghtFromParent()				{return LenghtFromParent_;}

	int		GetParentIndex()					{return Parent_;}
	void	SetParentIndex(int P)				{Parent_ = P;}

	void	AddPoint(size_t P, float Weight)	{ConnectedPoints_.push_back(P); PointWeights_.push_back(Weight);}
	size_t	GetNumberOfConnections()			{return ConnectedPoints_.size();}
	size_t	GetConnectedPointIndex(int index)	{return ConnectedPoints_[index];}
	float	GetConnectedPointWeight(int index)	{return PointWeights_[index];}

	void	SetToParent(Mat4f TP)				{ToParent_ = TP;}
	void	SetToWorld(Mat4f TW)				{ToWorld_ = TW;}
	void	SetToBind(Mat4f TB)					{ToBind_ = TB;}

	Mat4f	GetToWorld()						{return ToWorld_;}
	Mat4f	GetToBind()							{return ToBind_;}

	Vec3f	GetRotationDiff()					{return RotationDiff_;}
private:
	Mat4f ToWorld_;							//transform to world cordinates
	Mat4f ToBind_;							//transform to bind pose
	Mat4f ToParent_;						//transform to parents cordinate system

	int Parent_ = -1;
	float LenghtFromParent_;
	std::vector<size_t>		ConnectedPoints_;
	std::vector<float>		PointWeights_;

	Vec3f Position_ = Vec3f{0,0,0};
	Vec3f Rotation_ = Vec3f{0,0,0};
	Vec3f RotationDiff_ = Vec3f{0,0,0};
	Vec3f Color_ = Vec3f{1,1,1};
};

class Skeleton
{
public:
	Skeleton() {};
	void				Draw(Points &P, Lines &L);

	void				SolveIKVerlet(int Start, int End, int Iterations);
	void				SolveIKCRD(Vec3f Target, int EndI, int RootI, int Iterations);

	size_t				GetNumJoints()		{return Joints_.size();}

	void				SetJointPosition(int index, Vec3f Pos, bool IK);
	void				MoveJoint(int index, Vec3f RelativePos, bool IK);

	Vec3f				GetJointPosition(int index)							{return Joints_[index].GetWorldPos();}
	Vec3f				GetJointRotation(int index)							{return Joints_[index].GetRotation();}
	void				SetJointRotation(int index, Vec3f Rot)				{Joints_[index].SetRotation(Rot);}
	void				RotateJoint(int index, Vec3f Rot)					{Joints_[index].Rotate(Rot);}
	void				RotateJoint(int index, Vec3f Axis, float Angle)		{Joints_[index].Rotate(Axis, Angle);}
	Joint&				GetJoint(int index)									{return Joints_[index];}
	void				AddJoint(Joint &J)									{Joints_.push_back(J);}

	void				UpdateJoints();
	std::vector<Mat4f>	GetSSDTransforms();
	void				ComputeToBind();

	void								SaveKeyFrame(int AnimationNumber);
	void								StepAnimation(int AnimationNumber, float dt);
	void								SaveAnimation(int AnimationNumber, std::string Name);
	void								LoadAnimation(size_t AnimationNumber, std::string Name);
	void								ClearAnimation(int AnimationNumber);
private:
	std::vector<std::vector<Vec3fV>>	KeyFrames_;	
	int									CurrentFrame_ = 0;
	int									StepsOnFrame_ = 0;
	bool								ReverseAnimation_ = false;
	std::vector<Joint>	Joints_;
};

Skeleton CreateSkeleton(Mesh &M);