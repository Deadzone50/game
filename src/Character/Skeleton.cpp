#include <iostream>
#include <fstream>

#include "Mesh.h"
#include "Skeleton.h"
#include "GlobalDefines.h"
#include "GLHelperFunctions.h"


extern DebugPLManager PLManager;

void
UpdateJoint(std::vector<Joint> &Joints, Joint &J)
{
	//J.ToParent_ = Mat4f();
	//J.ToParent_.Rotate(J.Rotation_);				//rotate, in parents cordinate system	//J.Rotation_ = {0,0,0};

	//J.ToParent_.Translate(J.Position_);				//translate, in parents cordinate system

	//Mat4f ParentToWorld;							//update the transform from object cordinates to world cordinates
	//if(J.Parent_ != -1)
	//	ParentToWorld = Joints[J.Parent_].ToWorld_;

	//J.ToWorld_ = ParentToWorld*J.ToParent_;

	Mat4f ToParent;
	ToParent.Rotate(J.GetRotation());				//rotate, in parents cordinate system
	ToParent.Translate(J.GetPosition());			//translate, in parents cordinate system

	Mat4f ParentToWorld;							//update the transform from object cordinates to world cordinates
	int Parent = J.GetParentIndex();
	if(Parent != -1)
		ParentToWorld = Joints[Parent].GetToWorld();

	J.SetToWorld(ParentToWorld*ToParent);
}
void
Skeleton::MoveJoint(int index, Vec3f RelativePos, bool IK)
{
	if(IK && index)
	{
		Vec3f Pos = GetJointPosition(index)+RelativePos;
		SolveIKCRD(Pos, index, 0, 1);
	}
	else
	{
		Vec4f Move = INVERTaffine(GetJoint(index).GetToWorld()) * Vec4f{RelativePos, 1};
		Vec3f P = Move.GetXYZ();
		P = P + GetJointPosition(index);
		GetJoint(index).Move(P);
	}
}
void
Skeleton::SetJointPosition(int index, Vec3f Pos, bool IK)
{
	if(IK && index)
	{
		SolveIKCRD(Pos, index, 0, 3);
	}
	else
	{
		Vec3f WP = GetJointPosition(index);
		Vec3f RelativePos = Pos - WP;

		Vec4f Move = INVERTaffine(GetJoint(index).GetToWorld()) * Vec4f{RelativePos, 1};
		Vec3f P = Move.GetXYZ();
		P = P + GetJointPosition(index);
		GetJoint(index).Move(P);
	}
}
void
Skeleton::SolveIKVerlet(int Start, int End, int Iterations)		//start is the end of the limb
{
	for(int i = 0; i < Iterations; ++i)
	{

		int CurrentJoint = Start;
		while(true)
		{
			UpdateJoints();
			int ParentJoint = Joints_[CurrentJoint].GetParentIndex();
			Vec3f JointPos = Joints_[CurrentJoint].GetWorldPos();
			Vec3f ParentPos = Joints_[ParentJoint].GetWorldPos();

			Vec3f Diff = JointPos - ParentPos;						//calculate distance and direction between points
			float Dist = LEN(Diff);
			Vec3f f;
			if(Dist)												//avoid divide by zero
				f = (Joints_[CurrentJoint].GetLenghtFromParent() - Dist)*(Diff/Dist);
			else
				f = {0,0,0};

			if(ParentJoint != End)
			{
				MoveJoint(CurrentJoint, 0.5f*f, false);
				MoveJoint(ParentJoint, -0.5f*f, false);
			}
			else
			{
				MoveJoint(CurrentJoint, f, false);
				return;
			}
			CurrentJoint = ParentJoint;
		}
	}
}
void
Skeleton::SolveIKCRD(Vec3f Target, int EndI, int RootI, int Iterations)		//Cyclic coordinate decent, from end to root
{
	//PLManager.ClearPoints();
	//PLManager.ClearLines();
	//PLManager.DrawPoint(Target, {1,0,0});

	for(int i = 0; i < Iterations; ++i)
	{
		bool InLine = true;
		int CurrentJointI = GetJoint(EndI).GetParentIndex();

		while(true)
		{
			if(CurrentJointI == 0)
				break;
			UpdateJoints();
			Vec3f EndPos = GetJoint(EndI).GetWorldPos();

			//PLManager.DrawPoint(EndPos, {0,1,0});

			Joint &CurrentJoint = GetJoint(CurrentJointI);
			Vec3f JointPos = CurrentJoint.GetWorldPos();
			
			Vec3f PosToTarget = NORMALIZE(Target - JointPos);
			Vec3f PosToEnd = NORMALIZE(EndPos - JointPos);

			//PLManager.DrawLine(JointPos, Target, {1,0,0});
			//PLManager.DrawLine(JointPos, EndPos,{0,1,0});

			float D = DOT(PosToTarget, PosToEnd);
			if(D > 1)
				D = 1;									//fix floating point errors
			float Angle = acosf(D);													//acos(|A||B|cos(a))
			Vec3f RotationAxis = NORMALIZE(CROSS(PosToTarget, PosToEnd));			//|A||B|sin(a)*n
			if(Angle == Angle)							//check for nan infinity 
			{
				if(!InLine || (Angle != 0 && Angle != PI/2))
				{
					InLine = false;
				}
				if(Angle)
					CurrentJoint.Rotate(RotationAxis, Angle);

				CurrentJointI = CurrentJoint.GetParentIndex();
			}
			//else
			//{
			//	int a = 0;
			//}

			//break;
		}
		//if(InLine && (GetJoint(EndI).GetParentIndex()))
		//{
		//	GetJoint(GetJoint(EndI).GetParentIndex()).Rotate(NORMALIZE({1,1,1}), 1);
		//}
	}
}
void
Joint::Rotate(Vec3f U, float Angle)
{
	float Cos = cos(Angle);
	float Sin = sin(Angle);
	Rotation_.y += atan2(U.y * Sin- U.x * U.z * (1 - Cos) , 1 - (pow(U.y,2) + pow(U.z,2)) * (1 - Cos));
	Rotation_.z += asin(U.x * U.y * (1 - Cos) + U.z * Sin) ;
	Rotation_.x += atan2(U.x * Sin-U.y * U.z * (1 - Cos) , 1 - (pow(U.x,2) + pow(U.z,2)) * (1 - Cos));
}
Vec3f
Joint::GetWorldPos()
{
	Vec3f P{ToWorld_.m[3], ToWorld_.m[7], ToWorld_.m[11]};
	return P;
}
void
Joint::Draw(Points &DP, Lines &DL)				//draws the joint position and axes
{
	Mat4f T = ToWorld_;

	Vec3f P{T.m[3], T.m[7], T.m[11]};				//draw Pos
	DP.push_back(Point{P,Color_});

	Vec3f E = P + 0.3f*Vec3f{T.m[0],T.m[4],T.m[8]};	//draw axes
	DL.push_back(Line{P,E,{1,0,0}});

	E = P + 0.3f*Vec3f{T.m[1],T.m[5],T.m[9]};
	DL.push_back(Line{P,E,{0,1,0}});

	E = P + 0.3f*Vec3f{T.m[2],T.m[6],T.m[10]};
	DL.push_back(Line{P,E,{0,0,1}});
}
void
Skeleton::UpdateJoints()						//update rotation position and world transform
{
	for(auto it = Joints_.begin();it!=Joints_.end();++it)
	{
		UpdateJoint(Joints_, *it);
	}
}
void
Skeleton::ComputeToBind()						//compute transform matrix to bind pose per Joint
{
	for(auto J = Joints_.begin();J != Joints_.end(); ++J)
	{
		Mat4f T = J->GetToWorld();
		Mat4f B = INVERTaffine(T);
		J->SetToBind(B);
	}
}
std::vector<Mat4f>
Skeleton::GetSSDTransforms()						//get the transforms from bind to current pose
{
	UpdateJoints();
	std::vector<Mat4f> Transforms(Joints_.size());
	for(size_t i = 0; i < Joints_.size(); ++i)
	{
		Mat4f T = Joints_[i].GetToWorld() * Joints_[i].GetToBind();
		Transforms[i] = T;
	}
	return Transforms;
}
void
Skeleton::Draw(Points &DP, Lines &DL)
{
	for(auto it = Joints_.begin(); it != Joints_.end(); ++it)
	{
		it->Draw(DP, DL);					//draw joints
		if(it->GetParentIndex() != -1)		//draw lines between joints
		{
			//Vec3f S{it->ToWorld_.m[3], it->ToWorld_.m[7], it->ToWorld_.m[11]};
			Vec3f S = it->GetWorldPos();
			Vec3f E = Joints_[it->GetParentIndex()].GetWorldPos();
			DL.push_back(Line{S,E,{1,1,1}});
		}
	}
}
void
Skeleton::SaveKeyFrame(int AnimationNumber)					//save the current pose (rotations)
{
	size_t Size = Joints_.size();
	std::vector<Vec3f> Rotations(Size);
	for(size_t i = 0; i < Size; ++i)
	{
		Rotations[i] = Joints_[i].GetRotation();
	}
	if(AnimationNumber >= (int)KeyFrames_.size())
		KeyFrames_.resize(AnimationNumber+1);
	KeyFrames_[AnimationNumber].push_back(Rotations);
}
void
Skeleton::StepAnimation(int AnimationNumber, float dt)		//assumes to be in first frame blends into second if started fresh
{
	(void)dt;
	int Steps = 10;

	size_t Size = Joints_.size();
	int NextFrame;
	if(CurrentFrame_ == (int)KeyFrames_[AnimationNumber].size()-1)		//last frame
	{
		ReverseAnimation_ = true;
	}
	else if(CurrentFrame_ == 0)											//first frame
	{
		ReverseAnimation_ = false;
	}
	if(!ReverseAnimation_)
		NextFrame = CurrentFrame_+1;
	else
		NextFrame = CurrentFrame_-1;

	std::vector<Vec3f> NewRotations = KeyFrames_[AnimationNumber][NextFrame];
	for(size_t i = 0; i < Size; ++i)
	{
		for(int i2 = 0; i2 < 3; ++i2)									//go through x,y,z			
		{
			if(Joints_[i].GetRotation().V[i2] != NewRotations[i].V[i2])
			{
				if(StepsOnFrame_ == 0)
					Joints_[i].GetRotationDiff().V[i2] = (NewRotations[i].V[i2]-Joints_[i].GetRotation().V[i2]);
				Joints_[i].GetRotation().V[i2] += Joints_[i].GetRotationDiff().V[i2]/Steps;
			}
		}
	}
	++StepsOnFrame_;
	if(StepsOnFrame_ >= Steps)
	{
		CurrentFrame_ = NextFrame;
		StepsOnFrame_ = 0;
	}
}
void
Skeleton::ClearAnimation(int AnimationNumber)
{
	KeyFrames_[AnimationNumber].clear();
	CurrentFrame_ = 0;
	StepsOnFrame_ = 0;
	ReverseAnimation_ = false;
}
Joint
CreateJoint(Vec3f Dir, int ParentI, float &t, float Ratio, Mesh &M, Vec3f ParentWorldP)
{
	Joint J;
	M.RayCollisionTest(ParentWorldP, Dir, t);

	J.SetPosition(Ratio*t*Dir);											//relative to parent
	J.SetLenghtFromParent(); 
	J.SetParentIndex(ParentI);

	Vec3f Pos = ParentWorldP + Ratio*t*Dir;
	float Dist = FLT_MAX;

	size_t NumPoints = M.Vertices_.size();
	for(size_t i = 0; i < NumPoints; ++i)				//pick points within 1.2 times the distance to the closest point
	{
		Vec3f PPos = M.Vertices_[i].Position;
		float D = LEN(Pos - PPos);
		if(D < Dist)
			Dist = D;
	}
	for(size_t i = 0; i < NumPoints; ++i)
	{
		Vec3f PPos = M.Vertices_[i].Position;
		float D = LEN(Pos - PPos);
		if(D < 1.2f*Dist)
		{
			J.AddPoint(i, 1);
		}
	}
	return J;
}
Skeleton
CreateSkeleton(Mesh &M)
{
	Skeleton Result;
		
	Vec3f L = {-1,0,0};
	Vec3f R = {1,0,0};
	Vec3f U = {0,1,0};
	Vec3f D = {0,-1,0};
	
	int ParentI;
	float t;
	Joint J;
	Vec3f ParentPos = Vec3f{0.00001f};

	Result.AddJoint(J);						//root node center of object
	Result.GetJoint(0).SetPosition(ParentPos);
		
	ParentI = 0;
	Result.UpdateJoints();
	t = FLT_MAX;
	J = CreateJoint(U, ParentI, t, 0.75f, M, ParentPos);		//head node
	Result.AddJoint(J);
	
	t = FLT_MAX;
	J = CreateJoint(L, ParentI, t, 0.14f, M, ParentPos);		//left shoulder
	Result.AddJoint(J);
	
	ParentI = (int)Result.GetNumJoints()-1;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;
	J = CreateJoint(L, ParentI, t, 0.5f, M, ParentPos);						//left elbow
	Result.AddJoint(J);
	
	ParentI = (int)Result.GetNumJoints()-1;
	
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;
	J = CreateJoint(L, ParentI, t, 1, M, ParentPos);						//left hand
	Result.AddJoint(J);
	
	ParentI = 0;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;
	J = CreateJoint(R, ParentI, t, 0.14f, M, ParentPos);				//right shoulder
	Result.AddJoint(J);
	
	ParentI = (int)Result.GetNumJoints()-1;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;
	J = CreateJoint(R, ParentI, t, 0.5f, M, ParentPos);				//right elbow
	Result.AddJoint(J);
	
	ParentI = (int)Result.GetNumJoints()-1;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;
	J = CreateJoint(R, ParentI, t, 1, M, ParentPos);						//right hand
	Result.AddJoint(J);
	
	ParentI = 0;
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;
	J = CreateJoint(D, ParentI, t, 1, M, ParentPos);						//groin
	int G = (int)Result.GetNumJoints();
	J.SetPosition((t-0.01f)*D);
	Result.AddJoint(J);
		
	ParentI = G;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;										//left leg upper node
	J = CreateJoint(L, ParentI, t, 0.6f, M, ParentPos);
	Result.AddJoint(J);
	
	ParentI = (int)Result.GetNumJoints()-1;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;										//left knee node
	J = CreateJoint(D, ParentI, t, 0.5f, M, ParentPos);					
	Result.AddJoint(J);
	
	ParentI = (int)Result.GetNumJoints()-1;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;										//left foot node
	J = CreateJoint(D, ParentI, t, 1, M, ParentPos);
	Result.AddJoint(J);
	
	
	ParentI = G;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;										//right leg upper node
	J = CreateJoint(R, ParentI, t, 0.6f, M, ParentPos);
	Result.AddJoint(J);
	
	ParentI = (int)Result.GetNumJoints()-1;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;										//right knee node
	J = CreateJoint(D, ParentI, t, 0.5f, M, ParentPos);
	Result.AddJoint(J);
		
	ParentI = (int)Result.GetNumJoints()-1;
	Result.UpdateJoints();
	ParentPos = Result.GetJointPosition(ParentI);
	t = FLT_MAX;										//right foot node
	J = CreateJoint(D, ParentI, t, 1, M, ParentPos);
	Result.AddJoint(J);
	
	Result.UpdateJoints();
	//Skeleton_.Draw(DebugPoints_, DebugLines_);

	Result.ComputeToBind();
	return Result;
}