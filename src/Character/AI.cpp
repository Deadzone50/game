#include "AI.h"
#include "Collision/CollisionTestsBasic.h"

void
NavMesh::AddQuad(Vec3f LB, Vec3f RB, Vec3f RT, Vec3f LT)
{

}
Vec3f
NavMesh::GetNextWaypoint(Vec3f CurrentPos, Vec3f Target)
{
	return Target;
}
NavMeshNode*
NavMesh::GetNavMeshNode(Vec3f CurrentPos)
{
	float MaxLen = FLT_MAX;
	for(auto it = NavNodes_.begin(); it != NavNodes_.end(); ++it)		//assumes there is only one hit
	{
		if(it->Corners.size() == 3)
		{
			if(RayTriangleIntersection(it->Corners[0], it->Corners[1], it->Corners[2], CurrentPos, {0,-1,0}, MaxLen))
				return &*it;
		}
	}
	return nullptr;
}