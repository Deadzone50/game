#pragma once
#include <vector>
#include "Vector/Vector.h"
#include "Vector/Matrix.h"
#include "Skeleton.h"
#include "Object/PhysObject.h"
#include "AI.h"

class Character : public MeshObject
{
public:
	Character() {}
	Character(Vec3f Pos, Vec3f Rot, Mesh &M, Skeleton &S);
	~Character()	{DeleteBuffers();}

	void			DeleteBuffers();

	void			MoveForward(float Speed)					{Position_ += Speed*Direction_;}
	void			MoveToward(Vec3f Target, float Speed);
	void			SetPosition(Vec3f Pos)						{Position_ = Pos;}
	void			SetDirection(Vec3f Dir);
	Vec3f			GetPosition()								{return Position_;}
	Vec3f			GetDirection()								{return Direction_;}
	Vec3f			GetUp()										{return Up_;}
	Vec3f			GetRight()									{return Right_;}

	void			MoveJoint(int index, Vec3f RelativePos)		{Skeleton_.MoveJoint(index, RelativePos, true);}
	void			RotateJoint(int i, Vec3f Axis, float Angle)	{Skeleton_.RotateJoint(i, Axis, Angle);}
	void			RotateJoint(int i, Vec3f Rot)				{Skeleton_.RotateJoint(i, Rot);}
	void			SetJointPosition(int index, Vec3f Pos)		{Skeleton_.SetJointPosition(index, Pos, true);}
	Vec3f			GetJointPosition(int index)					{return Skeleton_.GetJointPosition(index);}

	void			DrawSkeleton();
	void			DrawJoint(int JointIndex, Points &DP, Lines &DL);
	int				RaySkeletonTest(Vec3f RayOrigin, Vec3f RayDir, float& MaxLen);

	Skeleton*		GetSkeleton()								{return &Skeleton_;}

	void			PlayAnimation(float dt);
	void			Step(float dt);

	Vec3f			GetMeshPointPosition(size_t PointIndex);
	bool			RayMeshCollisionTest(Vec3f RayOrigin, Vec3f RayDir, float &MaxLen);

	void			SetupMesh();
	size_t			Draw();
protected:
	Vec3f					Position_, Direction_, Rotation_, Up_, Right_;
	Skeleton				Skeleton_;

	//Lines					SkeletonLines_;
	//Points				SkeletonPoints_;

	AnimationType			CurrentAnimation_ = AnimationIdle;

	std::vector<SSDData>	SSDData_;
	unsigned int			VBOSSD_;

	Vec3f					DistanceToGround_;
};

class NPC : public Character
{
public:
	NPC() {}
	NPC(Vec3f Pos, Vec3f Rot, Mesh &M, Skeleton &S) : Character(Pos, Rot, M, S) {AI_.SetCurrentGoal({0,0,0});}
	void			SetWorld(World *W)											{World_ = W; AI_.SetWorld(W);}
	void			SetState(CharacterState S);
	void			Step(float dt);
private:
	AIClass			AI_;
	World*			World_;
};

class Player : public Character
{
public:
	Player() {};
	Player(Vec3f Pos, Vec3f Dir, float FOV, float Aspect, float NF, float FF);
	void					Move(Vec3f RelativeDirection);
	void					SetPosition(Vec3f Pos);
	void					SetDirection(Vec3f Dir);
	void					SetRotation(Vec3f Angles);
	void					Rotate(Vec3f Angles);
	void					SetFOV(float Degrees) {FOV_ = Degrees;}
	void					ChangeFOV(float Degrees);						//by amount
	void					SetNearAndFarFields(float NF, float FF) {NearField_ = NF; FarField_ = FF;}
	void					SetScreenSize(float Width, float Height) {ScreenW_ = Width;ScreenH_ = Height;Aspect_ = Width/Height;}

	//Vec3f					GetPosition();
	//Vec3f					GetDirection();

	Mat4f					GetPerspectiveVP();
	//Mat4f					GetOrthographicVP();		//Screen space view for rendering text
private:
	//Vec3f					Position_, Direction_;
	//Vec3f					Up_, Right_;		

	Mat4f					View_, Projection_;
	float					FOV_, NearField_, FarField_;
	float					Aspect_ = 0;
	float					ScreenW_ = 0;
	float					ScreenH_ = 0;
};

