﻿#include <fstream>
#include <iostream>

#include "Collision/CollisionTestsBasic.h"
#include "FileIO.h"
#include "Animator.h"
#include "GLHelperFunctions.h"

//Glut Special keys
#define  GLUT_KEY_F1                        0x0001
#define  GLUT_KEY_F2                        0x0002
#define  GLUT_KEY_F3                        0x0003
#define  GLUT_KEY_F4                        0x0004
#define  GLUT_KEY_F5                        0x0005
#define  GLUT_KEY_F6                        0x0006
#define  GLUT_KEY_F7                        0x0007
#define  GLUT_KEY_F8                        0x0008
#define  GLUT_KEY_F9                        0x0009
#define  GLUT_KEY_F10                       0x000A
#define  GLUT_KEY_F11                       0x000B
#define  GLUT_KEY_F12                       0x000C
#define  GLUT_KEY_LEFT                      0x0064
#define  GLUT_KEY_UP                        0x0065
#define  GLUT_KEY_RIGHT                     0x0066
#define  GLUT_KEY_DOWN                      0x0067
#define  GLUT_KEY_PAGE_UP                   0x0068
#define  GLUT_KEY_PAGE_DOWN                 0x0069
#define  GLUT_KEY_HOME                      0x006A
#define  GLUT_KEY_END                       0x006B
#define  GLUT_KEY_INSERT                    0x006C

#define  KEY_CTRL							114
#define  KEY_SHIFT							112
#define  KEY_ALT							116

extern TextureManager TexManager;
extern DebugPLManager PLManager;

void
Animator::Init()
{
	Player_ = Player(Vec3f{0,0,3}, Vec3f{0,0,-1}, 60.0f, 1.0f, 0.1f, 1000.0f);
	
	std::vector<Mesh> Meshes;
	std::vector<Material> Materials;
	ParseObjFile("Assets/models/", "man.obj", Meshes, Materials);
	CurrentMesh_ = Meshes.back();
	CurrentMesh_.TextureId_ = TexManager.GenerateColorTexture({0.3f,0.3f,0.3f,1});
	CurrentSkeleton_ = CreateSkeleton(Meshes.back());

	CurrentModel_ = NPC(Vec3f{0,0,0}, Vec3f{0,0,0}, CurrentMesh_, CurrentSkeleton_);
	CurrentModel_.RotateJoint(3, {0,0,1});

	std::vector<Vec4f> Colors = {Vec4f{1,0,0,1},Vec4f{0,1,0,1},Vec4f{0,0,1,1},Vec4f{1,1,1,1}};	//R,G,B,White
	LineMesh_.TextureId_ = TexManager.GenerateColorTexture(Colors, ColorCoords);
	LineMesh_.Type_ = MeshType_Line;
	PointMesh_.TextureId_ = LineMesh_.TextureId_;
	PointMesh_.Type_ = MeshType_Point;

	Output_.NPCs.push_back(&CurrentModel_);
	Output_.Meshes.push_back(&LineMesh_);
	Output_.Meshes.push_back(&PointMesh_);
	Output_.Player = &Player_;
	Output_.WarpMouse = false;
}

void
Animator::Reset()
{
	CurrentModel_ = NPC(Vec3f{0,0,0}, Vec3f{0,0,0}, CurrentMesh_, CurrentSkeleton_);
	CurrentModel_.RotateJoint(3, {0,0,1});
}

void DrawSelectedJoint(int i, Character &CurrentModel)
{
	Vec3f C = Vec3f{1,1,1};
	Vec3f S = CurrentModel.GetJointPosition(i) + Vec3f{-0.1f, -0.1f, 0};
	Vec3f E = CurrentModel.GetJointPosition(i) + Vec3f{0.1f, 0.1f, 0};
	PLManager.DrawLine(S,E,C);

	S = CurrentModel.GetJointPosition(i) + Vec3f{-0.1f, 0.1f, 0};
	E = CurrentModel.GetJointPosition(i) + Vec3f{0.1f, -0.1f, 0};
	PLManager.DrawLine(S,E,C);

	CurrentModel.DrawJoint(i, PLManager.GetPoints(), PLManager.GetLines());
}

MeshOutput&
Animator::GetMeshOutput()
{
	if(CurrentJointI_ != -1)
	{
		DrawSelectedJoint(CurrentJointI_, CurrentModel_);
	}
	

	Output_.SSText.clear();
	std::stringstream ss;
	TextLine TL;

	ss = std::stringstream();
	ss << "Player position: " << Player_.GetPosition() << " Speed: " << MoveSpeed_;
	TL.Pos = {-1,-1,0};
	TL.C = {1,0,1};
	TL.Text = ss.str();
	Output_.SSText.push_back(TL);

	PLManager.CreatePointAndLineMesh(PointMesh_, LineMesh_, ColorCoords);
	PLManager.ClearPoints();
	PLManager.ClearLines();
	return Output_;
}


bool Grabbed = false;
void
Animator::HandleMouse(Input &In)
{
	//float x = (float)(In.MousePos.x-In.ScreenSize.x/2)/(In.ScreenSize.x/2);
	//float y = (float)(In.MousePos.y-In.ScreenSize.y/2)/(In.ScreenSize.y/2);
	
	if(In.MouseButton[0])				//left mouse button
	{
		float DistanceToXYPlane = FLT_MAX;
		RayPlaneCollisionTest({0,0,1}, {0,0,0}, In.MousePos3d, In.MouseDir, DistanceToXYPlane);
		Vec3f MousePos = In.MousePos3d + DistanceToXYPlane*In.MouseDir;		//find where the ray collides with the XY plane

		if(!Grabbed)					//select joint to move
		{
			float Max = FLT_MAX;
			CurrentJointI_ = CurrentModel_.RaySkeletonTest(In.MousePos3d, In.MouseDir, Max);	//what joint did you click on
			if(CurrentJointI_ != -1)
				Grabbed = true;
		}
		if(Grabbed)						//move selected joint
		{
			CurrentModel_.SetJointPosition(CurrentJointI_, MousePos);
			std::cout << "moved " << MousePos << std::endl; 
		}

		Vec3f S = In.MousePos3d + Vec3f{0,0.001f,0};
		Vec3f E = MousePos;
		Vec3f C = Vec3f{1,1,1};
		PLManager.DrawLine(S,E,C);
	}
	else
	{
		Grabbed = false;
		std::cout << "released\n"; 
	}
	if(In.MouseButton[2])				//right mouse button
	{
		Vec2f MouseDelta;
		MouseDelta.x = (float)(In.MousePos.x-In.ScreenSize.x/2);
		MouseDelta.y = (float)(In.MousePos.y-In.ScreenSize.y/2);
		MouseDelta.x /= In.ScreenSize.x;
		MouseDelta.y /= In.ScreenSize.y;
		GetPlayer()->Rotate(10*In.dt*Vec3f{MouseDelta.y, MouseDelta.x, 0});
	}
	if(In.MouseButton[3])				//mousewheel
	{
		In.MouseButton[3] = false;
		MoveSpeed_ += 1;
	}
	if(In.MouseButton[4])
	{
		In.MouseButton[4] = false;
		MoveSpeed_ -= 1;
		if(MoveSpeed_ <= 1)
			MoveSpeed_ = 1;
	}
}

void
Animator::HandleButtons(Input& In)
{
	if(In.Changed)						//if a new key has been pressed
	{
		if(In.KeyStates['+'])
		{
			++CurrentJointI_;
			if(CurrentJointI_ > CurrentSkeleton_.GetNumJoints()-1)
				CurrentJointI_ = (int)CurrentSkeleton_.GetNumJoints()-1;
		}
		if(In.KeyStates['-'])
		{
			--CurrentJointI_;
			if(CurrentJointI_ < 0)
				CurrentJointI_ = 0;
		}
		if(In.KeyStates['c'])
		{
			CurrentSkeleton_.SaveKeyFrame(0);
		}
		if(In.KeyStates['x'])
		{
			//CurrentModel_.PlayAnimation_ = !CurrentModel_.PlayAnimation_;
			CurrentSkeleton_.StepAnimation(0,0);
		}
	}
	if(In.KeyStates['X'])
	{
		CurrentSkeleton_.StepAnimation(0,0);
	}
	if(In.KeyStates['C'])
	{
		CurrentSkeleton_.SaveAnimation(0, "testAnim");
	}
	if(In.KeyStates['L'])
	{
		CurrentSkeleton_.LoadAnimation(0, "testAnim");
	}
	if(In.KeyStates['z'])
	{
		CurrentSkeleton_.ClearAnimation(0);
	}
	float MoveAmount = 0.001f;
	if(In.KeySpecialStates[GLUT_KEY_UP])
	{
		if(CurrentJointI_ >= 0)
		{
			if(In.KeySpecialStates[KEY_CTRL])
				CurrentModel_.MoveJoint(CurrentJointI_, {0, MoveAmount, 0});
			else
			{
				//CurrentModel_.RotateJoint(CurrentJointI_, {-0.001f, 0, 0});
				CurrentModel_.RotateJoint(CurrentJointI_, {-1, 0, 0}, 0.001f);
			}
		}
	}
	if(In.KeySpecialStates[GLUT_KEY_DOWN])
	{
		if(CurrentJointI_ >= 0)
		{
			if(In.KeySpecialStates[KEY_CTRL])
				CurrentModel_.MoveJoint(CurrentJointI_, {0, -MoveAmount, 0});
			else
			{
				//CurrentModel_.RotateJoint(CurrentJointI_, {0.001f, 0, 0});
				CurrentModel_.RotateJoint(CurrentJointI_, {1, 0, 0}, 0.001f);
			}
		}
	}
	if(In.KeySpecialStates[GLUT_KEY_LEFT])
	{
		if(CurrentJointI_ >= 0)
		{
			if(In.KeySpecialStates[KEY_CTRL])
				CurrentModel_.MoveJoint(CurrentJointI_, {-MoveAmount, 0, 0});
			else
			{
				//CurrentModel_.RotateJoint(CurrentJointI_, {0, -0.001f, 0});
				CurrentModel_.RotateJoint(CurrentJointI_, {0, -1, 0}, 0.001f);
			}
		}
	}
	if(In.KeySpecialStates[GLUT_KEY_RIGHT])
	{
		if(CurrentJointI_ >= 0)
		{
			if(In.KeySpecialStates[KEY_CTRL])
				CurrentModel_.MoveJoint(CurrentJointI_, {MoveAmount, 0, 0});
			else
			{
				//CurrentModel_.RotateJoint(CurrentJointI_, {0, 0.001f, 0});
				CurrentModel_.RotateJoint(CurrentJointI_, {0, 1, 0}, 0.001f);
			}
		}
	}
	if(In.KeySpecialStates[GLUT_KEY_PAGE_UP])
	{
		if(CurrentJointI_ >= 0)
		{
			if(In.KeySpecialStates[KEY_CTRL])
				CurrentModel_.MoveJoint(CurrentJointI_, {0, 0, MoveAmount});
			else
			{
				//CurrentModel_.RotateJoint(CurrentJointI_, {0, 0, 0.001f});
				CurrentModel_.RotateJoint(CurrentJointI_, {0, 0, 1}, 0.001f);
			}
		}
	}
	if(In.KeySpecialStates[GLUT_KEY_PAGE_DOWN])
	{
		if(CurrentJointI_ >= 0)
		{
			if(In.KeySpecialStates[KEY_CTRL])
				CurrentModel_.MoveJoint(CurrentJointI_, {0, 0, -MoveAmount});
			else
			{
				//CurrentModel_.RotateJoint(CurrentJointI_, {0, 0, -0.001f});
				CurrentModel_.RotateJoint(CurrentJointI_, {0, 0, -1}, 0.001f);
			}
		}
	}

	float Factor = 3.0f/2.0f;
	if(In.KeyStates['w'])
	{
		GetPlayer()->Move(In.dt*MoveSpeed_*Factor*Vec3f{0,0,-1});
	}
	if(In.KeyStates['a'])
	{
		GetPlayer()->Move(In.dt*MoveSpeed_*Factor*Vec3f{-1,0,0});
	}
	if(In.KeyStates['s'])
	{
		GetPlayer()->Move(In.dt*MoveSpeed_*Factor*Vec3f{0,0,1});
	}
	if(In.KeyStates['d'])
	{
		GetPlayer()->Move(In.dt*MoveSpeed_*Factor*Vec3f{1,0,0});
	}
	if(In.KeyStates['r'])
	{
		Reset();
	}
	if(In.KeyStates['q'])
	{
		exit(EXIT_SUCCESS);
	}
	In.Changed = false;
}

void Animator::Step(float dt)
{
	CurrentModel_.Step(dt);
}