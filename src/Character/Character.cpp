#include "Character.h"
#include "FileIO.h"
#include "Collision/CollisionTestsBasic.h"
#include "GlobalDefines.h"
#include "float.h"
Character::Character(Vec3f Pos, Vec3f Rot, Mesh &M, Skeleton &S) : MeshObject(Pos, Rot, M)
{
	Skeleton_ = S;
	DistanceToGround_ = {0,-Skeleton_.GetJointPosition(11).y,0};		//left foot

	Position_ = Pos;
	Direction_ = {0,0,1};
	Rotation_ = {0,0,0};
	Up_ = {0,1,0};
	Right_ = {1,0,0};
	CurrentAnimation_ = AnimationIdle;


	//CreateSkeleton();
	//Skeleton_.ComputeToBind();

	SSDData_.resize(Mesh_->Vertices_.size());
	for(int Ji = 0; Ji < (int)Skeleton_.GetNumJoints(); ++Ji)
	{
		Joint J = Skeleton_.GetJoint(Ji);
		for(int i2 = 0; i2 < (int)J.GetNumberOfConnections(); ++i2)
		{
			size_t PointIndex = J.GetConnectedPointIndex(i2);
			float W = J.GetConnectedPointWeight(i2);

			SSDData_[PointIndex].Weight = W;
			SSDData_[PointIndex].JointIndex = Ji;
		}
	}
	for(size_t i = 0; i < SSDData_.size(); ++i)
	{
		if(SSDData_[i].Weight == 0)
		{
			SSDData_[i].Weight = 1;
		}
	}
	Skeleton_.SetJointRotation(2, {0,0,-1});
	Skeleton_.SetJointRotation(5, {0,0,1});
}
void
Character::MoveToward(Vec3f Target, float Speed)
{
	if(!(Target == Position_))
	{
		SetDirection(NORMALIZE(Target-Position_));		//TODO: slow turning
		MoveForward(Speed);
	}
}
void
Character::SetDirection(Vec3f Dir)
{
	Direction_ = Dir;
	Vec3f DirAlongXZ = NORMALIZE({Dir.x, 0, Dir.z});
	Rotation_.y = -acos(DOT(DirAlongXZ, {0,0,1}));
	if(DOT(CROSS(DirAlongXZ, {0,0,1}), {0,1,0}) > 0)		//check if it is over half a turn
	{
		Rotation_.y = -Rotation_.y;
	}
}
void Character::PlayAnimation(float dt)
{
	if(CurrentAnimation_ == AnimationWalking)
	{
		//Skeleton_.StepAnimation(CurrentAnimation_, dt);
		//RotateJoint(2, {0,0,1});

		//RotateJoint(0, {0.001f,0.001f,0.001f});
		//RotateJoint(2, {0.003f,0.002f,0.001f});
		//RotateJoint(5, {0.001f,0.003f,0.002f});
		//RotateJoint(8, {0.001f,0.001f,0.001f});
		RotateJoint(9, {-dt,0,0});
		Skeleton_.GetJoint(12).SetRotation(Skeleton_.GetJointRotation(9)+Vec3f{PI,0,0});
		//RotateJoint(10, {0.001f,0,0});
	}
}
void
Character::Step(float dt)
{
	Model_.SetRotation(Rotation_);				//update character model facing direction and position
	Model_.SetPosition(Position_+DistanceToGround_);
	PlayAnimation(dt);
}
CharacterState
AIClass::GetCurrentState(Vec3f CurrentPos)
{
	CharacterState Result;
	if(LEN2(CurrentGoal_ - CurrentPos) < 1)		//if distance squared is within 1 meters of goal
	{
		if(!(GlobalRand()%3000))
		{
			float x = (float)(GlobalRand()%101)*2-100;		//generate new goal
			//float y = (float)(GlobalRand()%101)*2-100;
			float z = (float)(GlobalRand()%101)*2-100;
			CurrentBehaviour_ = BehaviourAimlessWander;
			CurrentGoal_ = {x, 0, z};
			//get next waypoint
			
			Result.CurrentAnimation = AnimationWalking;
		}
		else
		{
			CurrentBehaviour_ = BehaviourIdle;
			CurrentGoal_ = CurrentPos;

			Result.CurrentAnimation = AnimationIdle;
		}
	}
	else
	{
		Result.CurrentAnimation = AnimationWalking;
	}
	Result.CurrentGoal = World_->GetNextWaypoint(CurrentPos, CurrentGoal_);
	return Result;
}
void NPC::SetState(CharacterState State)
{
	CurrentAnimation_ = State.CurrentAnimation;
}
void
NPC::Step(float dt)
{
	CharacterState CurrentState = AI_.GetCurrentState(Position_);
	SetState(CurrentState);
	MoveToward(CurrentState.CurrentGoal, 3*dt);						//should handle turning slowly
	Character::Step(dt);
}
//void
//Character::DrawSkeleton()
//{
//	SkeletonLines_.clear();
//	SkeletonPoints_.clear();
//	Skeleton_.Draw(SkeletonPoints_, SkeletonLines_);
//}
void
Character::DrawJoint(int JointIndex, Points &DP, Lines &DL)
{
	Skeleton_.GetJoint(JointIndex).Draw(DP,DL);
	
	Joint &J = Skeleton_.GetJoint(JointIndex);
	for(int i = 0; i < (int)J.GetNumberOfConnections(); ++i)
	{
		Vec3f P = GetMeshPointPosition(J.GetConnectedPointIndex(i));
		Vec3f Col = J.GetConnectedPointWeight(i) * Vec3f{1,0,1};
		DP.push_back(Point{P,Col});
	}
}
Lines
DrawCircle(Vec3f C, float R)
{
	Lines L;
	Line L1;

	for(int i1 = -1; i1 < 2; ++i1)
	{
		for(int i2 = -1; i2 < 2; ++i2)
		{
			for(int i3 = -1; i3 < 2; ++i3)
			{
				L1 = {C, C+R*NORMALIZE({(float)i1,(float)i2,(float)i3}),{1,0,0}};
				L.push_back(L1);
			}
		}
	}
	return L;
}
int
Character::RaySkeletonTest(Vec3f RayOrigin, Vec3f RayDir, float &MaxLen)
{
	int Result = -1;
	float R = 0.2f;
	float Len = 0;
	for(int i = 0; i < (int)Skeleton_.GetNumJoints(); ++i)
	{
		float MinLen = 0;
		
		Vec3f C = Skeleton_.GetJoint(i).GetWorldPos();
		bool Hit = RaySphereCollisionTest(C, R, RayOrigin, RayDir, MinLen, MaxLen);
		if(Hit)
		{
			Result = i;
			Len = MinLen;
		}
	}
	if(Result >= 0)
	{
		MaxLen = Len;
	}
	return Result;
}
Player::Player(Vec3f Pos, Vec3f Dir, float FOV, float Aspect, float NF, float FF)
{
	Rotation_ = {0,-PI/2,0};

	SetPosition(Pos);
	SetDirection(Dir);
	FOV_ = FOV;
	Aspect_ = Aspect;
	NearField_ = NF;
	FarField_ = FF;
}
void
Player::Move(Vec3f RDir)
{
	Position_ += RDir.x*Right_ + RDir.y*Up_ - RDir.z*Direction_;
	View_ = LookAt(Position_, Position_+Direction_, Up_); 
}
void
Player::SetPosition(Vec3f Pos)
{
	Position_ = Pos;
	View_ = LookAt(Position_, Position_+Direction_, Up_); 
}
void
Player::SetDirection(Vec3f Dir)
{
	Direction_ = NORMALIZE(Dir);

	Rotation_.x = asin(Direction_.y);
	Rotation_.y = acos(Direction_.x/cos(Rotation_.x));
	if(Direction_.z < 0)
		Rotation_.y = -Rotation_.y;
	Rotation_.z = 0;

	Right_ = NORMALIZE(CROSS(Direction_, {0,1,0}));
	Up_ = NORMALIZE(CROSS(Right_, Direction_));
	View_ = LookAt(Position_, Position_+Direction_, Up_); 
}
void
Player::SetRotation(Vec3f Angles)
{
	Rotation_ = Angles;

	Direction_.x = cos(Rotation_.x) * cos(Rotation_.y);
	Direction_.y = sin(Rotation_.x);
	Direction_.z = cos(Rotation_.x) * sin(Rotation_.y);
	Direction_ = NORMALIZE(Direction_);

	Right_ = NORMALIZE(CROSS(Direction_, {0,1,0}));
	Up_ = NORMALIZE(CROSS(Right_, Direction_));
	View_ = LookAt(Position_, Position_+Direction_, Up_); 
}
void
Player::Rotate(Vec3f Angles)
{
	Rotation_ += Angles;

	if(Rotation_.x > 1.55f)
		Rotation_.x = 1.55f;
	if(Rotation_.x < -1.55f)
		Rotation_.x = -1.55f;

	SetRotation(Rotation_);
}
void
Player::ChangeFOV(float Degrees)
{
	FOV_ += Degrees;
	if(FOV_ < 1)
		FOV_ = 1;
	if(FOV_ > 60)
		FOV_ = 60;
}
Mat4f
Player::GetPerspectiveVP()
{
	Projection_ = Perspective(FOV_, Aspect_, NearField_, FarField_);
	return Projection_ * View_;
}
bool
Character::RayMeshCollisionTest(Vec3f RayOrigin, Vec3f RayDir, float &MaxLen)		//returns closest hit
{
	size_t NumIndices = Mesh_->Indices_.size();
	bool Hit = false;
	for(size_t i = 0; i < NumIndices; i += 3)
	{
		Vec3f P1 = GetMeshPointPosition(Mesh_->Indices_[i]);
		Vec3f P2 = GetMeshPointPosition(Mesh_->Indices_[i+1]);
		Vec3f P3 = GetMeshPointPosition(Mesh_->Indices_[i+2]);

		Hit = RayTriangleIntersection(P1, P2, P3, RayOrigin, RayDir, MaxLen);

		//Vec3f AB = P2 - P1; 
		//Vec3f BC = P3 - P2; 

		//Vec3f Normal = NORMALIZE(CROSS(AB, BC));
		//float Len = MaxLen;
		//if(RayPlaneCollisionTest(Normal, P1, RayOrigin, RayDir, Len))
		//{
		//	Vec3f P = RayOrigin + Len*RayDir;

		//	Vec3f CA = P1 - P3;

		//	Vec3f AP = P - P1; 
		//	Vec3f BP = P - P2; 
		//	Vec3f CP = P - P3; 

		//	Vec3f ABP = CROSS(AB, AP);
		//	Vec3f BCP = CROSS(BC, BP);
		//	Vec3f CAP = CROSS(CA, CP);

		//	if (DOT(Normal, ABP) > 0 && 
		//		DOT(Normal, BCP) > 0 && 
		//		DOT(Normal, CAP) > 0)
		//	{
		//		//float Area = LEN(CROSS(AB, BC));
		//		//u = LEN(CAP) / Area;
		//		//v = LEN(ABP) / Area;
		//		Hit = true;		//P is inside the triangle
		//		MaxLen = Len;
		//	}
		//}
	}
	return Hit;
}
Vec3f
Character::GetMeshPointPosition(size_t PointIndex)
{
	std::vector<Mat4f> SSDT = Skeleton_.GetSSDTransforms();
	if(SSDT.size() != 15)
	{
		return Mesh_->Vertices_[PointIndex].Position;
	}
	Vec4f NP = {0,0,0,0};
	for(int Ji = 0; Ji < (int)Skeleton_.GetNumJoints(); ++Ji)
	{
		Joint J = Skeleton_.GetJoint(Ji);
		for(int i2 = 0; i2 < (int)J.GetNumberOfConnections(); ++i2)
		{
			size_t Pi = J.GetConnectedPointIndex(i2);
			if(PointIndex == Pi)
			{
				float W = J.GetConnectedPointWeight(i2);
				Vec3f OriginalP = Mesh_->Vertices_[Pi].Position;
				NP += W*SSDT[Ji]*Vec4f{OriginalP, 1};
			}
		}
	}
	return NP.GetXYZ();
}