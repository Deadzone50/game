#include "Game.h"

Vec3f
TranslateToMap(Vec3f P)
{
	Vec3f MP;
	MP.x = 0.1f*P.x;
	MP.y = 0.1f*-P.z;
	MP.z = 0;
	return MP;
}
MeshOutput&
Game::GetMeshOutput()
{
	if(DisplayedScreen_ == CameraScreen)
	{
		Output_.Objects.clear();
		Output_.NPCs.clear();
		for(auto it = MeshObjects_.begin(); it != MeshObjects_.end(); ++it)
		{
			Output_.Objects.push_back(&(*it));
		}
		for(auto it = NPCs_.begin(); it != NPCs_.end(); ++it)
		{
			Output_.NPCs.push_back(&(*it));
		}

		Output_.SSText.clear();
		std::stringstream ss;
		TextLine TL;

		ss = std::stringstream();
		ss << "Player position: " << Player_.GetPosition() << " Speed: " << MoveSpeed_ << " Skip: " << SkipFactor_;
		TL.Pos = {-1,-1,0};
		TL.C = {1,0,1};
		TL.Text = ss.str();
		Output_.SSText.push_back(TL);
	}
	return Output_;
}