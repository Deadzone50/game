#pragma once
#include "Containers.h"

inline bool CheckIfTriangleIsInfrontOfPlane(Triangle T, Vec3f N, Vec3f P);
inline bool CheckIfTriangleIsBehindPlane(Triangle T, Vec3f N, Vec3f P);
inline bool CheckPIsInfrontOfPlane(Vec3f TestP, Vec3f N, Vec3f P);
inline bool CheckIfLineIsInfrontOfPlane(Line L, Vec3f N, Vec3f P);
inline bool CheckIfLineIsBehindPlane(Line L, Vec3f N, Vec3f P);
inline size_t CullTriangles(Triangles &Trigs, Vec3f N, Vec3f NL, Vec3f NR, Vec3f P1, Vec3f P2);
inline size_t CullLines(Lines &L, Vec3f N, Vec3f NL, Vec3f NR, Vec3f P1, Vec3f P2);