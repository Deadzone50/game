#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>      // std::setprecision
#include "GLHelperFunctions.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "Mesh.h"

DebugPLManager PLManager;

bool BindAttribute(const char *AttributeName, GLint &Attribute, GLuint Prog)
{
	Attribute = glGetAttribLocation(Prog, AttributeName);
	if(Attribute == -1)
	{
		std::cout << "Could not bind attribute " << AttributeName << std::endl;
		return false;
	}
	return true;
}

bool BindUniform(const char *UniformName, GLint &Uniform, GLuint Prog)
{
	Uniform = glGetUniformLocation(Prog, UniformName);
	if(Uniform == -1)
	{
		std::cout << "Could not bind uniform " << UniformName << std::endl;
		return false;
	}
	return true;
}

inline unsigned int
BindTexture(int Width, int Height, int BPP, const unsigned char *Data)
{
	unsigned int TexId;
	glGenTextures(1, &TexId);
	glBindTexture(GL_TEXTURE_2D, TexId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	if(BPP == 3)																			//RGB
		glTexImage2D(GL_TEXTURE_2D,	0, GL_RGB, Width, Height, 0, GL_RGB, GL_UNSIGNED_BYTE, Data);
	else if(BPP ==4)																		//RGBA
		glTexImage2D(GL_TEXTURE_2D,	0, GL_RGBA, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, Data);
	else
		std::cout << "unknown imageformat\n";
	//target, level, 0 = base no minimap, interalformat, width, height, border, format, type
	//glBindTexture(GL_TEXTURE_2D, 0);
	return TexId;
}

void TextureManager::LoadFont(std::string FileName)
{
	//stbi_set_flip_vertically_on_load(false);
	int W, H, BPP;
	unsigned char *Data = stbi_load(FileName.c_str(), &W, &H, &BPP, 0);
	FontTextureId_ = BindTexture(W, H, BPP, Data);
	stbi_image_free(Data);
	//stbi_set_flip_vertically_on_load(true);
}

void GenerateLetter(char c, std::vector<Vertex>	&Vertices, std::vector<unsigned int> &Indices, Vec3f LetterPos)
{
	float ImageW = 321;
	float ImageH = 31;

	//float X0 = 0;
	//float X1 = 1;
	//float Y0 = 0;
	//float Y1 = 1;

	float FontW = 9;
	float FontH = 9;
	float Padding = 1;

	Vertex TL, BL, BR, TR;
	BL.Position = LetterPos;
	BR.Position = LetterPos+Vec3f{10,0,0};
	TR.Position = LetterPos+Vec3f{10,10,0};
	TL.Position = LetterPos+Vec3f{0,10,0};
#if 1
	int cy = 0;
	if(32 <= c && c <= 63)
	{
		c -= 32;
		cy = 2;

	}
	else if(64 <= c && c <= 95)
	{
		c -= 64;
		cy = 1;
	}
	else if(96 <= c && c <= 126)
	{
		c -= 96;
		cy = 0;
	}
	else
	{
		return;
	}
	float Y0 = cy*(FontH+Padding)+ Padding + 0.5f;		//add half a pixel
	float Y1 = Y0+FontH-Padding;
	float X0 = c*(FontW+Padding)+ Padding + 0.5f;
	float X1 = X0+FontW-Padding;
	X0/=ImageW;
	Y0/=ImageH;
	X1/=ImageW;
	Y1/=ImageH;
#endif
	BL.TexCoord = {X0,Y0};
	BR.TexCoord = {X1,Y0};
	TR.TexCoord = {X1,Y1};
	TL.TexCoord = {X0,Y1};

	unsigned int Size = (unsigned int)Vertices.size();
	Indices.push_back(Size);
	Indices.push_back(Size+1);
	Indices.push_back(Size+2);
	Indices.push_back(Size+2);
	Indices.push_back(Size+3);
	Indices.push_back(Size+0);

	Vertices.push_back(BL);
	Vertices.push_back(BR);
	Vertices.push_back(TR);
	Vertices.push_back(TL);
}

Mesh
TextureManager::CreateTextMesh(std::string Text, Vec3f TextPosition)
{
	Mesh TextObject;
	int SIZE = (int)Text.size();
	if(SIZE)
	{
		for(int i = 0; i < Text.size(); ++i)
		{
			Vec3f LetterPos = TextPosition + i*Vec3f{7,0,0};
			GenerateLetter(Text[i], TextObject.Vertices_, TextObject.Indices_, LetterPos);
		}
		TextObject.TextureId_ = FontTextureId_;
		//TextObject.SetupMesh();
	}
	else
	{
		TextObject.VAO_ = 0;
		TextObject.VBO_ = 0;
		TextObject.EBO_ = 0;
	}
	return TextObject;
}

unsigned int TextureManager::AddTexture(std::string FileName)			//setups and returns Texture id
{
	unsigned int TexId = ImageMap_[FileName];
	if(!TexId)
	{
		int W, H, BPP;
		unsigned char *Image = stbi_load(FileName.c_str(), &W, &H, &BPP, 0);
		TexId = BindTexture(W, H, BPP, Image);
		stbi_image_free(Image);
		ImageMap_[FileName] = TexId;
	}
	return TexId;
}

unsigned int
TextureManager::GenerateColorTexture(const Vec4f &Color)
{
	std::stringstream ss;
	ss << std::setprecision(6) << std::fixed << Color;
	std::string Key = ss.str();
	unsigned int TexId = GeneratedMap_[Key];
	int W = 1; int H = 1; int BPP = 4;
	if(!TexId)
	{
		///Generate textures
		unsigned char *Image = new unsigned char[W*H*BPP];
		for(int i = 0; i < W*H; ++i)
		{
			Image[i*BPP]	= (unsigned char)(255*Color.x);
			Image[i*BPP+1]	= (unsigned char)(255*Color.y);
			Image[i*BPP+2]	= (unsigned char)(255*Color.z);
			Image[i*BPP+3]	= (unsigned char)(255*Color.w);
		}

		TexId = BindTexture(W, H, BPP, Image);
		delete[] Image;
		GeneratedMap_[Key] = TexId;
	}
	return TexId;
}

unsigned int
TextureManager::GenerateColorTexture(const std::vector<Vec4f> &Colors, std::vector<Vec2f> &ColorCoords)
{
	int W = (int)Colors.size(); int H = 1; int BPP = 4;

	std::stringstream ss;
	ss << std::setprecision(6) << std::fixed;
	for(int i = 0; i < W; ++i)
	{
		ss << Colors[i];
	}
	std::string Key = ss.str();
	unsigned int TexId = GeneratedMap_[Key];

	if(!TexId)
	{
		///Generate textures
		unsigned char *Image = new unsigned char[W*H*BPP];
		for(int i = 0; i < W*H; ++i)
		{
			Image[i*BPP]	= (unsigned char)(255*Colors[i].x);
			Image[i*BPP+1]	= (unsigned char)(255*Colors[i].y);
			Image[i*BPP+2]	= (unsigned char)(255*Colors[i].z);
			Image[i*BPP+3]	= (unsigned char)(255*Colors[i].w);
		}

		TexId = BindTexture(W, H, BPP, Image);
		delete[] Image;
		GeneratedMap_[Key] = TexId;
	}
	float StartX = 0.5f/W;
	float StepX = 1.0f/W;
	float Y = 0.5f;
	ColorCoords.resize(W);
	for(int i = 0; i < W; ++i)
	{
		ColorCoords[i] = {StartX+StepX*i, Y};
	}
	return TexId;
}

GLuint CreateShader(const char *FileName, GLenum ShaderType)
{
	char *Source;
	if(LoadShader(FileName, &Source))
	{
		GLint Shader = glCreateShader(ShaderType);
		glShaderSource(Shader, 1, &Source, NULL);
		UnloadShader(Source);

		glCompileShader(Shader);
		GLint CompileOK = GL_FALSE;
		glGetShaderiv(Shader, GL_COMPILE_STATUS, &CompileOK);
		if(CompileOK == GL_FALSE)
		{
			std::cout << FileName << ":";
			PrintLog(Shader);
			glDeleteShader(Shader);
			return 0;
		}

		return Shader;
	}
	else
	{
		std::cout << "Error opening " << FileName << std::endl;
		return 0;
	}
}

GLuint CreateProgram(GLint VertexShader, GLint FragmentShader)
{
	GLint LinkOK = GL_FALSE;
	GLuint Program = glCreateProgram();
	glAttachShader(Program, VertexShader);
	glAttachShader(Program, FragmentShader);
	glLinkProgram(Program);
	glGetProgramiv(Program, GL_LINK_STATUS, &LinkOK);
	if(!LinkOK)
	{
		std::cout << "Error in glLinkProgram\n";
		PrintLog(Program);
		return 0;
	}
	return Program;
}

bool LoadShader(const char *Name, char **ShaderSource)
{
	std::ifstream file(Name);
	if(file.is_open())
	{
		std::stringstream stream;
		stream << file.rdbuf();
		file.close();

		std::string S = stream.str();
		*ShaderSource = new char[S.size()+1];
		memcpy(*ShaderSource, S.c_str(), S.size());
		ShaderSource[0][S.size()] = 0;
		return true;
	}
	else
	{
		std::cout << "Unable to open file: " << Name << std::endl;
		return false;
	}
}

void UnloadShader(char *ShaderSource)
{
	if (ShaderSource != 0)
		delete[] ShaderSource;
	ShaderSource = 0;
}

void PrintLog(GLuint Object)
{
	GLint LogLength = 0;
	if(glIsShader(Object))
		glGetShaderiv(Object, GL_INFO_LOG_LENGTH, &LogLength);
	else if(glIsProgram(Object))
		glGetProgramiv(Object, GL_INFO_LOG_LENGTH, &LogLength);
	else
	{
		std::cout << "PrintLog: Not a shader or a program\n";
		return;
	}

	char *Log = new char[LogLength];

	if(glIsShader(Object))
		glGetShaderInfoLog(Object, LogLength, NULL, Log);
	else if(glIsProgram(Object))
		glGetProgramInfoLog(Object, LogLength, NULL, Log);

	std::cout << Log;
	delete[] Log;
}

void
DebugPLManager::CreatePointAndLineMesh(Mesh &PM, Mesh &LM, const std::vector<Vec2f> &ColorCoords)
{
	PM.Changed_ = true;
	PM.Vertices_.clear();
	PM.Indices_.clear();
	for(unsigned int i = 0; i < (unsigned int)Points_.size(); ++i)
	{
		Vertex V;
		if(Points_[i].C == Vec3f{1,0,0})
			V.TexCoord = ColorCoords[0];
		else if(Points_[i].C == Vec3f{0,1,0})
			V.TexCoord = ColorCoords[1];
		else if(Points_[i].C == Vec3f{0,0,1})
			V.TexCoord = ColorCoords[2];
		else //if(Points_[i].C == Vec3f{1,1,1})
			V.TexCoord = ColorCoords[3];
		V.Position = Points_[i].P;
		PM.Vertices_.push_back(V);
		PM.Indices_.push_back(i);
	}

	LM.Changed_ = true;
	LM.Vertices_.clear();
	LM.Indices_.clear();
	for(unsigned int i = 0; i < (unsigned int)Lines_.size(); ++i)
	{
		Vertex V;
		if(Lines_[i].C == Vec3f{1,0,0})
			V.TexCoord = ColorCoords[0];
		else if(Lines_[i].C == Vec3f{0,1,0})
			V.TexCoord = ColorCoords[1];
		else if(Lines_[i].C == Vec3f{0,0,1})
			V.TexCoord = ColorCoords[2];
		else //if(Lines_[i].C == Vec3f{1,1,1})
			V.TexCoord = ColorCoords[3];
		V.Position = Lines_[i].S;
		LM.Vertices_.push_back(V);
		V.Position = Lines_[i].E;
		LM.Vertices_.push_back(V);

		LM.Indices_.push_back(i*2);
		LM.Indices_.push_back(i*2+1);
	}
}