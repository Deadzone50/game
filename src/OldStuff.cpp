void
Object::SubdivideIntoConvex()
{
	int S = -1;
	int	MaxPoints = 0;
	for(unsigned int Si = 0; Si < SurfacePoints_.size(); ++Si)
	{
		int PBehind = 0;
		int PInfront = 0;
		if(!CheckIfConvex(Si, PBehind, PInfront))
		{
			if(MaxPoints < PInfront)
			{
				S = Si;
				MaxPoints = PInfront;
			}
		}
	}
	if(S != -1)
	{
		DrawDebugSurface(S, false, {0,0.5f,0.5f});
		CreateDivisionSurfaces(S);
		SubdivideIntoConvex();			//recursive check
	}
}
void
Object::CreateDivisionSurfaces(int SSurfaceIndex)
{
	Surface SplittingSurface = SurfacePlanes_[SSurfaceIndex];
	std::vector<int> ToDo;
	std::vector<std::vector<int>> OldSurfaces;
	std::vector<std::vector<int>> NewSurfaces;

	int SurIndexStart = 0;
	int SurIndexEnd = SurfacePoints_.size();
	if(!DivideIndex_.empty())
	{
		for(auto D = DivideIndex_.begin(); D != DivideIndex_.end(); ++D)	//which part of the object to check
		{
			if(SSurfaceIndex >= *D)
			{
				SurIndexStart = *D;
			}
			else
			{
				SurIndexEnd = *D;
				break;
			}
		}
	}

	for(int i1 = SurIndexStart; i1 < SurIndexEnd; ++i1)				//split the affected surfaces with the S-Surface
	{
		std::vector<int> OldS;
		std::vector<int> NewS;
		for(auto SPi = SurfacePoints_[i1].begin(); SPi != SurfacePoints_[i1].end(); ++SPi)
		{
			if(DOT(SplittingSurface.N, SplittingSurface.P - GetPointPos(*SPi))+EPSILON >= 0)//points behind and on the S-Surface																				
			{																		//for old surface
				OldS.push_back(*SPi);
			}
			else																	//points infront of the S-Surface												
			{																		//for new surface
				NewS.push_back(*SPi);
			}
		}
		if(!NewS.empty() && !OldS.empty())
			ToDo.push_back(i1);		//have to handle this

		OldSurfaces.push_back(OldS);
		NewSurfaces.push_back(NewS);
	}
	std::vector<int> CutSurface;
	std::vector<int> NewPoints;
	for(auto SE = SurfaceEdges_.begin(); SE != SurfaceEdges_.end(); ++ SE)
	{
		//for(auto Suri = ToDo.begin(); Suri != ToDo.end(); ++ Suri)
		{
			//if(SE->MySurface1 == *Suri || SE->MySurface2 == *Suri)
			{
				if(SE->MySurface1 == SSurfaceIndex || SE->MySurface2 == SSurfaceIndex)	//if it is part of the splitting surface skip it
					continue;

				bool Stop = true;
				int SurI1 = -1;
				int SurI2 = -1;
				for(auto Suri = ToDo.begin(); Suri != ToDo.end(); ++ Suri)		//check if the edge is part of any of the surfaces involved
				{
					if(SE->MySurface1 == *Suri)
					{
						SurI1 = SE->MySurface1;
						Stop = false;
					}
					else if(SE->MySurface2 == *Suri)
					{
						SurI2 = SE->MySurface2;
						Stop = false;
					}
				}
				if(Stop)
					continue;


				Vec3f S;			//TODO: maybe move startpoint back or figure out something smarter
				Vec3f E;
				int Si;
				int Ei;
				//if(SE->MySurface2 == *Suri)
				//{
				//	S = SE->E;			//flip the direction so it always goes counterclockwise
				//	E = SE->S;
				//	Si = SE->Ei;
				//	Ei = SE->Si;
				//}
				//else
				{
					S = SE->S ;			//TODO: maybe move startpoint back or figure out something smarter
					E = SE->E;
					Si = SE->Si;
					Ei = SE->Ei;
				}


				float MaxLen = LEN(E - S);
				float t = MaxLen;
				Vec3f Dir = NORMALIZE(E - S);
				if(RayPlaneCollisionTest(SplittingSurface.N, SplittingSurface.P, S, Dir, t))
				{
					if(MaxLen-t > EPSILON && t > EPSILON)	//collision between endpoints
					{
						PhysPoint P(S + t*Dir);				//create new point
						int i = Points_.size();

						DrawDebugLine(S,S + t*Dir, {0,1,0});
						DrawDebugPoint(S + t*Dir+0.01f*RandomCol(),{0,1,0});
						{
							Points_.push_back(P);
							NewPoints.push_back(i);
							CutSurface.push_back(i);
						}
						//OldSurfaces[*Suri - SurIndexStart].push_back(i);	//add the point to both surfaces
						//NewSurfaces[*Suri - SurIndexStart].push_back(i);
						if(SurI1 != -1)
						{
							int SurI = SurI1;
							auto it = find(OldSurfaces[SurI - SurIndexStart].begin(), OldSurfaces[SurI - SurIndexStart].end(), Si);	//find the start or endpoint in the surface
							if(it != OldSurfaces[SurI - SurIndexStart].end())														//and insert the value before or after it
							{
								OldSurfaces[SurI - SurIndexStart].insert(it+1, i);
							}
							else
							{
								it = find(OldSurfaces[SurI - SurIndexStart].begin(), OldSurfaces[SurI - SurIndexStart].end(), Ei);
								OldSurfaces[SurI - SurIndexStart].insert(it, i);
							}
							it = find(NewSurfaces[SurI - SurIndexStart].begin(), NewSurfaces[SurI - SurIndexStart].end(), Si);		//find the start or endpoint in the surface
							if(it != NewSurfaces[SurI - SurIndexStart].end())														//and insert the value before or after it
							{
								NewSurfaces[SurI - SurIndexStart].insert(it+1, i);
							}
							else
							{
								it = find(NewSurfaces[SurI - SurIndexStart].begin(), NewSurfaces[SurI - SurIndexStart].end(), Ei);
								NewSurfaces[SurI - SurIndexStart].insert(it, i);
							}
						}
						if(SurI2 != -1)
						{
							int SurI = SurI2;
							auto it = find(OldSurfaces[SurI - SurIndexStart].begin(), OldSurfaces[SurI - SurIndexStart].end(), Ei);		//find the start or endpoint in the surface
							if(it != OldSurfaces[SurI - SurIndexStart].end())														//and insert the value before or after it
							{
								OldSurfaces[SurI - SurIndexStart].insert(it+1, i);
							}
							else
							{
								it = find(OldSurfaces[SurI - SurIndexStart].begin(), OldSurfaces[SurI - SurIndexStart].end(), Si);
								OldSurfaces[SurI - SurIndexStart].insert(it, i);
							}
							it = find(NewSurfaces[SurI - SurIndexStart].begin(), NewSurfaces[SurI - SurIndexStart].end(), Ei);		//find the start or endpoint in the surface
							if(it != NewSurfaces[SurI - SurIndexStart].end())														//and insert the value before or after it
							{
								NewSurfaces[SurI - SurIndexStart].insert(it+1, i);
							}
							else
							{
								it = find(NewSurfaces[SurI - SurIndexStart].begin(), NewSurfaces[SurI - SurIndexStart].end(), Si);
								NewSurfaces[SurI - SurIndexStart].insert(it, i);
							}
						}
					}
					else
					{
						if(MaxLen-t < EPSILON && DOT(SplittingSurface.P - S, SplittingSurface.N) < 0)		//has to start outside the surface
						{
							if(SurI1 != -1)
							{
								int SurI = SurI1;
								auto it = find(NewSurfaces[SurI - SurIndexStart].begin(), NewSurfaces[SurI - SurIndexStart].end(), Si);
								NewSurfaces[SurI - SurIndexStart].insert(it+1, Ei);

								//it = std::find(OldSurfaces[SurI - SurIndexStart].begin(), OldSurfaces[SurI - SurIndexStart].end(), Ei);
								//OldSurfaces[SurI - SurIndexStart].erase(it);

								//DrawDebugLine(GetPointPos(Si),GetPointPos(Ei), {1,0,0});
								//DrawDebugPoint(GetPointPos(Ei)+0.01f*RandomCol(),{1,0,0});
							}
							if(SurI2 != -1)
							{
								int SurI = SurI2;																	//edge goes wrong way
								auto it = find(NewSurfaces[SurI - SurIndexStart].begin(), NewSurfaces[SurI - SurIndexStart].end(), Si);
								NewSurfaces[SurI - SurIndexStart].insert(it, Ei);

								//it = std::find(OldSurfaces[SurI - SurIndexStart].begin(), OldSurfaces[SurI - SurIndexStart].end(), Ei);
								//OldSurfaces[SurI - SurIndexStart].erase(it);

								//DrawDebugLine(GetPointPos(Si),GetPointPos(Ei), {1,0,0});
								//DrawDebugPoint(GetPointPos(Ei)+0.01f*RandomCol(),{1,0,0});
							}
							CutSurface.push_back(Ei);		//TODO: add the points to this surface in the right order

						}		
						else if(t < EPSILON && DOT(SplittingSurface.P - E, SplittingSurface.N) < 0)		//has to end outside the surface				
						{
							if(!(DOT(SplittingSurface.P - E, SplittingSurface.N)+EPSILON < 0))
							{
								DrawDebugPoint(E, {1,1,1});
								continue;
							}
							if(SurI1 != -1)
							{
								int SurI = SurI1;
								auto it = find(NewSurfaces[SurI - SurIndexStart].begin(), NewSurfaces[SurI - SurIndexStart].end(), Ei);
								NewSurfaces[SurI - SurIndexStart].insert(it, Si);

								//it = std::find(OldSurfaces[SurI - SurIndexStart].begin(), OldSurfaces[SurI - SurIndexStart].end(), Si);
								//OldSurfaces[SurI - SurIndexStart].erase(it);

								//DrawDebugLine(GetPointPos(Si),GetPointPos(Ei), {1,0,0});
								//DrawDebugPoint(GetPointPos(Si)+0.01f*RandomCol(),{1,0,0});
							}
							if(SurI2 != -1)
							{
								int SurI = SurI2;																	//edge goes wrong way
								auto it = find(NewSurfaces[SurI - SurIndexStart].begin(), NewSurfaces[SurI - SurIndexStart].end(), Ei);
								NewSurfaces[SurI - SurIndexStart].insert(it+1, Si);

								//it = std::find(OldSurfaces[SurI - SurIndexStart].begin(), OldSurfaces[SurI - SurIndexStart].end(), Si);
								//OldSurfaces[SurI - SurIndexStart].erase(it);

								//DrawDebugLine(GetPointPos(Si),GetPointPos(Ei), {1,0,0});
								//DrawDebugPoint(GetPointPos(Si)+0.01f*RandomCol(),{1,0,0});
							}
							CutSurface.push_back(Si);

						}
					}
				}
			}
		}
	}
	SurfacePoints_.erase(SurfacePoints_.begin()+SurIndexStart, SurfacePoints_.begin()+SurIndexEnd);
	for(auto it = DivideIndex_.begin(); it != DivideIndex_.end();)
	{
		if(*it <= 0 || *it == SurIndexStart)
		{
			it = DivideIndex_.erase(it);
			if(DivideIndex_.empty())
				break;
		}
		else
		{
			if(*it > SurIndexStart)
			{
				*it -= SurIndexEnd-SurIndexStart;
			}
			++it;
		}
	}
	DivideIndex_.push_back(SurfacePoints_.size());
	std::vector<Surface> NewSurfacePlanes;
	Surface S;

	std::vector<Vec2i> ToMerge;
	for(int i = SurIndexStart; i < SurIndexEnd; ++i)		//remove doubles from surfaces
	{
		int Size = OldSurfaces[i-SurIndexStart].size();
		for(int i1 = 0; i1 < Size; ++i1)
			for(int i2 = i1+1; i2 < Size; ++i2)
			{
				if(OldSurfaces[i-SurIndexStart][i1] == OldSurfaces[i-SurIndexStart][i2])
				{
					OldSurfaces[i-SurIndexStart].erase(OldSurfaces[i-SurIndexStart].begin()+i2);
					--Size;
					--i2;
				}
			}
		Size = NewSurfaces[i-SurIndexStart].size();
		for(int i1 = 0; i1 < Size; ++i1)
			for(int i2 = i1+1; i2 < Size; ++i2)
			{
				if(NewSurfaces[i-SurIndexStart][i1] == NewSurfaces[i-SurIndexStart][i2])
				{
					NewSurfaces[i-SurIndexStart].erase(NewSurfaces[i-SurIndexStart].begin()+i2);
					--Size;
					--i2;
				}
			}

	}

	Vec3f OldCol = SurfacePlanes_[SurIndexStart].C;
	for(int i = SurIndexStart; i < SurIndexEnd; ++i)
	{
		int Size = OldSurfaces[i-SurIndexStart].size();
		int TM = 0;
		if(Size > 2)
		{
			for(int Pi1 = 0; Pi1 < Size; ++Pi1)
			{
				for(int Pi2 = Pi1+1; Pi2 < Size; ++Pi2)
				{
					if(LEN(GetPointPos(OldSurfaces[i-SurIndexStart][Pi1]) - GetPointPos(OldSurfaces[i-SurIndexStart][Pi2]))< EPSILON)
					{
						assert(OldSurfaces[i-SurIndexStart][Pi1] != OldSurfaces[i-SurIndexStart][Pi2]);
						ToMerge.push_back({OldSurfaces[i-SurIndexStart][Pi1], OldSurfaces[i-SurIndexStart][Pi2]});
						++TM;
					}
				}
			}
			if(Size-TM > 2)
			{
				SurfacePoints_.push_back(OldSurfaces[i-SurIndexStart]);
				S = Surface(SurfacePlanes_[i].N, GetPointPos(OldSurfaces[i-SurIndexStart][0]), OldCol);
				NewSurfacePlanes.push_back(S);
			}
		}
	}
	std::vector<int> Cut = CreateNewSurface(CutSurface, SplittingSurface.N);
	int Size = Cut.size();
	int TM = 0;
	if(Size > 2)
	{
		for(int Pi1 = 0; Pi1 < Size; ++Pi1)
		{
			for(int Pi2 = Pi1+1; Pi2 < Size; ++Pi2)
			{
				if(LEN(GetPointPos(Cut[Pi1]) - GetPointPos(Cut[Pi2]))< EPSILON)
				{
					assert(Cut[Pi1] != Cut[Pi2]);
					ToMerge.push_back({Cut[Pi1], Cut[Pi2]});
					++TM;
				}
			}
		}
	}
	if(Size-TM > 2)
	{
		SurfacePoints_.push_back(Cut);		//create cut surface	TODO: merge with Splittingsurface
		S = Surface(SplittingSurface.N, GetPointPos(Cut[0]), OldCol);
		NewSurfacePlanes.push_back(S);
	}

	DivideIndex_.push_back(SurfacePoints_.size());
	Vec3f NewCol = RandomCol();

	for(int i = SurIndexStart; i < SurIndexEnd; ++i)
	{
		Size = NewSurfaces[i-SurIndexStart].size();
		TM = 0;
		if(Size > 2)
		{
			for(int Pi1 = 0; Pi1 < Size; ++Pi1)
			{
				for(int Pi2 = Pi1+1; Pi2 < Size; ++Pi2)
				{
					if(LEN(GetPointPos(NewSurfaces[i-SurIndexStart][Pi1]) - GetPointPos(NewSurfaces[i-SurIndexStart][Pi2]))< EPSILON)
					{
						assert(NewSurfaces[i-SurIndexStart][Pi1] != NewSurfaces[i-SurIndexStart][Pi2]);
						ToMerge.push_back({NewSurfaces[i-SurIndexStart][Pi1], NewSurfaces[i-SurIndexStart][Pi2]});
						++TM;
					}
				}
			}
		}
		if(Size-TM > 2)
		{
			SurfacePoints_.push_back(NewSurfaces[i-SurIndexStart]);
			S = Surface(SurfacePlanes_[i].N, GetPointPos(NewSurfaces[i-SurIndexStart][0]), NewCol);
			NewSurfacePlanes.push_back(S);
		}
	}
	Cut = CreateNewSurface(CutSurface, -SplittingSurface.N);
	Size = Cut.size();
	TM = 0;
	if(Size > 2)
	{
		for(int Pi1 = 0; Pi1 < Size; ++Pi1)
		{
			for(int Pi2 = Pi1+1; Pi2 < Size; ++Pi2)
			{
				if(LEN(GetPointPos(Cut[Pi1]) - GetPointPos(Cut[Pi2]))< EPSILON)
				{
					assert(Cut[Pi1] != Cut[Pi2]);
					ToMerge.push_back({Cut[Pi1], Cut[Pi2]});
					++TM;
				}
			}
		}
	}
	if(Size - TM > 2)
	{
		SurfacePoints_.push_back(Cut);
		S = Surface(-SplittingSurface.N, GetPointPos(CutSurface[0]), NewCol);
		NewSurfacePlanes.push_back(S);
	}
	Size = ToMerge.size();
	if(Size)
	{
		for(int i1 = 0; i1 < Size; ++i1)				//prune doubles
		{
			for(int i2 = i1+1; i2 < Size; ++i2)
			{
				if((ToMerge[i1].x == ToMerge[i2].x && ToMerge[i1].y == ToMerge[i2].y)||(ToMerge[i1].y == ToMerge[i2].x && ToMerge[i1].x == ToMerge[i2].y))
				{
					ToMerge.erase(ToMerge.begin()+i2);
					--Size;
					--i2;
				}

			}
		}
		for(auto it = SurfacePoints_.begin(); it!= SurfacePoints_.end(); ++it)
		{
			for(auto it2 = ToMerge.begin(); it2!= ToMerge.end(); ++it2)
			{
				auto itx = std::find(it->begin(), it->end(), it2->x);
				auto ity = std::find(it->begin(), it->end(), it2->y);
				if(itx != it->end() && ity != it->end())
				{
					it->erase(ity);
				}
			}
		}
	}


	SurfacePlanes_.erase(SurfacePlanes_.begin()+SurIndexStart, SurfacePlanes_.begin()+SurIndexEnd);
	//SurfaceVector_ = NewSurfacePlanes;
	SurfacePlanes_.insert(SurfacePlanes_.end(),  NewSurfacePlanes.begin(), NewSurfacePlanes.end());
	UpdateSurfaceEdges();
	//UpdateSurfaceNormalsAndPos();
	//for(auto S = SurfaceVector_.begin(); S != SurfaceVector_.end(); ++S)
	//	DrawDebugSurface(*S, RandomCol());
}