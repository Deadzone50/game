﻿#pragma once
#include "Character/Animator.h"
#include "Game.h"
#include "Containers.h"
#include "Menu.h"

enum CurrentActive
{
	AnimatorActive,
	GameActive
};

class App
{
public:
	void			InitGame()						{Game_.Init();}
	void			InitAnimator()					{Animator_.Init();}
	void			InitMenu()						{Menu_.Init();}
	void			SetActive(CurrentActive A)		{CurrentActive_ = A;}
	CurrentActive	GetActive()						{return CurrentActive_;}
	Player*			GetPlayer();
	MeshOutput&		GetMeshOutput();
	void			HandleInput(Input &In);
	void			HandleButtons(Input &In);
	void			HandleMouse(Input &In);
private:
	CurrentActive	CurrentActive_;
	Game			Game_;
	Animator		Animator_;
	Menu			Menu_;
	bool			MenuActive_ = false;
	TextLines		Text_ = TextLines(1);
};