#include "Game.h"

//Glut Special keys
#define GLUT_KEY_F1					0x0001
#define GLUT_KEY_F2					0x0002
#define GLUT_KEY_F3					0x0003
#define GLUT_KEY_F4					0x0004
#define GLUT_KEY_F5					0x0005
#define GLUT_KEY_F6					0x0006
#define GLUT_KEY_F7					0x0007
#define GLUT_KEY_F8					0x0008
#define GLUT_KEY_F9					0x0009
#define GLUT_KEY_F10				0x000A
#define GLUT_KEY_F11				0x000B
#define GLUT_KEY_F12				0x000C
#define GLUT_KEY_LEFT				0x0064
#define GLUT_KEY_UP					0x0065
#define GLUT_KEY_RIGHT				0x0066
#define GLUT_KEY_DOWN				0x0067
#define GLUT_KEY_PAGE_UP			0x0068
#define GLUT_KEY_PAGE_DOWN			0x0069
#define GLUT_KEY_HOME				0x006A
#define GLUT_KEY_END				0x006B
#define GLUT_KEY_INSERT				0x006C
#define GLUT_KEY_SHIFT				112

int SkeletonJointI = -1;
int static SF = 0;
bool ReleaseP;
bool ReleaseN;
bool Gravity = true;

void
Game::HandleMouse(Input &In)
{
	if(DisplayedScreen_ == CameraScreen)
	{
		if(!MouseInit_)
		{
			Vec2f MouseDelta;
			MouseDelta.x = (float)(In.MousePos.x-In.ScreenSize.x/2);
			MouseDelta.y = (float)(In.MousePos.y-In.ScreenSize.y/2);

			Output_.WarpMouse = false;

			//if((MouseDelta.x < 200) && (MouseDelta.y < 200))
			if(MouseDelta.x || MouseDelta.y)
			{
				//In.MouseDelta.x /= In.ScreenCenter.x;
				//In.MouseDelta.y /= In.ScreenCenter.y;
				MouseDelta.x /= 400;
				MouseDelta.y /= 400;
				GetPlayer()->Rotate({MouseDelta.y, MouseDelta.x, 0});
				//Output_.MouseDelta = MouseDelta;
				Output_.WarpMouse = true;
			}
		}
		else
		{
			MouseInit_ = false;
			//In.MouseDelta.x = 1;
			//Output_.MouseDelta.x = 1;
		}
	}
}

int
Game::HandleButtons(const Input &In)
{
	if(In.MouseButton[0])				//left mouse button
	{
		float Max = FLT_MAX/100;
		DebugLines_.clear();
		for(auto PO =PhysObjects_.begin(); PO != PhysObjects_.end(); ++PO)
		{
			int i = PO->RayCollisionTest(Player_.GetPosition(), Player_.GetDirection(), Max, true);
			if(i >= 0)
			{
				PO->ClearDebugDraws();
				PO->DrawDebugSurface(i, false);
			}
		}
		Line l;
		l.S = Player_.GetPosition()+0.0001f*Player_.GetUp()+0.01f*Player_.GetDirection();
		l.E = Player_.GetPosition() + Max*Player_.GetDirection();
		l.C = Vec3f{10,10,10};
		DebugLines_.push_back(l);
	}
	if(In.MouseButton[3])				//mousewheel
	{
		In.MouseButton[3] = false;
		MoveSpeed_ += 10;
	}
	if(In.MouseButton[4])
	{
		In.MouseButton[4] = false;
		MoveSpeed_ -= 10;
		if(MoveSpeed_ <= 1)
			MoveSpeed_ = 1;
	}
	if(In.KeyStates['p'])
	{
		Pause();
	}
	if(In.KeyStates['P'])
	{
		UnPause();
	}
	if(In.KeyStates['r'])
	{
		Reset();
		//In.KeyStates['r'] = false;
		CurrentSurface_ = 0;
		return SF;
		//LoadScene(CurrentScene_);
	}
	if(In.Changed)						//if a new key has been pressed
	{
		if(In.KeyStates['\t'])			//tab
		{
			if(DisplayedScreen_ == CameraScreen)
				DisplayMap();
			else
				DisplayCamera();
		}
		if(In.KeyStates['e'])
		{
			for(size_t i = 0; i< PhysObjects_.size(); ++i)
			{
				std::vector<Surface> SV = *PhysObjects_[i].GetCollisionSurfaces();
				if(CurrentSurface_ == SV.size())
					CurrentSurface_ = 0;
				PhysObjects_[i].ClearDebugDraws();
				PhysObjects_[i].DrawDebugSurface(CurrentSurface_, true);
			}
			++CurrentSurface_;
		}
		if(In.KeyStates['U'])
		{
			for(size_t i = 0; i< PhysObjects_.size(); ++i)
			{
				PhysObjects_[i].ClearDebugDraws();

				std::vector<Surface> SV = *PhysObjects_[i].GetCollisionSurfaces();
				for(size_t i2 = 0; i2< SV.size(); ++i2)
				{
					PhysObjects_[i].DrawDebugSurface(i2, true);
				}
			}
		}
		if(In.KeyStates['E'])
		{
			for(size_t i = 0; i< PhysObjects_.size(); ++i)
			{
				PhysObjects_[i].ClearDebugDraws();

				std::vector<Surface> SV = *PhysObjects_[i].GetSurfaces();
				for(size_t i2 = 0; i2< SV.size(); ++i2)
				{
					PhysObjects_[i].DrawDebugSurface(i2, false);
				}
			}
		}
		if(In.KeyStates['t'])
		{
			for(size_t i = 0; i< PhysObjects_.size(); ++i)
			{
				if(PhysObjects_[i].DrawOutline)
					PhysObjects_[i].DrawOutline = false;
				else
					PhysObjects_[i].DrawOutline = true;
			}
		}
		if(In.KeyStates['T'])
		{
			for(size_t i = 0; i< PhysObjects_.size(); ++i)
			{
				if(PhysObjects_[i].DrawLinks)
					PhysObjects_[i].DrawLinks = false;
				else
					PhysObjects_[i].DrawLinks = true;
			}
		}
		if(In.KeyStates['y'])
		{
			for(size_t i = 0; i< PhysObjects_.size(); ++i)
			{
				if(PhysObjects_[i].DrawCollisionLinks)
					PhysObjects_[i].DrawCollisionLinks = false;
				else
					PhysObjects_[i].DrawCollisionLinks = true;
			}
		}
		if(In.KeyStates['g'])
		{
			for(size_t i = 0; i < PhysObjects_.size(); ++i)
			{
				PhysObjects_[i].SetGravity(Gravity);
			}
			Gravity = !Gravity;
		}
	}
	float Factor = 3.0f/2.0f;
	if(In.KeySpecialStates[GLUT_KEY_SHIFT])
	{
		Factor*=10;
	}
	if(In.KeyStates['w']||In.KeyStates['W'])
	{
		GetPlayer()->Move(In.dt*MoveSpeed_*Factor*Vec3f{0,0,-1});
	}
	if(In.KeyStates['a']||In.KeyStates['A'])
	{
		GetPlayer()->Move(In.dt*MoveSpeed_*Factor*Vec3f{-1,0,0});
	}
	if(In.KeyStates['s']||In.KeyStates['S'])
	{
		GetPlayer()->Move(In.dt*MoveSpeed_*Factor*Vec3f{0,0,1});
	}
	if(In.KeyStates['d']||In.KeyStates['D'])
	{
		GetPlayer()->Move(In.dt*MoveSpeed_*Factor*Vec3f{1,0,0});
	}
	if(In.KeyStates[' '])
	{
		Vec3f CurrentPos = GetPlayer()->GetPosition();
		GetPlayer()->SetPosition({CurrentPos.x, 1.8f, CurrentPos.z});
	}
	if(In.KeyStates['0']){MoveSpeed_ = 10;}
	if(In.KeyStates['1']){MoveSpeed_ = 100;}
	if(In.KeyStates['2']){MoveSpeed_ = 500;}
	if(In.KeyStates['3']){MoveSpeed_ = 1000;}
	if(In.KeyStates['4']){MoveSpeed_ = 1500;}
	if(In.KeyStates['5']){MoveSpeed_ = 2000;}
	if(In.KeyStates['6']){MoveSpeed_ = 2500;}
	if(In.KeyStates['7']){MoveSpeed_ = 3000;}
	if(In.KeyStates['8']){MoveSpeed_ = 3500;}
	if(In.KeyStates['9']){MoveSpeed_ = 4000;}
	if(In.KeyStates['+'])
	{
		if(ReleaseP)
		{
			--SkipFactor_;
			if(SkipFactor_< 0)
				SkipFactor_ = 0;
			SF=0;
			ReleaseP = false;
		}
	}
	else
	{
		ReleaseP = true;
	}
	if(In.KeyStates['-'])
	{
		if(ReleaseN)
		{
			++SkipFactor_;
			SF=0;
			ReleaseN = false;
		}
	}
	else
	{
		ReleaseN = true;
	}
	if(In.KeySpecialStates[GLUT_KEY_F1])
	{
		ChangeScene("Scene1.txt");
		CurrentSurface_ = 0;
	}
	if(In.KeySpecialStates[GLUT_KEY_F2])
	{
		ChangeScene("Scene2.txt");
		CurrentSurface_ = 0;
	}
	if(In.KeySpecialStates[GLUT_KEY_F3])
	{
		ChangeScene("Scene3.txt");
		CurrentSurface_ = 0;
	}
	if(In.KeySpecialStates[GLUT_KEY_F4])
	{
		ChangeScene("Scene4.txt");
		CurrentSurface_ = 0;
	}
	if(In.KeyStates['q'])
	{
		Exit();
	}
	return SF;
}