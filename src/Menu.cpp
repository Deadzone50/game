#include "Menu.h"
#include "FileIO.h"
#include "GLHelperFunctions.h"

extern TextureManager TexManager;
void Menu::Init()
{
	CreateMenu("Menu.txt");
	Player_ = Player(Vec3f{0,0,1}, Vec3f{0,0,-1}, 60.0f, 1.0f, 0.1f, 10.0f);
	std::vector<Vec4f> Colors = {Vec4f{1,0,0,1},Vec4f{0,1,0,1},Vec4f{0,0,1,1},Vec4f{1,1,1,1},Vec4f{0.5f,0.5f,0.5f,1}};	//R,G,B,White,Gray
	//PointMesh_.TextureId_ = LineMesh_.TextureId_;
	//PointMesh_.Type_ = MeshType_Point;
	
	TriangleMesh_.TextureId_ = TexManager.GenerateColorTexture(Colors, ColorCoords_);
	TriangleMesh_.Type_ = MeshType_Triangle;
	LineMesh_.TextureId_ = TriangleMesh_.TextureId_;
	LineMesh_.Type_ = MeshType_Line;

	Output_.WarpMouse = false;
	Output_.Player = &Player_;
	Output_.SSMeshes.push_back(&LineMesh_);
	//Output_.SSMeshes.push_back(&PointMesh_);
	Output_.SSMeshes.push_back(&TriangleMesh_);

}
void
Menu::CreateMenu(std::string Name)
{
	Items_.clear();
	LoadMenuFile(Name, Items_);
	//for(size_t i = 0; i < Items_.size(); ++i)
	//{
	//	Items_[i].Update();
	//}
}
void
Menu::UpdateMenuMesh()
{
	TriangleMesh_.Clear();
	for(auto it = Items_.begin(); it != Items_.end(); ++it)
	{
		Vec2f Min = ((it->GetMin() + Vec2f{1.0f, 1.0f})*0.5f)*Resolution_;
		Vec2f Max = ((it->GetMax() + Vec2f{1.0f, 1.0f})*0.5f)*Resolution_;
		Vec3f Color = it->GetColor();
		Vertex LB, RB, RT, LT;
		LB.Position = {Min.x, Min.y, -0.1f};RB.Position = {Max.x, Min.y, -0.1f};
		RT.Position = {Max.x, Max.y, -0.1f};LT.Position = {Min.x, Max.y, -0.1f};
		if(Color == Vec3f{1,0,0})
		{
			LB.TexCoord = ColorCoords_[0];RB.TexCoord = ColorCoords_[0];
			RT.TexCoord = ColorCoords_[0];LT.TexCoord = ColorCoords_[0];
		}
		else if(Color == Vec3f{0,1,0})
		{
			LB.TexCoord = ColorCoords_[1];RB.TexCoord = ColorCoords_[1];
			RT.TexCoord = ColorCoords_[1];LT.TexCoord = ColorCoords_[1];
		}
		else if(Color == Vec3f{0,0,1})
		{
			LB.TexCoord = ColorCoords_[2];RB.TexCoord = ColorCoords_[2];
			RT.TexCoord = ColorCoords_[2];LT.TexCoord = ColorCoords_[2];
		}														
		else if(Color == Vec3f{1,1,1})							
		{														
			LB.TexCoord = ColorCoords_[3];RB.TexCoord = ColorCoords_[3];
			RT.TexCoord = ColorCoords_[3];LT.TexCoord = ColorCoords_[3];
		}														
		else //if(Color == Vec3f{0.5f,0.5f,0.5f})				
		{														
			LB.TexCoord = ColorCoords_[4];RB.TexCoord = ColorCoords_[4];
			RT.TexCoord = ColorCoords_[4];LT.TexCoord = ColorCoords_[4];
		}
		TriangleMesh_.AddQuad(LB, RB, RT, LT);
	}

	LineMesh_.Clear();
	if(SelectedInput_ != -1)
	{
		Vec2f Min = ((Items_[SelectedInput_].GetMin() + Vec2f{1.0f, 1.0f})*0.5f)*Resolution_;
		Vec2f Max = ((Items_[SelectedInput_].GetMax() + Vec2f{1.0f, 1.0f})*0.5f)*Resolution_;
		Vec3f Color = {1,0,0};
		Vertex LB, RB, RT, LT;
		LB.Position = {Min.x, Min.y, -0.1f};RB.Position = {Max.x, Min.y, -0.1f};
		RT.Position = {Max.x, Max.y, -0.1f};LT.Position = {Min.x, Max.y, -0.1f};
		unsigned int FirstI = LineMesh_.GetCurrentIndex();
		LineMesh_.AddVertex(LB);LineMesh_.AddVertex(RB);
		LineMesh_.AddVertex(RT);LineMesh_.AddVertex(LT);
		LineMesh_.AddIndex(FirstI);
		LineMesh_.AddIndex(FirstI+1);

		LineMesh_.AddIndex(FirstI+1);
		LineMesh_.AddIndex(FirstI+2);

		LineMesh_.AddIndex(FirstI+2);
		LineMesh_.AddIndex(FirstI+3);

		LineMesh_.AddIndex(FirstI+3);
		LineMesh_.AddIndex(FirstI);
	}
}
MeshOutput &Menu::GetMeshOutput()
{
	UpdateMenuMesh();
	Output_.SSText.clear();
	TextLine TL;
	for(auto it = Items_.begin(); it != Items_.end(); ++it)
	{
		TL.Pos = it->GetPosition();
		TL.C = it->GetColor();
		TL.Text = it->GetText();
		Output_.SSText.push_back(TL);

		TL.Pos = it->GetPosition()+Vec3f{0,-0.05f,0};
		TL.Text = it->GetInputText();
		Output_.SSText.push_back(TL);
	}

	return Output_;
}

std::string
Menu::Click(Vec2f P)
{
	for(size_t i = 0; i < Items_.size(); ++i)
	{
		if(P.x >= Items_[i].GetMin().x && P.x <= Items_[i].GetMax().x)
		if(P.y >= Items_[i].GetMin().y && P.y <= Items_[i].GetMax().y)
		{
			if(Items_[i].GetType() == MenuInput)
				SelectedInput_ = (int)i;
			return Items_[i].GetText();
		}
	}
	return("");
}
int Menu::GetValue(std::string Name)
{
	for(int i = 0; i < (int)Items_.size(); ++i)
	{
		if(!Items_[i].GetText().compare(Name))
			return Items_[i].GetValue();
	}
	return 0;
}

void
Menu::InputNumber(std::string N)
{
	if(SelectedInput_ != -1)
	{
		Items_[SelectedInput_].InputNumber(N);
	}
}

void
Menu::InputBackspace()
{
	if(SelectedInput_ != -1)
	{
		Items_[SelectedInput_].InputBackspace();
	}
}

MenuItem::MenuItem(Vec2f Size, Vec2f Position, std::string Text, MenuType Type, Vec3f Color)
{
	Position_ = Position;
	Min_ = Vec2f{-Size.x/2, -Size.y/2}+Position;
	Max_ = Vec2f{Size.x/2, Size.y/2}+Position;
	Type_ = Type;
	Text_ = Text;
	Color_ = Color;
}

void
MenuItem::InputNumber(std::string N)
{
	if(Input_.size() < 9)
	{
		Input_.append(N);
		Value_ = std::stoi(Input_);
		//Update();
	}
}

void
MenuItem::InputBackspace()
{
	if(!Input_.empty())
	{
		Input_.pop_back();
		if(Input_.size() < 10 && Input_.size() > 0)
			Value_ = std::stoi(Input_);
		//Update();
	}
}
