#include <GL/glew.h>
#include "Character/Character.h"
#include "Collision/CollisionTestsBasic.h"
#include "Mesh.h"

extern GLint AttributeVertexPosition, AttributeTexCoord, AttributeSSDJointIndex, AttributeSSDWeights;
extern GLint UniformModel, UniformSSDT;

void Mesh::DeleteBuffers()
{
	glDeleteVertexArrays(1, &VAO_);
	glDeleteBuffers(1, &VBO_);
	glDeleteBuffers(1, &EBO_);
	VAO_ = 0;
	VBO_ = 0;
	EBO_ = 0;
}
bool
Mesh::RayCollisionTest(Vec3f RayOrigin, Vec3f RayDir, float &MaxLen)
{
	size_t NumIndices = Indices_.size();
	bool Hit = false;
	for(size_t i = 0; i < NumIndices; i += 3)
	{
		Vec3f P1 = Vertices_[(Indices_[i])].Position;
		Vec3f P2 = Vertices_[(Indices_[i+1])].Position;
		Vec3f P3 = Vertices_[(Indices_[i+2])].Position;

		Vec3f AB = P2 - P1; 
		Vec3f BC = P3 - P2; 

		Vec3f Normal = NORMALIZE(CROSS(AB, BC));
		float Len = MaxLen;
		if(RayPlaneCollisionTest(Normal, P1, RayOrigin, RayDir, Len))
		{
			Vec3f P = RayOrigin + Len*RayDir;

			Vec3f CA = P1 - P3;

			Vec3f AP = P - P1; 
			Vec3f BP = P - P2; 
			Vec3f CP = P - P3; 

			Vec3f ABP = CROSS(AB, AP);
			Vec3f BCP = CROSS(BC, BP);
			Vec3f CAP = CROSS(CA, CP);

			if (DOT(Normal, ABP) > 0 && 
				DOT(Normal, BCP) > 0 && 
				DOT(Normal, CAP) > 0)
			{
				//float Area = LEN(CROSS(AB, BC));
				//u = LEN(CAP) / Area;
				//v = LEN(ABP) / Area;
				Hit = true;		//P is inside the triangle
				MaxLen = Len;
			}
		}
	}
	return Hit;
}
void Mesh::SetupMesh()
{
	DeleteBuffers();
	size_t SIZE = Vertices_.size();
	if(!SIZE)
	{
		Changed_ = false;
		return;
	}

	///Generate Buffers
	glGenVertexArrays(1, &VAO_);
	glGenBuffers(1, &VBO_);
	glGenBuffers(1, &EBO_);

	glBindVertexArray(VAO_);			//Set the mesh VAO, remembers AttribPointers

	glBindBuffer(GL_ARRAY_BUFFER, VBO_);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*Vertices_.size(), &Vertices_[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*Indices_.size(), &Indices_[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, VBO_);
	glEnableVertexAttribArray(AttributeVertexPosition);
	glVertexAttribPointer(AttributeVertexPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

	//glEnableVertexAttribArray(AttributeVertexNormal);
	//glVertexAttribPointer(AttributeVertexNormal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));

	glEnableVertexAttribArray(AttributeTexCoord);
	glVertexAttribPointer(AttributeTexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoord));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	Changed_ = false;
}
size_t Mesh::DrawMesh()
{
	if(Changed_)
		SetupMesh();
	glActiveTexture(GL_TEXTURE0);				//activate texture unit 0
	glBindTexture(GL_TEXTURE_2D, TextureId_);	//bind texture to texture unit 0

	glBindVertexArray(VAO_);					//Set the mesh VAO
	int Size = (int)Indices_.size();
	GLenum type;
	if(Type_ == MeshType_Point)		type = GL_POINTS;
	else if(Type_ == MeshType_Line)	type = GL_LINES;
	else				type = GL_TRIANGLES;

	glDrawElements(type, Size, GL_UNSIGNED_INT, (void*)0);

	return Size/3;
}
void Mesh::Reserve(unsigned int Quads)
{
	Vertices_.reserve(Quads*4+Vertices_.size());
	Indices_.reserve(Quads*6+Indices_.size());
}
void Mesh::ShrinkToFit()
{
	Vertices_.shrink_to_fit();
	Indices_.shrink_to_fit();
}
void Character::DeleteBuffers()
{
	glDeleteBuffers(1, &VBOSSD_);
	VBOSSD_ = 0;
}
void Character::SetupMesh()
{
	DeleteBuffers();
	size_t SIZE = Mesh_->Vertices_.size();
	if(!SIZE)
	{
		Mesh_->Changed_ = false;
		return;
	}
	Mesh_->SetupMesh();

	glGenBuffers(1, &VBOSSD_);
	glBindBuffer(GL_ARRAY_BUFFER, VBOSSD_);
	glBufferData(GL_ARRAY_BUFFER, sizeof(SSDData)*SSDData_.size(), &SSDData_[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(AttributeSSDJointIndex);
	glVertexAttribIPointer(AttributeSSDJointIndex, 1, GL_INT, sizeof(SSDData), (void*)0);

	glEnableVertexAttribArray(AttributeSSDWeights);
	glVertexAttribPointer(AttributeSSDWeights, 1, GL_FLOAT, GL_FALSE, sizeof(SSDData), (void*)offsetof(SSDData, Weight));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	Mesh_->Changed_ = false;
}
size_t Character::Draw()
{
	if(Mesh_->Changed_)
		SetupMesh();
	glUniformMatrix4fv(UniformModel, 1, GL_TRUE, Model_.m);

	std::vector<Mat4f> SSDT = Skeleton_.GetSSDTransforms();
	glUniformMatrix4fv(UniformSSDT, 15, GL_TRUE, &SSDT[0].m[0]);

	glActiveTexture(GL_TEXTURE0);						//activate texture unit 0
	glBindTexture(GL_TEXTURE_2D, Mesh_->TextureId_);	//bind texture to texture unit 0

	glBindVertexArray(Mesh_->VAO_);						//Set the mesh VAO
	int Size = (int) Mesh_->Indices_.size();
	glDrawElements(GL_TRIANGLES, Size, GL_UNSIGNED_INT, (void*)0);
	//glDrawElements(GL_POINTS, Size, GL_UNSIGNED_INT, (void*)0);

	return Size/3;
}
size_t MeshObject::Draw()
{
	glUniformMatrix4fv(UniformModel, 1, GL_TRUE, Model_.m);
	//for(auto it = Meshes_.begin(); it != Meshes_.end(); ++it)
	//{
	//	(*it)->DrawMesh();
	//}
	return Mesh_->DrawMesh();
}