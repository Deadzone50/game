#pragma once
#include "Containers.h"
#include "Character/Character.h"
enum MenuType
{
	MenuButton, MenuInput
};

class MenuItem
{
public:
	MenuItem(Vec2f Size, Vec2f Position, std::string Text, MenuType Type, Vec3f Color);
	
	void InputNumber(std::string N);
	void InputBackspace();

	Vec3f			GetColor()					{return Color_;}
	Vec3f			GetPosition()				{return {Position_.x, Position_.y,0};}
	std::string		GetText()					{return Text_;}
	std::string		GetInputText()				{return Input_;}

	Vec2f			GetMin()					{return Min_;}
	Vec2f			GetMax()					{return Max_;}

	MenuType		GetType()					{return Type_;}
	int				GetValue()					{return Value_;}
	void			SetValue(int V)				{Value_ = V;}
	void			SetInput(std::string &S)	{Input_ = S;}

private:
	Vec2f Min_, Max_, Position_;
	std::string Text_;
	MenuType Type_;
	std::string Input_ = {};
	int Value_;
	Vec3f Color_;
};

class Menu
{
public:
	void			Init();
	void			CreateMenu(std::string Name);
	void			UpdateMenuMesh();
	Player*			GetPlayer()						{return &Player_;}
	MeshOutput&		GetMeshOutput();
	void			SetResolution(Vec2i ScreenSize)	{Resolution_ = ScreenSize;}

	std::string		Click(Vec2f P);

	int				GetValue(std::string Name);
	void			InputNumber(std::string N);
	void			InputBackspace();

private:
	Player					Player_;
	Mesh					PointMesh_, LineMesh_, TriangleMesh_;
	std::vector<Vec2f>		ColorCoords_;
	int						SelectedInput_ = -1;
	std::vector<MenuItem>	Items_;
	MeshOutput				Output_;
	Vec2i					Resolution_;
};