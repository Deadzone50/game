#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>
#include <vector>
#include <map>
#include "Character/Skeleton.h"
#include "Containers.h"
#include "Menu.h"
#include "FileIO.h"

void
PrintProgessToCout(float Progress)
{
	std::cout << std::setprecision(3) << std::fixed << Progress*100 << " %\r";
}

void
LoadMenuFile(std::string MenuName, std::vector<MenuItem> &Items)
{
	std::ifstream file("Assets/"+MenuName);
	if(!file.is_open())
	{
		std::cout << "Could not load menu\n";
		return;
	}
	std::string Input;
	std::string TypeText;
	bool Run = true;
	//float F1, F2, F3;

	std::vector<std::string> Text;
	std::vector<MenuType> Type;
	std::vector<int> Value;
	int I;
	int Entries = 0;

	file >> Input;
	while(Run)
	{
		if(!Input.compare("{"))					//text
		{
			file.ignore(1);
			std::getline(file, Input);
			Text.push_back(Input);
			Value.push_back(0);
		}
		else if(!Input.compare("Type"))			//type
		{
			file >> TypeText;
			if(!TypeText.compare("Button"))
				Type.push_back(MenuButton);
			else if(!TypeText.compare("Input"))
				Type.push_back(MenuInput);
		}
		else if(!Input.compare("Value"))
		{
			file >> I;
			Value.back() = I;
		}
		//else if(!Input.compare("Color"))
		//{
		//	file >> F1 >> F2 >> F3;
		//	Color = Vec3f{F1,F2,F3};
		//}
		else if(!Input.compare("}"))
		{
			++Entries;
		}

		file >> Input;
		if(!Input.compare("end"))							//file end
		{
			Run = false;
			file.close();
		}
	}
	Vec2f Size = {0.5f, 0.2f};
	Vec3f Color = Vec3f{0.5f,0.5f,0.5f};
	for(int i = 0; i < Entries; ++i)
	{
		Vec2f Position = {0, 1.0f -(i+1)*(2.0f/(Entries+1))};
		Items.push_back(MenuItem(Size, Position, Text[i], Type[i], Color));
		Items.back().SetValue(Value[i]);
		if(Value[i])
		{
			std::string Str = std::to_string(Value[i]);
			Items.back().SetInput(Str);
		}
	}
}
void
LoadPhysObject(std::ifstream &file, std::vector<PhysObject> &PhysObjects)		//TODO: load to arbitrary struct
{
	std::string Input;
	float F1, F2, F3;
	std::string Name;
	Vec3f Pos = Vec3f{0,0,0};
	Vec3f Color = Vec3f{0,0,0};
	Vec3f Rot = Vec3f{0,0,0};
	Vec3f Speed = Vec3f{0,0,0};
	float Scale = 1;
	float Mass = 0;

	file >> Input;
	while(true)
	{
		if(!Input.compare("{"))
		{
			file >> Name;
		}
		else if(!Input.compare("Pos"))
		{
			file >> F1 >> F2 >> F3;
			Pos = Vec3f{F1,F2,F3};
		}
		else if(!Input.compare("Mass"))
		{
			file >> Mass;
		}
		else if(!Input.compare("Color"))
		{
			file >> F1 >> F2 >> F3;
			Color = Vec3f{F1,F2,F3};
		}
		else if(!Input.compare("Speed"))
		{
			file >> F1 >> F2 >> F3;
			Speed = Vec3f{F1,F2,F3};
		}
		else if(!Input.compare("Rotation"))
		{
			file >> F1 >> F2 >> F3;
			Rot = Vec3f{F1,F2,F3};
		}
		else if(!Input.compare("Scale"))
		{
			file >> F1;
			Scale = F1;
		}
		else if(!Input.compare("}"))			//end of object
		{
			PhysObject O(Name, Pos, Rot, Speed, Scale, Color, Mass);
			PhysObjects.push_back(O);
			Pos = Vec3f{0,0,0};
			Color = Vec3f{0,0,0};
			Rot = Vec3f{0,0,0};
			Speed = Vec3f{0,0,0};
			Scale = 1;
			Mass = 0;

			break;
		}
		file >> Input;
	}
}

void
LoadWorld(std::ifstream &file, World &W)
{
	std::string Input;
	std::string Name;
	int Seed = 0;
	int MaxH = 0;
	int Size = 0;
	bool Z = false;

	file >> Input;
	while(true)
	{
		if(!Input.compare("Seed"))
		{
			file >> Seed;
		}
		else if(!Input.compare("MaxH"))
		{
			file >> MaxH;
		}
		else if(!Input.compare("Size"))
		{
			file >> Size;
		}
		else if(!Input.compare("Zone"))
		{
			file >> Z;
		}
		else if(!Input.compare("}"))			//end of World
		{
			W = World(Seed, MaxH, Size, Z);
			break;
		}
		file >> Input;
	}
}

void
LoadScene(std::string SceneName, World	&World, std::vector<Object> &Objects,
		  std::vector<PhysObject> &PhysObjects)
{
	(void)Objects;
	std::ifstream file("Assets/Scenes/"+SceneName);
	if(!file.is_open())
	{
		std::cout << "Could not load scene\n";
		return;
	}

	std::string Input;
	file >> Input;

	while(true)
	{
		if(!Input.compare("PO"))
		{
			LoadPhysObject(file, PhysObjects);
		}
		//if(!Input.compare("NPC"))
		//{
		//	LoadNPC(file, NPCs, ID);
		//}
		if(!Input.compare("WORLD"))
		{
			LoadWorld(file, World);
		}

		file >> Input;
		if(!Input.compare("end"))							//file end
		{
			file.close();
			break;
		}
	}
}

void
LoadMesh(std::string Name, Vec3f Color, std::vector<PhysPoint> &Points, std::vector<Surface> &Surfaces)
{
	std::ifstream file("Assets/Models/"+Name);
	if(!file.is_open())
	{
		std::cout << "Could not load mesh\n";
		return;
	}
	std::string Input;
	bool Run = true;
	std::vector<Vec3f> Normals;
	float F1,F2,F3;
	int I1,I2;
	int I3 = 0;

	int SurfaceIndex = 0;
	file >> Input;

	while(Run)
	{
		if(!Input.compare("v"))								//point
		{
			PhysPoint P;
			file >> F1 >> F2 >> F3;
			P.CPos = Vec3f{F1, F2, F3};
			P.LPos = P.CPos;
			Points.push_back(P);
		}
		else if(!Input.compare("vn"))
		{
			file >> F1 >> F2 >> F3;
			Normals.push_back({F1,F2,F3});
		}
		else if(!Input.compare("f"))						//surface
		{
			std::vector<int> S;
			file >> I1;
			while(!file.fail())
			{
				S.push_back(I1-1);

				int c = file.peek();
				if(c == '/')
				{
					file.ignore();			//ignore slash
					c = file.peek();
					if(c == (int)'/')
					{
						file.ignore();			//ignore slash
						file >> I3;				//read the normal
					}
					else
					{
						file >> I2;				//read the texture
						c = file.peek();
						if(c == (int)'/')
						{
							file.ignore();			//ignore slash
							file >> I3;				//read the normal
						}

					}
				}
				file >> I1;
			}
			file.clear();									//clear the failbit
			Surfaces.push_back(Surface());
			Surfaces[SurfaceIndex].Points = S;
			Surfaces[SurfaceIndex].C = Color;
			Surfaces[SurfaceIndex].P = Points[S[0]].CPos;
			if(I3)
				Surfaces[SurfaceIndex].N = Normals[I3-1];
			else
			{
				size_t NumPoints = S.size();
				Vec3f C = {0};
				for(size_t i2 = 0; i2 < NumPoints; ++i2)		//calculate center, helps if the surface is small and far from world origin
				{
					C += Points[S[i2]].CPos;
				}
				C = C/(float)NumPoints;
				Vec3f A = Points[S[NumPoints-1]].CPos;
				Vec3f B = Points[S[0]].CPos;
				Vec3f CrossSum = CROSS(A-C, B-C);
				for(size_t i2 = 0; i2 < NumPoints-1; ++i2)		//take cross of all pairs of vectors, that way the majority should be in the right orientation
				{
					A = Points[S[i2]].CPos;
					B = Points[S[i2+1]].CPos;
					CrossSum += CROSS(A-C, B-C);
				}
				Surfaces[SurfaceIndex].N = NORMALIZE(CrossSum);
			}
			++SurfaceIndex;
		}
		else if(!Input.compare("o"))
		{
			file >> Input;									//discard name of object
		}
		else if(!Input.compare("#"))
		{
			file.ignore(1000,'\n'); //large enough?
		}

		file >> Input;
		if(file.eof())							//file end
		{
			Run = false;
			file.close();
		}
	}
}

void
ParseMtlFile(std::string FullPath, std::string Name, std::vector<Material> &Materials, std::map<std::string, unsigned int> &MaterialMap)
{
	std::string MaterialName;
	Material *CurrentMaterial = new Material;
	std::string Line;
	std::string Word;
	//Vec3f V3;
	//float F1;

	std::ifstream file;
	file.open(FullPath+Name);											//parse materials
	while(std::getline(file, Line))
	{
		if((Line[0] == '#') || (Line[0] == '\0'))				//skip comments and empty lines
			continue;
		std::istringstream iss(Line);
		iss >> Word;
		if(Word == "newmtl")	//material name
		{
			iss >> MaterialName;
			MaterialMap[MaterialName] = (unsigned int)Materials.size();

			Materials.push_back(Material{});
			CurrentMaterial = &Materials.back();
		}
		else if (Word == "Ka")	//ambient color
		{

		}
		else if (Word == "Kd")	//diffuse color
		{

		}
		else if (Word == "Ks")	//specular color
		{

		}
		else if (Word == "Ns")	//specular exponent
		{

		}
		else if (Word == "Ke")	//emissive color
		{

		}
		else if (Word == "d")	//dissolve
		{

		}
		else if (Word == "Tr")	//transparency
		{

		}
		else if (Word == "Ni")	//index of refraction
		{

		}
		else if (Word == "Tf")	//transmission filter, Any light passing through the object is filtered by the transmission filter
		{

		}
		else if (Word == "map_Ka")
		{

		}
		else if (Word == "map_Ks")
		{
			std::string TexName;
			iss >> TexName;
			std::string FullTexName = FullPath+"/"+TexName;
			//CurrentMaterial->SpecularMap = TexManager.AddTexture(FullTexName);
		}
		else if (Word == "map_Kd")
		{
			std::string TexName;
			iss >> TexName;
			std::string FullTexName = FullPath+"/"+TexName;
			//CurrentMaterial->DiffuseTexture = TexManager.AddTexture(FullTexName);
		}
		else if (Word == "map_d")
		{
			//iss >> Materials[MaterialMap[MaterialName]].map_d;
		}
		else if (Word == "map_bump")
		{
			std::string TexName;
			iss >> TexName;
			std::string FullTexName = FullPath+"/"+TexName;
			//CurrentMaterial->NormalMap = TexManager.AddTexture(FullTexName);
		}
		else if (Word == "bump")
		{
			//iss >> Materials[MaterialMap[MaterialName]].bump;
		}
		else if (Word == "disp")
		{
			//std::string TexName;
			//iss >> TexName;
			//std::string FullTexName = FullPath+"/"+TexName;
			//ReadImageTexture(FullTexName, Materials[MaterialMap[MaterialName]].TextureMaps[map_disp]);
		}
		else if (Word == "illum")		//illumination model, not used
		{
			//discard
		}
		else
		{
			continue;					//unknown material property
		}
	}
	file.close();
}

bool operator==(const Vertex &V1, const Vertex &V2)
{
	if(V1.Position == V2.Position)
		if(V1.Normal == V2.Normal)
			if(V1.TexCoord == V2.TexCoord)
				return true;
	return false;
}

void
ParseObjFile(std::string FullPath, std::string Name, std::vector<Mesh> &Meshes, std::vector<Material> &Materials)
{
	std::vector<Vec3f> VertexPositions;
	std::vector<Vec2f> TextureCordinates;
	std::vector<Vec3f> VertexNormals;

	std::string Line;
	std::string Word;
	Vec3f V3;
	Vec2f V2;

	std::string ObjectName = "";
	std::string MaterialName;
	std::map<std::string, unsigned int> MaterialMap;

	std::vector<Vertex> *Vertices;
	std::vector<unsigned int> *Indices;
	Mesh *CurrentMesh;

	Meshes.push_back(Mesh{});				//setup first mesh
	CurrentMesh = &Meshes.back();
	Vertices = &Meshes.back().Vertices_;
	Indices = &Meshes.back().Indices_;

	std::ifstream file;
	file.open(FullPath+Name);
	if(!file.is_open())
	{
		std::cout << FullPath+Name << " not found\n";
	}

	while(std::getline(file, Line))
	{
		if((Line[0] == '#') || (Line[0] == '\0'))	//skip comments and empty lines
			continue;
		std::istringstream iss(Line);
		iss >> Word;
		if(Word == "v")								//vertex, assumes no w cordinate
		{
			iss >> V3;
			VertexPositions.push_back(V3);
		}
		else if(Word == "vt")						//texture coordinate, assumes no w cordinate
		{
			iss >> V2;
			TextureCordinates.push_back(V2);
		}
		else if(Word == "vn")						//vertex normal
		{
			iss >> V3;
			VertexNormals.push_back(V3);
		}
		else if (Word == "f")						//face element
		{
			int FV[4] = {0};
			int FT[4] = {0};
			int FN[4] = {0};
			for(int i = 0; i<4; ++i)
			{
				iss >> FV[i];						//extract the vertex index
				if (iss.peek() == '/')				//if there are texture, and normal indices
				{
					iss.ignore();
					if(iss.peek() == '/')			//normal index
					{
						iss.ignore();
						iss >> FN[i];
					}
					else
					{								//texture index
						iss >> FT[i];
						if(iss.peek() == '/')		//normal index
						{
							iss.ignore();
							iss >> FN[i];
						}
					}
				}
			}

			if(!FV[3])								//only 3 vertices
			{
				Vertex Vert[3]{};
				Vert[0].Position = VertexPositions[FV[0]-1];
				Vert[1].Position = VertexPositions[FV[1]-1];
				Vert[2].Position = VertexPositions[FV[2]-1];
				if(FN[0])
				{
					Vert[0].Normal = VertexNormals[FN[0]-1];
					Vert[1].Normal = VertexNormals[FN[1]-1];
					Vert[2].Normal = VertexNormals[FN[2]-1];
				}
				if(FT[0])
				{
					Vert[0].TexCoord = TextureCordinates[FT[0]-1];
					Vert[1].TexCoord = TextureCordinates[FT[1]-1];
					Vert[2].TexCoord = TextureCordinates[FT[2]-1];
				}

				int Found[3] = {-1, -1, -1};
				unsigned int VertI[3];

				int SIZE = (int)Vertices->size();
				if(SIZE)
				{
					for(int i2 = SIZE-1; i2 >= 0; --i2)
					{
						for(int i1 = 0; i1 < 3; ++i1)
						{
							if((*Vertices)[i2] == Vert[i1])
							{
								Found[i1] = i2;
								VertI[i1] = i2;
								break;
							}
						}
						if(Found[0] > -1 && Found[1] > -1 && Found[2] > -1)
							break;
					}
				}

				if(Found[0] == -1)
				{
					VertI[0] = (unsigned int)Vertices->size();
					Vertices->push_back(Vert[0]);
				}
				if(Found[1] == -1)
				{
					VertI[1] = (unsigned int)Vertices->size();
					Vertices->push_back(Vert[1]);
				}
				if(Found[2] == -1)
				{
					VertI[2] = (unsigned int)Vertices->size();
					Vertices->push_back(Vert[2]);
				}

				Indices->push_back(VertI[0]);
				Indices->push_back(VertI[1]);
				Indices->push_back(VertI[2]);
			}
			else
				abort();
		}
		else if(Word == "o")						//object
		{
			if(ObjectName != "")					//if not hte first
			{
				Meshes.push_back(Mesh{});
				CurrentMesh = &Meshes.back();
				Vertices = &Meshes.back().Vertices_;
				Indices = &Meshes.back().Indices_;
			}
			iss >> ObjectName;
		}
		else if(Word == "usemtl")					//material
		{
			iss >> MaterialName;
			//CurrentMesh->MaterialIndex = MaterialMap[MaterialName];
		}
		else if(Word == "mtllib")					//material file
		{
			iss >> Word;
			//ParseMtlFile(FullPath, Word, Materials, MaterialMap);
		}
		else										//unknown obj property
		{
			continue;
		}
	}
	file.close();
}

template<class T>
std::ostream& Write(std::ostream &os, const T &x)
{
	return os.write(reinterpret_cast<const char*>(&x), sizeof(x));
}
template<class T>
std::istream& Read(std::istream &is, T &x)
{
	return is.read(reinterpret_cast<char*>(&x), sizeof(x));
}

void Skeleton::SaveAnimation(int AnimationNumber, std::string Name)
{
	std::ofstream file("Assets/Animations/"+Name, std::ios::binary);
	if(!file.is_open())
	{
		std::cout << "Could not open file for saving animation\n";
		return;
	}
	size_t Size1 = KeyFrames_[AnimationNumber].size();
	Write(file, Size1);			//number of frames
	size_t Size2 = KeyFrames_[AnimationNumber][0].size();

	for(size_t i1 = 0; i1 < Size1; ++i1)
	{
		for(size_t i2 = 0; i2 < Size2; ++i2)
		{
			Write(file, KeyFrames_[AnimationNumber][i1][i2]);		//rotation
		}
	}
	file.close();
}
void Skeleton::LoadAnimation(size_t AnimationNumber, std::string Name)
{
	std::ifstream file("Assets/Animations/"+Name, std::ios::binary);
	if(!file.is_open())
	{
		std::cout << "Could not open animation\n";
		return;
	}
	size_t Size1; // = KeyFrames_[AnimationNumber].size();
	Read(file, Size1);			//number of frames
	size_t Size2 = Joints_.size();

	if(AnimationNumber >= KeyFrames_.size())
		KeyFrames_.push_back(std::vector<Vec3fV>(Size1));

	for(size_t i1 = 0; i1 < Size1; ++i1)
	{
		Vec3fV VV(Size2);
		KeyFrames_[AnimationNumber][i1] = VV;
		for(size_t i2 = 0; i2 < Size2; ++i2)
		{
			Vec3f V;
			Read(file, V);		//rotation
			KeyFrames_[AnimationNumber][i1][i2] = V;
		}
	}
	file.close();
}