#pragma once
#include <map>
#include <vector>
#include <GL/glew.h>
#include <GL/glut.h>

#include "Vector/Vector.h"
#include "Containers.h"

bool BindAttribute(const char *AttributeName, GLint &Attribute, GLuint Prog);
bool BindUniform(const char *UniformName, GLint &Uniform, GLuint Prog);

GLuint CreateShader(const char *FileName, GLenum ShaderType);
GLuint CreateProgram(GLint VertexShader, GLint FragmentShader);
bool LoadShader(const char *Name, char **ShaderSource);
void UnloadShader(char *ShaderSource);
void PrintLog(GLuint Object);

class TextureManager
{
public:
	void			LoadFont(std::string FileName);
	unsigned int	AddTexture(std::string FileName);		//returns texture id
	unsigned int	GenerateColorTexture(const std::vector<Vec4f> &Colors, std::vector<Vec2f> &ColorCoords);
	unsigned int	GenerateColorTexture(const Vec4f &Color);
	Mesh			CreateTextMesh(std::string Text, Vec3f Position);
private:
	std::map<std::string, unsigned int> ImageMap_;
	std::map<std::string, unsigned int> GeneratedMap_;
	unsigned int FontTextureId_;
};

class DebugPLManager
{
public:
	Points&			GetPoints()		{return Points_;}
	Lines&			GetLines()		{return Lines_;}
	void			ClearPoints()	{Points_.clear();}
	void			ClearLines()	{Lines_.clear();}

	void			DrawLine(Vec3f Start, Vec3f End, Vec3f Color)	{Lines_.push_back(Line{Start,End,Color});}
	void			DrawPoint(Vec3f Pos, Vec3f Color)				{Points_.push_back(Point{Pos,Color});}
	void			CreatePointAndLineMesh(Mesh &PM, Mesh &LM, const std::vector<Vec2f> &ColorCoords);
private:
	Points			Points_;
	Lines			Lines_;
};
