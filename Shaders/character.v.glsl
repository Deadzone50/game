#ifdef _WIN32
#include "glsl.h"    // Facilitate Intellisense in the shader code
#endif
#version 330
attribute vec3	VPos;
attribute vec3	VNormal;
attribute vec2	TexCoord;
attribute vec3	VColor;

///Transforms
uniform mat4	VP;
uniform mat4	Model;

///SSD
attribute float	SSDWeight;
attribute int	SSDJointIndex;
uniform mat4	SSDT[15];


///To fragment shader
varying vec3	fNormal;
varying vec2	fTexCoord;
varying vec3	fColor;

void main(void)
{
	gl_Position = VP * Model * SSDWeight * SSDT[SSDJointIndex] * vec4(VPos, 1.0);

	fNormal = VNormal;
	fTexCoord = TexCoord;
	fColor = VColor;
}

