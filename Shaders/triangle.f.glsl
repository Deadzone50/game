#ifdef _WIN32
#include "glsl.h"    // Facilitate Intellisense in the shader code
#endif
#version 120

uniform sampler2D	Texture;

///from vertex shader
varying vec3		fNormal;
varying vec2		fTexCoord;
varying vec3		fColor;

void main(void)
{
	//gl_FragColor = vec4(fColor.r, fColor.g, fColor.b, 1.0);

	vec4 Color =  texture2D(Texture, fTexCoord);
	if(Color.a < 0.1)
	{
		discard;
	}
	gl_FragColor = Color;
	
}