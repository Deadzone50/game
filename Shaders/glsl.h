struct vec2
{
	float x,y;
	vec2();
	vec2(vec3);
	vec2(float,float);
};
struct vec3
{
	float x,y,z,  r,g,b;
	vec2 xx,xy,xz,yx,yy,yz,zx,zy,zz;
	vec3();
	vec3(float);
	vec3(vec4);
	vec3(vec2,float);
	vec3(float,vec2);
	vec3(float,float,float);
};

struct vec4
{
	float x,y,z,w,  r,g,b,a,  s,t,p,q;
	vec2 xx,xy,xz,xw, yx,yy,yz,yw, zx,zy,zz,zw, wx,wy,wz,ww;
	vec3 xyz;
	vec4();
	vec4(float);
	vec4(vec3, float);
	vec4(float, vec3);
	vec4(float,float,float,float);
};
struct mat4
{
	vec4 x,y,z,w;
	mat4();
	mat4(float);						//diagonal
	mat4(vec4, vec4, vec4, vec4);
	mat4(float, float, float, float,
		 float, float, float, float,
		 float, float, float, float,
		 float, float, float, float);
};
struct mat3
{
	vec3 x,y,z;
	mat3();
	mat3(float);
	mat3(vec3, vec3, vec3);
	mat3(float, float, float,
		 float, float, float,
		 float, float, float);
};
struct mat2
{
	vec3 x,y,z;
	mat2();
	mat2(float);
	mat2(vec2, vec2);
	mat2(float, float,
		 float, float);
};
struct mat2x3{}; struct mat3x2{};
struct mat2x4{}; struct mat4x2{};
struct mat3x4{}; struct mat4x3{};
struct bvec2{}; struct bvec3{}; struct bvec4{};
struct ivec2{}; struct ivec3{}; struct ivec4{};
//math operators
mat4 operator*(mat4,mat4);
vec4 operator*(mat4,vec4);		vec4 operator*(vec4,mat4);
vec3 operator*(mat3,vec3);		vec3 operator*(vec3,mat3);
vec2 operator*(mat2,vec2);		vec2 operator*(vec2,mat2);

mat4 operator*(mat4,float);		mat4 operator*(float,mat4);
mat3 operator*(mat3,float);		mat3 operator*(float,mat3);
mat2 operator*(mat2,float);		mat2 operator*(float,mat2);
mat4 operator+(mat4,float);		mat4 operator+(float,mat4);
mat3 operator+(mat3,float);		mat3 operator+(float,mat3);
mat2 operator+(mat2,float);		mat2 operator+(float,mat2);

vec4 operator*(vec4,float);		vec4 operator*(float,vec4);
vec3 operator*(vec3,float);		vec3 operator*(float,vec3);
vec2 operator*(vec2,float);		vec2 operator*(float,vec2);
vec4 operator+(vec4,float);		vec4 operator+(float,vec4);
vec3 operator+(vec3,float);		vec3 operator+(float,vec3);
vec2 operator+(vec2,float);		vec2 operator+(float,vec2);




#define attribute
#define uniform
#define varying

#define sampler1D int
#define sampler2D int
#define sampler3D int
#define samplerCube int
#define sampler1DShadow int
#define sampler2DShadow int

#define discard;			// in the fragment shader language only

///Built in variables
vec4 gl_Position;
float gl_PointSize;
vec4 gl_ClipVertex;

vec4 gl_FragCoord;
bool gl_FrontFacing;
vec4 gl_FragColor;
vec4 gl_FragData[];		//vec4 gl_FragData[gl_MaxDrawBuffers];
float gl_FragDepth;

#define GENFUNC(name)  float name(float); vec2 name(vec2); vec3 name(vec3); vec4 name(vec4)
#define GENFUNC2(name) float name(float,float); vec2 name(vec2,vec2); vec3 name(vec3,vec3); vec4 name(vec4,vec4)
#define GENFUNC2f(name) float name(float, float); vec2 name(vec2, float); vec3 name(vec3, float); vec4 name(vec4, float)
#define GENFUNCf2(name) float name(float,float); vec2 name(float,vec2); vec3 name(float,vec3); vec4 name(float,vec4)
#define GENFUNC3(name) float name(float,float,float); vec2 name(vec2,vec2,vec2); vec3 name(vec3,vec3,vec3); vec4 name(vec4,vec4,vec4)
#define GENFUNC3f(name) float name(float,float,float); vec2 name(vec2,vec2,float); vec3 name(vec3,vec3,float); vec4 name(vec4,vec4,float)

///functions version 1.2
//Angle and Trigonometry Functions
GENFUNC(radians);	GENFUNC(degrees);
GENFUNC(sin);		GENFUNC(cos);	GENFUNC(tan);
GENFUNC(asin);		GENFUNC(acos);	GENFUNC(atan);	GENFUNC2(atan);
//Exponential Functions
GENFUNC2(pow);	GENFUNC(exp);	GENFUNC(log);
GENFUNC(exp2);	GENFUNC(log2);	GENFUNC(sqrt);	GENFUNC(inversesqrt);
//Common functions
GENFUNC(abs);	GENFUNC(sign);	GENFUNC(floor);	GENFUNC(ceil);	GENFUNC(fract);
GENFUNC2f(mod);	GENFUNC2(mod);
GENFUNC2(min);	GENFUNC2f(min);	GENFUNC2(max); GENFUNC2f(max);
float clamp(float,float,float);vec2 clamp(vec2,float,float);vec3 clamp(vec3,float,float); vec4 clamp(vec4,float,float);
GENFUNC3(clamp);
GENFUNC3(mix);	GENFUNC3f(mix);
GENFUNC2(step);	GENFUNCf2(step);
GENFUNC3(smoothstep);
float smoothstep(float,float,float);vec2 smoothstep(float,float,vec2);
vec3 smoothstep(float,float,vec3); vec4 smoothstep(float,float,vec4);
//Geometric Functions
float length(vec2); float length(vec3); float length(vec4);
float distance(vec2,vec2); float distance(vec3,vec3); float distance(vec4,vec4);
float dot(vec2,vec2); float dot(vec3,vec3); float dot(vec4,vec4);
vec3 cross(vec3,vec3);
GENFUNC(normalize);
vec4 ftransform();
GENFUNC3(faceforward);
GENFUNC2(reflect);
GENFUNC3f(refract);
//Matrix Functions
mat4 matrixCompMult (mat4, mat4); mat3 matrixCompMult(mat3, mat3); mat2 matrixCompMult(mat2, mat2);
mat4 outerProduct(vec4, vec4); mat3 outerProduct(vec3, vec3); mat2 outerProduct(vec2, vec2);
mat2x3 outerProduct(vec3,vec2); mat3x2 outerProduct(vec2,vec3);
mat2x4 outerProduct(vec4,vec2); mat4x2 outerProduct(vec2,vec4);
mat3x4 outerProduct(vec4,vec3); mat4x3 outerProduct(vec3,vec4);
mat4 transpose(mat4); mat3 transpose(mat3); mat2 transpose(mat2);
mat2x3 transpose(mat3x2); mat3x2 transpose(mat2x3);
mat2x4 transpose(mat4x2); mat4x2 transpose(mat2x4);
mat3x4 transpose(mat4x3); mat4x3 transpose(mat3x4);
//Vector Relational Functions
#define GENFUNCvec1(name)  bvec2 name(vec2,vec2); bvec3 name(vec3,vec3); bvec4 name(vec4,vec4); bvec2 name(ivec2,ivec2); bvec3 name(ivec3,ivec3); bvec4 name(ivec4,ivec4)
GENFUNCvec1(lessThan);		GENFUNCvec1(lessThanEqual);
GENFUNCvec1(greaterThan);	GENFUNCvec1(greaterThanEqual);
GENFUNCvec1(equal); bvec2 equal(bvec2,bvec2); bvec3 equal(bvec3,bvec3); bvec4 equal(bvec4,bvec4);
GENFUNCvec1(notEqual); bvec2 notEqual(bvec2,bvec2); bvec3 notEqual(bvec3,bvec3); bvec4 notEqual(bvec4,bvec4);
bool any(bvec2); bool any(bvec3); bool any(bvec4);
bool all(bvec2); bool all(bvec3); bool all(bvec4);
bool not(bvec2); bool not(bvec3); bool not(bvec4);
//Texture Lookup Functions
vec4 texture1D(sampler1D sampler,float coord , float bias = 0);
vec4 texture1DProj(sampler1D sampler, vec2 coord , float bias = 0);
vec4 texture1DProj(sampler1D sampler, vec4 coord , float bias = 0);
vec4 texture1DLod (sampler1D sampler, float coord, float lod);
vec4 texture1DProjLod (sampler1D sampler, vec2 coord, float lod);
vec4 texture1DProjLod (sampler1D sampler, vec4 coord, float lod);

vec4 texture2D (sampler2D sampler, vec2 coord , float bias = 0);
vec4 texture2DProj (sampler2D sampler, vec3 coord , float bias = 0);
vec4 texture2DProj (sampler2D sampler, vec4 coord , float bias = 0);
vec4 texture2DLod (sampler2D sampler, vec2 coord, float lod);
vec4 texture2DProjLod (sampler2D sampler, vec3 coord, float lod);
vec4 texture2DProjLod (sampler2D sampler, vec4 coord, float lod);

vec4 texture3D (sampler3D sampler, vec3 coord, float bias = 0);
vec4 texture3DProj (sampler3D sampler, vec4 coord, float bias = 0);
vec4 texture3DLod (sampler3D sampler, vec3 coord, float lod);
vec4 texture3DProjLod (sampler3D sampler, vec4 coord, float lod);

vec4 textureCube (samplerCube sampler, vec3 coord, float bias = 0);
vec4 textureCubeLod (samplerCube sampler, vec3 coord, float lod);

vec4 shadow1D (sampler1DShadow sampler, vec3 coord, float bias = 0);
vec4 shadow2D (sampler2DShadow sampler, vec3 coord, float bias = 0);
vec4 shadow1DProj (sampler1DShadow sampler, vec4 coord, float bias = 0);
vec4 shadow2DProj (sampler2DShadow sampler, vec4 coord, float bias = 0);
vec4 shadow1DLod (sampler1DShadow sampler, vec3 coord, float lod);
vec4 shadow2DLod (sampler2DShadow sampler, vec3 coord, float lod);
vec4 shadow1DProjLod(sampler1DShadow sampler, vec4 coord, float lod);
vec4 shadow2DProjLod(sampler2DShadow sampler, vec4 coord, float lod);
//Fragment Processing Functions
GENFUNC(dFdx);
GENFUNC(dFdy);
GENFUNC(fwidth);
//Noise functions
float noise1(float); float noise1(vec2); float noise1(vec3); float noise1(vec4);
vec2 noise2(float); vec2 noise2(vec2); vec2 noise2(vec3); vec2 noise2(vec4);
vec3 noise3(float); vec3 noise3(vec2); vec3 noise3(vec3); vec3 noise3(vec4);
vec4 noise4(float); vec4 noise4(vec2); vec4 noise4(vec3); vec4 noise4(vec4);